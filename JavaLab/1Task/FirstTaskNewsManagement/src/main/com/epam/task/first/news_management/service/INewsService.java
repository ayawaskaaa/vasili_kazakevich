package main.com.epam.task.first.news_management.service;

import java.util.List;

import main.com.epam.task.first.news_management.entity.NewsTO;
import main.com.epam.task.first.news_management.entity.NewsVO;
import main.com.epam.task.first.news_management.entity.SearchCriteria;
import main.com.epam.task.first.news_management.service.exception.ServiceException;
/**
 * NewsService interface.
 * 
 * Contains methods for operating news entity.
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public interface INewsService {
	/**
	 * Adds single news transfer object.
	 * 
	 * @param news
	 * @return ID of new inserted news.
	 * @throws ServiceException
	 */
	public Long addNews(NewsTO news) throws ServiceException;
	
	/**
	 * Edits news entity in data repository.
	 * 
	 * @param news
	 * @throws ServiceException
	 */
	public void editNews (NewsTO news) throws ServiceException;
	
	/**
	 * Produces actions to get news value object.
	 * 
	 * @param newsId
	 * @return news transfer object. 
	 * @throws ServiceException
	 */
	public NewsTO getSingleNews(Long newsId) throws ServiceException;
	
	/**
	 * Deletes news entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteNews(Long newsId) throws ServiceException;
	
	/**
	 * Counts items in search result.
	 * 
	 * @param searchCriteria
	 * @return count result.
	 * @throws ServiceException
	 */
	public int countNewsInSearch(SearchCriteria searchCriteria) throws ServiceException;
	
	/**
	 * Searches certain news according search criteria, start ans end index.
	 * 
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return list of news according search criteria, start and end index.
	 * @throws ServiceException
	 */
	public List<NewsTO> search(SearchCriteria searchCriteria, int start, int end) throws ServiceException;
	
	
	

}
