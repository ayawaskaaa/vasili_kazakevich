package main.com.epam.task.first.news_management.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import main.com.epam.task.first.news_management.dao.ICommentDao;
import main.com.epam.task.first.news_management.dao.exception.DaoException;
import main.com.epam.task.first.news_management.entity.CommentTO;
import main.com.epam.task.first.news_management.service.ICommentService;
import main.com.epam.task.first.news_management.service.exception.ServiceException;

/**
 *CommentService implementation.
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class CommentServiceImpl implements ICommentService {
	
	private static final Logger log = Logger.getLogger(CommentServiceImpl.class);
	
	private ICommentDao commentDao = null;
	
	public CommentServiceImpl(){}


	/**
	 * Adds single comment transfer object.
	 * 
	 * @param comment
	 * @return new comment ID
	 * @throws ServiceException
	 */
	@Override
	public Long addComment(CommentTO comment) throws ServiceException {
		
		try {
			return commentDao.add(comment);
		} catch (DaoException e) {
			log.error("Exception while adding comment.", e);
			throw new ServiceException("Exception while adding comment.", e);
		}
	}


	/**
	 * Deletes comment entity from data repository according to newsId.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void deleteCommentByNewsId(Long newsId) throws ServiceException {
		try {
			commentDao.deleteByNewsId(newsId);
		} catch (DaoException e) {
			log.error("Exception while deleting comment.", e);
			throw new ServiceException("Exception while deleting comment.", e);
		}

	}
	/**
	 * Deletes comment entity from data repository.
	 * 
	 * @param commentId
	 * @throws ServiceException
	 */
	@Override
	public void deleteComment(Long commentId) throws ServiceException {
		try {
			commentDao.delete(commentId);
		} catch (DaoException e) {
			log.error("Exception while deleting comment.", e);
			throw new ServiceException("Exception while deleting comment.", e);
		}
		
	}
	
	/**
	 * Gets list of comments  related to news ID.
	 * 
	 * @param newsId
	 * @return list of comments which relate to news ID.
	 * @throws ServiceException
	 */
	@Override
	public List<CommentTO> getCommentsByNewsId(Long newsId)	throws ServiceException {
		List<CommentTO> comments = null;
		try {
			comments=commentDao.getCommentsByNewsId(newsId);
		} catch (DaoException e) {
			log.error("Exception while getting comment.", e);
			throw new ServiceException("Exception while getting comment.", e);
		}

		return comments;
	}
	
	public ICommentDao getCommentDao() {
		return commentDao;
	}

	public void setCommentDao(ICommentDao commentDao) {
		this.commentDao = commentDao;
	}

	

	

}
