package main.com.epam.task.first.news_management.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import main.com.epam.task.first.news_management.dao.ITagDao;
import main.com.epam.task.first.news_management.dao.exception.DaoException;
import main.com.epam.task.first.news_management.dao.util.DAOUtil;
import main.com.epam.task.first.news_management.entity.TagTO;

public class TagDaoImpl implements ITagDao {
	
	
	private static final String SQL_GET_ALL_TAGS_SELECT = "SELECT tg_tag_id_pk, tg_tag_name FROM tags ";
	
	private static final String SQL_GET_TAG_BY_TAG_ID_SELECT="SELECT tg_tag_id_pk, tg_tag_name FROM tags"
			+ " WHERE tg_tag_id_pk = ? ";
	
	private static final String SQL_ADD_TAG_INSERT  = "INSERT INTO tags (tg_tag_id_pk, tg_tag_name) VALUES "
			+ " (tg_tag_id_pk_seq.nextval, ?) ";
	
	private static final String SQL_DELETE_TAG_DELETE = "DELETE FROM tags WHERE tg_tag_id_pk = ? ";
	
	private static final String SQL_EDIT_TAG_UPDATE = "UPDATE  tags SET tg_tag_name =  "
			+ " ? WHERE tg_tag_id_pk = ? ";
	
	private static final String SQL_LINK_TAG_WHITH_NEWS_INSERT = "INSERT INTO news_tags (nwtg_news_id_fk, nwtg_tag_id_fk) VALUES (?, ?) ";
	
	private static final String SQL_UNLINK_TAG_WHITH_NEWS_BY_NEWS_ID_DELETE = "DELETE FROM news_tags WHERE nwtg_news_id_fk = ? ";
	
	private static final String SQL_GET_TAGS_BY_NEWS_ID_SELECT= "SELECT t.tg_tag_id_pk, t.tg_tag_name FROM tags t JOIN "
			+ " news_tags ON tg_tag_id_pk = nwtg_tag_id_fk WHERE nwtg_news_id_fk = ?  ";
	
	private static final String SQL_ADD_MULTIPLE_TAGS_INSERT = "INSERT INTO tags (tg_tag_id_pk, tg_tag_name)";

	private static final String SQL_UNLINK_TAG_WHITH_NEWS_BY_TAG_ID_DELETE = "DELETE FROM news_tags WHERE nwtg_tag_id_fk = ? ";
	
	
	private DataSource dataSource = null;
	
	public TagDaoImpl(){}
	
	public TagDaoImpl(DataSource ds){
		
		this.dataSource=ds;
		
	}
	
	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	

	@Override
	public Long add(TagTO tag) throws DaoException {
		Long tagID = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String column[]={"tg_tag_id_pk"};
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_ADD_TAG_INSERT, column);
			
			ps.setString(1,tag.getTagName());
			ps.executeUpdate();
			
			rs = ps.getGeneratedKeys();
			if(rs != null && rs.next()) {
				tagID = rs.getLong(1);
			}
		} catch (SQLException e) {
					throw new DaoException("Exception while adding tag.", e);
				}finally{
					DAOUtil.closeConnection(con, ps, rs, dataSource);
				}
		return tagID;
	}

	@Override
	public List<TagTO> getAll() throws DaoException {
		List<TagTO> tagList = new ArrayList<TagTO>();
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			st=con.createStatement();
			rs=st.executeQuery(SQL_GET_ALL_TAGS_SELECT);
			if(rs != null) {
				while(rs.next()){
					tagList.add(buildTag(rs));
				}
				
			}
			
		} catch (SQLException e) {
			throw new DaoException("Exception while getting tags.", e);
		}finally{
			DAOUtil.closeConnection(con, st, rs, dataSource);
		}

		return tagList;
	}

	@Override
	public TagTO get(Long tagId) throws DaoException {
		TagTO tag = null;
		
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_GET_TAG_BY_TAG_ID_SELECT);
			
			ps.setLong(1, tagId);			
			rs = ps.executeQuery();
			if(rs != null) {
				while(rs.next()){
				tag=buildTag(rs);
				}
				}
		} catch (SQLException e) {
					throw new DaoException("Exception while getting tag.", e);
				}finally{
					DAOUtil.closeConnection(con, ps, rs, dataSource);
				}
		
		return tag;
	}

	@Override
	public void edit(TagTO tag) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		Long tagId=  tag.getTagId();
		String tagName=tag.getTagName();
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_EDIT_TAG_UPDATE);
			
			ps.setString(1, tagName);
			ps.setLong(2, tagId);			
			ps.executeUpdate();
			
		} catch (SQLException e) {
					throw new DaoException("Exception while editing tag.", e);
				}finally{
					DAOUtil.closeConnection(con, ps, dataSource);
				}
		
	}

	@Override
	public void delete(Long tagId) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_DELETE_TAG_DELETE);
			ps.setLong(1, tagId);
			ps.executeUpdate();
			
			
		} catch (SQLException e) {
					throw new DaoException("Exception while deleting tag.", e);
				}finally{
					DAOUtil.closeConnection(con, ps, dataSource);
				}
		
	}

	@Override
	public List<TagTO> getTagsByNewsId(Long newsId) throws DaoException {
		List<TagTO> tagList = new ArrayList<TagTO>();
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_GET_TAGS_BY_NEWS_ID_SELECT);
			ps.setLong(1, newsId);
			rs=ps.executeQuery();
			if(rs != null) {
				while(rs.next()){
					tagList.add(buildTag(rs));
				}
				
			}
			
		} catch (SQLException e) {
			throw new DaoException("Exception while getting tags.", e);
		}finally{
			DAOUtil.closeConnection(con, ps, dataSource);
		}

		return tagList;
	}

	@Override
	public void unlinkByNewsId(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_UNLINK_TAG_WHITH_NEWS_BY_NEWS_ID_DELETE);
			ps.setLong(1, newsId);
			
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new DaoException("Exception while unlinking tags whith news.", e);
		}finally{
			DAOUtil.closeConnection(con, ps, dataSource);
		}

		
	}
	
	@Override
	public void unlinkByTagId(Long tagId) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_UNLINK_TAG_WHITH_NEWS_BY_TAG_ID_DELETE);
			ps.setLong(1, tagId);
			
			ps.executeUpdate();
			
		} catch (SQLException e) {
			throw new DaoException("Exception while unlinking tags whith news.", e);
		}finally{
			DAOUtil.closeConnection(con, ps, dataSource);
		}
		
	}

	@Override
	public void link(List<TagTO> tags, Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_LINK_TAG_WHITH_NEWS_INSERT);
			
			for(TagTO i:tags){
				ps.setLong(1, newsId);
				ps.setLong(2, i.getTagId());
				ps.addBatch();			
				}
			
			ps.executeBatch();
			
			
			
		} catch (SQLException e) {
			throw new DaoException("Exception while linking tags whith news.", e);
		}finally{
			DAOUtil.closeConnection(con, ps, dataSource);
		}
		
	}

	
	
	


	private TagTO buildTag(ResultSet rs) throws SQLException {
		TagTO tag = new TagTO();
		tag.setTagId(rs.getLong(1));
		tag.setTagName(rs.getString(2));
		
		return tag;
	}





}
