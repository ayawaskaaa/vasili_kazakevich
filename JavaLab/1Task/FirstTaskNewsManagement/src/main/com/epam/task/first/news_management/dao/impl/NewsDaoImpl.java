package main.com.epam.task.first.news_management.dao.impl;

/**
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import main.com.epam.task.first.news_management.dao.INewsDao;
import main.com.epam.task.first.news_management.dao.exception.DaoException;
import main.com.epam.task.first.news_management.dao.util.DAOUtil;
import main.com.epam.task.first.news_management.entity.NewsTO;
import main.com.epam.task.first.news_management.entity.SearchCriteria;

public class NewsDaoImpl implements INewsDao {
	
	private static final String SQL_GET_ALL_NEWS_SELECT=" SELECT n_id, title, sh_text, full_text, "
			+ "cre_d, mod_d, amount FROM (SELECT DISTINCT  nw_news_id_pk AS n_id, "
			+ "nw_title AS title, nw_short_text AS sh_text, nw_full_text AS full_text, "
			+ "nw_creation_date AS cre_d, nw_modification_date AS mod_d, comments_amount AS amount , "
			+ "ROW_NUMBER() OVER (ORDER BY comments_amount , nw_modification_date  ) as r_n FROM news "
			+ "LEFT JOIN (SELECT nw_news_id_pk AS news_id, COUNT(nw_news_id_pk) AS comments_amount FROM news "
			+ "INNER JOIN comments ON nw_news_id_pk=cm_news_id_fk GROUP BY nw_news_id_pk) ON nw_news_id_pk=news_id ) "
			+ "WHERE r_n BETWEEN ? AND ?";
	
	private static final String SQL_GET_NEWS_BY_ID_SELECT ="SELECT nw_news_id_pk, nw_title, nw_short_text, "
			+ "nw_full_text, nw_creation_date, nw_modification_date FROM news WHERE nw_news_id_pk = ? ";	
	
	private static final String SQL_GET_UNSORTED_NEWS_SELECT="SELECT nw_news_id_pk, nw_title, nw_short_text, "
			+ "nw_full_text, nw_creation_date, nw_modification_date FROM news";
	
	private static final String SQL_ADD_NEWS_INSERT = "INSERT INTO news ( nw_news_id_pk, nw_title,"
			+ " nw_short_text, nw_full_text, nw_creation_date, nw_modification_date ) "
			+ "VALUES (NW_NEWS_ID_PK_SEQ.nextval, ?, ?, ?, CURRENT_TIMESTAMP, SYSDATE ) ";
	
	private static final String SQL_EDIT_NEWS_UPDATE="UPDATE news SET  nw_title =? ,"
			+ " nw_short_text=? , nw_full_text=? , nw_modification_date = SYSDATE "
			+ "  WHERE  nw_news_id_pk = ? ";
	
	private static final String SQL_DELETE_NEWS_DELETE="DELETE FROM news WHERE nw_news_id_pk=? ";
	
	private static final String SQL_COUNT_ALL_NEWS_SELECT = "SELECT COUNT(nw_news_id_pk) FROM news  ";
	
	private static final String SQL_SEARCH_ALL_SELECT="SELECT n_id, title, sh_text, full_text, "
			+ "cre_d, mod_d, amount FROM (SELECT DISTINCT  nw_news_id_pk AS n_id, "
			+ "nw_title AS title, nw_short_text AS sh_text, nw_full_text AS full_text, "
			+ "nw_creation_date AS cre_d, nw_modification_date AS mod_d, comments_amount AS amount , "
			+ "ROW_NUMBER() OVER (ORDER BY comments_amount , nw_modification_date  ) as r_n FROM news "
			+ "LEFT JOIN (SELECT nw_news_id_pk AS news_id, COUNT(nw_news_id_pk) AS comments_amount FROM news "
			+ "INNER JOIN comments ON nw_news_id_pk=cm_news_id_fk GROUP BY nw_news_id_pk) ON nw_news_id_pk=news_id )";


	private static final String SQL_SEARCH_JOIN_AUTHOR = " JOIN news_authors ON  n_id=nwau_news_id_fk ";

	private static final String SQL_SEARCH_JOIN_TAGS =" JOIN news_tags ON n_id=nwtg_news_id_fk ";

	private static final String SQL_SEARCH_BY_AUTHOR_ID = "AND nwau_author_id_fk= ? ";

	private static final String SQL_SEARCH_BY_TAG_ID = "AND nwtg_tag_id_fk = ? ";	

	private static final String SQL_SEARCH_CHAIN = "WHERE r_n BETWEEN ? AND ? ";
	
	
	private static final String SQL_COUNT_ALL_FROM_SEARCH="Select COUNT(n_id) FROM("
			+ "SELECT n_id, mod_d, amount FROM (SELECT DISTINCT  nw_news_id_pk AS n_id, "
			+ "nw_modification_date AS mod_d, comments_amount AS amount , "
			+ "ROW_NUMBER() OVER (ORDER BY comments_amount , nw_modification_date  ) as r_n FROM news "
			+ "LEFT JOIN (SELECT nw_news_id_pk AS news_id, COUNT(nw_news_id_pk) AS comments_amount FROM news "
			+ "INNER JOIN comments ON nw_news_id_pk=cm_news_id_fk GROUP BY nw_news_id_pk) ON nw_news_id_pk=news_id )";
	
	private static final String SQL_COUNT_JOIN_AUTHOR="JOIN news_authors ON  n_id=nwau_news_id_fk ";
	private static final String SQL_COUNT_JOIN_TAG="JOIN news_tags ON n_id=nwtg_news_id_fk";
	private static final String SQL_COUNT_BY_AUTHOR_ID="AND nwau_author_id_fk= ? ";
	private static final String SQL_COUNT_BY_TAG_ID="AND nwtg_tag_id_fk = ?";
	private static final String SQL_COUNT_CHAIN=" WHERE ";
	private static final String SQL_COUNT_END="  ) ";
	
	
	private DataSource dataSource = null;
	
	public NewsDaoImpl(){}
	
	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public NewsDaoImpl(DataSource ds){
		
		this.dataSource=ds;
		
	}
	
	
	
	
	@Override
	public Long add(NewsTO news) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long nenwsId = null;
		String column[] = {"nw_news_id_pk"};
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_ADD_NEWS_INSERT, column);
			
			
				
				ps.setString(1, news.getTitle());
				ps.setString(2, news.getShortText());
				ps.setString(3, news.getFullText());			
				ps.executeUpdate();
				
				rs = ps.getGeneratedKeys();
				
				if(rs != null && rs.next()) {
					nenwsId = rs.getLong(1);
				}
			
			
		} catch (SQLException e) {
					throw new DaoException("Exception while adding news.", e);
				}finally{
					DAOUtil.closeConnection(con, ps, rs, dataSource);
				}
		return nenwsId;
	}

	@Override
	public List<NewsTO> getAll() throws DaoException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		List<NewsTO> newsList = new ArrayList<NewsTO>();
		
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			st=con.createStatement();
			rs = st.executeQuery(SQL_GET_UNSORTED_NEWS_SELECT);
			if(rs != null) {
				while(rs.next()){
					newsList.add(buildNewsItem(rs));
				}
				
			}
			
			
		} catch (SQLException e) {
					throw new DaoException("Exception while getting news.", e);
				}finally{
					DAOUtil.closeConnection(con, st, rs, dataSource);
				}
		return newsList;
	}

	@Override
	public NewsTO get(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		NewsTO news = null;
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_GET_NEWS_BY_ID_SELECT);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			if(rs != null) {
				if(rs.next()){
					news=buildNewsItem(rs);
					
				}
				
			}
			
			
		} catch (SQLException e) {
					throw new DaoException("Exception while getting news.", e);
				}finally{
					DAOUtil.closeConnection(con, ps, rs, dataSource);
				}
		
		return news;
	}

	@Override
	public void edit(NewsTO news) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		
		
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_EDIT_NEWS_UPDATE);
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setLong(4, news.getNewsId());
			ps.executeUpdate();
			
			
		} catch (SQLException e) {
					throw new DaoException("Exception while editing news.", e);
				}finally{
					DAOUtil.closeConnection(con, ps, dataSource);
				}
		
	}

	@Override
	public void delete(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		
		
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_DELETE_NEWS_DELETE);
			ps.setLong(1, newsId);
			ps.executeUpdate();
			
			
			
		} catch (SQLException e) {
					throw new DaoException("Exception while deleting news.", e);
				}finally{
					DAOUtil.closeConnection(con, ps, dataSource);
				}
		
	}

	@Override
	public int countNewsInSearch(SearchCriteria searchCriteria) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int countResult=0;
		String countQuery = generateCountQuery(searchCriteria);
		
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(countQuery);
			ps=fillCountPreparedStatement(ps, searchCriteria);
			rs=ps.executeQuery();
			if(rs!=null){
				if(rs.next()){
					countResult=rs.getInt(1);
				}
			}
			
		} catch (SQLException e) {
					throw new DaoException("Exception while counting news.", e);
				}finally{
					DAOUtil.closeConnection(con, ps, rs, dataSource);
				}
		return countResult;
	}

	

	@Override
	public List<NewsTO> search(SearchCriteria searchCriteria, int start, int end)
			throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<NewsTO> newsList = new ArrayList<NewsTO>();
		String searchQuery = generateSearchQuery(searchCriteria);
		
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(searchQuery);
			ps=fillPreparedStatement(ps, searchCriteria, start, end);
			rs=ps.executeQuery();
			
			if(rs != null) {
				while(rs.next()){
					newsList.add(buildNewsItem(rs));
				}
				
			}
			
			
		} catch (SQLException e) {
					throw new DaoException("Exception while searching news.", e);
				}finally{
					DAOUtil.closeConnection(con, ps, rs, dataSource);
				}
		return newsList;
	}


	

	private NewsTO buildNewsItem(ResultSet rs) throws SQLException {
		NewsTO news = new NewsTO();
		news.setNewsId(rs.getLong(1));
		news.setTitle(rs.getString(2));
		news.setShortText(rs.getString(3));
		news.setFullText(rs.getString(4));
		news.setCreationDate(rs.getTimestamp(5));
		news.setModificationDate(rs.getDate(6));
	
		return news;
	}
	
	private String generateSearchQuery(SearchCriteria searchCriteria) {
		StringBuilder searchQuery = new StringBuilder();
		Long authorId= searchCriteria.getAuthorId();
		List<Long> tagsId = searchCriteria.getTagIdList();
		searchQuery.append(SQL_SEARCH_ALL_SELECT);
		
		int authorQueries =0;
		int tagQueries = 0;
		if(authorId!=null){			
			searchQuery.append(SQL_SEARCH_JOIN_AUTHOR);	
			authorQueries=1;
		}
		
		if(tagsId!=null){
			searchQuery.append(SQL_SEARCH_JOIN_TAGS);	
			tagQueries=tagsId.size();
		}
		
		searchQuery.append(SQL_SEARCH_CHAIN);
		
		for(int i = 0; i<authorQueries; i++){
			searchQuery.append(SQL_SEARCH_BY_AUTHOR_ID);
		}
		for(int i = 0; i<tagQueries; i++){
			searchQuery.append(SQL_SEARCH_BY_TAG_ID);
		}
		
		
		return searchQuery.toString();
	}
	
	private PreparedStatement fillPreparedStatement(PreparedStatement ps, SearchCriteria searchCriteria, int start, int end) throws SQLException{
		
		Long authorId= searchCriteria.getAuthorId();
		List<Long> tagsId = searchCriteria.getTagIdList();
		int nextParameterIndex = 3;
		
		ps.setInt(1, start);
		ps.setInt(2, end);
		
		if(authorId!=null){
			ps.setLong(nextParameterIndex, authorId);
			nextParameterIndex=nextParameterIndex++;
		}
		
		if(tagsId!=null){
			for(Long i:tagsId){
				ps.setLong(nextParameterIndex, i);
				nextParameterIndex++;
			}
		}		
		
		return ps;
		
	}
	
	private String generateCountQuery(SearchCriteria searchCriteria) {
		StringBuilder countQuery = new StringBuilder();
		Long authorId= searchCriteria.getAuthorId();
		List<Long> tagsId = searchCriteria.getTagIdList();
		countQuery.append(SQL_COUNT_ALL_FROM_SEARCH);
		
		int authorQueries =0;
		int tagQueries = 0;
		if(authorId!=null){			
			countQuery.append(SQL_COUNT_JOIN_AUTHOR);	
			authorQueries=1;
		}
		
		if(tagsId!=null){
			countQuery.append(SQL_COUNT_JOIN_TAG);	
			tagQueries=tagsId.size();
		}
		if(authorQueries!=0&&tagQueries!=0){
		countQuery.append(SQL_COUNT_CHAIN);
		}
		
		for(int i = 0; i<authorQueries; i++){
			countQuery.append(SQL_COUNT_BY_AUTHOR_ID);
		}
		for(int i = 0; i<tagQueries; i++){
			countQuery.append(SQL_COUNT_BY_TAG_ID);
		}
		countQuery.append(SQL_COUNT_END);
		
		return countQuery.toString();
		
	}
	
	private PreparedStatement fillCountPreparedStatement(PreparedStatement ps,
			SearchCriteria searchCriteria) throws SQLException {
		
		Long authorId= searchCriteria.getAuthorId();
		List<Long> tagsId = searchCriteria.getTagIdList();
		int nextParameterIndex = 3;
		

		
		if(authorId!=null){
			ps.setLong(nextParameterIndex, authorId);
			nextParameterIndex=nextParameterIndex++;
		}
		
		if(tagsId!=null){
			for(Long i:tagsId){
				ps.setLong(nextParameterIndex, i);
				nextParameterIndex++;
			}
		}		
		
		return ps;
	}




	
	
	
	

	




}
