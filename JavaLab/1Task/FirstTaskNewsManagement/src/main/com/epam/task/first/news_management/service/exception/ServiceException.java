package main.com.epam.task.first.news_management.service.exception;
/**
 * Exception of Service layer.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class ServiceException extends Exception {	

	private static final long serialVersionUID = 1L;
		
		public ServiceException(String msg){
			super(msg);
		}

		public ServiceException(String msg, Exception e ){
			super(msg, e);
			
		}

	}


