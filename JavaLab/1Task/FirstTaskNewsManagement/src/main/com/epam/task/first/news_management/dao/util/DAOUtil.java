package main.com.epam.task.first.news_management.dao.util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.sql.DataSource;
import org.apache.log4j.Logger;
import org.springframework.jdbc.datasource.DataSourceUtils;

public class DAOUtil {
	
	private static final Logger log = Logger.getLogger(DAOUtil.class);
	
	public static void closeConnection(Connection con, DataSource dataSource){
		if(con!=null){
			DataSourceUtils.releaseConnection(con, dataSource);
		}else{
			log.error("Error while closing connection. Connection is null.");
			}
		}
	
	public static void closeConnection (Connection con, Statement st, DataSource dataSource){

		
		if(st!=null){
			try {
				st.close();
			} catch (SQLException e) {
				log.error("Exception while closing statement.",e);
			}
		}else{
			log.error("Error while closing statement. Statement is null.");
		}
				
		closeConnection(con, dataSource);
		

	}
	
	public static void closeConnection (Connection con, Statement st, ResultSet rs, DataSource dataSource) {
		
		if(rs!=null){
			try {
				rs.close();
			} catch (SQLException e) {
				log.error("Exception while closing resultset.");
			}
			}else{
				log.error("Error while closing resultset. Resultset is null.");
			}
		closeConnection(con, st, dataSource);
	}
	
	
		

}
