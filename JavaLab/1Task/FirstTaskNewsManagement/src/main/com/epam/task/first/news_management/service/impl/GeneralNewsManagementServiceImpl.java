package main.com.epam.task.first.news_management.service.impl;


import java.util.List;

import org.springframework.transaction.annotation.Transactional;
import main.com.epam.task.first.news_management.entity.AuthorTO;
import main.com.epam.task.first.news_management.entity.CommentTO;
import main.com.epam.task.first.news_management.entity.NewsTO;
import main.com.epam.task.first.news_management.entity.NewsVO;
import main.com.epam.task.first.news_management.entity.SearchCriteria;
import main.com.epam.task.first.news_management.entity.TagTO;
import main.com.epam.task.first.news_management.service.IAuthorService;
import main.com.epam.task.first.news_management.service.ICommentService;
import main.com.epam.task.first.news_management.service.IGeneralNewsManagementService;
import main.com.epam.task.first.news_management.service.INewsService;
import main.com.epam.task.first.news_management.service.ITagService;
import main.com.epam.task.first.news_management.service.exception.ServiceException;

/**
 * GeneralNewsManagementService implementation.
 * 
 * Facade class that provides simplified methods required
 * by client and delegates calls to methods of existing system classes. 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
@Transactional(rollbackFor=Exception.class)

public class GeneralNewsManagementServiceImpl implements IGeneralNewsManagementService {
	
	private INewsService newsService = null;
	private ICommentService commentService = null;
	private IAuthorService authorService = null;
	private ITagService tagService = null;
	
	public GeneralNewsManagementServiceImpl(){}
	

	
	public INewsService getNewsService() {
		return newsService;
	}
	public void setNewsService(INewsService newsService) {
		this.newsService = newsService;
	}
	public ICommentService getCommentService() {
		return commentService;
	}
	public void setCommentService(ICommentService commentService) {
		this.commentService = commentService;
	}
	public IAuthorService getAuthorService() {
		return authorService;
	}
	public void setAuthorService(IAuthorService authorService) {
		this.authorService = authorService;
	}
	public ITagService getTagService() {
		return tagService;
	}
	public void settagService(ITagService tagService) {
		this.tagService = tagService;
	}
	
	
	/**
	 * Adds single news transfer object.
	 * 
	 * @param news
	 * @throws ServiceException
	 */

	
	@Override
	public void addNews(NewsTO news) throws ServiceException {
		
		newsService.addNews(news);
	}

	
	/**
	 * Adds single author transfer object.
	 * 
	 * @param author
	 * @throws ServiceException
	 */
	@Override
	public void addAuthor(AuthorTO author) throws ServiceException {
		authorService.addAuthor(author);
		
	}
	
	/**
	 * Adds single comment transfer object.
	 * 
	 * @param comment
	 * @throws ServiceException
	 */
	@Override
	public void addComment(CommentTO comment) throws ServiceException {
		commentService.addComment(comment);
		
	}


	/**
	 * Deletes tag entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void deleteTag(Long tagId) throws ServiceException {
		tagService.unlinkTagWithNewsByTagId(tagId);
		tagService.deleteTag(tagId);
		
	}
	
	/**
	 * Deletes author entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void deleteAuthor(Long authorId) throws ServiceException {
		
		authorService.unlinkAutorWhithNewsByAuthorId(authorId);
		authorService.deleteAuthor(authorId);
		
	}
	/**
	 * Deletes comment entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void deleteComment(Long commentId) throws ServiceException {
		commentService.deleteComment(commentId);
		
	}
	
	/**
	 * Adds single tag transfer object.
	 * 
	 * @param tag
	 * @throws ServiceException
	 */
	@Override
	public void addTag(TagTO tag) throws ServiceException {
		
		tagService.addTag(tag);
		
		
	}


	/**
	 * Edits news entity in data repository.
	 * 
	 * @param newsTO
	 * @throws ServiceException
	 */
	@Override
	public void editNews(NewsTO newsTO) throws ServiceException {
		newsService.editNews(newsTO);
		
	}


	/**
	 * Searches certain news according search criteria, start ans end index.
	 * 
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return list of news according search criteria, start and end index.
	 * @throws ServiceException
	 */
	@Override
	public List<NewsTO> searchByAuthorAndTag(SearchCriteria searchCriteria, int start, int end) throws ServiceException {
		List<NewsTO> newsList = null;
		newsList=newsService.search(searchCriteria, start, end);
		return newsList;
	}


	/**
	 * Counts items in search result.
	 * 
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return
	 * @throws ServiceException
	 */
	@Override
	public int countNewsInSearch(SearchCriteria searchCriteria, int start, int end) throws ServiceException {
		int countresult = newsService.countNewsInSearch(searchCriteria);
		return countresult;
	}


	/**
	 * Produces actions for adding in to data repository.
	 * 
	 * @param newsVO
	 * @throws ServiceException
	 */
	@Override
	public void addComplexNews(NewsVO newsVO) throws ServiceException {
		
		Long newsId=null;
		NewsTO news = newsVO.getNewsItem();
		AuthorTO author = newsVO.getAuthorItem();
		List <TagTO> tagList = newsVO.getTagItemList();	
		newsId=newsService.addNews(news);
		authorService.linkAuthorWhithNews(author, newsId);
		tagService.linkTagsWithNews(tagList, newsId);
		
		
	}


	/**
	 * Deletes news entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void deleteNews(Long newsId) throws ServiceException {
		
		commentService.deleteCommentByNewsId(newsId);
		tagService.unlinkTagWithNews(newsId);
		authorService.unlinkAutorWhithNewsByNewsId(newsId);
		newsService.deleteNews(newsId);
		
		
	}


	/**
	 * Produces actions to get news value object.
	 * 
	 * @param newsId
	 * @return
	 * @throws ServiceException
	 */
	@Override
	public NewsVO getSingleNews(Long newsId) throws ServiceException {
		NewsVO newsVO = new NewsVO();
		newsVO.setNewsItem(newsService.getSingleNews(newsId));
		newsVO.setComment(commentService.getCommentsByNewsId(newsId));
		newsVO.setAuthorItem(authorService.getAuthorByNewsId(newsId));
		newsVO.setTagItemList(tagService.getTagsByNewsId(newsId));
		return newsVO;
	}




	
	


	


	
	

}
