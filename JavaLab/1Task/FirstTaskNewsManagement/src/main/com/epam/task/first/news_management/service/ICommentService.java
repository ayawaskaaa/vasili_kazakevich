package main.com.epam.task.first.news_management.service;

import java.util.List;

import main.com.epam.task.first.news_management.entity.CommentTO;
import main.com.epam.task.first.news_management.service.exception.ServiceException;
/**
 *CommentService interface.
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public interface ICommentService {
	
	/**
	 * Adds single comment transfer object.
	 * 
	 * @param comment
	 * @return new comment ID
	 * @throws ServiceException
	 */
	public Long addComment (CommentTO comment) throws ServiceException;
	
	/**
	 * Deletes comment entity from data repository.
	 * 
	 * @param commentId
	 * @throws ServiceException
	 */
	public void deleteComment(Long commentId) throws ServiceException;
	
	/**
	 * Deletes comment entity from data repository according to newsId.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteCommentByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * Gets list of comments  related to news ID.
	 * 
	 * @param newsId
	 * @return list of comments which relate to news ID.
	 * @throws ServiceException
	 */
	public List<CommentTO> getCommentsByNewsId(Long newsId) throws ServiceException;
	

}
