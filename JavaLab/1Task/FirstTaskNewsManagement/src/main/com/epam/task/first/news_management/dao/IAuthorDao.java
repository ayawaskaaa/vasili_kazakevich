package main.com.epam.task.first.news_management.dao;

import main.com.epam.task.first.news_management.dao.exception.DaoException;
import main.com.epam.task.first.news_management.entity.AuthorTO;
/**
 * 
 * @author Vasili_Kazakevich
 * @version 1.0
 */
public interface IAuthorDao extends IBasicDao<AuthorTO> {

	/**
	 * 
	 * @param author
	 * @param newsId
	 * @throws DaoException
	 */
	public void link(AuthorTO author, Long newsId) throws DaoException;
	/**
	 * 
	 * @param newsId
	 * @throws DaoException
	 */
	public void unlinkByNewsId(Long newsId) throws DaoException;
	/**
	 * 
	 * @param authorId
	 * @throws DaoException
	 */
	public void unlinkByAuthorId(Long authorId) throws DaoException;
	/**
	 * 
	 * @param newsId
	 * @return
	 * @throws DaoException
	 */
	public AuthorTO getAuthorByNewsId(Long newsId)throws DaoException;
	
	
	
	
}
