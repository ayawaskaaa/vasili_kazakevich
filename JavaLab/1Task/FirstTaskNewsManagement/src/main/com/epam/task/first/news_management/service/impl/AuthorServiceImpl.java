package main.com.epam.task.first.news_management.service.impl;

import org.apache.log4j.Logger;

import main.com.epam.task.first.news_management.dao.IAuthorDao;
import main.com.epam.task.first.news_management.dao.exception.DaoException;
import main.com.epam.task.first.news_management.entity.AuthorTO;
import main.com.epam.task.first.news_management.service.IAuthorService;
import main.com.epam.task.first.news_management.service.exception.ServiceException;

/**
 * AuthorService implementation.
 * 
 * Contains methods for operating author entity.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class AuthorServiceImpl implements IAuthorService {
	
	private IAuthorDao authorDao;
	
	private static final Logger log = Logger.getLogger(AuthorServiceImpl.class);

	public AuthorServiceImpl(){}

	
	public IAuthorDao getAuthorDao() {
		return authorDao;
	}

	public void setAuthorDao(IAuthorDao authorDao) {
		this.authorDao = authorDao;
	}
	
	

	/**
	 * Adds single author transfer object.
	 * 
	 * @param author
	 * @return new author ID.
	 * @throws ServiceException
	 */
	@Override
	public Long addAuthor(AuthorTO author) throws ServiceException {
		Long authorId = null;
		try {
			authorId=authorDao.add(author);			
			
		} catch (DaoException e) {
			log.error("Exception in DAO layer while adding author", e);
			throw new ServiceException("Exception in DAO layer while adding author", e);
		}
		return authorId;
	}

	/**
	 * Makes author expired.
	 * 
	 * @param authorId
	 * @throws ServiceException
	 */
	@Override
	public void deleteAuthor(Long authorId) throws ServiceException {
		try {
			authorDao.unlinkByAuthorId(authorId);
			authorDao.delete(authorId);
		} catch (DaoException e) {
			log.error("Exception in DAO layer while deleting author", e);
			throw new ServiceException("Exception in DAO layer while deleting author", e);
		}
		
	}

	/**
	 * Gets certain author according to news ID.
	 * 
	 * @param newsId
	 * @return author transfer object.
	 * @throws ServiceException
	 */
	@Override
	public AuthorTO getAuthorByNewsId(Long newsId) throws ServiceException {
		AuthorTO author = null;
		
		try {
			author=authorDao.getAuthorByNewsId(newsId);
		} catch (DaoException e) {
			log.error("Exception in DAO layer while getting author", e);
			throw new ServiceException("Exception in DAO layer while getting author", e);
		}

		return author;
	}

	/**
	 * Destroys links between author table and news table, using ID of news.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void unlinkAutorWhithNewsByNewsId(Long newsId) throws ServiceException {
		try {
			authorDao.unlinkByNewsId(newsId);
		} catch (DaoException e) {
			log.error("Exception in DAO layer while unlinking author", e);
			throw new ServiceException("Exception in DAO layer while deleting author", e);
		}
		
	}
	
	/**
	 * Destroys links between author table and news table, using ID of author.
	 * 
	 * @param authorId
	 * @throws ServiceException
	 */
	@Override
	public void unlinkAutorWhithNewsByAuthorId(Long authorId) throws ServiceException {
		try {
			authorDao.unlinkByAuthorId(authorId);
		} catch (DaoException e) {
			log.error("Exception in DAO layer while unlinking author", e);
			throw new ServiceException("Exception in DAO layer while deleting author", e);
		}
		
	}

	/**
	 * Edit author entity in data repository.
	 * 
	 * @param author
	 * @throws ServiceException
	 */
	@Override
	public void editAuthor(AuthorTO author) throws ServiceException {
		try {
			authorDao.edit(author);
		} catch (DaoException e) {
			log.error("Exception in DAO layer while editing author", e);
			throw new ServiceException("Exception in DAO layer while editing author", e);
		}
	}

	/**
	 * Links author with news according to their ID's.
	 * 
	 * @param author
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void linkAuthorWhithNews(AuthorTO author, Long newsId)
			throws ServiceException {
		try {
			authorDao.link(author, newsId);;
		} catch (DaoException e) {
			log.error("Exception in DAO layer while unlinking author", e);
			throw new ServiceException("Exception in DAO layer while deleting author", e);
		}
		
	}


	



	

	

}
