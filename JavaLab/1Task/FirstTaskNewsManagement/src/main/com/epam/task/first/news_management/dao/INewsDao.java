package main.com.epam.task.first.news_management.dao;

import java.util.List;

import main.com.epam.task.first.news_management.dao.exception.DaoException;
import main.com.epam.task.first.news_management.entity.NewsTO;
import main.com.epam.task.first.news_management.entity.NewsVO;
import main.com.epam.task.first.news_management.entity.SearchCriteria;
/**
 * 
 * @author Vasili_Kazakevich
 * @version 1.0
 */
public interface INewsDao extends IBasicDao<NewsTO> {
	
	/**
	 * 
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return
	 * @throws DaoException
	 */
	public List<NewsTO> search(SearchCriteria searchCriteria, int start, int end) throws DaoException;
	/**
	 * 
	 * @param searchCriteria
	 * @return
	 * @throws DaoException
	 */
	public int countNewsInSearch(SearchCriteria searchCriteria) throws DaoException;;

}
