package main.com.epam.task.first.news_management.service;


/**
 * GeneralNewsManagementService interface.
 * 
 * Facade class that provides simplified methods required
 * by client and delegates calls to methods of existing system classes. 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */

import java.util.List;

import main.com.epam.task.first.news_management.entity.AuthorTO;
import main.com.epam.task.first.news_management.entity.CommentTO;
import main.com.epam.task.first.news_management.entity.NewsTO;
import main.com.epam.task.first.news_management.entity.NewsVO;
import main.com.epam.task.first.news_management.entity.SearchCriteria;
import main.com.epam.task.first.news_management.entity.TagTO;
import main.com.epam.task.first.news_management.service.exception.ServiceException;

public interface IGeneralNewsManagementService {
	
	/**
	 * Adds single news transfer object.
	 * 
	 * @param news
	 * @throws ServiceException
	 */
	public void addNews(NewsTO news) throws ServiceException;
	
	/**
	 * Produces actions for adding in to data repository.
	 * 
	 * @param newsVO
	 * @throws ServiceException
	 */
	public void addComplexNews(NewsVO newsVO) throws ServiceException;
	
	/**
	 * Adds single author transfer object.
	 * 
	 * @param author
	 * @throws ServiceException
	 */
	public void addAuthor(AuthorTO author) throws ServiceException;
	
	/**
	 * Adds single comment transfer object.
	 * 
	 * @param comment
	 * @throws ServiceException
	 */
	public void addComment(CommentTO comment) throws ServiceException;
	
	/**
	 * Adds single tag transfer object.
	 * 
	 * @param tag
	 * @throws ServiceException
	 */
	public void addTag(TagTO tag) throws ServiceException;
	
	/**
	 * Deletes news entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteNews(Long newsId) throws ServiceException;
	
	/**
	 * Deletes tag entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteTag(Long newsId) throws ServiceException;
	
	/**
	 * Deletes author entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteAuthor(Long newsId) throws ServiceException;
	
	/**
	 * Deletes comment entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteComment(Long newsId) throws ServiceException;
	
	/**
	 * Edits news entity in data repository.
	 * 
	 * @param newsTO
	 * @throws ServiceException
	 */
	public void editNews(NewsTO newsTO) throws ServiceException;
	
	/**
	 * Produces actions to get news value object.
	 * 
	 * @param newsId
	 * @return
	 * @throws ServiceException
	 */
	public NewsVO getSingleNews(Long newsId) throws ServiceException;
	
	/**
	 * Searches certain news according search criteria, start ans end index.
	 * 
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return list of news according search criteria, start and end index.
	 * @throws ServiceException
	 */
	public List<NewsTO> searchByAuthorAndTag(SearchCriteria searchCriteria, int start, int end ) throws ServiceException;
	
	/**
	 * Counts items in search result.
	 * 
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return
	 * @throws ServiceException
	 */
	public int countNewsInSearch(SearchCriteria searchCriteria, int start, int end) throws ServiceException;
	
	
	
	
	

}
