package main.com.epam.task.first.news_management.dao.exception;

/**
 * Exception of Dao layer.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class DaoException extends Exception {

private static final long serialVersionUID = 1L;
	
	public DaoException(String msg){
		super(msg);
	}

	public DaoException(String msg, Exception e ){
		super(msg, e);
		
	}

}
