package test.com.epam.task.first.news_management.dao.impl;

import static org.junit.Assert.*;

import java.util.List;

import javax.sql.DataSource;

import main.com.epam.task.first.news_management.dao.ICommentDao;
import main.com.epam.task.first.news_management.dao.exception.DaoException;
import main.com.epam.task.first.news_management.dao.impl.CommentDaoImpl;
import main.com.epam.task.first.news_management.entity.CommentTO;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;



@DataSet(value="NewsManagementDataSet.xml",loadStrategy = CleanInsertLoadStrategy.class)
public class CommentDaoImplTest extends UnitilsJUnit4 {
	
	private ICommentDao commentDao;
	
	@TestDataSource
	private DataSource dataSource;

	@Before
	public void setUp()  {
		
		assertNotNull(dataSource);
		commentDao = new CommentDaoImpl(dataSource);
		assertNotNull(commentDao);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void addTest() throws DaoException {
		CommentTO expectedCommentTO =new CommentTO();
		CommentTO resultCommentTO = null;
		Long generatedId = null;
		expectedCommentTO.setCommentText("testText3");
		expectedCommentTO.setNewsId(3L);
		generatedId=commentDao.add(expectedCommentTO);
		expectedCommentTO.setCommentId(generatedId);
		resultCommentTO=commentDao.get(generatedId);
		assertEquals(expectedCommentTO.getCommentText(),resultCommentTO.getCommentText());
		
		
		
	}
	@Test
	public void getAllTest() throws DaoException {
		List <CommentTO> commentList = null;
		commentList=commentDao.getAll();
		assertNotNull(commentList);
		
	}
	@Test
	public void getTest() throws DaoException {
		CommentTO expectedCommentTO =new CommentTO();
		CommentTO resultCommentTO = null;
		expectedCommentTO.setCommentText("testComment1");
		resultCommentTO=commentDao.get(1L);
		assertEquals(expectedCommentTO.getCommentText(), resultCommentTO.getCommentText());
		
	}
	@Test
	public void editTest() throws DaoException {
		CommentTO expectedCommentTO =new CommentTO();
		CommentTO resultCommentTO = null;
		expectedCommentTO.setCommentId(1L);
		
		expectedCommentTO.setCommentText("edit");
		commentDao.edit(expectedCommentTO);
		resultCommentTO=commentDao.get(1L);
		
		
		assertEquals(expectedCommentTO.getCommentText(), resultCommentTO.getCommentText());
		
	}
	@Test
	public void deleteTest() throws DaoException {
		CommentTO resultCommentTO = null;
		commentDao.delete(1L);
		resultCommentTO=commentDao.get(1L);
		assertNull(resultCommentTO);
		
	}
	@Test
	public void getCommentsByNewsIdTest() throws DaoException {
		CommentTO expectedCommentTO =new CommentTO();
		CommentTO resultCommentTO = null;
		
		expectedCommentTO.setCommentText("testComment1");
		
		expectedCommentTO.setNewsId(1L);
		expectedCommentTO.setCommentId(1L);
		resultCommentTO=commentDao.get(1L);
		assertEquals(expectedCommentTO.getCommentText(),resultCommentTO.getCommentText());
		
		
	}
	@Test
	public void deleteByNewsIdTest() throws DaoException {
		CommentTO resultCommentTO = null;
		commentDao.deleteByNewsId(1L);
		resultCommentTO=commentDao.get(1L);
		assertNull(resultCommentTO);
		
	}

	
	

}
