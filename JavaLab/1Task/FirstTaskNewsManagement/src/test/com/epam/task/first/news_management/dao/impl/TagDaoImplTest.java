package test.com.epam.task.first.news_management.dao.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import main.com.epam.task.first.news_management.dao.ITagDao;
import main.com.epam.task.first.news_management.dao.exception.DaoException;
import main.com.epam.task.first.news_management.dao.impl.TagDaoImpl;
import main.com.epam.task.first.news_management.entity.TagTO;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;

@DataSet(value="NewsManagementDataSet.xml",loadStrategy = CleanInsertLoadStrategy.class)
public class TagDaoImplTest extends UnitilsJUnit4{

	private ITagDao tagDao;

	@TestDataSource
	private DataSource dataSource;
	

	@Before
	public void setUp() {
		
		assertNotNull(dataSource);
		tagDao = new TagDaoImpl(dataSource);
		assertNotNull(tagDao);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void addTest() throws DaoException {
		TagTO expectedTag = new TagTO();
		Long generatedId = null;
		expectedTag.setTagName("testName");
		generatedId=tagDao.add(expectedTag);
		expectedTag.setTagId(generatedId);
		assertEquals(expectedTag, tagDao.get(generatedId));
		
	}
	@Test
	public void getAllTest() throws DaoException {
		List <TagTO> tagList =null;
		tagList=tagDao.getAll();
		assertNotNull(tagList);
		
		
	}
	@Test
	
	public void getTest() throws DaoException {
		TagTO expextedTag = new TagTO();
		TagTO resultTag=null;
		expextedTag.setTagId(1L);
		expextedTag.setTagName("testName1");
		resultTag=tagDao.get(1L);
		assertEquals(expextedTag,resultTag);
		
		
	}
	@Test
	public void editTest() throws DaoException {
		TagTO expextedTag = new TagTO();
		TagTO resultTag=null;
		expextedTag.setTagId(1L);
		expextedTag.setTagName("edit");
		tagDao.edit(expextedTag);
		
		resultTag=tagDao.get(1L);
		assertEquals(expextedTag,resultTag);
		
	}
	@Test
	public void deleteTest() throws DaoException {
		TagTO resultTag=null;
		tagDao.unlinkByTagId(1L);
		tagDao.delete(1L);
		resultTag=tagDao.get(1L);
		assertNull(resultTag);
		
	}
	@Test
	public void getTagsByNewsIdTest() throws DaoException {
		List<TagTO> expextedTagList  = new ArrayList<TagTO>();
		List<TagTO> resultTagList=null;
		TagTO tag = new TagTO();
		tag.setTagId(2L);
		tag.setTagName("testName2");
		expextedTagList.add(tag);
		resultTagList=tagDao.getTagsByNewsId(2L);
		assertEquals(expextedTagList,resultTagList);
		
	}
	@Test
	@ExpectedDataSet
	public void unlinkByNewsIdTest() throws DaoException {
		
		tagDao.unlinkByNewsId(1L);
		
	}
	@Test
	@ExpectedDataSet
	public void unlinkByTagIdTest() throws DaoException {
		tagDao.unlinkByNewsId(1L);
	}
	
	@Test
	public void linkTest() throws DaoException {
		List <TagTO> tagList = new ArrayList<TagTO>();
		TagTO tag = new TagTO();
		TagTO tag2 = new TagTO();
		tag.setTagId(1L);
		tag2.setTagId(2L);
		tagList.add(tag);
		tagList.add(tag2);
		tagDao.link(tagList, 1L);
	}

}
