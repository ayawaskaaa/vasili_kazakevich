package com.epam.kazakevich.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public final class AutoCloseResources {

	public static void close(BufferedReader in, PrintWriter out, Socket socket)
			throws IOException {

			if (in!=null) {
				in.close();
			}
			if (out!=null) {
				out.close();
			}
			close(socket);
			
		
	}

	public static void close(Socket socket) throws IOException {
		if (socket!=null) {
			socket.close();
		}
		
	}

	public static void close(BufferedReader in, PrintWriter out, Socket socket,
			Scanner scan) throws IOException {
		close(in, out, socket);
		if (scan!=null) {
			scan.close();
		}
		
	}
}
