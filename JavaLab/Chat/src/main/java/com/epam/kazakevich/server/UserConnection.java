package com.epam.kazakevich.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.List;

import com.epam.kazakevich.util.AutoCloseResources;

public class UserConnection implements Runnable {

	private Socket socket;
	private List<UserConnection> connectionsList;
	private BufferedReader in = null;
	private PrintWriter out = null;

	public UserConnection(Socket socket, List<UserConnection> connectionsList) {
		this.socket = socket;
		this.connectionsList = connectionsList;

	}

	public void run() {
		String name = "";

		try {

			in = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream(), true);

			name = in.readLine();

			synchronized (connectionsList) {

				for (UserConnection userConnection : connectionsList) {
					userConnection.clientJoined(out, name);
				}
			}

			String message = "";
			while (true) {

				message = in.readLine();
				if (message.equals("exit"))
					break;
				synchronized (connectionsList) {
					for (UserConnection userConnection : connectionsList) {
						userConnection.sendMessage(out, name, message);

					}

				}
			}

			synchronized (connectionsList) {
				for (UserConnection userConnection : connectionsList) {
					userConnection.clientLeft(out, name);
				}

			}

		} catch (IOException e) {
			System.out.println("Connection lost.");

		} finally {
			close();
		}

	}

	public void clientJoined(PrintWriter out, String name) {
		out.println(name + " joined");

	}

	public void sendMessage(PrintWriter out, String name, String message) {
		out.println(name + ":" + message);
	}

	public void clientLeft(PrintWriter out, String name) {
		out.println(name + " left");
	}

	public void close() {
		try {
			AutoCloseResources.close(in, out, socket);
		} catch (IOException e) {
			System.out.println("Failed to close resources.");
		}
		connectionsList.remove(this);

	}

}
