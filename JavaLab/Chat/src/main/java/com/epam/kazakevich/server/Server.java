package com.epam.kazakevich.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

import com.epam.kazakevich.util.AutoCloseResources;

public class Server {

	public void startServer(int port) {
		ServerSocket server;
		Socket socket = null;
		List<UserConnection> connectionsList = null;
		try {
			server = new ServerSocket(port);

			connectionsList = new CopyOnWriteArrayList<UserConnection>();
			System.out.println("Server started.");
			while (true) {
				socket = server.accept();
				UserConnection connection = new UserConnection(socket,
						connectionsList);
				Thread newUser = new Thread(connection);
				connectionsList.add(connection);
				newUser.start();

			}
		} catch (IOException e) {
			System.out.println("Server failed to start");
		} finally {
			close(socket, connectionsList);
		}
	}

	private void close(Socket socket, List<UserConnection> connectionList) {
		try {
			AutoCloseResources.close(socket);
		} catch (IOException e) {
			System.out.println("Socet wasn't closed.");

		}
		for (UserConnection i : connectionList) {
			i.close();
		}
	}

}
