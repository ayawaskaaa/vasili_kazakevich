package com.epam.kazakevich.client;

import java.io.BufferedReader;
import java.io.IOException;

public class ClientListener extends Thread {
	private BufferedReader in;

	public ClientListener(BufferedReader input) {
		this.in = input;
	}

	private boolean stoped;

	public void setStop() {
		stoped = true;
	}

	@Override
	public void run() {
		try {
			while (!stoped) {
				String str = in.readLine();
				System.out.println(str);
			}
		} catch (IOException e) {
			System.err.println("Can not get message.");
		}
	}
}
