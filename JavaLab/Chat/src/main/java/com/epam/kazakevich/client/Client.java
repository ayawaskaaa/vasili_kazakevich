package com.epam.kazakevich.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

import com.epam.kazakevich.util.AutoCloseResources;

public class Client {

	public void startClient(int port) {
		BufferedReader in = null;
		PrintWriter out = null;
		Socket socket = null;
		Scanner scan = new Scanner(System.in);
		boolean restart = true;
		boolean connected = false;
		ClientListener listener = null;

		while (restart) {
			try {
				System.out.println("Enter IP adress to connect the server.");
				System.out.println("Format: xxx.xxx.xxx.xxx");
				String ip = scan.nextLine();
				socket = new Socket(ip, port);
				connected=true;
				break;
			} catch (IOException e) {
				System.out.println("Unable to connect the server.");
				System.out.println("Try again? Y/N");
				String answer = scan.nextLine();
				if (answer.equalsIgnoreCase("Y")) {
					restart=true;
				}
				if (answer.equalsIgnoreCase("N")) {
					restart=false;
					close(in, out,socket,scan);
				}
			}
		}
		
		try {
			if (connected) {
				in = new BufferedReader(new InputStreamReader(
						socket.getInputStream()));
				out = new PrintWriter(socket.getOutputStream(), true);
				System.out
						.println("..................Welcom to chat................. ");
				System.out.println("For exit enter:'exit'.");
				System.out.println("Enter your nickname:");
				out.println(scan.nextLine());
				listener = new ClientListener(in);
				listener.start();
				String str = "";
				while (!str.equals("exit")) {
					str = scan.nextLine();
					out.println(str);
				}
				listener.setStop();
			} 

		} catch (IOException e) {
			System.out.println("Can not connect to server");

		} finally {
			listener.setStop();
			close(in, out, socket, scan);
		}

	}

	private void close(BufferedReader in, PrintWriter out,
			Socket socket, Scanner scan) {
		try {
			AutoCloseResources.close(in, out, socket,scan);
		} catch (IOException e) {
			System.out.println("Failed to close resources");
		}
		
	}

}
