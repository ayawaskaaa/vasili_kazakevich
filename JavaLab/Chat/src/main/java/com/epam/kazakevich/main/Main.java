package com.epam.kazakevich.main;

import java.util.Scanner;

import com.epam.kazakevich.client.Client;
import com.epam.kazakevich.server.Server;


public class Main {
	public static final int PORT=8283;
	public static void main(String[] args) {
		
		Scanner input = new Scanner(System.in);
		System.out
				.println("Do you want to be a server or a client? (Server - enter: s. Client- enter c.)");

		while (true) {
			String answer =input.nextLine();
			if (answer.equalsIgnoreCase("s")) {
				Server server = new Server();
				server.startServer(PORT);
				break;
			} else if (answer.equalsIgnoreCase("c")) {
				Client client =new Client();
				client.startClient(PORT);
				break;
			} else {
				System.out
						.println("You entered wrong parameter. (Server - enter: s. Client- enter c.)");
			}

		}
	}

}
