<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<link rel="stylesheet" type="text/css"	href="<c:url value="/resources/css/mainHeader.css" />">
	
	<div id="mainHeaderBlock">
		<div id="leftHeaderDiv" >

			<h1>
				<spring:message code="local.text.header" />
			</h1>

		</div>
		
	<div id="rightHeaderDiv"> 
		
		<div id="logoutBlock"  >
			<div ">
				<form action="logout" method="post">
					<input type="submit" value="<spring:message code="local.button.logout" />">
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
				</form>
			</div>
			<div id="greetingBlock" >
				<spring:message code="local.text.greeting" /> <security:authentication property="principal.username" />
			</div>		`	
			
			
		</div>
		
		<div id="localeBlock" align="right" >
			<a href="?language=en <c:if test="${queryString!=null}">${queryString}</c:if>"><spring:message code="local.button.en" /></a>
			<a href="?language=ru <c:if test="${queryString!=null}">${queryString}</c:if>"><spring:message code="local.button.ru" /></a>
		</div>
		
		</div>
	</div>
