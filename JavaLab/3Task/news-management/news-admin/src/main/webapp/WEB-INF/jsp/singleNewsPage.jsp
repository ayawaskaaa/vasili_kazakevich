<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>


<link rel="stylesheet" type="text/css"	href="<c:url value="/resources/css/singleNewsPage.css" />">
<fmt:setBundle basename="locale" var="bundle" />
<fmt:message bundle="${bundle}" key="local.date.format" var="pattern" />

	<div class="viewNewsMainBlock">

		<div align="left" style="margin-bottom: 15px;">
			<a href="/news-admin/"><spring:message
					code="local.button.back" /></a>

		</div>


		<div class="newsHead" style="width: 100%; height: 50px;">
			<div>
				<strong> ${news.title}</strong> (
				<spring:message code="local.text.byAuthor" />
				${news.authorsList[0].authorName} )
			</div>
			<div align="right" style="text-decoration: underline; float: right;">${newsVO.newsItem.modificationDate}</div>
		</div>


		<div class="fullTextBlock" align="left">${news.fullText}</div>

		<div class="commentTextBlock" align="left">
		
			<c:forEach var="commentItem" items="${news.commentList}">
			<fmt:formatDate value="${commentItem.creationDate}" pattern="${pattern}" var="formattedDate" />
				<div>${formattedDate}</div>
				<div
					style="background-color: #E0E0E0; width: 30%; position: relative;">
					<div id="boxclose"">
						<a
							href="/news-admin/single-news/delete?&commentId=${commentItem.commentId }&newsId=${commentItem.newsId }"
							style="color: black; text-decoration: none;">&#10006</a>
					</div>
					<c:out value="${commentItem.commentText}"></c:out>

				</div>
			</c:forEach>
		</div>
		<div class="postCommentBlock" align="left">
			<form:form method="POST" commandName="commentTO" action="postComment">
				<div>
					<form:textarea path="commentText" rows="3" cols="30"  />					
					<form:hidden path="newsId" value="${news.newsId}" />
				</div>
				<div id="errorMessage">
					<c:if test="${postCommentError}">
						<spring:message code="local.error.message.postComment" />
					</c:if>
					
				</div>
				<div>

					<input type="submit"
						value="<spring:message code="local.button.postComment" />">
				</div>
			</form:form>
		</div>
		<c:if test="${previousNewsId!=0}">
			<c:if test="${previousNewsId!=null}">
				<div style="float: left; margin-top: 15px;">
					<a href="${previousNewsId}"><spring:message
							code="local.button.previous" /></a>
				</div>
			</c:if>
		</c:if>
		<c:if test="${nextNewsId!=0}">
			<c:if test="${nextNewsId!=null}">
				<div style="float: right; margin-top: 15px;">
					<a href="${nextNewsId}"><spring:message
							code="local.button.next" /></a>
				</div>
			</c:if>
		</c:if>

	</div>
