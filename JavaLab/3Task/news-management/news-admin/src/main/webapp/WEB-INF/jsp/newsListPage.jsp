<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%@ page pageEncoding="UTF-8"%>

<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/newsListPage.css" />">
<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/simplePagination.css" />">
<script type="text/javascript"
	src="<spring:url value="/resources/js/tagSelectBar.js"/>"></script>
<script type="text/javascript"
	src="<spring:url value="/resources/js/jquery-1.11.3.min.js"/>"></script>
<script type="text/javascript"
	src="<spring:url value="/resources/js/jquery.simplePagination.js"/>"></script>



<div id="mainDiv">

	<input type="hidden" id="currentPage" class="currentPage"
		value="${sessionScope.homePgN}">


	<script type="text/javascript">
		$(function() {
			$('#light-pagination').pagination({
				pages : '${pagesAmount}',
				itemsOnPage : 10,
				hrefTextPrefix : '',
				currentPage : '${sessionScope.homePgN}',
				cssStyle : 'light-theme'
			});
		});
	</script>

	<fmt:setBundle basename="locale" var="bundle" />
	<fmt:message bundle="${bundle}" key="local.date.format" var="pattern" />

	<div class="filterBlock" align="center">
		<div class="box">
			<div>
				<form:form method="get" action="filter" commandName="searchCriteria">
					<div>
						<form:select name="asd" path="authorId">
							<form:option value="0" label="Please select the author" />
							<c:forEach var="authorList" items="${authors}">
								<c:if test="${authorList.expired==null}">

									<form:option value="${authorList.authorId}"
										label="${authorList.authorName}"
										selected="${sessionScope.authorId == authorList.authorId ? 'selected' : ' ' }" />
								</c:if>



							</c:forEach>
						</form:select>
					</div>
					<div>
						<div class="multiselect">
							<div class="selectBox" onclick="showCheckboxes()">
								<select>
									<option>Select an option</option>
								</select>
								<div class="overSelect"></div>
							</div>
							<div id="checkboxes">
								<c:forEach var="tagsList" items="${tags}">

									<c:set var="contains" value="false" />
									<c:forEach var="item" items="${sessionScope.tagListId}">
										<c:if test="${item eq tagsList.tagId}">
											<c:set var="contains" value="true" />
										</c:if>
									</c:forEach>
									<label for="${tagsList.tagId}"> <form:checkbox
											id="${tagsList.tagId}" path="tagIdList"
											value="${tagsList.tagId}"
											checked="${contains? 'cheked' :  ''	}" />${tagsList.tagName}
									</label>
								</c:forEach>
							</div>
						</div>
					</div>

					<div>
						<input type="submit"
							value="<spring:message code="local.button.filter" />">
					</div>

				</form:form>
			</div>

			<div>
				<form action="reset" method="get">
					<input type="submit"
						value="<spring:message code="local.button.reset" />">
				</form>
			</div>
		</div>
	</div>

	<form action="delete" method="post">
		<div class="newsBlock">
			<c:forEach var="news" items="${newsList}">
				<fmt:formatDate value="${news.modificationDate}"
					pattern="${pattern}" var="formattedDate" />

				<div class="titleBlock" align="left">
					<a href="/news-admin/single-news/${news.newsId}"><strong>
							${news.title}</strong></a> (
					<spring:message code="local.text.byAuthor" />
					${news.authorsList[0].authorName} )
					<div align="right">${formattedDate}</div>
				</div>
				<div align="left">${news.shortText}</div>

				<div class="box" align="right">
					<div class="tagsBlock">
						<c:forEach var="tagList" items="${news.tagsList}">${tagList.tagName } </c:forEach>
					</div>

					<div>
						<spring:message code="local.text.comments" />
						( ${fn:length(news.commentList)} )
					</div>
					<div>
						<a href="/news-admin/edit-news/${news.newsId}">
							<spring:message code="local.button.edit" />
						</a>
					</div>
					<div>
						<input type="checkbox" name="newsIdToDelete"
							value="${news.newsId}">
					</div>

				</div>

			</c:forEach>
		</div>
		<div align="right">
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" /> <input type="submit"
				value="<spring:message code="local.button.delete" />">
		</div>
	</form>
	<div class="searchResultMessage" align="center">
		<c:if test="${empty newsList}">
			<strong><spring:message
					code="local.text.searchResultMessage" /></strong>
		</c:if>
	</div>


	<div id="paginationBlock" align="center">

		<div id="light-pagination" class="pagination"></div>
	</div>



</div>

