<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page pageEncoding="UTF-8"%>


<script type="text/javascript" src="<c:url value="/resources/js/jquery-1.11.3.min.js" />"></script>
<link rel="stylesheet" type="text/css" href="<c:url value="/resources/css/addTagPage.css" />">


	<script type="text/javascript">
	function showText(show,hide)
	{
	    document.getElementById(show).className = "show";
	    document.getElementById(hide).className = "hide";
	}
	</script>
	
	
	
	
	
	<div id="addTagMainBlock">
	<c:forEach  var="tags" items="${tagList}">
	
		<script type="text/javascript">
		$(function() {
		    $('#baseUrl${tags.tagId}').click( function() {
		        window.location = $(this).attr('href') +  $('#appendUrl${tags.tagId}').val();
		        return false;
		    });
		});
		</script>
		
		<div id="editTagRow">
			<div class="tagText">
				<spring:message code="local.text.tag" />
			</div>
			<div class="tagName">
				<input   type="text" id="appendUrl${tags.tagId}" value="${tags.tagName}" size="50"  maxlength="30" >
				
			</div>
			<div id="${tags.tagId}"> 
				<a onclick="showText('${tags.tagName}','${tags.tagId}')" href="javascript:void(0);">
					<spring:message code="local.ref.edit" />
				</a>
			</div>
			<div class="editPanel" id= "${tags.tagName}">
				<a href="/news-admin/tag/update?&tagId=${tags.tagId}&tagName=" id="baseUrl${tags.tagId}" ><spring:message code="local.ref.update" /></a>
				
				<a href="/news-admin/tag/delete?&tagId=${tags.tagId}"><spring:message code="local.ref.delete" /></a>
				<a onclick="showText('${tags.tagId}','${tags.tagName}')" href="javascript:void(0);">
					<spring:message code="local.ref.cancel" />
				</a>				
			</div>
			
			
		</div>
		</c:forEach>
		<div id="newTagBlock">
			<form:form id ="addTag" action="add" commandName="tagTO" method="post">
			
				<div class="tagText">
					<spring:message code="local.text.addTag" />
				</div>
				<div class="tagName">
					<form:input path="tagName" required="required" maxlength="30" size="50"/>
					<form:errors path="tagName" cssclass="error"></form:errors>
				</div>
				<div id="savePanel" >
					<a href="javascript:;" onclick="document.getElementById('addTag').submit();">
				      <spring:message code="local.ref.save" />
				     </a>
								
				</div>
				<div>
					
				</div>
			</form:form>	
			
		</div>
	</div>
