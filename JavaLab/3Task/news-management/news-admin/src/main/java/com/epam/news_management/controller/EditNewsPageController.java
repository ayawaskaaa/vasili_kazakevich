package com.epam.news_management.controller;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.news_management.controller.util.DateHelper;
import com.epam.news_management.entity.Author;
import com.epam.news_management.entity.News;
import com.epam.news_management.entity.Tag;
import com.epam.news_management.service.IGeneralNewsManagementService;
import com.epam.news_management.service.exception.ServiceException;

@Controller
@RequestMapping("/edit-news")
public class EditNewsPageController {

	private Logger log = Logger.getLogger(EditNewsPageController.class);

	@Autowired
	private IGeneralNewsManagementService service;

	@RequestMapping(value = "/{newsId}", method = RequestMethod.GET)
	public String changePage(Model model, @PathVariable() Long newsId) {
		String page=null;
		DateHelper dateHelper = new DateHelper();
		News news = null;
		List<Author> authorsList = null;
		List<Tag> tagList = null;

		try {
			news = service.getSingleNews(newsId);
			authorsList = service.getAllAuthors();
			tagList = service.getAllTags();

		} catch (Exception e) {
			log.error("Error while getting data for edit page.", e);
			page= "errorPage";
		}

		model.addAttribute("authorTO", new Author());
		model.addAttribute("tagTO", new Tag());
		model.addAttribute("news", news);
		model.addAttribute("queryString", "&newsId=" + newsId);
		model.addAttribute("tagList", tagList);
		model.addAttribute("authorsList", authorsList);
		model.addAttribute("date", dateHelper.getCurrentDate());
		page="editNewsPage";
		return page;
	}

	@RequestMapping(value = "/edit", method = RequestMethod.POST)
	public String editNews(
			@Valid News news,
			BindingResult result,
			Model model,
			@RequestParam(value = "creationDate", required = false) String creationDate) {
		String page=null;

		DateHelper dateHelper = new DateHelper();

		if (!dateHelper.isThisDateValid(creationDate)) {
			List<Author> authorsList = null;
			List<Tag> tagList = null;

			try {

				authorsList = service.getAllAuthors();
				tagList = service.getAllTags();

			} catch (Exception e) {
				log.error("Error while getting data for edit page.", e);
				page="errorPage";
			}

			model.addAttribute("dateError", true);
			model.addAttribute("tagList", tagList);
			model.addAttribute("authorsList", authorsList);
			page= "editNewsPage";
			
		}
		if (result.hasErrors()) {
			List<Author> authorsList = null;
			List<Tag> tagList = null;

			try {

				authorsList = service.getAllAuthors();
				tagList = service.getAllTags();

			} catch (Exception e) {
				log.error("Error while getting data for edit page.", e);
				page= "errorPage";
			}

			model.addAttribute("tagList", tagList);
			model.addAttribute("authorsList", authorsList);
			page="redirect:/edit-news/" + news.getNewsId();
			
		}

		try {
			Author author = new Author();
			author.setAuthorId(news.getAuthorId());
			List<Author> authorsList = new ArrayList<Author>();
			authorsList.add(author);
			List<Tag> tagsList = new ArrayList<Tag>();
			for (Long id : news.getTagsIdList()) {
				Tag tag = new Tag();
				tag.setTagId(id);
				tagsList.add(tag);
			}

			news.setTagsList(tagsList);
			news.setAuthorsList(authorsList);
			news.setModificationDate(dateHelper.getCurrentDate());
			news.setCreationDate(dateHelper.getNewTimestamp(creationDate));

			boolean success = service.updateComplexNews(news);

			if (!success) {

				List<Tag> tagList = null;

				try {

					authorsList = service.getAllAuthors();
					tagList = service.getAllTags();

				} catch (Exception e) {
					log.error("Error while getting data for edit page.", e);
					page= "errorPage";
				}

				model.addAttribute("lockError", true);
				model.addAttribute("tagList", tagList);
				model.addAttribute("authorsList", authorsList);
				news.setVersion(news.getVersion() - 1);
				model.addAttribute("news", news);
				model.addAttribute("newsId", news.getNewsId());
				page= "editNewsPage";

			}

		} catch (Exception e) {
			log.error("Error while updating news.", e);
			page= "errorPage";
		}
		page="redirect:/edit-news/" + news.getNewsId();
		return page;
	}

}
