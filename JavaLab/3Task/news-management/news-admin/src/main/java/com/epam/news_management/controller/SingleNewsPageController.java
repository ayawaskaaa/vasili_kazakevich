package com.epam.news_management.controller;

import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.news_management.entity.Comment;
import com.epam.news_management.entity.News;
import com.epam.news_management.entity.SearchCriteria;
import com.epam.news_management.service.IGeneralNewsManagementService;
import com.epam.news_management.service.exception.ServiceException;

@Controller
@RequestMapping("/single-news")
public class SingleNewsPageController {

	private Logger log = Logger.getLogger(SingleNewsPageController.class);

	@Autowired
	private IGeneralNewsManagementService service;

	@RequestMapping(value = "/postComment", method = RequestMethod.POST)
	public String postComment(@Valid Comment comment, BindingResult result,
			Model model, HttpSession session) {
		String page = null;

		if (result.hasErrors()) {
			model.addAttribute("postCommentError", true);
			return viewNews(model, session, comment.getNewsId());
		}

		try {
			service.addComment(comment);
		} catch (Exception e) {
			log.error("Error while getting data for view page.", e);
			page = "errorPage";
		}
		page = "redirect:/single-news/" + comment.getNewsId();
		return page;
	}

	@SuppressWarnings("unchecked")
	@RequestMapping(value = "/{newsId}", method = RequestMethod.GET)
	public String viewNews(Model model, HttpSession session,
			@PathVariable() Long newsId) {
		String page = null;
		News news = null;
		Long nextNewsId = null;
		Long previousNewsId = null;
		SearchCriteria sessionSearchCriteria = new SearchCriteria();
		sessionSearchCriteria.setAuthorId((Long) session
				.getAttribute("authorId"));
		sessionSearchCriteria.setTagIdList((List<Long>) session
				.getAttribute("tagListId"));

		try {

			news = service.getSingleNews(newsId);

			nextNewsId = service.getNextNewsiId(sessionSearchCriteria, newsId);
			previousNewsId = service.getPreviousNewsId(sessionSearchCriteria,
					newsId);

		} catch (Exception e) {
			log.error("Error while getting data for view page.", e);
			page = "errorPage";
		}
		model.addAttribute("nextNewsId", nextNewsId);
		model.addAttribute("previousNewsId", previousNewsId);
		model.addAttribute("queryString", "&newsId=" + newsId);
		model.addAttribute("news", news);
		model.addAttribute("commentText", new Comment());
		model.addAttribute("commentTO", new Comment());
		page = "singleNewsPage";
		return page;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String deleteComment(
			Model model,
			HttpSession session,
			@RequestParam(value = "commentId", required = false) Long commentId,
			@RequestParam(value = "newsId", required = false) Long newsId) {
		String page = null;

		try {
			service.deleteComment(commentId);
		} catch (Exception e) {
			log.error("Error while deleting comment.", e);
			page = "errorPage";
		}
		page = "redirect:/single-news/" + newsId;
		return page;

	}

}
