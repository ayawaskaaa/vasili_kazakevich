package com.epam.news_management.controller;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.epam.news_management.entity.UserTO;
import com.epam.news_management.service.IGeneralNewsManagementService;
import com.epam.news_management.service.exception.ServiceException;

@Controller
public class LoginPageController {

	private Logger log = Logger.getLogger(LoginPageController.class);

	@Autowired
	private IGeneralNewsManagementService service;

	@RequestMapping({ "/login" })
	public String login() {

		return "loginPage";

	}

	@RequestMapping({ "/logout" })
	public String logout(HttpSession session) {
		session.invalidate();

		return "loginPage";

	}

}
