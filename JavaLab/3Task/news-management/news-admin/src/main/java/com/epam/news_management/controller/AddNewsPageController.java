package com.epam.news_management.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.news_management.controller.util.DateHelper;
import com.epam.news_management.entity.Author;
import com.epam.news_management.entity.News;
import com.epam.news_management.entity.Tag;
import com.epam.news_management.service.IGeneralNewsManagementService;

@Controller
@RequestMapping("/news")
public class AddNewsPageController {

	private Logger log = Logger.getLogger(AddNewsPageController.class);

	@Autowired
	private IGeneralNewsManagementService service;

	@RequestMapping(value = "/add-news-page", method = RequestMethod.GET)
	public String addNewsPage(Model model, ServletRequest request) {
		String page = null;
		DateHelper dateHelper = new DateHelper();

		List<Author> authorsList = null;
		List<Tag> tagList = null;

		try {

			authorsList = service.getAllAuthors();
			tagList = service.getAllTags();

		} catch (Exception e) {

			log.error("Error while getting data for add news page.", e);
			page = "errorPage";
		}
		model.addAttribute("authorTO", new Author());
		model.addAttribute("tagTO", new Tag());
		model.addAttribute("news", new News());
		model.addAttribute("date", dateHelper.getCurrentDate());
		model.addAttribute("tagList", tagList);
		model.addAttribute("authorsList", authorsList);
		model.addAttribute("addNewsPageFlag", true);
		page = "addNewsPage";
		return page;
	}

	@RequestMapping({ "/add" })
	public String addNews(
			@Valid News news,
			BindingResult result,
			Model model,
			@RequestParam(value = "creationDate", required = false) String creationDate) {
		String page = null;
		Long newNewsIdLong = null;
		DateHelper dateHelper = new DateHelper();
		if (!dateHelper.isThisDateValid(creationDate)) {

			List<Author> authorsList = null;
			List<Tag> tagList = null;

			try {

				authorsList = service.getAllAuthors();
				tagList = service.getAllTags();

			} catch (Exception e) {

				log.error("Error while getting data for edit page.", e);
				page = "errorPage";
			}
			model.addAttribute("tagList", tagList);
			model.addAttribute("authorsList", authorsList);
			model.addAttribute("dateError", true);
			model.addAttribute("date", dateHelper.getCurrentDate());
			page = "addNewsPage";

		}

		if (result.hasErrors()) {
			List<Author> authorsList = null;
			List<Tag> tagList = null;

			try {

				authorsList = service.getAllAuthors();
				tagList = service.getAllTags();

			} catch (Exception e) {

				log.error("Error while getting data for edit page.", e);
				page = "errorPage";
			}
			model.addAttribute("tagList", tagList);
			model.addAttribute("authorsList", authorsList);

			model.addAttribute("date", dateHelper.getCurrentDate());

			page = "addNewsPage";
		}

		try {

			Author author = new Author();
			author.setAuthorId(news.getAuthorId());
			List<Author> authorsList = new ArrayList<Author>();
			authorsList.add(author);
			List<Tag> tagsList = new ArrayList<Tag>();
			for (Long id : news.getTagsIdList()) {
				Tag tag = new Tag();
				tag.setTagId(id);
				tagsList.add(tag);
			}

			news.setTagsList(tagsList);
			news.setAuthorsList(authorsList);
			news.setCreationDate(dateHelper.getNewTimestamp(creationDate));
			news.setModificationDate(dateHelper.getCurrentDate());
			newNewsIdLong = service.addComplexNews(news);
		} catch (Exception e) {

			log.error("Error while adding coplex news.", e);
			page = "errorPage";
		}
		page = "redirect:/single-news/" + newNewsIdLong;
		return page;

	}

}
