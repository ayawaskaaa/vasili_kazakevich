package com.epam.news_management.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.news_management.controller.util.PageConfig;
import com.epam.news_management.entity.Author;
import com.epam.news_management.entity.Comment;
import com.epam.news_management.entity.News;
import com.epam.news_management.entity.NewsVO;
import com.epam.news_management.entity.SearchCriteria;
import com.epam.news_management.entity.Tag;
import com.epam.news_management.entity.UserTO;
import com.epam.news_management.service.IGeneralNewsManagementService;
import com.epam.news_management.service.exception.ServiceException;

@Controller
@RequestMapping("/")
public class NewsListPageController {

	private Logger log = Logger.getLogger(NewsListPageController.class);

	@Autowired
	private IGeneralNewsManagementService service;

	@SuppressWarnings("unchecked")
	@RequestMapping("/{pgn}")
	public String newsListPage(Model model, HttpSession session,
			@PathVariable() Long pgn) {
		String page=null;

		SearchCriteria sessionSearchCriteria = new SearchCriteria();
		List<News> newsList = null;
		List<Author> authorsList = null;
		List<Tag> tagsList = null;
		int pagesAmount = 0;
		int pageNumber = 1;

		sessionSearchCriteria.setAuthorId((Long) session
				.getAttribute("authorId"));
		sessionSearchCriteria.setTagIdList((List<Long>) session
				.getAttribute("tagListId"));

		if (pgn != null) {
			session.setAttribute("homePgN", pgn);
		}

		if (session.getAttribute("homePgN") != null) {
			pageNumber = Integer.parseInt(session.getAttribute("homePgN")
					.toString());

		}
		int itemAmount = 0;

		int startIndex = PageConfig.calculateStartIndex(pageNumber);
		int endIndex = PageConfig.calculateEndIndex(pageNumber);

		try {

			newsList = service.searchByAuthorAndTag(sessionSearchCriteria,
					startIndex, endIndex);
			authorsList = service.getAllAuthors();
			tagsList = service.getAllTags();

			itemAmount = service.countNewsInSearch(sessionSearchCriteria);

			pagesAmount = PageConfig.countPageAmount(itemAmount);

		} catch (Exception e) {
			log.error("Error while getting data for view page.", e);
			page= "errorPage";
		}

		model.addAttribute("searchCriteria", new SearchCriteria());
		model.addAttribute("authors", authorsList);
		model.addAttribute("newsList", newsList);
		model.addAttribute("tags", tagsList);
		model.addAttribute("pagesAmount", pagesAmount);
		model.addAttribute("newsListFlag", true);
		page="newsListPage";
		return page;
	}

	@RequestMapping(value = "filter", method = RequestMethod.GET)
	public String filter(Model model, HttpSession session,
			SearchCriteria searchCriteria) {

		Long authorId = null;
		if (searchCriteria.getAuthorId() != null) {
			if (searchCriteria.getAuthorId() == 0) {
				authorId = null;
			} else {
				authorId = searchCriteria.getAuthorId();
			}
			session.removeAttribute("homePgN");
			session.setAttribute("authorId", authorId);
			session.setAttribute("tagListId", searchCriteria.getTagIdList());
		}

		return newsListPage(model, session, null);
	}

	@RequestMapping(value = "reset", method = RequestMethod.GET)
	public String reset(Model model, HttpSession session,
			SearchCriteria searchCriteria) {

		session.removeAttribute("authorId");
		session.removeAttribute("tagListId");
		session.removeAttribute("homePgN");

		return newsListPage(model, session, null);
	}

	@RequestMapping(value = "delete", method = RequestMethod.POST)
	public String deleteNews(
			Model model,
			HttpSession session,
			@RequestParam(value = "newsIdToDelete", required = false) List<Long> newsIdList) {
		String page=null;
		try {
			if (newsIdList != null) {
				for (Long i : newsIdList) {

					service.deleteNews(i);

				}
			}

		} catch (Exception e) {
			log.error("Error while deleting news.", e);
			page= "errorPage";
		}
		page="redirect:/";
		return page;
	}

	@RequestMapping(value = "")
	public String startPage(HttpSession session, Model model) {
		return newsListPage(model, session, 1L);
	}

}
