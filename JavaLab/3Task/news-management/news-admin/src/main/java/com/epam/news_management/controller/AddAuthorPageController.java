package com.epam.news_management.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.news_management.entity.Author;
import com.epam.news_management.entity.Tag;
import com.epam.news_management.service.IGeneralNewsManagementService;
import com.epam.news_management.service.exception.ServiceException;

@Controller
@RequestMapping("/author")
public class AddAuthorPageController {

	private Logger log = Logger.getLogger(AddAuthorPageController.class);

	@Autowired
	private IGeneralNewsManagementService service;

	@RequestMapping(value = "/add-author-page ", method = RequestMethod.GET)
	public String addAuthorPage(Model model) {
		String page = null;
		List<Author> authorList = null;

		try {

			authorList = service.getAllAuthors();

		} catch (Exception e) {
			log.error("Error while getting data for autor  page.", e);
			page = "errorPage";
		}
		model.addAttribute("authorTO", new Author());
		model.addAttribute("authorList", authorList);
		model.addAttribute("addAuthorPageFlag", true);
		page = "addAuthorPage";
		return page;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addNewsAuthor(@Valid Author author, BindingResult result,
			Model model) {
		String page = null;

		if (result.hasErrors()) {
			List<Author> authorList = null;
			try {

				authorList = service.getAllAuthors();

			} catch (Exception e) {
				log.error("Error while getting data for autor  page.", e);
				page = "errorPage";
			}

			model.addAttribute("authorList", authorList);

			page = "addAuthorPage";
		}

		try {
			if (!author.getAuthorName().isEmpty()) {
				service.addAuthor(author);
			}

		} catch (Exception e) {
			log.error("Error while saving author.", e);
			page = "errorPage";
		}
		page = "redirect:/author/add-author-page";
		return page;
	}

	@RequestMapping(value = "/update", method = RequestMethod.GET)
	public String udateAuthor(
			Model model,
			@RequestParam(value = "authorId", required = false) Long authorId,
			@RequestParam(value = "authorName", required = false) String authorName

	) {
		String page = null;
		Author authorTO = new Author();
		authorTO.setAuthorId(authorId);
		authorTO.setAuthorName(authorName);

		try {
			if (!authorName.isEmpty()) {
				service.updateAuthor(authorTO);
			}
		} catch (Exception e) {
			log.error("Error while updating author.", e);
			page = "errorPage";
		}
		page = "redirect:/author/add-author-page";
		return page;
	}

	@RequestMapping(value = "/make-expired", method = RequestMethod.GET)
	public String makeAuthorExpired(Model model,
			@RequestParam(value = "authorId", required = false) Long authorId) {
		String page = null;
		Author authorTO = new Author();
		authorTO.setAuthorId(authorId);

		try {
			service.deleteAuthor(authorId);
		} catch (Exception e) {
			log.error("Error while updating author.", e);
			page = "errorPage";
		}
		page = "redirect:/author/add-author-page";
		return page;
	}

}
