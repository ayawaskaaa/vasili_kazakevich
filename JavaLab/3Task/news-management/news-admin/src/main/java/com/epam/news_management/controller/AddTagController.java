package com.epam.news_management.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.news_management.entity.Tag;
import com.epam.news_management.service.IGeneralNewsManagementService;
import com.epam.news_management.service.exception.ServiceException;

@Controller
@RequestMapping("/tag")
public class AddTagController {

	private Logger log = Logger.getLogger(AddTagController.class);

	@Autowired
	private IGeneralNewsManagementService service;

	@RequestMapping(value = "/add-tag-page", method = RequestMethod.GET)
	public String addTagPage(Model model) {

		String page = null;

		List<Tag> tagList = null;

		try {

			tagList = service.getAllTags();

		} catch (Exception e) {
			log.error("Error while getting data for tag page.", e);
			page = "errorPage";
		}
		model.addAttribute("tagTO", new Tag());
		model.addAttribute("tagList", tagList);
		model.addAttribute("addTagPageFlag", true);
		page = "addTagPage";
		return page;
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addNewsAuthor(@Valid Tag tag, BindingResult result,
			Model model) {
		String page = null;

		if (result.hasErrors()) {
			List<Tag> tagList = null;

			try {

				tagList = service.getAllTags();

			} catch (Exception e) {
				log.error("Error while getting data for tag page.", e);
				page = "errorPage";
			}

			model.addAttribute("tagList", tagList);

			page = "addTagPage";
		}

		try {
			if (!tag.getTagName().isEmpty()) {
				service.addTag(tag);
			}

		} catch (Exception e) {
			log.error("Error while saving tag.", e);
			page = "errorPage";
		}
		page = "redirect:/tag/add-tag-page";
		return page;
	}

	@RequestMapping(value = "/update", method = RequestMethod.GET)
	public String udateTag(Model model,
			@RequestParam(value = "tagId", required = false) Long tagId,
			@RequestParam(value = "tagName", required = false) String tagName

	) {
		String page = null;
		Tag tagTO = new Tag();
		tagTO.setTagId(tagId);
		tagTO.setTagName(tagName);
		try {
			if (!tagName.isEmpty()) {
				service.updateTag(tagTO);
			}
		} catch (Exception e) {
			log.error("Error while updating tag.", e);
			page = "errorPage";
		}
		page = "redirect:/tag/add-tag-page";
		return page;
	}

	@RequestMapping(value = "/delete", method = RequestMethod.GET)
	public String deleteTag(Model model,
			@RequestParam(value = "tagId", required = false) Long tagId) {
		String page = null;
		try {

			service.deleteTag(tagId);
		} catch (Exception e) {
			log.error("Error while getting data for tag page.", e);
			page = "errorPage";
		}
		page = "redirect:/tag/add-tag-page";
		return page;
	}

}
