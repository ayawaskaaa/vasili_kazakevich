package com.epam.news_client.controller.util;

public final class PageConfig {

	public static final int ITEMS_PER_PAGE = 3;
	
	public static int countPageAmount(int itemAmount) {
		int pagesAmount = 0;
		
		if (itemAmount % ITEMS_PER_PAGE == 0) {
			pagesAmount = itemAmount / ITEMS_PER_PAGE;
		} else {
			pagesAmount = (itemAmount / ITEMS_PER_PAGE) + 1;
		}
		return pagesAmount;
	}
	
	public static int calculateStartIndex(int pageNumber){
		return pageNumber * ITEMS_PER_PAGE - ITEMS_PER_PAGE + 1;
		}
	public static int calculateEndIndex(int pageNumber){
		return pageNumber * ITEMS_PER_PAGE;
		
	}
	
	

}
