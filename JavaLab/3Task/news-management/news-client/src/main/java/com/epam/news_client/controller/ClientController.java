package com.epam.news_client.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.news_client.controller.util.PageConfig;
import com.epam.news_management.entity.Author;
import com.epam.news_management.entity.News;
import com.epam.news_management.entity.SearchCriteria;
import com.epam.news_management.entity.Tag;
import com.epam.news_management.service.IGeneralNewsManagementService;

@Controller
@RequestMapping("/")
public class ClientController {

	private Logger log = Logger.getLogger(ClientController.class);

	public ClientController() {
	}

	@Autowired
	private IGeneralNewsManagementService service;

	@RequestMapping("/{pgn}")
	public String home(Model model, HttpSession session,
			@PathVariable() Long pgn) {
		String page = null;
		SearchCriteria sessionSearchCriteria = new SearchCriteria();
		List<News> newsList = null;
		List<Author> authorsList = null;
		List<Tag> tagsList = null;
		int pagesAmount = 0;
		int pageNumber = 1;
		int itemAmount = 0;
		sessionSearchCriteria.setAuthorId((Long) session
				.getAttribute("authorId"));
		sessionSearchCriteria.setTagIdList((List<Long>) session
				.getAttribute("tagListId"));

		if (pgn != null) {
			session.setAttribute("homePgN", pgn);
		}

		if (session.getAttribute("homePgN") != null) {
			pageNumber = Integer.parseInt(session.getAttribute("homePgN")
					.toString());

		}

		int startIndex = PageConfig.calculateStartIndex(pageNumber);
		int endIndex = PageConfig.calculateEndIndex(pageNumber);

		try {
			newsList = service.searchByAuthorAndTag(sessionSearchCriteria,
					startIndex, endIndex);
			authorsList = service.getAllAuthors();
			tagsList = service.getAllTags();

			itemAmount = service.countNewsInSearch(sessionSearchCriteria);
			pagesAmount = PageConfig.countPageAmount(itemAmount);

		} catch (Exception e) {
			log.error("Error while getting data for view page.", e);
			page = "errorPage";
		}

		model.addAttribute("searchCriteria", new SearchCriteria());
		model.addAttribute("authorTO", new Author());
		model.addAttribute("tagIdList", new ArrayList<Long>());
		model.addAttribute("authors", authorsList);
		model.addAttribute("newsList", newsList);
		model.addAttribute("tags", tagsList);
		model.addAttribute("pagesAmount", pagesAmount);

		page = "home";
		return page;
	}

	@RequestMapping(value = "filter", method = RequestMethod.GET)
	public String filter(Model model, HttpSession session,
			SearchCriteria searchCriteria) {

		Long authorId = null;
		if (searchCriteria.getAuthorId() != null) {
			if (searchCriteria.getAuthorId() == 0) {
				authorId = null;
			} else {
				authorId = searchCriteria.getAuthorId();
			}
			session.removeAttribute("homePgN");
			session.setAttribute("authorId", authorId);
			session.setAttribute("tagListId", searchCriteria.getTagIdList());
		}

		return home(model, session, null);
	}

	@RequestMapping(value = "reset", method = RequestMethod.GET)
	public String reset(Model model, HttpSession session,
			SearchCriteria searchCriteria) {

		session.removeAttribute("authorId");
		session.removeAttribute("tagListId");
		session.removeAttribute("homePgN");
		return home(model, session, null);
	}

	@RequestMapping(value = "")
	public String startPage(HttpSession session, Model model) {
		return home(model, session, 1L);
	}

}
