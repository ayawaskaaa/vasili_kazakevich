<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

	<div width="100%">
		<div align="left">

			<h1>
				<spring:message code="local.text.header" />
			</h1>

		</div>
		<div align="right">
	

			<a href="?language=en <c:if test="${queryString!=null}">${queryString}</c:if>"><spring:message code="local.button.en" /></a>
			<a href="?language=ru <c:if test="${queryString!=null}">${queryString}</c:if>"><spring:message code="local.button.ru" /></a>



		</div>
	</div>
