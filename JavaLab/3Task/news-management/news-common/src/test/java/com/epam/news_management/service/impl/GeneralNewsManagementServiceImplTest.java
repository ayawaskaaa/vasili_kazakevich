package com.epam.news_management.service.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;


















import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.news_management.entity.Author;
import com.epam.news_management.entity.Comment;
import com.epam.news_management.entity.News;
import com.epam.news_management.entity.NewsVO;
import com.epam.news_management.entity.SearchCriteria;
import com.epam.news_management.entity.Tag;
import com.epam.news_management.service.IAuthorService;
import com.epam.news_management.service.ICommentService;
import com.epam.news_management.service.INewsService;
import com.epam.news_management.service.ITagService;
import com.epam.news_management.service.exception.ServiceException;
import com.epam.news_management.service.impl.jdbc.GeneralNewsManagementServiceImplJDBC;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:NewsManagementSpringContexTest.xml"})
@ActiveProfiles("hibernate")
public class GeneralNewsManagementServiceImplTest {
	
	
	@Mock
	private INewsService newsServiceMock;
	
	@Mock
	private IAuthorService authorServiceMock;
	
	@Mock
	private ITagService tagServiceMock;
	
	@Mock
	private ICommentService commentServiceMock;
	
	@Mock
	private NewsVO newsVOMock;
	
	
	@InjectMocks
	@Autowired
	private GeneralNewsManagementServiceImplJDBC generalService;
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		 assertNotNull(newsServiceMock);
		 assertNotNull(authorServiceMock);
		 assertNotNull(tagServiceMock);
		 assertNotNull(commentServiceMock);
		 
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void addNewsTest() throws ServiceException {
		
		when(newsServiceMock.addNews(new News())).thenReturn(anyLong());
		generalService.addNews(new News());
		verify(newsServiceMock, times(1)).addNews(new News());
		
	}
	
	@Test
	public void addAuthorTest() throws ServiceException {
		
		when(authorServiceMock.addAuthor(new Author())).thenReturn(anyLong());
		generalService.addAuthor(new Author());
		verify(authorServiceMock, times(1)).addAuthor(new Author());
		
	}
	
	@Test
	public void addCommentTest() throws ServiceException {
		when(commentServiceMock.addComment(new Comment())).thenReturn(anyLong());
		generalService.addComment(new Comment());
		verify(commentServiceMock, times(1)).addComment(new Comment());
		
	}
	
	@Test
	public void deleteTagTest() throws ServiceException {
		generalService.deleteTag(anyLong());
		verify(tagServiceMock, times(1)).deleteTag(anyLong());
		
	}
	
	@Test
	public void deleteAuthorTest() throws ServiceException {
		generalService.deleteAuthor(anyLong());
		verify(authorServiceMock, times(1)).deleteAuthor(anyLong());
		
	}
	
	@Test
	public void deleteCommentTest() throws ServiceException {
		generalService.deleteComment(anyLong());
		verify(commentServiceMock, times(1)).deleteComment(anyLong());
		
	}
	
	@Test
	public void addTagTest() throws ServiceException {
		
		generalService.addTag(new Tag());
		verify(tagServiceMock, times(1)).addTag(new Tag());
		
	}
	
	@Test
	public void editNewsTest() throws ServiceException {
		generalService.editNews(new News());
		verify(newsServiceMock, times(1)).editNews(new News());
		
	}
	
	@Test
	public void searchByAuthorAndTagTest() throws ServiceException {
		
		List<News> resultNewsList = null;
		when(newsServiceMock.search(any(SearchCriteria.class), anyInt(), anyInt())).thenReturn(Arrays.asList(new News(),new News(),new News()));
		resultNewsList=generalService.searchByAuthorAndTag(any(SearchCriteria.class), anyInt(), anyInt());
		verify(newsServiceMock, times(1)).search(any(SearchCriteria.class), anyInt(), anyInt());
		assertNotNull(resultNewsList);
		
	}
	
	@Test
	public void countNewsInSearchTest() throws ServiceException {
		int expectedCount = 1;
		int resultCount = 0;
		when(newsServiceMock.countNewsInSearch(any(SearchCriteria.class))).thenReturn(Integer.valueOf(1));
		resultCount=newsServiceMock.countNewsInSearch(any(SearchCriteria.class));
		verify(newsServiceMock, times(1)).countNewsInSearch(any(SearchCriteria.class));
		assertEquals(expectedCount, resultCount);
	}
	
	@Test
	public void addComplexNewsTest() throws ServiceException {
		
		News news = new News();
		Tag tagTO = new Tag();
		tagTO.setTagId(1L);
/*		news.setNewsItem(new News());
		news.setAuthorItem(new Author());
		news.setTagItemList(Arrays.asList(tagTO,tagTO,tagTO));*/
		
		when(newsVOMock.getNewsItem()).thenReturn(new News());
		when(newsVOMock.getAuthorItem()).thenReturn(new Author());
		when(newsVOMock.getTagItemList()).thenReturn(Arrays.asList(new Tag(),new Tag(),new Tag()));
		
		when(newsServiceMock.addNews(new News())).thenReturn(anyLong());
	
		
		
		generalService.addComplexNews(news);
		verify(newsServiceMock, times(1)).addNews(any(News.class));
		verify(authorServiceMock, times(1)).linkAuthorWhithNews( anyLong(), anyLong());
		verify(tagServiceMock, times(1)).linkTagsWithNews(anyListOf(Long.class),anyLong());
	}
	
	@Test
	public void deleteNewsTest() throws ServiceException {
		generalService.deleteNews(anyLong());
		verify(commentServiceMock, times(1)).deleteCommentByNewsId(anyLong());
		verify(authorServiceMock, times(1)).unlinkAutorWhithNewsByNewsId(anyLong());
		verify(tagServiceMock, times(1)).unlinkTagWithNews(anyLong());		
		verify(newsServiceMock, times(1)).deleteNews(anyLong());
	}
	
	@Test
	public void getSingleNewsTest() throws ServiceException {
		
		News resultNewsVO =null;
		when(newsServiceMock.getSingleNews(anyLong())).thenReturn(new News());
		when(authorServiceMock.getAuthorByNewsId(anyLong())).thenReturn(new Author());
		when(commentServiceMock.getCommentsByNewsId(anyLong())).thenReturn(Arrays.asList(new Comment(),new Comment(),new Comment()));
		when(tagServiceMock.getTagsByNewsId(anyLong())).thenReturn(Arrays.asList(new Tag(),new Tag(),new Tag()));
		resultNewsVO= generalService.getSingleNews(anyLong());
		verify(commentServiceMock, times(1)).getCommentsByNewsId(anyLong());
		verify(authorServiceMock, times(1)).getAuthorByNewsId(anyLong());			
		verify(newsServiceMock, times(1)).getSingleNews(anyLong());
		verify(tagServiceMock, times(1)).getTagsByNewsId(anyLong());
		assertNotNull(resultNewsVO);
	}
	
	
	
	
	
	
}
