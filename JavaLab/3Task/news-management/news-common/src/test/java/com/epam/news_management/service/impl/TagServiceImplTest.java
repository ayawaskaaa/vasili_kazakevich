package com.epam.news_management.service.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.news_management.dao.ITagDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.Tag;
import com.epam.news_management.service.exception.ServiceException;
import com.epam.news_management.service.impl.jdbc.TagServiceImpl;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:test/resources/NewsManagementSpringContexTest.xml"})
public class TagServiceImplTest {
	
	@Mock
	private ITagDao tagDaoMock;

	
	@InjectMocks
	@Autowired
	private TagServiceImpl tagAction;
	
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		 MockitoAnnotations.initMocks(this);
		 assertNotNull(tagAction);
		 assertNotNull(tagDaoMock);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void addTagTest() throws ServiceException, DaoException {
		
		tagAction.addTag(new Tag());
		verify(tagDaoMock, times(1)).add(new Tag());
		
	}
	
	@Test
	public void deleteTagTest() throws ServiceException, DaoException {
		tagAction.deleteTag(anyLong());
		verify(tagDaoMock, times(1)).delete(anyLong());
		
	}
	@Test
	public void getTagsByNewsIdTest() throws ServiceException, DaoException {
		List<Tag> expectedTagList = Arrays.asList(new Tag(),new Tag(),new Tag());
		List<Tag> resultTagList = null;
		when(tagDaoMock.getTagsByNewsId(anyLong())).thenReturn(Arrays.asList(new Tag(),new Tag(),new Tag()));
		resultTagList=tagAction.getTagsByNewsId(anyLong());
		verify(tagDaoMock, times(1)).getTagsByNewsId(anyLong());
		assertEquals(expectedTagList, resultTagList);
		
		
		
	}
	@Test
	public void editTagTest() throws ServiceException, DaoException {
		
		tagAction.editTag(new Tag());
		verify(tagDaoMock, times(1)).edit(new Tag());
		
		
	}
	@Test
	public void linkTagsWithNews()throws ServiceException, DaoException{
		
		tagAction.linkTagsWithNews(anyList(), anyLong());
		verify(tagDaoMock, times(1)).link(anyList(), anyLong());
		
	}
	@Test
	public void unlinkTagWithNewsTest() throws ServiceException, DaoException{
		tagAction.unlinkTagWithNews(anyLong());
		verify(tagDaoMock, times(1)).unlinkByNewsId(anyLong());
		
	}
	
	@Test
	public void unlinkTagWithNewsByTagIdTest() throws ServiceException, DaoException{
		tagAction.unlinkTagWithNewsByTagId(anyLong());
		verify(tagDaoMock, times(1)).unlinkByTagId(anyLong());
		
	}

}
