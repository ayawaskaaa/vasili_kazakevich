package com.epam.news_management.dao.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;












import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import com.epam.news_management.dao.IAuthorDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.Author;

@ActiveProfiles("jdbc-beans")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:NewsManagementSpringContexTest.xml"})
@DataSet(value="NewsManagementDataSet.xml",loadStrategy = CleanInsertLoadStrategy.class)
public class AuthorDaoImplTest extends UnitilsJUnit4 {
	
	@Autowired
	private IAuthorDao authorDao;
	
	@Before
	public void setUp() throws Exception {

		assertNotNull(authorDao);
	}
	



	@Test
	public void addTest() throws DaoException {
		Author expectedAuthor = new Author();
		Long generatedId = null;
		expectedAuthor.setAuthorName("testName");
		generatedId=authorDao.add(expectedAuthor);
		expectedAuthor.setAuthorId(generatedId);
		assertEquals(expectedAuthor, authorDao.get(generatedId));
	}
	@Test
	public void getAllTest() throws DaoException {
		List <Author> resultAuthorsList = new  ArrayList<Author>();
		resultAuthorsList=authorDao.getAll();
		assertNotNull(resultAuthorsList);
		
	}
	@Test
	public void getTest() throws DaoException {
		Author expectedAuthor = new Author();
		Author resultAuthor = null;
		expectedAuthor.setAuthorId(3L);
		expectedAuthor.setAuthorName("testName3");
		expectedAuthor.setExpired(null);
		resultAuthor=authorDao.get(3L);
		assertEquals(expectedAuthor, resultAuthor);
		
	}
	@Test
	public void editTest() throws DaoException {
		Author expectedAuthor = new Author();
		Author resultAuthor = null;
		expectedAuthor.setAuthorId(2L);
		expectedAuthor.setAuthorName("edit");
		expectedAuthor.setExpired(null);
		authorDao.edit(expectedAuthor);
		resultAuthor=authorDao.get(2L);
		assertEquals(expectedAuthor, resultAuthor);
		
	}
	@Test
	public void deleteTest() throws DaoException {
		Author resultAuthor = null;
		authorDao.delete(1L);
		resultAuthor = authorDao.get(1L);
		assertNotNull(resultAuthor.getExpired());
		
	}
//	@Test
	@ExpectedDataSet
	public void linkTest() throws DaoException {
				
		authorDao.link(3L, 3L);
		
		
		
	}

//	@Test
	@ExpectedDataSet
	public void unlinkByNewsIdTest() throws DaoException {
		authorDao.unlinkByNewsId(1L);
	}

//	@Test
	@ExpectedDataSet
	public void unlinkByAuthorIdTest() throws DaoException {
		
		authorDao.unlinkByAuthorId(1L);
		
	}

//	@Test
	public void getAuthorByNewsIdTest() throws DaoException {
		Author expectedAuthor = new Author();
		Author resultAuthor = null;
		expectedAuthor.setAuthorId(1L);
		expectedAuthor.setAuthorName("testName1");
		expectedAuthor.setExpired(null);
		resultAuthor=authorDao.getAuthorByNewsId(1L);
		assertEquals(expectedAuthor, resultAuthor);
	}


}
