package com.epam.news_management.dao.impl;

import static org.junit.Assert.*;

import java.util.Date;
import java.util.List;

import javax.sql.DataSource;











import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import com.epam.news_management.dao.ICommentDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.Comment;


@ActiveProfiles("hibernate")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:NewsManagementSpringContexTest.xml"})
@DataSet(value="NewsManagementDataSet.xml",loadStrategy = CleanInsertLoadStrategy.class)
public class CommentDaoImplTest extends UnitilsJUnit4 {


	@Autowired
	private ICommentDao commentDao;


	@Before
	public void setUp()  {
		assertNotNull(commentDao);
	}



	@Test
	public void addTest() throws DaoException {
		Comment expectedCommentTO =new Comment();
		Comment resultCommentTO = null;
		Long generatedId = null;

		expectedCommentTO.setCommentText("testText2");
		expectedCommentTO.setNewsId(2L);
		expectedCommentTO.setCreationDate(new Date());
		generatedId=commentDao.add(expectedCommentTO);
		expectedCommentTO.setCommentId(generatedId);
		resultCommentTO=commentDao.get(generatedId);
		assertEquals(expectedCommentTO.getCommentText(),resultCommentTO.getCommentText());
		
		
		
	}
//	@Test
	public void getAllTest() throws DaoException {
		List <Comment> commentList = null;
		commentList=commentDao.getAll();
		assertNotNull(commentList);
		
	}
	@Test
	public void getTest() throws DaoException {
		Comment expectedCommentTO =new Comment();
		Comment resultCommentTO = null;
		expectedCommentTO.setCommentText("testComment2");
		resultCommentTO=commentDao.get(2L);
		assertEquals(expectedCommentTO.getCommentText(), resultCommentTO.getCommentText());
		
	}
	@Test
	public void editTest() throws DaoException {
		Comment expectedCommentTO =new Comment();
		Comment resultCommentTO = null;
		expectedCommentTO.setCommentId(3L);
		expectedCommentTO.setNewsId(3L);
		expectedCommentTO.setCommentText("edit");
		expectedCommentTO.setCreationDate(new Date());
		commentDao.edit(expectedCommentTO);
		resultCommentTO=commentDao.get(3L);
		
		
		assertEquals(expectedCommentTO.getCommentText(), resultCommentTO.getCommentText());
		
	}
	@Test
	public void deleteTest() throws DaoException {
		Comment resultCommentTO = null;
		commentDao.delete(1L);
		resultCommentTO=commentDao.get(1L);
		assertNull(resultCommentTO);
		
	}
//	@Test
	public void getCommentsByNewsIdTest() throws DaoException {
		Comment expectedCommentTO =new Comment();
		Comment resultCommentTO = null;
		
		expectedCommentTO.setCommentText("testComment1");
		
		expectedCommentTO.setNewsId(1L);
		expectedCommentTO.setCommentId(1L);
		resultCommentTO=commentDao.get(1L);
		assertEquals(expectedCommentTO.getCommentText(),resultCommentTO.getCommentText());
		
		
	}
	/*@Test
	public void deleteByNewsIdTest() throws DaoException {
		Comment resultCommentTO = null;
		commentDao.deleteByNewsId(2L);
		resultCommentTO=commentDao.get(2L);
		assertNull(resultCommentTO);
		
	}*/

	
	

}
