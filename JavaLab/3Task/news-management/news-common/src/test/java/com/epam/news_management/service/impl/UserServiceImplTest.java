package com.epam.news_management.service.impl;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyList;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.Matchers.any;

import org.mockito.internal.matchers.Any;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.news_management.dao.IUserDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.UserTO;
import com.epam.news_management.service.IUserService;
import com.epam.news_management.service.exception.ServiceException;



@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:NewsManagementSpringContexTest.xml"})
public class UserServiceImplTest {
	
	@Mock
	private IUserDao userDaoMock;
	
	@InjectMocks
	@Autowired
	private IUserService userService;

	@Before
	public void setUp() throws Exception {
		 MockitoAnnotations.initMocks(this);
		 assertNotNull(userService);
		 assertNotNull(userDaoMock);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void loginTest() throws DaoException, ServiceException {
		UserTO resultUserTO=null;
		when(userDaoMock.login(new UserTO())).thenReturn(new UserTO());
		resultUserTO=userService.login(new UserTO());
		verify(userDaoMock, times(1)).login(any(UserTO.class));
		assertNotNull(resultUserTO);
	}

}
