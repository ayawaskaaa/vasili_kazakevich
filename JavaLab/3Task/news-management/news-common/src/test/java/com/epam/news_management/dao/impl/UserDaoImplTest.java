package com.epam.news_management.dao.impl;

import static org.junit.Assert.*;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;

import com.epam.news_management.dao.IUserDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.UserTO;
@RunWith(SpringJUnit4ClassRunner.class)
@ActiveProfiles("hibernate")
@ContextConfiguration(locations = {"classpath:NewsManagementSpringContexTest.xml"})
@DataSet(value="NewsManagementDataSet.xml",loadStrategy = CleanInsertLoadStrategy.class)
public class UserDaoImplTest extends UnitilsJUnit4 {
	
	@Autowired
	private IUserDao userDao;
	

	@Before
	public void setUp() throws Exception {
		assertNotNull(userDao);
	}



	/*@Test
	public void loginTest() throws DaoException {
		UserTO user=new UserTO();
		UserTO expectedUserTO=null;
		user.setUserLogin("testLogin");
		user.setPassword("testPassword");
		expectedUserTO=userDao.login(user);
		System.out.println(expectedUserTO.toString());
		assertNotNull(expectedUserTO);
	}*/
	
	@Test
	public void addUserTest() throws DaoException{
		UserTO user=new UserTO();
		//user.setUserId(1L);
		user.setUserLogin("login");
		user.setUserName("name");
		user.setPassword("password");
		userDao.addUser(user);
		
		
	}

}
