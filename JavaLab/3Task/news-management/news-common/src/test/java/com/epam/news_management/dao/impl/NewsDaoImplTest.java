package com.epam.news_management.dao.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.unitils.UnitilsJUnit4;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;


import com.epam.news_management.dao.INewsDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.Author;
import com.epam.news_management.entity.News;
import com.epam.news_management.entity.SearchCriteria;
import com.epam.news_management.entity.Tag;

@ActiveProfiles("eclipselink")
@RunWith(SpringJUnit4ClassRunner.class)
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
    DirtiesContextTestExecutionListener.class,
    })
@ContextConfiguration(locations = {"classpath:NewsManagementSpringContexTest.xml"})
@DataSet(value="NewsManagementDataSet.xml",loadStrategy = CleanInsertLoadStrategy.class)
public class NewsDaoImplTest extends UnitilsJUnit4 {
	
	 
	@Autowired
	private INewsDao newsDao;



	@Test
	public void addTest() throws DaoException  {
		News news = new News();
		
		News resultnews=null;
		Author author = new Author();
		List<Author> authorsList=new ArrayList<Author>();
		
		authorsList.add(author);
		author.setAuthorId(1L);
		author.setAuthorName("testName1");
		
		List<Tag> tagsList=new ArrayList<Tag>();
		Tag tag =new Tag();
		tag.setTagId(2L);
		tag.setTagName("testName2");
		tagsList.add(tag);
		
		Long generetedId = null;
		String expectedTitle = "testTitle1";
		String expectedShortText="testShortText1";
		String expectedFullText = "testFullText1";
		String resultTitle = null;
		String resultShortText=null;
		String resultFullText = null;
		news.setTitle("testTitle1");
		news.setShortText("testShortText1");
		news.setFullText("testFullText1");	
		news.setCreationDate( new Date());
		news.setModificationDate( new Date());
		news.setAuthorsList(authorsList);
		news.setTagsList(tagsList);
		
		generetedId = newsDao.add(news);
		
		news.setNewsId(generetedId);		
		
		resultnews=newsDao.get(generetedId);	
		
		resultTitle=resultnews.getTitle();
		resultShortText=resultnews.getShortText();
		resultFullText=resultnews.getFullText();
		
		assertEquals(expectedTitle, resultTitle);
		assertEquals(expectedShortText, resultShortText);
		assertEquals(expectedFullText, resultFullText);

	}
	
//	@Test
	public void getAllTest() throws DaoException{
		List<News> resultNewsList = new ArrayList<News>();
		resultNewsList=newsDao.getAll();
		assertNotNull(resultNewsList);
		
	}

//	@Test
	
	public void getTest() throws DaoException {
		
		News resultnews = null;
		String expectedTitle = "testTitle2";
		String expectedShortText="testShortText2";
		String expectedFullText = "testFullText2";
		String resultTitle = null;
		String resultShortText=null;
		String resultFullText = null;
		resultnews=newsDao.get(2L);

		resultTitle=resultnews.getTitle();
		resultShortText=resultnews.getShortText();
		resultFullText=resultnews.getFullText();
		assertEquals(expectedTitle, resultTitle);
		assertEquals(expectedShortText, resultShortText);
		assertEquals(expectedFullText, resultFullText);
		
		System.out.println(resultnews.toString());

	}
	
	
//	@Test
	public void editTest() throws DaoException{
		News news = new News();
		News resultnews=null;
		
		String expectedTitle = "edit1";
		String expectedShortText="edit1";
		String expectedFullText = "edit1";
		String resultTitle = null;
		String resultShortText=null;
		String resultFullText = null;
		
		news.setTitle("edit1");
		news.setShortText("edit1");
		news.setFullText("edit1");
		news.setNewsId(1L);
		news.setCreationDate(new Date());
		news.setModificationDate(new Date());
	
		newsDao.edit(news);
		
		resultnews=newsDao.get(2L);
		
		resultTitle=resultnews.getTitle();
		resultShortText=resultnews.getShortText();
		resultFullText=resultnews.getFullText();
		
		assertEquals(expectedTitle, resultTitle);
		assertEquals(expectedShortText, resultShortText);
		assertEquals(expectedFullText, resultFullText);

		
			
	}
//	@Test
	public void deleteTest() throws DaoException{
		Long testId = 3L;
		News news = null;		
		newsDao.delete(testId);
		news=newsDao.get(testId);
		assertNull(news);
		
		
	}
//	@Test
	public void countNewsInSearchTest() throws DaoException{
		SearchCriteria searchCriteria = new SearchCriteria();
		Long auIdLong = 1L;
		
		List<Long> tagList = new ArrayList<Long>();
		tagList.add(1L);
		searchCriteria.setAuthorId(auIdLong);
		searchCriteria.setTagIdList(tagList);

		int result = 0;
		result=newsDao.countNewsInSearch(searchCriteria);
		assertNotNull(Integer.valueOf(result));
		
		
	}
	
//	@Test
	public void searchTest() throws DaoException{
		SearchCriteria searchCriteria = new SearchCriteria();		
		List<News> result = null;
		Long auIdLong = 3L;
		
		List<Long> tagList = new ArrayList<Long>();
		tagList.add(3L);
		searchCriteria.setAuthorId(null);
		searchCriteria.setTagIdList(null);
		result=newsDao.search(searchCriteria, 1, 3);
		System.out.println("search"+result.toString());
		assertNotNull(result);
		
	}
	
	@Test
	public  void getNextIdTest() throws DaoException{
		SearchCriteria searchCriteria = new SearchCriteria();		
		List<News> result = null;
		Long auIdLong = 3L;
		
		List<Long> tagList = new ArrayList<Long>();
		tagList.add(3L);
		searchCriteria.setAuthorId(null);
		searchCriteria.setTagIdList(null);
		
		newsDao.getNextNewsId(searchCriteria, 2L);
		
	}

	


}
