package com.epam.news_management.dao.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;






import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;




import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;
import org.unitils.spring.annotation.SpringApplicationContext;
import org.unitils.spring.annotation.SpringBean;

import com.epam.news_management.dao.ITagDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.Tag;
@ActiveProfiles("hibernate")
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:NewsManagementSpringContexTest.xml"})
@DataSet(value="NewsManagementDataSet.xml",loadStrategy = CleanInsertLoadStrategy.class)

public class TagDaoImplTest extends UnitilsJUnit4{


	@Autowired
	private ITagDao tagDao;


	@Before
	public void setUp() {

		assertNotNull(tagDao);
	}


//	@Test
	public void addTest() throws DaoException {
		Tag expectedTag = new Tag();
		Long generatedId = null;
		expectedTag.setTagName("testName");
		generatedId=tagDao.add(expectedTag);
		expectedTag.setTagId(generatedId);
		assertEquals(expectedTag, tagDao.get(generatedId));
		
	}
//	@Test
	public void getAllTest() throws DaoException {
		List <Tag> tagList =null;
		tagList=tagDao.getAll();
		assertNotNull(tagList);
		
		
	}
//	@Test
	
	public void getTest() throws DaoException {
		Tag expextedTag = new Tag();
		Tag resultTag=null;
		expextedTag.setTagId(2L);
		expextedTag.setTagName("testName2");
		resultTag=tagDao.get(2L);
		assertEquals(expextedTag,resultTag);
		
		
	}
//	@Test
	public void editTest() throws DaoException {
		Tag expextedTag = new Tag();
		Tag resultTag=null;
		expextedTag.setTagId(3L);
		expextedTag.setTagName("edit");
		tagDao.edit(expextedTag);
		
		resultTag=tagDao.get(3L);
		assertEquals(expextedTag,resultTag);
		
	}
	@Test
	public void deleteTest() throws DaoException {
		Tag resultTag=null;
		//tagDao.unlinkByTagId(1L);
		tagDao.delete(1L);
		resultTag=tagDao.get(1L);
		assertNull(resultTag);
		
	}
//	@Test
	/*public void getTagsByNewsIdTest() throws DaoException {
		List<Tag> expextedTagList  = new ArrayList<Tag>();
		List<Tag> resultTagList=null;
		Tag tag = new Tag();
		tag.setTagId(2L);
		tag.setTagName("testName2");
		expextedTagList.add(tag);
		resultTagList=tagDao.getTagsByNewsId(2L);
		assertEquals(expextedTagList,resultTagList);
		
	}
*/

}
