package com.epam.news_management.service.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.news_management.dao.ICommentDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.Comment;
import com.epam.news_management.service.exception.ServiceException;
import com.epam.news_management.service.impl.jdbc.CommentServiceImpl;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:NewsManagementSpringContexTest.xml"})
public class CommentServiceImplTest {
	
	@Mock
	private ICommentDao commentDaoMock;

	
	
	@InjectMocks
	@Autowired
	private CommentServiceImpl commentAction;

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		assertNotNull(commentDaoMock);
		assertNotNull(commentAction);
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void addCommentTest() throws DaoException, ServiceException {
		Long expectedId = 1L;
		Long resultId = null;
		when(commentDaoMock.add(new Comment())).thenReturn(Long.valueOf(1L));
		resultId=commentAction.addComment(new Comment());
		verify(commentDaoMock, times(1)).add(new Comment());
		assertEquals(expectedId, resultId);
	}
	@Test
	public void deleteCommentByNewsIdTest() throws ServiceException, DaoException {
		commentAction.deleteCommentByNewsId(anyLong());
		verify(commentDaoMock, times(1)).deleteByNewsId(anyLong());
		
		
	}
	@Test
	public void deleteCommentTest() throws ServiceException, DaoException {
		commentAction.deleteComment(anyLong());
		verify(commentDaoMock, times(1)).delete(anyLong());
		
		
	}
	@Test
	public void getCommentsByNewsIdTest() throws DaoException, ServiceException {
		List<Comment> expectedCommentsList = Arrays.asList(new Comment(),new Comment(),new Comment());
		List<Comment> resultCommentsList = null;
		when(commentDaoMock.getCommentsByNewsId(anyLong())).thenReturn(Arrays.asList(new Comment(),new Comment(),new Comment()));
		resultCommentsList=commentAction.getCommentsByNewsId(anyLong());
		assertEquals(expectedCommentsList, resultCommentsList);
		
		
		
	}
	
}
