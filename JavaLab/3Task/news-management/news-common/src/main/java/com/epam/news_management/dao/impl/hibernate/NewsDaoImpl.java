package com.epam.news_management.dao.impl.hibernate;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.epam.news_management.dao.INewsDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.News;
import com.epam.news_management.entity.SearchCriteria;

@Repository
@Component
@Profile("hibernate")
@SuppressWarnings("unchecked")
public class NewsDaoImpl implements INewsDao {
	@Autowired
	private SessionFactory sessionFactory;

	NewsDaoImpl() {
	}

	@Override
	public Long add(News news) throws DaoException {
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.persist(news);
		tx.commit();
		session.close();
		return news.getNewsId();
	}

	@Override
	public List<News> getAll() throws DaoException {
		List<News> newsList = null;
		Session session = this.sessionFactory.openSession();
		newsList = session.createCriteria(News.class).list();
		session.close();

		return newsList;
	}

	@Override
	public News get(Long newsId) throws DaoException {
		News news = null;
		Session session = this.sessionFactory.openSession();

		news = (News) session.createCriteria(News.class, "news")
				.add(Restrictions.eq("newsId", newsId)).uniqueResult();

		session.close();

		return news;
	}

	@Override
	public boolean edit(News news) throws DaoException {
		boolean success = true;

		try {
			Session session = this.sessionFactory.openSession();
			Transaction tx = session.beginTransaction();

			session.update(news);

			tx.commit();
			session.close();

		} catch (org.hibernate.StaleObjectStateException e) {
			success = false;
		}

		return success;

	}

	@Override
	public void delete(Long newsId) throws DaoException {
		News news = null;
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		news = (News) session.createCriteria(News.class, "news")
				.add(Restrictions.eq("newsId", newsId)).uniqueResult();
		session.delete(news);

		tx.commit();
		session.close();

	}

	@Override
	public List<News> search(SearchCriteria searchCriteria, int start, int end)
			throws DaoException {

		List<News> newsList = null;
		Session session = this.sessionFactory.openSession();

		Criteria criteria = createSearchCriteria(searchCriteria, session,
				start, end);

		newsList = criteria.list();

		session.close();
		return newsList;
	}

	@Override
	public int countNewsInSearch(SearchCriteria searchCriteria)
			throws DaoException {
		Long totalCount = null;
		Session session = this.sessionFactory.openSession();

		Criteria criteria = createCountCriteria(searchCriteria, session);

		totalCount = (Long) criteria.setProjection(Projections.rowCount())
				.uniqueResult();

		session.close();
		return totalCount.intValue();
	}

	@Override
	public Long getNextNewsId(SearchCriteria searchCriteria, Long currentNewsId)
			throws DaoException {
		Long nextId = null;
		News news = null;
		Session session = this.sessionFactory.openSession();
		int totalNewsCount = countNewsInSearch(searchCriteria);
		news = (News) session.load(News.class, currentNewsId);

		int elementsBefore = countElemetsBefore(searchCriteria,
				news.getModificationDate());

		if (totalNewsCount - elementsBefore != 1) {

			int startIndex = (elementsBefore + 1);

			Criteria criteria = getNextNewsIdCriteriaBuilder(searchCriteria,
					startIndex, session);

			news = (News) criteria.uniqueResult();

			nextId = news.getNewsId();
			session.close();
		}

		return nextId;
	}

	@Override
	public Long getPreviousNewsId(SearchCriteria searchCriteria,
			Long currentNewsId) throws DaoException {
		Long previousId = null;
		News news = null;
		Session session = this.sessionFactory.openSession();

		news = (News) session.load(News.class, currentNewsId);

		int elementsBefore = countElemetsBefore(searchCriteria,
				news.getModificationDate());
		if (elementsBefore != 0) {

			int startIndex = elementsBefore - 1;

			Criteria criteria = getPreviousNewsIdCriteriaBuilder(
					searchCriteria, startIndex, session);

			news = (News) criteria.uniqueResult();

			previousId = news.getNewsId();
			session.close();
		}

		return previousId;
	}

	private Criteria createSearchCriteria(SearchCriteria searchCriteria,
			Session session, int start, int end) {
		Long authorId = null;
		List<Long> tagsIdList = null;
		if (searchCriteria != null) {
			authorId = searchCriteria.getAuthorId();
			tagsIdList = searchCriteria.getTagIdList();
		}
		int maxResults = end - start + 1;
		Conjunction conjunction = Restrictions.conjunction();
		Criteria criteria = session.createCriteria(News.class);

		if (authorId != null) {
			criteria.createAlias("authorsList", "author");
			conjunction.add(Restrictions.eq("author.authorId", authorId));

		}
		if (tagsIdList != null && tagsIdList.size() != 0) {
			criteria.createAlias("tagsList", "tag");
			conjunction.add(Restrictions.in("tag.tagId", tagsIdList));
		}
		criteria.add(conjunction);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		// criteria.addOrder(Property.forName("commentsCount").desc());
		criteria.addOrder(Property.forName("modificationDate").desc());
		criteria.setFirstResult(start - 1);
		criteria.setMaxResults(maxResults);

		return criteria;
	}

	private Criteria createCountCriteria(SearchCriteria searchCriteria,
			Session session) {
		Long authorId = null;
		List<Long> tagsIdList = null;
		if (searchCriteria != null) {
			authorId = searchCriteria.getAuthorId();
			tagsIdList = searchCriteria.getTagIdList();
		}
		Conjunction conjunction = Restrictions.conjunction();
		Criteria criteria = session.createCriteria(News.class);

		if (authorId != null) {
			criteria.createAlias("authorsList", "author");
			conjunction.add(Restrictions.eq("author.authorId", authorId));

		}
		if (tagsIdList != null && tagsIdList.size() != 0) {
			criteria.createAlias("tagsList", "tag");
			conjunction.add(Restrictions.in("tag.tagId", tagsIdList));
		}
		criteria.add(conjunction);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		// criteria.addOrder(Property.forName("commentsCount").desc());
		criteria.addOrder(Property.forName("modificationDate").desc());

		return criteria;
	}

	private Criteria getNextNewsIdCriteriaBuilder(
			SearchCriteria searchCriteria, int startElement, Session session) {
		Long authorId = null;
		List<Long> tagsIdList = null;
		if (searchCriteria != null) {
			authorId = searchCriteria.getAuthorId();
			tagsIdList = searchCriteria.getTagIdList();
		}

		Conjunction conjunction = Restrictions.conjunction();
		Criteria criteria = session.createCriteria(News.class);

		if (authorId != null) {
			criteria.createAlias("authorsList", "author");
			conjunction.add(Restrictions.eq("author.authorId", authorId));

		}
		if (tagsIdList != null && tagsIdList.size() != 0) {
			criteria.createAlias("tagsList", "tag");
			conjunction.add(Restrictions.in("tag.tagId", tagsIdList));
		}
		criteria.add(conjunction);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		// criteria.addOrder(Property.forName("commentsCount").desc());
		criteria.addOrder(Property.forName("modificationDate").desc());
		criteria.setFirstResult(startElement);
		criteria.setMaxResults(1);

		return criteria;

	}

	private Criteria getPreviousNewsIdCriteriaBuilder(
			SearchCriteria searchCriteria, int startIndex, Session session) {
		Long authorId = null;
		List<Long> tagsIdList = null;
		if (searchCriteria != null) {
			authorId = searchCriteria.getAuthorId();
			tagsIdList = searchCriteria.getTagIdList();
		}

		Conjunction conjunction = Restrictions.conjunction();
		Criteria criteria = session.createCriteria(News.class);

		if (authorId != null) {
			criteria.createAlias("authorsList", "author");
			conjunction.add(Restrictions.eq("author.authorId", authorId));

		}
		if (tagsIdList != null && tagsIdList.size() != 0) {
			criteria.createAlias("tagsList", "tag");
			conjunction.add(Restrictions.in("tag.tagId", tagsIdList));
		}
		criteria.add(conjunction);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.addOrder(Property.forName("modificationDate").desc());
		criteria.setFirstResult(startIndex);
		criteria.setMaxResults(1);

		return criteria;
	}

	private int countElemetsBefore(SearchCriteria searchCriteria,
			Date modificationDate) {
		Session session = this.sessionFactory.openSession();
		Long authorId = null;
		List<Long> tagsIdList = null;
		if (searchCriteria != null) {
			authorId = searchCriteria.getAuthorId();
			tagsIdList = searchCriteria.getTagIdList();
		}
		Conjunction conjunction = Restrictions.conjunction();
		Criteria criteria = session.createCriteria(News.class);

		if (authorId != null) {
			criteria.createAlias("authorsList", "author");
			conjunction.add(Restrictions.eq("author.authorId", authorId));

		}
		if (tagsIdList != null && tagsIdList.size() != 0) {
			criteria.createAlias("tagsList", "tag");
			conjunction.add(Restrictions.in("tag.tagId", tagsIdList));
		}
		criteria.add(conjunction);
		criteria.setResultTransformer(Criteria.DISTINCT_ROOT_ENTITY);
		criteria.addOrder(Property.forName("modificationDate").desc());
		criteria.add(Restrictions.gt("modificationDate", modificationDate));

		Long totalCount = null;

		totalCount = (Long) criteria.setProjection(Projections.rowCount())
				.uniqueResult();

		session.close();
		return totalCount.intValue();

	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

}
