package com.epam.news_management.service.impl.hibernate;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.epam.news_management.dao.ICommentDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.Comment;
import com.epam.news_management.service.ICommentService;
import com.epam.news_management.service.exception.ServiceException;



/**
 *CommentService implementation.
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
@Profile("hibernate")
@Component
public class CommentServiceImpl implements ICommentService {
	
	private static final Logger log = Logger.getLogger(CommentServiceImpl.class);
	
	@Qualifier("hibernate")
	private ICommentDao commentDao = null;
	
	public CommentServiceImpl(){}


	/**
	 * Adds single comment transfer object.
	 * 
	 * @param comment
	 * @return new comment ID
	 * @throws ServiceException
	 */
	@Override
	public Long addComment(Comment comment) throws ServiceException {
		
		try {
			return commentDao.add(comment);
		} catch (DaoException e) {
			log.error("Exception while adding comment.", e);
			throw new ServiceException("Exception while adding comment.", e);
		}
	}


	/**
	 * Deletes comment entity from data repository according to newsId.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void deleteCommentByNewsId(Long newsId) throws ServiceException {
		throw new UnsupportedOperationException();
	}
	/**
	 * Deletes comment entity from data repository.
	 * 
	 * @param commentId
	 * @throws ServiceException
	 */
	@Override
	public void deleteComment(Long commentId) throws ServiceException {
		try {
			commentDao.delete(commentId);
		} catch (DaoException e) {
			log.error("Exception while deleting comment.", e);
			throw new ServiceException("Exception while deleting comment.", e);
		}
		
	}
	

	
	/**
	 * Gets list of comments  related to news ID.
	 * 
	 * @param newsId
	 * @return list of comments which relate to news ID.
	 * @throws ServiceException
	 */
	@Override
	public List<Comment> getCommentsByNewsId(Long newsId)	throws ServiceException {
		throw new UnsupportedOperationException();
	}
	
	public ICommentDao getCommentDao() {
		return commentDao;
	}

	public void setCommentDao(ICommentDao commentDao) {
		this.commentDao = commentDao;
	}

	


	

	

}
