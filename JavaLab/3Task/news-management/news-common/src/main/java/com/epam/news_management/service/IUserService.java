package com.epam.news_management.service;

import com.epam.news_management.entity.UserTO;
import com.epam.news_management.service.exception.ServiceException;


/**
 * User service interface.
 * 
 * Contains methods for operating user entity.
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public interface IUserService {
	
	/**
	 * Gets user entity from data repository is it exists.
	 * 
	 * @param user
	 * @return
	 * @throws ServiceException
	 */
	public UserTO login (UserTO user) throws ServiceException;
	
	public Long addUser(UserTO user)throws ServiceException;

}
