package com.epam.news_management.dao.impl.hibernate;

import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.epam.news_management.dao.IAuthorDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.Author;
import com.epam.news_management.entity.Tag;
@Repository
@Component
@Profile("hibernate")
@SuppressWarnings("unused")
public class AuthorDaoImpl implements IAuthorDao {
	
	private static final String SQL_GET_AUTHOR_BY_NEWS_ID_JOIN = " FROM authors a JOIN news_authors ON (nwau_author_id_fk = a.au_author_id_pk) WHERE (nwau_news_id_fk = :newsId )  ";
	
	
	@Autowired	
    private SessionFactory sessionFactory;
	
	
	AuthorDaoImpl(){}
	
	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}



	@Override
	public Long add(Author author) throws DaoException {
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.persist(author);
		tx.commit();
		session.close();
		return author.getAuthorId();
		
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<Author> getAll() throws DaoException {
		List<Author> authorsList = null;
		Session session = this.sessionFactory.openSession();
		authorsList = session.createCriteria(Author.class).list();
		session.close();
		return authorsList;
	}

	@Override
	public Author get(Long authorId) throws DaoException {
		Author author = null;
		Session session = this.sessionFactory.openSession();
		
		Criteria criteria = session.createCriteria(Author.class).add(
				Restrictions.eq("authorId", authorId));
		
		author = (Author) criteria.uniqueResult();
		session.close();
		return author;
	}

	@Override
	public boolean edit(Author author) throws DaoException {
		boolean success = true;

		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		session.update(author);
		
		tx.commit();
		session.close();
		return success;
		
		
	}

	@Override
	public void delete(Long authorId) throws DaoException {

		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		
		Author author = (Author)session.createCriteria(Author.class).add(Restrictions.eq("authorId", authorId)).uniqueResult();
		author.setExpired(new Date());		
		
		
		tx.commit();
		session.close();
		
	}

	@Override
	public void link(Long authorId, Long newsId) throws DaoException {
		throw new UnsupportedOperationException();		
	}

	@Override
	public void unlinkByNewsId(Long newsId) throws DaoException {
		throw new UnsupportedOperationException();		
	}

	@Override
	public void unlinkByAuthorId(Long authorId) throws DaoException {
		throw new UnsupportedOperationException();		
	}

	@Override
	public Author getAuthorByNewsId(Long newsId) throws DaoException {
		throw new UnsupportedOperationException();
	}

}
