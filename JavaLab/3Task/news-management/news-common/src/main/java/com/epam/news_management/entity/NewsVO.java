package com.epam.news_management.entity;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Author entity class.
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class NewsVO {
	
	private static final long serialVersionUID = -304637856394596079L;
	@Valid
	private News newsItem;
	
	private Author authorItem;
	
	private List<Comment> comment;
	
	private List<Tag> tagItemList;
	@NotEmpty(message="{local.error.message.emptyTags}")
	private List<Long>tagsIdList ;
	
	
	@Min(value = 1, message="{local.error.message.emptyAuthor}" )
	private Long authorId;
	
	

	public News getNewsItem() {
		return newsItem;
	}

	public void setNewsItem(News newsItem) {
		this.newsItem = newsItem;
	}

	public Author getAuthorItem() {
		return authorItem;
	}

	public void setAuthorItem(Author authorItem) {
		this.authorItem = authorItem;
	}

	public List<Comment> getComment() {
		return comment;
	}

	public void setComment(List<Comment> comment) {
		this.comment = comment;
	}

	public List<Tag> getTagItemList() {
		return tagItemList;
	}

	public void setTagItemList(List<Tag> tagItemList) {
		this.tagItemList = tagItemList;
	}

	public List<Long> getTagsIdList() {
		return tagsIdList;
	}

	public void setTagsIdList(List<Long> tagsIdList) {
		this.tagsIdList = tagsIdList;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}
	
	public News convertToNews(NewsVO newsVO){
		News news = new News();
		List<Author> authorsList= new ArrayList<Author>();
		List<Tag> tagsList= new ArrayList<Tag>();
		Author author = new Author();
		author.setAuthorId(newsVO.getAuthorId());
		author.setAuthorName(newsVO.getAuthorItem().getAuthorName());
		authorsList.add(author);
		for(Long i: newsVO.getTagsIdList()){
			Tag tag = new Tag();
			tag.setTagId(i);
			tagsList.add(tag);
			
		}
		List<Comment> commentsList = new ArrayList<Comment>();
		
		for(Comment c:newsVO.getComment()){
			commentsList.add(c);
		}
		
		news.setCommentList(commentsList);
		news.setAuthorsList(authorsList);
		news.setTagsList(tagsList);
		news.setNewsId(newsVO.getNewsItem().getNewsId());
		news.setTitle(newsVO.getNewsItem().getTitle());
		news.setShortText(newsVO.getNewsItem().getShortText());
		news.setFullText(newsVO.getNewsItem().getFullText());
		news.setCreationDate(newsVO.getNewsItem().getCreationDate());
		news.setModificationDate(newsVO.getNewsItem().getModificationDate());
		
		return news;		
		
		}
	public NewsVO convertToNewsVO(News news){
		System.out.println(news.toString());
		NewsVO newsVO = new NewsVO();
		newsVO.setNewsItem(news);
		Author author = new Author();
		if(news.getAuthorsList()!=null){
			for(Author a: news.getAuthorsList()){
				author.setAuthorId(a.getAuthorId());
				author.setAuthorName(a.getAuthorName());
				author.setExpired(a.getExpired());
			}
		}
		
		List<Tag> tagItemList = new ArrayList<Tag>();
		List<Long> tagsIdList = new ArrayList<Long>();
		if(news.getTagsList()!=null){
			for(Tag t: news.getTagsList()){
				tagItemList.add(t);
				tagsIdList.add(t.getTagId());
			}
		}
		
		newsVO.setTagItemList(tagItemList);
		newsVO.setTagsIdList(tagsIdList);
		List<Comment> commentsList = new ArrayList<Comment>();
		if(news.getCommentList()!=null){
			for(Comment c:news.getCommentList()){
				commentsList.add(c);
			}
		}
		
		newsVO.setComment(commentsList);
		
		return newsVO;
		
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result
				+ ((authorItem == null) ? 0 : authorItem.hashCode());
		result = prime * result + ((comment == null) ? 0 : comment.hashCode());
		result = prime * result
				+ ((newsItem == null) ? 0 : newsItem.hashCode());
		result = prime * result
				+ ((tagItemList == null) ? 0 : tagItemList.hashCode());
		result = prime * result
				+ ((tagsIdList == null) ? 0 : tagsIdList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsVO other = (NewsVO) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (authorItem == null) {
			if (other.authorItem != null)
				return false;
		} else if (!authorItem.equals(other.authorItem))
			return false;
		if (comment == null) {
			if (other.comment != null)
				return false;
		} else if (!comment.equals(other.comment))
			return false;
		if (newsItem == null) {
			if (other.newsItem != null)
				return false;
		} else if (!newsItem.equals(other.newsItem))
			return false;
		if (tagItemList == null) {
			if (other.tagItemList != null)
				return false;
		} else if (!tagItemList.equals(other.tagItemList))
			return false;
		if (tagsIdList == null) {
			if (other.tagsIdList != null)
				return false;
		} else if (!tagsIdList.equals(other.tagsIdList))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NewsVO [newsItem=" + newsItem + ", authorItem=" + authorItem
				+ ", comment=" + comment + ", tagItemList=" + tagItemList
				+ ", tagsIdList=" + tagsIdList + ", AuthorId=" + authorId + "]";
	}

	
	

}
