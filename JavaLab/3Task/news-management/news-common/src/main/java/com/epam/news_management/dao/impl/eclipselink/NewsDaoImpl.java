package com.epam.news_management.dao.impl.eclipselink;

import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.OptimisticLockException;
import javax.persistence.PersistenceUnit;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.sql.DataSource;

import org.eclipse.persistence.expressions.ExpressionBuilder;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Conjunction;
import org.hibernate.criterion.Property;
import org.hibernate.criterion.Restrictions;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.epam.news_management.dao.INewsDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.Author;
import com.epam.news_management.entity.Comment;
import com.epam.news_management.entity.News;
import com.epam.news_management.entity.SearchCriteria;
import com.epam.news_management.entity.Tag;

@Repository
@Component
@Profile("eclipselink")
@SuppressWarnings({ "unchecked", "rawtypes", "unused" })
public class NewsDaoImpl implements INewsDao {

	@PersistenceUnit(unitName = "entityManagerFactory")
	private EntityManagerFactory factory;

	NewsDaoImpl() {
	}

	@Override
	public Long add(News news) throws DaoException {

		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		news.setModificationDate(news.getCreationDate());
		entityManager.persist(news);

		entityTransaction.commit();

		return news.getNewsId();
	}

	@Override
	public List<News> getAll() throws DaoException {
		List<News> newsList = null;
		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		newsList = (List<News>) entityManager.createQuery("from NEWS n",
				News.class).getResultList();

		entityTransaction.commit();

		return newsList;
	}

	@Override
	public News get(Long newsId) throws DaoException {
		News news = null;
		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		news = entityManager.find(News.class, newsId);

		entityTransaction.commit();

		return news;
	}

	@Override
	public boolean edit(News news) throws DaoException {
		boolean success = true;

		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		try {
			entityTransaction.begin();

			news.setModificationDate(new Date());

			entityManager.merge(news);

			entityTransaction.commit();
		} catch (javax.persistence.OptimisticLockException e) {
			success = false;
		}

		return success;

	}

	@Override
	public void delete(Long newsId) throws DaoException {

		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		News news = entityManager.find(News.class, newsId);
		entityManager.remove(news);

		entityTransaction.commit();

	}

	@Override
	public List<News> search(SearchCriteria searchCriteria, int start, int end)
			throws DaoException {

		List<News> newsList = null;

		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		Query query = createSearchCriteria(searchCriteria, entityManager,
				start, end);
		newsList = query.getResultList();

		entityTransaction.commit();

		return newsList;
	}

	@Override
	public int countNewsInSearch(SearchCriteria searchCriteria)
			throws DaoException {
		Long totalCount = null;

		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		Query query = createCountCriteria(searchCriteria, entityManager);
		totalCount = (Long) query.getSingleResult();
		entityTransaction.commit();

		return totalCount.intValue();
	}

	@Override
	public Long getNextNewsId(SearchCriteria searchCriteria, Long currentNewsId)
			throws DaoException {
		Long nextId = null;
		News news = null;

		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		int totalNewsCount = countNewsInSearch(searchCriteria);
		news = (News) entityManager.find(News.class, currentNewsId);

		int elementsBefore = countElemetsBefore(searchCriteria,
				news.getModificationDate(), entityManager);

		if (totalNewsCount - elementsBefore != 1) {

			int startIndex = (elementsBefore + 1);

			Query query = getNextNewsIdCriteriaBuilder(searchCriteria,
					entityManager, startIndex);
			news = (News) query.getSingleResult();
			nextId = news.getNewsId();

		}

		entityTransaction.commit();

		return nextId;
	}

	@Override
	public Long getPreviousNewsId(SearchCriteria searchCriteria,
			Long currentNewsId) throws DaoException {
		Long previousId = null;
		News news = null;

		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		news = (News) entityManager.find(News.class, currentNewsId);

		int elementsBefore = countElemetsBefore(searchCriteria,
				news.getModificationDate(), entityManager);
		if (elementsBefore != 0) {
			int startIndex = elementsBefore - 1;
			Query query = getPreviousNewsIdCriteriaBuilder(searchCriteria,
					entityManager, startIndex);
			news = (News) query.getSingleResult();
			previousId = news.getNewsId();
		}

		entityTransaction.commit();

		return previousId;
	}

	private Query createSearchCriteria(SearchCriteria searchCriteria,
			EntityManager entityManager, int start, int end) {
		Query query = null;
		Long authorId = null;
		List<Long> tagsIdList = null;

		if (searchCriteria != null) {
			authorId = searchCriteria.getAuthorId();
			tagsIdList = searchCriteria.getTagIdList();
		}
		int maxResults = end - start + 1;

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		Predicate predicate = criteriaBuilder.conjunction();
		CriteriaQuery<News> criteriaQuery = criteriaBuilder
				.createQuery(News.class);
		Root<News> root = criteriaQuery.from(News.class);
		criteriaQuery.distinct(true);

		if (authorId != null) {
			Join join = root.join("authorsList", JoinType.LEFT);
			predicate = criteriaBuilder.and(predicate,
					criteriaBuilder.equal(join.get("authorId"),	authorId));
		}

		if (tagsIdList != null && tagsIdList.size() != 0) {
			Join join = root.join("tagsList", JoinType.LEFT);
			predicate = criteriaBuilder.and(predicate,
					join.get("tagId").in(tagsIdList));
			
		}
		criteriaQuery.where(predicate);
		criteriaQuery
				.orderBy(criteriaBuilder.desc(root.get("modificationDate")));

		query = entityManager.createQuery(criteriaQuery);
		query.setFirstResult(start - 1);
		query.setMaxResults(maxResults);
		return query;
	}

	private Query createCountCriteria(SearchCriteria searchCriteria,
			EntityManager entityManager) {
		Query query = null;
		Long authorId = null;
		List<Long> tagsIdList = null;
		if (searchCriteria != null) {
			authorId = searchCriteria.getAuthorId();
			tagsIdList = searchCriteria.getTagIdList();
		}
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		Predicate predicate = criteriaBuilder.conjunction();
		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
		criteriaQuery.distinct(true);
		Root root = criteriaQuery.from(News.class);
		criteriaQuery.distinct(true);

		if (authorId != null) {
			Join join = root.join("authorsList", JoinType.LEFT);
			predicate = criteriaBuilder.and(
					predicate,
					criteriaBuilder.equal(join.get("authorId"),
							searchCriteria.getAuthorId()));
		}

		if (tagsIdList != null && tagsIdList.size() != 0) {
			Join join = root.join("tagsList", JoinType.LEFT);
			predicate = criteriaBuilder.and(predicate,
					join.get("tagId").in(tagsIdList));
			criteriaQuery.where(predicate);
		}

		criteriaQuery.select(criteriaBuilder.countDistinct(root.get("newsId")));
		query = entityManager.createQuery(criteriaQuery);

		return query;
	}

	private Query getNextNewsIdCriteriaBuilder(SearchCriteria searchCriteria,
			EntityManager entityManager, int start) {
		Query query = null;
		Long authorId = null;
		List<Long> tagsIdList = null;
		if (searchCriteria != null) {
			authorId = searchCriteria.getAuthorId();
			tagsIdList = searchCriteria.getTagIdList();
		}

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<News> criteriaQuery = criteriaBuilder
				.createQuery(News.class);
		Predicate predicate = criteriaBuilder.conjunction();
		Root<News> root = criteriaQuery.from(News.class);
		criteriaQuery.distinct(true);

		if (authorId != null) {
			Join join = root.join("authorsList", JoinType.LEFT);
			predicate = criteriaBuilder.and(
					predicate,
					criteriaBuilder.equal(join.get("authorId"),
							searchCriteria.getAuthorId()));
		}

		if (tagsIdList != null && tagsIdList.size() != 0) {
			Join join = root.join("tagsList", JoinType.LEFT);
			predicate = criteriaBuilder.and(predicate,
					join.get("tagId").in(tagsIdList));
			criteriaQuery.where(predicate);
		}

		criteriaQuery
				.orderBy(criteriaBuilder.desc(root.get("modificationDate")));

		query = entityManager.createQuery(criteriaQuery);
		query.setFirstResult(start);
		query.setMaxResults(1);

		return query;

	}

	private Query getPreviousNewsIdCriteriaBuilder(
			SearchCriteria searchCriteria, EntityManager entityManager,
			int startIndex) {
		Query query = null;
		Long authorId = null;
		List<Long> tagsIdList = null;
		if (searchCriteria != null) {
			authorId = searchCriteria.getAuthorId();
			tagsIdList = searchCriteria.getTagIdList();
		}

		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<News> criteriaQuery = criteriaBuilder
				.createQuery(News.class);
		Predicate predicate = criteriaBuilder.conjunction();
		Root<News> root = criteriaQuery.from(News.class);
		criteriaQuery.distinct(true);

		if (authorId != null) {
			Join join = root.join("authorsList", JoinType.LEFT);
			predicate = criteriaBuilder.and(
					predicate,
					criteriaBuilder.equal(join.get("authorId"),
							searchCriteria.getAuthorId()));
		}

		if (tagsIdList != null && tagsIdList.size() != 0) {
			Join join = root.join("tagsList", JoinType.LEFT);
			predicate = criteriaBuilder.and(predicate,
					join.get("tagId").in(tagsIdList));
			criteriaQuery.where(predicate);
		}

		criteriaQuery
				.orderBy(criteriaBuilder.desc(root.get("modificationDate")));

		query = entityManager.createQuery(criteriaQuery);
		query.setFirstResult(startIndex);
		query.setMaxResults(1);

		return query;
	}

	private int countElemetsBefore(SearchCriteria searchCriteria,
			Date modificationDate, EntityManager entityManager) {
		Long elementsBefore = null;
		Query query = null;
		Long authorId = null;
		List<Long> tagsIdList = null;
		if (searchCriteria != null) {
			authorId = searchCriteria.getAuthorId();
			tagsIdList = searchCriteria.getTagIdList();
		}
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();

		CriteriaQuery criteriaQuery = criteriaBuilder.createQuery();
		Predicate predicate = criteriaBuilder.conjunction();
		Root root = criteriaQuery.from(News.class);
		criteriaQuery.distinct(true);

		if (authorId != null) {
			Join join = root.join("authorsList", JoinType.LEFT);
			predicate = criteriaBuilder.and(
					predicate,
					criteriaBuilder.equal(join.get("authorId"),
							searchCriteria.getAuthorId()));
		}

		if (tagsIdList != null && tagsIdList.size() != 0) {
			Join join = root.join("tagsList", JoinType.LEFT);
			predicate = criteriaBuilder.and(predicate,
					join.get("tagId").in(tagsIdList));
			criteriaQuery.where(predicate);
		}

		predicate = criteriaBuilder.and(predicate, criteriaBuilder.greaterThan(
				root.get("modificationDate"), modificationDate));

		criteriaQuery.where(predicate);

		criteriaQuery.select(criteriaBuilder.countDistinct(root.get("newsId")));

		query = entityManager.createQuery(criteriaQuery);
		elementsBefore = (Long) query.getSingleResult();
		return elementsBefore.intValue();

	}

	private List<Long> createNewsIdList(Query query) {
		List<Long> newsIdList = new ArrayList<Long>();

		List<Object[]> newsParameters = query.getResultList();
		Iterator<Object[]> iterator = newsParameters.iterator();
		while (iterator.hasNext()) {
			newsIdList.add(((News) iterator.next()[0]).getNewsId());
		}
		return newsIdList;
	}

}
