package com.epam.news_management.service;

import java.util.List;

import com.epam.news_management.entity.Author;
import com.epam.news_management.service.exception.ServiceException;



/**
 * AuthorService interface.
 * 
 * Contains methods for operating author entity.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public interface IAuthorService {
	
	/**
	 * Adds single author transfer object.
	 * 
	 * @param author
	 * @return new author ID.
	 * @throws ServiceException
	 */
	public Long addAuthor(Author author) throws ServiceException;
	
	/**
	 * Makes author expired.
	 * 
	 * @param authorId
	 * @throws ServiceException
	 */
	public void deleteAuthor(Long authorId) throws ServiceException;
	
	/**
	 * Gets certain author according to news ID.
	 * 
	 * @param newsId
	 * @return author transfer object.
	 * @throws ServiceException
	 */
	public Author getAuthorByNewsId (Long newsId) throws ServiceException;
	
	/**
	 * Destroys links between author table and news table, using ID of news.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void unlinkAutorWhithNewsByNewsId(Long newsId)throws ServiceException;
	
	/**
	 * Destroys links between author table and news table, using ID of author.
	 * 
	 * @param authorId
	 * @throws ServiceException
	 */
	public void unlinkAutorWhithNewsByAuthorId(Long authorId)throws ServiceException;
	
	/**
	 * Edit author entity in data repository.
	 * 
	 * @param author
	 * @throws ServiceException
	 */
	public void editAuthor(Author author) throws ServiceException;
	
	/**
	 * Links author with news according to their ID's.
	 * 
	 * @param author
	 * @param newsId
	 * @throws ServiceException
	 */
	public void linkAuthorWhithNews(Long authorId, Long newsId) throws ServiceException;
	
	/**
	 * Gets all authors from data repository.
	 * 
	 * @return
	 * @throws ServiceException 
	 */
	public List<Author> getAllAuthors() throws ServiceException;
	
	

}
