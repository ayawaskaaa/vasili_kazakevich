package com.epam.news_management.dao;

import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.Author;


/**
 * 
 * @author Vasili_Kazakevich
 * @version 1.0
 */
public interface IAuthorDao extends IBasicDao<Author> {

	/**
	 * 
	 * @param author
	 * @param newsId
	 * @throws DaoException
	 */
	public void link(Long authorId, Long newsId) throws DaoException;
	/**
	 * 
	 * @param newsId
	 * @throws DaoException
	 */
	public void unlinkByNewsId(Long newsId) throws DaoException;
	/**
	 * 
	 * @param authorId
	 * @throws DaoException
	 */
	public void unlinkByAuthorId(Long authorId) throws DaoException;
	/**
	 * 
	 * @param newsId
	 * @return
	 * @throws DaoException
	 */
	public Author getAuthorByNewsId(Long newsId)throws DaoException;
	
	
	
	
}
