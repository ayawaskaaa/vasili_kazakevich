package com.epam.news_management.dao.impl.eclipselink;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.epam.news_management.dao.IAuthorDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.Author;

@Repository
@Component
@Profile("eclipselink")
public class AuthorDaoImpl implements IAuthorDao {
	
	@PersistenceUnit(unitName = "entityManagerFactory")
	private EntityManagerFactory factory;
	
	AuthorDaoImpl(){}

	@Override
	public Long add(Author author) throws DaoException {
		
		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		entityManager.persist(author);

		entityTransaction.commit();
		return author.getAuthorId();
		
	}

	
	@Override
	public List<Author> getAll() throws DaoException {
		List<Author> authorsList = null;
		
		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		authorsList = (List<Author>) entityManager.createQuery("from AUTHORS a",
				Author.class).getResultList();
		entityTransaction.commit();
		
		return authorsList;
	}

	@Override
	public Author get(Long authorId) throws DaoException {
		Author author = null;
		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		author = entityManager.find(Author.class, authorId);
		entityTransaction.commit();
		return author;
	}

	@Override
	public boolean edit(Author author) throws DaoException {
		boolean success = true;

		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		entityManager.merge(author);

		entityTransaction.commit();
		return success;
		
		
	}

	@Override
	public void delete(Long authorId) throws DaoException {		
		
		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		Author author = entityManager.find(Author.class, authorId);
		author.setExpired(new Date());
		entityManager.merge(author);

		entityTransaction.commit();
		
	}

	@Override
	public void link(Long authorId, Long newsId) throws DaoException {
		throw new UnsupportedOperationException();		
	}

	@Override
	public void unlinkByNewsId(Long newsId) throws DaoException {
		throw new UnsupportedOperationException();		
	}

	@Override
	public void unlinkByAuthorId(Long authorId) throws DaoException {
		throw new UnsupportedOperationException();		
	}

	@Override
	public Author getAuthorByNewsId(Long newsId) throws DaoException {
		throw new UnsupportedOperationException();
	}
	
	public EntityManagerFactory getFactory() {
		return factory;
	}

	public void setFactory(EntityManagerFactory factory) {
		this.factory = factory;
	}

}
