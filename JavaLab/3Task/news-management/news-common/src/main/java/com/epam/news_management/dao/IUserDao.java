package com.epam.news_management.dao;


import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.UserTO;

public interface IUserDao {
	
	public UserTO login (UserTO user) throws DaoException;
	
	public Long addUser(UserTO user) throws DaoException;
	
}
