package com.epam.news_management.dao;

import java.util.List;

import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.News;
import com.epam.news_management.entity.NewsVO;
import com.epam.news_management.entity.SearchCriteria;


/**
 * 
 * @author Vasili_Kazakevich
 * @version 1.0
 */
public interface INewsDao extends IBasicDao<News> {
	
	/**
	 * 
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return
	 * @throws DaoException
	 */
	public List<News> search(SearchCriteria searchCriteria, int start, int end) throws DaoException;
	/**
	 * 
	 * @param searchCriteria
	 * @return
	 * @throws DaoException
	 */
	public int countNewsInSearch(SearchCriteria searchCriteria) throws DaoException;
	
	/**
	 * Gets the next news in search.
	 * 
	 * @param searchCriteria
	 * @param currentNewsId
	 * @return
	 * @throws DaoException
	 */
	public Long getNextNewsId(SearchCriteria searchCriteria, Long currentNewsId) throws DaoException;
	
	/**
	 * Gets the previous news in search.
	 * 
	 * @param searchCriteria
	 * @param currentNewsId
	 * @return
	 * @throws DaoException
	 */
	public Long getPreviousNewsId (SearchCriteria searchCriteria, Long currentNewsId) throws DaoException;

}
