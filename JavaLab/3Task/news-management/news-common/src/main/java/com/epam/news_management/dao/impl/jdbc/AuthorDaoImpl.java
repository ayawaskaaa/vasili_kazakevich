package com.epam.news_management.dao.impl.jdbc;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.news_management.dao.IAuthorDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.dao.util.DAOUtil;
import com.epam.news_management.entity.Author;

public class AuthorDaoImpl implements IAuthorDao {

	private static final String SQL_GET_AUTOR_SELECT = "SELECT au_author_id_pk, au_author_name, au_expired FROM authors";

	private static final String SQL_ADD_AUTHOR_INSERT = "INSERT INTO authors (au_author_id_pk, "
			+ " au_author_name, au_expired  ) VALUES (au_author_id_pk_seq.nextval, ?, ?) ";

	private static final String SQL_MAKE_AUTHOR_EXPIRED_INSERT = "UPDATE  authors SET au_expired = CURRENT_TIMESTAMP WHERE "
			+ " au_author_id_pk = ? ";

	private static final String SQL_EDIT_AUTHOR_UPDATE = "UPDATE authors SET au_author_name = ? WHERE au_author_id_pk = ? ";

	private static final String SQL_LINK_AUTHOR_WHITH_NEWS_INSERT = "INSERT INTO news_authors "
			+ " (nwau_news_id_fk, nwau_author_id_fk) VALUES (?, ?)  ";

	private static final String SQL_UNLINK_AUTHOR_WHITH_NEWS_DELETE = "DELETE FROM news_authors WHERE nwau_news_id_fk = ? ";

	private static final String SQL_GET_AUTHOR_BY_NEWS_ID_SELECT = "SELECT a.au_author_id_pk, a.au_author_name, a.au_expired "
			+ " FROM authors a JOIN news_authors ON nwau_author_id_fk = a.au_author_id_pk WHERE nwau_news_id_fk = ? ";

	private static final String SQL_GET_AUTHOR_BY_AUTHOR_ID_SELECT = "SELECT au_author_id_pk, au_author_name, au_expired "
			+ " FROM authors WHERE au_author_id_pk = ? ";

	private static final String SQL_UNLINK_AUTHOR_WHITH_NEWS_BY_AUTHOR_ID_DELETE = "DELETE FROM news_authors WHERE nwau_author_id_fk = ? ";

	public AuthorDaoImpl() {
	}

	private DataSource dataSource = null;

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public AuthorDaoImpl(DataSource ds) {

		this.dataSource = ds;

	}

	@Override
	public Long add(Author author) throws DaoException {
		Long authorID = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String column[] = { "au_author_id_pk" };
		try {
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_ADD_AUTHOR_INSERT, column);

			ps.setString(1, author.getAuthorName());
			ps.setDate(2, (Date) author.getExpired());
			ps.executeUpdate();

			rs = ps.getGeneratedKeys();
			if (rs != null && rs.next()) {
				authorID = rs.getLong(1);
			}

		} catch (SQLException e) {
			throw new DaoException("Exception while adding new author.", e);
		} finally {
			DAOUtil.closeConnection(con, ps, rs, dataSource);
		}
		return authorID;
	}

	@Override
	public List<Author> getAll() throws DaoException {
		List<Author> authorList = new ArrayList<Author>();
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;

		try {
			con = DataSourceUtils.doGetConnection(dataSource);
			st = con.createStatement();
			rs = st.executeQuery(SQL_GET_AUTOR_SELECT);
			if (rs != null) {
				while (rs.next()) {

					authorList.add(buildAutor(rs));

				}

			}

		} catch (SQLException e) {
			throw new DaoException("Exception while getting author.", e);
		} finally {
			DAOUtil.closeConnection(con, st, rs, dataSource);
		}

		return authorList;
	}

	@Override
	public Author get(Long authorId) throws DaoException {
		Author author = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;

		try {
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_GET_AUTHOR_BY_AUTHOR_ID_SELECT);

			ps.setLong(1, authorId);
			rs = ps.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					author = buildAutor(rs);
				}
			}

		} catch (SQLException e) {
			throw new DaoException("Exception while getting author.", e);
		} finally {
			DAOUtil.closeConnection(con, ps, rs, dataSource);
		}
		return author;
	}

	@Override
	public boolean edit(Author authorTO) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		boolean success = true;

		String authorName = authorTO.getAuthorName();
		Long authorId = authorTO.getAuthorId();

		try {
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_EDIT_AUTHOR_UPDATE);
			ps.setString(1, authorName);
			ps.setLong(2, authorId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("Exception while editing author.", e);
		} finally {
			DAOUtil.closeConnection(con, ps, dataSource);
		}
		return success;
	}

	@Override
	public void delete(Long authorId) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_MAKE_AUTHOR_EXPIRED_INSERT);
			ps.setLong(1, authorId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("Exception while making autor expired.", e);
		} finally {
			DAOUtil.closeConnection(con, ps, dataSource);
		}

	}

	@Override
	public void link(Long authorId, Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_LINK_AUTHOR_WHITH_NEWS_INSERT);
			ps.setLong(1, newsId);
			ps.setLong(2, authorId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("Exception while linking author with news.",
					e);
		} finally {
			DAOUtil.closeConnection(con, ps, dataSource);
		}

	}

	@Override
	public void unlinkByNewsId(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_UNLINK_AUTHOR_WHITH_NEWS_DELETE);
			ps.setLong(1, newsId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException(
					"Exception while unlinking author with news.", e);
		} finally {
			DAOUtil.closeConnection(con, ps, dataSource);
		}

	}

	@Override
	public void unlinkByAuthorId(Long authorId) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_UNLINK_AUTHOR_WHITH_NEWS_BY_AUTHOR_ID_DELETE);
			ps.setLong(1, authorId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException(
					"Exception while unlinking author with news.", e);
		} finally {
			DAOUtil.closeConnection(con, ps, dataSource);
		}

	}

	@Override
	public Author getAuthorByNewsId(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Author author = null;

		try {
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_GET_AUTHOR_BY_NEWS_ID_SELECT);

			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			if (rs != null) {
				while (rs.next()) {
					author = buildAutor(rs);
				}
			}

		} catch (SQLException e) {
			throw new DaoException("Exception while getting author.", e);
		} finally {
			DAOUtil.closeConnection(con, ps, rs, dataSource);
		}
		return author;
	}

	private Author buildAutor(ResultSet rs) throws SQLException {

		Author author = new Author();

		author.setAuthorId(rs.getLong(1));
		author.setAuthorName(rs.getString(2));
		author.setExpired(rs.getDate(3));
		return author;

	}

}
