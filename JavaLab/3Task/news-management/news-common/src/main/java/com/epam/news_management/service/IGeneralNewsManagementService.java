package com.epam.news_management.service;

import java.util.List;

import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.Author;
import com.epam.news_management.entity.Comment;
import com.epam.news_management.entity.News;
import com.epam.news_management.entity.NewsVO;
import com.epam.news_management.entity.SearchCriteria;
import com.epam.news_management.entity.Tag;
import com.epam.news_management.entity.UserTO;
import com.epam.news_management.service.exception.ServiceException;

;

public interface IGeneralNewsManagementService {
	
	/**
	 * Adds single news transfer object.
	 * 
	 * @param news
	 * @throws ServiceException
	 */
	public void addNews(News news) throws ServiceException;
	
	/**
	 * Produces actions for adding in to data repository.
	 * 
	 * @param news
	 * @throws ServiceException
	 */
	public Long addComplexNews(News news) throws ServiceException;
	
	/**
	 * Adds single author transfer object.
	 * 
	 * @param author
	 * @throws ServiceException
	 */
	public void addAuthor(Author author) throws ServiceException;
	
	/**
	 * Adds single comment transfer object.
	 * 
	 * @param comment
	 * @throws ServiceException
	 */
	public void addComment(Comment comment) throws ServiceException;
	
	/**
	 * Adds single tag transfer object.
	 * 
	 * @param tag
	 * @throws ServiceException
	 */
	public void addTag(Tag tag) throws ServiceException;
	
	/**
	 * Deletes news entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteNews(Long newsId) throws ServiceException;
	
	/**
	 * Deletes tag entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteTag(Long newsId) throws ServiceException;
	
	/**
	 * Deletes author entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteAuthor(Long authorId) throws ServiceException;
	
	/**
	 * Deletes comment entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteComment(Long commentId) throws ServiceException;
	
	
	/**
	 * Edits news entity in data repository.
	 * 
	 * @param newsTO
	 * @throws ServiceException
	 */
	public void editNews(News newsTO) throws ServiceException;
	
	/**
	 * Produces actions to get news value object.
	 * 
	 * @param newsId
	 * @return
	 * @throws ServiceException
	 */
	public News getSingleNews(Long newsId) throws ServiceException;
	
	/**
	 * Searches certain news according search criteria, start ans end index.
	 * 
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return list of news according search criteria, start and end index.
	 * @throws ServiceException
	 */
	public List<News> searchByAuthorAndTag(SearchCriteria searchCriteria, int start, int end ) throws ServiceException;
	
	/**
	 * Counts items in search result.
	 * 
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return
	 * @throws ServiceException
	 */
	public int countNewsInSearch(SearchCriteria searchCriteria) throws ServiceException;
	
	/**
	 * Gets all authors from data repository.
	 * 
	 * @return
	 * @throws ServiceException 
	 */
	public List<Author> getAllAuthors() throws ServiceException;
	
	/**
	 * Gets all tags from data repository.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	public List<Tag> getAllTags() throws ServiceException;
	
	/**
	 * Gets the next news id in search.
	 * 
	 * @param searchCriteria
	 * @param currentNewsId
	 * @return
	 * @throws DaoException
	 */
	public Long getNextNewsiId(SearchCriteria searchCriteria, Long currentNewsId) throws ServiceException;
	
	/**
	 * Gets the previous news id in search.
	 * 
	 * @param searchCriteria
	 * @param currentNewsId
	 * @return
	 * @throws DaoException
	 */
	public Long getPreviousNewsId (SearchCriteria searchCriteria, Long currentNewsId) throws ServiceException;
	
	/**
	 * Gets user entity from data repository is it exists.
	 * 
	 * @param user
	 * @return
	 * @throws ServiceException
	 */
	public UserTO login (UserTO user) throws ServiceException;
	/**
	 * Updates news, author, tags data .
	 * 
	 * @throws ServiceException
	 */
	public boolean updateComplexNews(News news) throws ServiceException;

	/**
	 * Updates tag data.
	 * 
	 * @throws ServiceException
	 */
	public void updateTag(Tag tagTO) throws ServiceException;
	/**
	 * Updates author data.
	 * 
	 * @throws ServiceException
	 */
	public void updateAuthor(Author authorTO) throws ServiceException;

}

