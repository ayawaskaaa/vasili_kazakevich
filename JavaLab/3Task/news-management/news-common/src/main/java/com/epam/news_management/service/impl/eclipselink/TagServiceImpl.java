package com.epam.news_management.service.impl.eclipselink;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.epam.news_management.dao.ITagDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.Tag;
import com.epam.news_management.service.ITagService;
import com.epam.news_management.service.exception.ServiceException;



/**
 * TagService implementation.
 * 
 * Contains methods for operating tag entity.
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
@Profile("eclipselink")
@Component
public class TagServiceImpl implements ITagService {
	
	private static final Logger log = Logger.getLogger(TagServiceImpl.class);
	private ITagDao tagDao = null;
	
	public TagServiceImpl(){}
	
	public ITagDao getTagDao() {
		return tagDao;
	}

	public void setTagDao(ITagDao tagDao) {
		this.tagDao = tagDao;
	}

	/**
	 * Adds single tag transfer object.
	 * 
	 * @param tag
	 * @throws ServiceException
	 */
	@Override
	public void addTag(Tag tag) throws ServiceException {
		try {
			tagDao.add(tag);
			
		} catch (DaoException e) {
			log.error("Exception while adding new tag.", e);
			throw new ServiceException("Exception while adding tag.", e);
		}
		
		
	}

	/**
	 * Deletes tag entity from data repository.
	 * 
	 * @param tagId
	 * @throws ServiceException
	 */
	@Override
	public void deleteTag(Long tagId) throws ServiceException {
		try {
			tagDao.delete(tagId);
		} catch (DaoException e) {
			log.error("Exception while deleting tag.", e);
			throw new ServiceException("Exception while deleting tag.", e);
		}
		
	}

	/**
	 * Destroys links between tags table and news table, using ID of news.
	 * 
	 * @param tagId
	 * @throws ServiceException
	 */
	@Override
	public void unlinkTagWithNews(Long newsId) throws ServiceException {
		
		throw new UnsupportedOperationException();
		
	}

	/**
	 * Gets list of certain tags entity according news id.
	 * 
	 * @param newsId
	 * @return List of tag entities linked with concrete news.
	 * @throws ServiceException
	 */
	@Override
	public List<Tag> getTagsByNewsId(Long newsId) throws ServiceException {
		throw new UnsupportedOperationException();
		
	}

	/**
	 * Edits certain tag.
	 * 
	 * @param tag
	 * @throws ServiceException
	 */
	@Override
	public void editTag(Tag tag) throws ServiceException {
		try {
			tagDao.edit(tag);
		} catch (DaoException e) {
			log.error("Exception while editing tag.", e);
			throw new ServiceException("Exception while editing tag.", e);
		}
		
	}

	/**
	 * Links tags with news according to their ID's.
	 * 
	 * @param tagList
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void linkTagsWithNews(List<Long> tagsIdList, Long newsId) throws ServiceException {
		throw new UnsupportedOperationException();
	}

	/**
	 * Destroys links between tags table and news table, using ID of tag.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void unlinkTagWithNewsByTagId(Long tagId) throws ServiceException {
		throw new UnsupportedOperationException();
	}

	/**
	 * Gets all tags from data repository.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@Override
	public List<Tag> getAllTags() throws ServiceException {
		List<Tag> tagsList = null;
		try {
			tagsList=tagDao.getAll();
		} catch (DaoException e) {
			log.error("Exception while getting tags.", e);
			throw new ServiceException("Exception while getting tags.", e);
		}
		return tagsList;
	}

	

		

}
