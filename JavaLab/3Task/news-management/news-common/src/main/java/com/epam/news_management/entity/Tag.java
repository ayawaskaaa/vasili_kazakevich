package com.epam.news_management.entity;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Tag transfer object class.
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */

@Entity(name = "TAGS")
@Table(name = "TAGS")
@SequenceGenerator(name = "TG_TAG_ID_PK_SEQ", sequenceName = "TG_TAG_ID_PK_SEQ", allocationSize = 1)
public class Tag {

	private static final long serialVersionUID = -4837160615973765243L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TG_TAG_ID_PK_SEQ")
	@Column(name = "TG_TAG_ID_PK", nullable = false, precision = 20)
	private Long tagId;

	@Column(name = "TG_TAG_NAME", nullable = false, precision = 30)
	@NotNull
	@Size(min = 2, max = 30, message = "{local.error.message.addTag}")
	private String tagName;

	@ManyToMany
	@JoinTable(name = "NEWS_TAGS", joinColumns = { @JoinColumn(name = "NWTG_TAG_ID_FK",
	referencedColumnName = "TG_TAG_ID_PK") }, inverseJoinColumns = { @JoinColumn(name = "NWTG_NEWS_ID_FK",
	referencedColumnName = "NW_NEWS_ID_PK") })
	private List<News> newsList;

	public Long getTagId() {
		return tagId;
	}

	public void setTagId(Long tagId) {
		this.tagId = tagId;
	}

	public String getTagName() {
		return tagName;
	}

	public void setTagName(String tagName) {
		this.tagName = tagName;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((tagId == null) ? 0 : tagId.hashCode());
		result = prime * result + ((tagName == null) ? 0 : tagName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tag other = (Tag) obj;
		if (tagId == null) {
			if (other.tagId != null)
				return false;
		} else if (!tagId.equals(other.tagId))
			return false;
		if (tagName == null) {
			if (other.tagName != null)
				return false;
		} else if (!tagName.equals(other.tagName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TagTO [tagId=" + tagId + ", tagName=" + tagName + "]";
	}

}
