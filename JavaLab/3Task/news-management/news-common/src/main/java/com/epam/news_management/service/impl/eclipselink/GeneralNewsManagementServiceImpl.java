package com.epam.news_management.service.impl.eclipselink;


import java.util.ArrayList;
import java.util.List;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.Author;
import com.epam.news_management.entity.Comment;
import com.epam.news_management.entity.News;
import com.epam.news_management.entity.SearchCriteria;
import com.epam.news_management.entity.Tag;
import com.epam.news_management.entity.UserTO;
import com.epam.news_management.service.IAuthorService;
import com.epam.news_management.service.ICommentService;
import com.epam.news_management.service.IGeneralNewsManagementService;
import com.epam.news_management.service.INewsService;
import com.epam.news_management.service.ITagService;
import com.epam.news_management.service.IUserService;
import com.epam.news_management.service.exception.ServiceException;


/**
 * GeneralNewsManagementService implementation.
 * 
 * Facade class that provides simplified methods required
 * by client and delegates calls to methods of existing system classes. 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */

@Service
@Component
@Profile("eclipselink")
@Transactional(rollbackFor=Exception.class)
public class GeneralNewsManagementServiceImpl implements IGeneralNewsManagementService {
	
	private INewsService newsService = null;
	private ICommentService commentService = null;
	private IAuthorService authorService = null;
	private ITagService tagService = null;
	private IUserService userService=null;
	
	public GeneralNewsManagementServiceImpl(){}
	

	
	public INewsService getNewsService() {
		return newsService;
	}
	public void setNewsService(INewsService newsService) {
		this.newsService = newsService;
	}
	public ICommentService getCommentService() {
		return commentService;
	}
	public void setCommentService(ICommentService commentService) {
		this.commentService = commentService;
	}
	public IAuthorService getAuthorService() {
		return authorService;
	}
	public void setAuthorService(IAuthorService authorService) {
		this.authorService = authorService;
	}
	public ITagService getTagService() {
		return tagService;
	}
	public void settagService(ITagService tagService) {
		this.tagService = tagService;
	}
	public IUserService getUserService() {
		return userService;
	}
	public void setUserService(IUserService userService) {
		this.userService = userService;
	}
	
	
	/**
	 * Adds single news transfer object.
	 * 
	 * @param news
	 * @throws ServiceException
	 */

	
	@Override
	public void addNews(News news) throws ServiceException {
		
		newsService.addNews(news);
	}

	
	/**
	 * Adds single author transfer object.
	 * 
	 * @param author
	 * @throws ServiceException
	 */
	@Override
	public void addAuthor(Author author) throws ServiceException {
		authorService.addAuthor(author);
		
	}
	
	/**
	 * Adds single comment transfer object.
	 * 
	 * @param comment
	 * @throws ServiceException
	 */
	@Override
	public void addComment(Comment comment) throws ServiceException {
		commentService.addComment(comment);
		
	}


	/**
	 * Deletes tag entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void deleteTag(Long tagId) throws ServiceException {
		
		tagService.deleteTag(tagId);
		
	}
	
	/**
	 * Deletes author entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void deleteAuthor(Long authorId) throws ServiceException {
		
		
		authorService.deleteAuthor(authorId);
		
	}
	/**
	 * Deletes comment entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void deleteComment(Long commentId) throws ServiceException {
		commentService.deleteComment(commentId);
		
	}
	
	/**
	 * Adds single tag transfer object.
	 * 
	 * @param tag
	 * @throws ServiceException
	 */
	@Override
	public void addTag(Tag tag) throws ServiceException {
		
		tagService.addTag(tag);
		
		
	}


	/**
	 * Edits news entity in data repository.
	 * 
	 * @param newsTO
	 * @throws ServiceException
	 */
	@Override
	public void editNews(News newsTO) throws ServiceException {
		newsService.editNews(newsTO);
		
	}


	/**
	 * Searches certain news according search criteria, start ans end index.
	 * 
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return list of news according search criteria, start and end index.
	 * @throws ServiceException
	 */
	@Override
	public List<News> searchByAuthorAndTag(SearchCriteria searchCriteria, int start, int end) throws ServiceException {
		List<News> newsList = new ArrayList<News>();
		newsList=newsService.search(searchCriteria, start, end);		
		return newsList;
	}


	/**
	 * Counts items in search result.
	 * 
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return
	 * @throws ServiceException
	 */
	@Override
	public int countNewsInSearch(SearchCriteria searchCriteria) throws ServiceException {
		int countresult = newsService.countNewsInSearch(searchCriteria);
		return countresult;
	}


	/**
	 * Produces actions for adding in to data repository.
	 * 
	 * @param news
	 * @throws ServiceException
	 */
	@Override
	public Long addComplexNews(News news) throws ServiceException {
		Long newsId = null;
		newsId=newsService.addNews(news);

		return newsId;
		
		
	}


	/**
	 * Deletes news entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void deleteNews(Long newsId) throws ServiceException {
		
		
		newsService.deleteNews(newsId);
		
		
	}


	/**
	 * Produces actions to get news value object.
	 * 
	 * @param newsId
	 * @return
	 * @throws ServiceException
	 */
	@Override
	public News getSingleNews(Long newsId) throws ServiceException {
		News news = null;
		news=newsService.getSingleNews(newsId);
		return news;
	}

	/**
	 * Gets all authors from data repository.
	 * 
	 * @return
	 * @throws ServiceException 
	 */

	@Override
	public List<Author> getAllAuthors() throws ServiceException {
		List<Author> authorTOList = null;
		authorTOList=authorService.getAllAuthors();
	
		return authorTOList;
	}


	/**
	 * Gets all tags from data repository.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@Override
	public List<Tag> getAllTags() throws ServiceException {
		List<Tag> tagsList = null;
		tagsList=tagService.getAllTags();
		return tagsList;
	}

	/**
	 * Gets the next news id in search.
	 * 
	 * @param searchCriteria
	 * @param currentNewsId
	 * @return
	 * @throws DaoException
	 */
	
	@Override
	public Long getNextNewsiId(SearchCriteria searchCriteria, Long currentNewsId)
			throws ServiceException {
		Long nextNewsId = null;
		nextNewsId=newsService.getNextNewsId(searchCriteria, currentNewsId);
		return nextNewsId;
	}


	/**
	 * Gets the previous news id in search.
	 * 
	 * @param searchCriteria
	 * @param currentNewsId
	 * @return
	 * @throws DaoException
	 */
	@Override
	public Long getPreviousNewsId(SearchCriteria searchCriteria,
			Long currentNewsId) throws ServiceException {
		Long previousNewsId=null;
		previousNewsId = newsService.getPreviousNewsId(searchCriteria, currentNewsId);
		return previousNewsId;
	}


	/**
	 * Gets user entity from data repository is it exists.
	 * 
	 * @param user
	 * @return
	 * @throws ServiceException
	 */
	@Override
	public UserTO login(UserTO user) throws ServiceException {
		UserTO userTO =null;
		userTO=userService.login(user);

		return userTO;
	}

	/**
	 * Updates news, author, tags data .
	 * @return 
	 * 
	 * @throws ServiceException
	 */

	@Override
	public boolean updateComplexNews(News news) throws ServiceException {
	

		return newsService.editNews(news);
		
	}

	

	/**
	 * Updates tag data.
	 * 
	 * @throws ServiceException
	 */
	@Override
	public void updateTag(Tag tagTO) throws ServiceException {
		tagService.editTag(tagTO);
		
	}


	/**
	 * Updates author data.
	 * 
	 * @throws ServiceException
	 */
	@Override
	public void updateAuthor(Author authorTO) throws ServiceException {
		authorService.editAuthor(authorTO);
		
	}



}
