package com.epam.news_management.dao.impl.jdbc;

/**
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.news_management.dao.INewsDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.dao.util.DAOUtil;
import com.epam.news_management.entity.News;
import com.epam.news_management.entity.SearchCriteria;

public class NewsDaoImpl implements INewsDao {

	private static final String SQL_GET_NEWS_BY_ID_SELECT = "SELECT nw_news_id_pk, nw_title, nw_short_text, "
			+ "nw_full_text, nw_creation_date, nw_modification_date FROM news WHERE nw_news_id_pk = ? ";

	private static final String SQL_GET_UNSORTED_NEWS_SELECT = "SELECT nw_news_id_pk, nw_title, nw_short_text, "
			+ "nw_full_text, nw_creation_date, nw_modification_date FROM news";

	private static final String SQL_ADD_NEWS_INSERT = "INSERT INTO news ( nw_news_id_pk, nw_title,"
			+ " nw_short_text, nw_full_text, nw_creation_date, nw_modification_date, nw_version ) "
			+ "VALUES (NW_NEWS_ID_PK_SEQ.nextval, ?, ?, ?, ? , SYSDATE, ? ) ";

	private static final String SQL_EDIT_NEWS_UPDATE = "UPDATE news SET  nw_title =? ,"
			+ " nw_short_text=? , nw_full_text=? , nw_creation_date = ? ,  nw_modification_date = SYSDATE "
			+ "  WHERE  nw_news_id_pk = ? ";

	private static final String SQL_DELETE_NEWS_DELETE = "DELETE FROM news WHERE nw_news_id_pk=? ";

	private static final String SQL_SEARCH_ALL_SELECT = " SELECT DISTINCT n_id, title, sh_text, "
			+ " full_text, cre_d, mod_d, amount, r_n "
			+ " FROM ( SELECT DISTINCT n_id, title, sh_text, full_text, cre_d, mod_d, amount,  "
			+ " ROW_NUMBER() OVER (ORDER BY amount DESC , mod_d DESC ) as r_n  "
			+ " FROM (SELECT DISTINCT  nw_news_id_pk AS n_id, nw_title AS title, nw_short_text AS sh_text, "
			+ " nw_full_text AS full_text, nw_creation_date AS cre_d, nw_modification_date AS mod_d, "
			+ " comments_amount AS amount  FROM news LEFT JOIN (SELECT nw_news_id_pk AS news_id, "
			+ " COUNT(nw_news_id_pk) AS comments_amount FROM news "
			+ " INNER JOIN comments ON nw_news_id_pk=cm_news_id_fk GROUP BY nw_news_id_pk) ON nw_news_id_pk=news_id ) ";

	private static final String SQL_JOIN_AUTHOR = " JOIN news_authors ON  n_id=nwau_news_id_fk ";

	private static final String SQL_JOIN_TAGS = " JOIN news_tags ON n_id=nwtg_news_id_fk ";

	private static final String SQL_SEARCH_CHAIN = "WHERE r_n BETWEEN ? AND ? ";

	private static final Object SQL_SEARCH_CHAIN_WITH_PARAMETERS = " WHERE n_id>0 ";

	private static final String SQL_SEQRCH_END = " ORDER BY r_n ";

	private static final String SQL_COUNT_ALL_FROM_SEARCH = "Select COUNT(n_id) FROM ("
			+ "SELECT n_id, mod_d, amount FROM (SELECT DISTINCT  nw_news_id_pk AS n_id, "
			+ "nw_modification_date AS mod_d, comments_amount AS amount , "
			+ "ROW_NUMBER() OVER (ORDER BY comments_amount , nw_modification_date  ) as r_n FROM news "
			+ "LEFT JOIN (SELECT nw_news_id_pk AS news_id, COUNT(nw_news_id_pk) AS comments_amount FROM news "
			+ "INNER JOIN comments ON nw_news_id_pk=cm_news_id_fk GROUP BY nw_news_id_pk) ON nw_news_id_pk=news_id )";

	private static final String SQL_COUNT_CHAIN = " WHERE n_id>0 ";
	private static final String SQL_COUNT_END = "  ) ";

	private static final String SQL_SELECT_NEXT_ID = "SELECT sel1.next_id, sel1.current_id"
			+ " FROM (SELECT LEAD (current_id) OVER (ORDER BY   comments_amount desc, nw_modification_date desc ) AS next_id, sel2.current_id "
			+ " FROM (SELECT  sel3.news_id as current_id, sel3.nw_modification_date, sel3.comments_amount"
			+ " FROM (SELECT nw_news_id_pk as news_id, nw_modification_date, comments_amount "
			+ " FROM news LEFT JOIN (SELECT nw_news_id_pk AS news_id, "
			+ " COUNT(nw_news_id_pk) AS comments_amount  FROM news INNER JOIN comments "
			+ " ON nw_news_id_pk=cm_news_id_fk GROUP BY nw_news_id_pk) ON nw_news_id_pk=news_id ) sel3 ";

	private static final String SQL_SELECT_PREVIOUS_ID = "SELECT sel1.previous_id, sel1.current_id "
			+ " FROM (SELECT LAG (current_id) OVER (ORDER BY   comments_amount desc, nw_modification_date desc ) AS previous_id, sel2.current_id  "
			+ " FROM (SELECT  sel3.news_id as current_id, sel3.nw_modification_date, sel3.comments_amount "
			+ " FROM (SELECT nw_news_id_pk as news_id, nw_modification_date, comments_amount "
			+ " FROM news LEFT JOIN (SELECT nw_news_id_pk AS news_id, "
			+ " COUNT(nw_news_id_pk) AS comments_amount  FROM news INNER JOIN comments "
			+ " ON nw_news_id_pk=cm_news_id_fk GROUP BY nw_news_id_pk) ON nw_news_id_pk=news_id ) sel3 ";

	private static final String SQL_SELECT_NEXT_ADN_PREVIOUS_JOIN_AUTHOR = " JOIN news_authors ON  news_id=nwau_news_id_fk ";

	private static final String SQL_SELECT_NEXT_ADN_PREVIOUS_JOIN_TAGS = " JOIN news_tags ON news_id=nwtg_news_id_fk ";

	private static final String SQL_BY_AUTHOR_ID = " AND nwau_author_id_fk IN ( ? ) ";

	private static final String SQL_BY_TAG_ID = " AND nwtg_tag_id_fk IN ( ";

	private static final String SQL_CHAIN = " WHERE news_id>0 ";

	private static final String SQL_END = "  ) sel2)  sel1 WHERE sel1.current_id =? ";

	private static final String CLOSE_BRACKETS = " ) ";

	private static final String QUESTION_MARK = " ? ";

	private DataSource dataSource = null;

	public NewsDaoImpl() {
	}

	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	public NewsDaoImpl(DataSource ds) {

		this.dataSource = ds;

	}

	@Override
	public Long add(News news) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long nenwsId = null;
		String column[] = { "nw_news_id_pk" };
		try {
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_ADD_NEWS_INSERT, column);

			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setTimestamp(4, (Timestamp) news.getCreationDate());
			ps.setInt(5, news.getVersion() + 1);
			ps.executeUpdate();

			rs = ps.getGeneratedKeys();

			if (rs != null && rs.next()) {
				nenwsId = rs.getLong(1);
			}

		} catch (SQLException e) {
			throw new DaoException("Exception while adding news.", e);
		} finally {
			DAOUtil.closeConnection(con, ps, rs, dataSource);
		}
		return nenwsId;
	}

	@Override
	public List<News> getAll() throws DaoException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		List<News> newsList = new ArrayList<News>();

		try {
			con = DataSourceUtils.doGetConnection(dataSource);
			st = con.createStatement();
			rs = st.executeQuery(SQL_GET_UNSORTED_NEWS_SELECT);
			if (rs != null) {
				while (rs.next()) {
					newsList.add(buildNewsItem(rs));
				}

			}

		} catch (SQLException e) {
			throw new DaoException("Exception while getting news.", e);
		} finally {
			DAOUtil.closeConnection(con, st, rs, dataSource);
		}
		return newsList;
	}

	@Override
	public News get(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		News news = null;
		try {
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_GET_NEWS_BY_ID_SELECT);
			ps.setLong(1, newsId);
			rs = ps.executeQuery();
			if (rs != null) {
				if (rs.next()) {
					news = buildNewsItem(rs);

				}

			}

		} catch (SQLException e) {
			throw new DaoException("Exception while getting news.", e);
		} finally {
			DAOUtil.closeConnection(con, ps, rs, dataSource);
		}

		return news;
	}

	@Override
	public boolean edit(News news) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		boolean success = true;

		try {
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_EDIT_NEWS_UPDATE);
			ps.setString(1, news.getTitle());
			ps.setString(2, news.getShortText());
			ps.setString(3, news.getFullText());
			ps.setTimestamp(4, (Timestamp) news.getCreationDate());
			ps.setLong(5, news.getNewsId());
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("Exception while editing news.", e);
		} finally {
			DAOUtil.closeConnection(con, ps, dataSource);
		}
		return success;

	}

	@Override
	public void delete(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;

		try {
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_DELETE_NEWS_DELETE);
			ps.setLong(1, newsId);
			ps.executeUpdate();

		} catch (SQLException e) {
			throw new DaoException("Exception while deleting news.", e);
		} finally {
			DAOUtil.closeConnection(con, ps, dataSource);
		}

	}

	@Override
	public int countNewsInSearch(SearchCriteria searchCriteria)
			throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		int countResult = 0;
		String countQuery = generateCountQuery(searchCriteria);

		try {
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(countQuery);
			ps = fillCountPreparedStatement(ps, searchCriteria);
			rs = ps.executeQuery();
			if (rs != null) {
				if (rs.next()) {
					countResult = rs.getInt(1);
				}
			}

		} catch (SQLException e) {
			throw new DaoException("Exception while counting news.", e);
		} finally {
			DAOUtil.closeConnection(con, ps, rs, dataSource);
		}
		return countResult;
	}

	@Override
	public List<News> search(SearchCriteria searchCriteria, int start, int end)
			throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<News> newsList = new ArrayList<News>();
		String searchQuery = generateSearchQuery(searchCriteria);
		try {
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(searchQuery);
			ps = fillPreparedStatement(ps, searchCriteria, start, end);
			rs = ps.executeQuery();

			if (rs != null) {
				while (rs.next()) {
					newsList.add(buildNewsItem(rs));
				}

			}

		} catch (SQLException e) {
			throw new DaoException("Exception while searching news.", e);
		} finally {
			DAOUtil.closeConnection(con, ps, rs, dataSource);
		}
		return newsList;
	}

	@Override
	public Long getNextNewsId(SearchCriteria searchCriteria, Long currentNewsId)
			throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long nextNewsId = null;
		String getNextQuery = generateGetNextNewsQuery(searchCriteria);

		try {
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(getNextQuery);
			ps = fillGetNextOrPreviousNewsQuery(ps, searchCriteria,
					currentNewsId);
			rs = ps.executeQuery();

			if (rs != null) {
				while (rs.next()) {
					nextNewsId = rs.getLong(1);
				}

			}

		} catch (SQLException e) {
			throw new DaoException("Exception while getting news.", e);
		} finally {
			DAOUtil.closeConnection(con, ps, rs, dataSource);
		}

		return nextNewsId;
	}

	@Override
	public Long getPreviousNewsId(SearchCriteria searchCriteria,
			Long currentNewsId) throws DaoException {

		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		Long previousNewsId = null;
		String getPreviousQuery = generateGetPreviousNewsQuery(searchCriteria);
		try {
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(getPreviousQuery);
			ps = fillGetNextOrPreviousNewsQuery(ps, searchCriteria,
					currentNewsId);
			rs = ps.executeQuery();

			if (rs != null) {
				while (rs.next()) {
					previousNewsId = rs.getLong(1);
				}

			}

		} catch (SQLException e) {
			throw new DaoException("Exception while getting news.", e);
		} finally {
			DAOUtil.closeConnection(con, ps, rs, dataSource);
		}

		return previousNewsId;
	}

	private News buildNewsItem(ResultSet rs) throws SQLException {
		News news = new News();
		news.setNewsId(rs.getLong(1));
		news.setTitle(rs.getString(2));
		news.setShortText(rs.getString(3));
		news.setFullText(rs.getString(4));
		news.setCreationDate(rs.getTimestamp(5));
		news.setModificationDate(rs.getDate(6));

		return news;
	}

	private String generateSearchQuery(SearchCriteria searchCriteria) {
		StringBuilder searchQuery = new StringBuilder();
		Long authorId = null;
		List<Long> tagsId = null;
		boolean withParameter = false;
		if (searchCriteria != null) {
			authorId = searchCriteria.getAuthorId();
			tagsId = searchCriteria.getTagIdList();
		}

		searchQuery.append(SQL_SEARCH_ALL_SELECT);

		int authorQueries = 0;
		int tagQueries = 0;
		if (authorId != null) {
			searchQuery.append(SQL_JOIN_AUTHOR);
			authorQueries = 1;
			withParameter = true;
		}

		if (tagsId != null) {
			searchQuery.append(SQL_JOIN_TAGS);
			tagQueries = tagsId.size();
			withParameter = true;
		}

		for (int i = 0; i < authorQueries; i++) {
			searchQuery.append(SQL_BY_AUTHOR_ID);
		}

		if (tagsId != null) {
			searchQuery.append(SQL_BY_TAG_ID);
			for (int i = 0; i < tagQueries; i++) {
				searchQuery.append(QUESTION_MARK);

				if (!((i + 1) == tagQueries)) {
					searchQuery.append(" , ");
				}
			}
			searchQuery.append(CLOSE_BRACKETS);
		}

		searchQuery.append(CLOSE_BRACKETS);
		searchQuery.append(SQL_SEARCH_CHAIN);

		searchQuery.append(SQL_SEQRCH_END);
		return searchQuery.toString();
	}

	private PreparedStatement fillPreparedStatement(PreparedStatement ps,
			SearchCriteria searchCriteria, int start, int end)
			throws SQLException {

		Long authorId = null;
		List<Long> tagsId = null;
		boolean withParameter = false;
		if (searchCriteria != null) {
			authorId = searchCriteria.getAuthorId();
			tagsId = searchCriteria.getTagIdList();
		}
		if (authorId != null || tagsId != null) {
			withParameter = true;
		}

		int nextParameterIndex = 1;

		if (authorId != null) {
			ps.setLong(nextParameterIndex, authorId);
			nextParameterIndex++;
		}

		if (tagsId != null) {
			for (Long i : tagsId) {

				ps.setLong(nextParameterIndex, i);
				nextParameterIndex++;
			}
		}

		ps.setInt(nextParameterIndex, start);
		nextParameterIndex++;
		ps.setInt(nextParameterIndex, end);

		return ps;
	}

	private String generateCountQuery(SearchCriteria searchCriteria) {
		StringBuilder countQuery = new StringBuilder();
		Long authorId = null;
		List<Long> tagsId = null;
		if (searchCriteria != null) {
			authorId = searchCriteria.getAuthorId();
			tagsId = searchCriteria.getTagIdList();
		}
		countQuery.append(SQL_COUNT_ALL_FROM_SEARCH);

		int authorQueries = 0;
		int tagQueries = 0;
		if (authorId != null) {
			countQuery.append(SQL_JOIN_AUTHOR);
			authorQueries = 1;
		}

		if (tagsId != null) {
			countQuery.append(SQL_JOIN_TAGS);
			tagQueries = tagsId.size();
		}
		if (authorQueries != 0 && tagQueries != 0) {
			countQuery.append(SQL_COUNT_CHAIN);
		}

		for (int i = 0; i < authorQueries; i++) {
			countQuery.append(SQL_BY_AUTHOR_ID);
		}
		if (tagsId != null) {
			countQuery.append(SQL_BY_TAG_ID);
			for (int i = 0; i < tagQueries; i++) {
				countQuery.append(QUESTION_MARK);

				if (!((i + 1) == tagQueries)) {
					countQuery.append(" , ");
				}
			}
			countQuery.append(CLOSE_BRACKETS);
		}
		countQuery.append(SQL_COUNT_END);

		return countQuery.toString();

	}

	private PreparedStatement fillCountPreparedStatement(PreparedStatement ps,
			SearchCriteria searchCriteria) throws SQLException {

		Long authorId = null;
		List<Long> tagsId = null;
		if (searchCriteria != null) {
			authorId = searchCriteria.getAuthorId();
			tagsId = searchCriteria.getTagIdList();
		}
		int nextParameterIndex = 1;

		if (authorId != null) {
			ps.setLong(nextParameterIndex, authorId);
			nextParameterIndex++;
		}

		if (tagsId != null) {
			for (Long i : tagsId) {
				ps.setLong(nextParameterIndex, i);
				nextParameterIndex++;
			}
		}

		return ps;
	}

	private String generateGetNextNewsQuery(SearchCriteria searchCriteria) {
		StringBuilder getNextQuery = new StringBuilder();
		Long authorId = null;
		List<Long> tagsId = null;
		boolean withTagParameters = false;
		if (searchCriteria != null) {
			authorId = searchCriteria.getAuthorId();
			tagsId = searchCriteria.getTagIdList();
		}
		getNextQuery.append(SQL_SELECT_NEXT_ID);

		int authorQueries = 0;
		int tagQueries = 0;
		if (authorId != null) {
			getNextQuery.append(SQL_SELECT_NEXT_ADN_PREVIOUS_JOIN_AUTHOR);
			authorQueries = 1;

		}

		if (tagsId != null) {
			getNextQuery.append(SQL_SELECT_NEXT_ADN_PREVIOUS_JOIN_TAGS);
			tagQueries = tagsId.size();
			withTagParameters = true;

		}

		getNextQuery.append(SQL_CHAIN);

		for (int i = 0; i < authorQueries; i++) {
			getNextQuery.append(SQL_BY_AUTHOR_ID);
		}

		if (tagsId != null) {
			getNextQuery.append(SQL_BY_TAG_ID);
			for (int i = 0; i < tagQueries; i++) {
				getNextQuery.append(QUESTION_MARK);

				if (!((i + 1) == tagQueries)) {
					getNextQuery.append(" , ");
				}
			}

		}
		if (withTagParameters) {
			getNextQuery.append(CLOSE_BRACKETS);
		}
		getNextQuery.append(SQL_END);

		return getNextQuery.toString();

	}

	private PreparedStatement fillGetNextOrPreviousNewsQuery(
			PreparedStatement ps, SearchCriteria searchCriteria,
			Long currentNewsId) throws SQLException {
		Long authorId = null;
		List<Long> tagsId = null;
		if (searchCriteria != null) {
			authorId = searchCriteria.getAuthorId();
			tagsId = searchCriteria.getTagIdList();
		}
		int nextParameterIndex = 1;

		if (authorId != null) {
			ps.setLong(nextParameterIndex, authorId);
			nextParameterIndex++;
		}

		if (tagsId != null) {
			for (Long i : tagsId) {

				ps.setLong(nextParameterIndex, i);
				nextParameterIndex++;
			}
		}
		ps.setLong(nextParameterIndex, currentNewsId);

		return ps;

	}

	private String generateGetPreviousNewsQuery(SearchCriteria searchCriteria) {
		StringBuilder getPreviousQuery = new StringBuilder();
		Long authorId = null;
		List<Long> tagsId = null;
		boolean withTagParameters = false;
		if (searchCriteria != null) {
			authorId = searchCriteria.getAuthorId();
			tagsId = searchCriteria.getTagIdList();
		}
		getPreviousQuery.append(SQL_SELECT_PREVIOUS_ID);

		int authorQueries = 0;
		int tagQueries = 0;
		if (authorId != null) {
			getPreviousQuery.append(SQL_SELECT_NEXT_ADN_PREVIOUS_JOIN_AUTHOR);
			authorQueries = 1;

		}

		if (tagsId != null) {
			getPreviousQuery.append(SQL_SELECT_NEXT_ADN_PREVIOUS_JOIN_TAGS);
			tagQueries = tagsId.size();
			withTagParameters = true;

		}

		getPreviousQuery.append(SQL_CHAIN);

		for (int i = 0; i < authorQueries; i++) {
			getPreviousQuery.append(SQL_BY_AUTHOR_ID);
		}
		if (tagsId != null) {
			getPreviousQuery.append(SQL_BY_TAG_ID);
			for (int i = 0; i < tagQueries; i++) {
				getPreviousQuery.append(QUESTION_MARK);

				if (!((i + 1) == tagQueries)) {
					getPreviousQuery.append(" , ");
				}
			}

		}
		if (withTagParameters) {
			getPreviousQuery.append(CLOSE_BRACKETS);
		}

		getPreviousQuery.append(SQL_END);

		return getPreviousQuery.toString();

	}

}
