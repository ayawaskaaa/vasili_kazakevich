package com.epam.news_management.dao.impl.hibernate;

import java.util.List;
import org.hibernate.Criteria;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.epam.news_management.dao.ITagDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.Tag;

@Repository
@Component
@Profile("hibernate")
public class TagDaoImpl implements ITagDao {

	@Autowired
	private SessionFactory sessionFactory;

	TagDaoImpl() {
	}

	@Override
	public Long add(Tag tag) throws DaoException {

		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.persist(tag);
		tx.commit();
		session.close();
		return tag.getTagId();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Tag> getAll() throws DaoException {

		List<Tag> tagsList = null;
		Session session = this.sessionFactory.openSession();
		tagsList = session.createCriteria(Tag.class).list();
		session.close();
		return tagsList;
	}

	@Override
	public Tag get(Long tagId) throws DaoException {
		Tag tag = null;
		Session session = this.sessionFactory.openSession();
		Criteria criteria = session.createCriteria(Tag.class).add(
				Restrictions.eq("tagId", tagId));

		tag = (Tag) criteria.uniqueResult();

		session.close();
		return tag;
	}

	@Override
	public boolean edit(Tag tag) throws DaoException {
		boolean success = true;

		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		session.update(tag);

		tx.commit();
		session.close();
		return success;

	}

	@Override
	public void delete(Long tagId) throws DaoException {
		Tag tag = new Tag();
		tag.setTagId(tagId);
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		session.delete(tag);

		tx.commit();
		session.close();

	}

	@Override
	public List<Tag> getTagsByNewsId(Long newsId) throws DaoException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void unlinkByNewsId(Long newsId) throws DaoException {
		throw new UnsupportedOperationException();

	}

	@Override
	public void unlinkByTagId(Long tagId) throws DaoException {
		throw new UnsupportedOperationException();

	}

	@Override
	public void link(List<Long> tags, Long newsId) throws DaoException {
		throw new UnsupportedOperationException();

	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}
}
