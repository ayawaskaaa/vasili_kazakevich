package com.epam.news_management.service.impl.eclipselink;

import java.util.List;

import org.apache.log4j.Logger;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import com.epam.news_management.dao.IAuthorDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.Author;
import com.epam.news_management.service.IAuthorService;
import com.epam.news_management.service.exception.ServiceException;



/**
 * AuthorService implementation.
 * 
 * Contains methods for operating author entity.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
@Profile("eclipselink")
@Component
public class AuthorServiceImpl implements IAuthorService {
	
	private IAuthorDao authorDao;
	
	private static final Logger log = Logger.getLogger(AuthorServiceImpl.class);

	public AuthorServiceImpl(){}

	
	public IAuthorDao getAuthorDao() {
		return authorDao;
	}

	public void setAuthorDao(IAuthorDao authorDao) {
		this.authorDao = authorDao;
	}
	
	

	/**
	 * Adds single author transfer object.
	 * 
	 * @param author
	 * @return new author ID.
	 * @throws ServiceException
	 */
	@Override
	public Long addAuthor(Author author) throws ServiceException {
		Long authorId = null;
		try {
			authorId=authorDao.add(author);			
			
		} catch (DaoException e) {
			log.error("Exception in DAO layer while adding author", e);
			throw new ServiceException("Exception in DAO layer while adding author", e);
		}
		return authorId;
	}

	/**
	 * Makes author expired.
	 * 
	 * @param authorId
	 * @throws ServiceException
	 */
	@Override
	public void deleteAuthor(Long authorId) throws ServiceException {
		try {
			authorDao.delete(authorId);
		} catch (DaoException e) {
			log.error("Exception in DAO layer while deleting author", e);
			throw new ServiceException("Exception in DAO layer while deleting author", e);
		}
		
	}

	/**
	 * Gets certain author according to news ID.
	 * 
	 * @param newsId
	 * @return author transfer object.
	 * @throws ServiceException
	 */
	@Override
	public Author getAuthorByNewsId(Long newsId) throws ServiceException {
		
		throw new UnsupportedOperationException();
	}

	/**
	 * Destroys links between author table and news table, using ID of news.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void unlinkAutorWhithNewsByNewsId(Long newsId) throws ServiceException {
		throw new UnsupportedOperationException();
		
	}
	
	/**
	 * Destroys links between author table and news table, using ID of author.
	 * 
	 * @param authorId
	 * @throws ServiceException
	 */
	@Override
	public void unlinkAutorWhithNewsByAuthorId(Long authorId) throws ServiceException {
		throw new UnsupportedOperationException();
	}

	/**
	 * Edit author entity in data repository.
	 * 
	 * @param author
	 * @throws ServiceException
	 */
	@Override
	public void editAuthor(Author author) throws ServiceException {
		try {
			authorDao.edit(author);
		} catch (DaoException e) {
			log.error("Exception in DAO layer while editing author", e);
			throw new ServiceException("Exception in DAO layer while editing author", e);
		}
	}

	/**
	 * Links author with news according to their ID's.
	 * 
	 * @param author
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void linkAuthorWhithNews(Long authorId, Long newsId)
			throws ServiceException {
		throw new UnsupportedOperationException();
		
	}


	@Override
	public List<Author> getAllAuthors() throws ServiceException {
		List<Author> authorsList = null;
		try {
			authorsList=authorDao.getAll();
		} catch (DaoException e) {
			log.error("Exception in DAO layer while getting authors.", e);
			throw new ServiceException("Exception in DAO layer while getting authors.", e);
		}
		
		return authorsList;
	}


	



	

	

}
