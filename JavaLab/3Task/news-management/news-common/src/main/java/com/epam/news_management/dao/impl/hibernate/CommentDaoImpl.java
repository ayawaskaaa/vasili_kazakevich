package com.epam.news_management.dao.impl.hibernate;

import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.epam.news_management.dao.ICommentDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.Comment;
import com.epam.news_management.entity.Tag;

@Repository
@Component
@Profile("hibernate")
@SuppressWarnings("unused")
public class CommentDaoImpl implements ICommentDao {

	@Autowired
	private SessionFactory sessionFactory;

	public SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	public void setSessionFactory(SessionFactory sessionFactory) {
		this.sessionFactory = sessionFactory;
	}

	private DataSource dataSource = null;

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}

	CommentDaoImpl() {
	}

	@Override
	public Long add(Comment comment) throws DaoException {
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		comment.setCreationDate(new Date());
		session.persist(comment);

		tx.commit();
		session.close();
		return comment.getCommentId();
	}

	@Override
	public List<Comment> getAll() throws DaoException {
		throw new UnsupportedOperationException();
	}

	@Override
	public Comment get(Long commentId) throws DaoException {
		Comment comment = null;
		Session session = this.sessionFactory.openSession();
		Criteria criteria = session.createCriteria(Comment.class).add(
				Restrictions.eq("commentId", commentId));

		comment = (Comment) criteria.uniqueResult();

		session.close();
		return comment;
	}

	@Override
	public boolean edit(Comment comment) throws DaoException {
		boolean success = true;

		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		session.update(comment);

		tx.commit();
		session.close();
		return success;

	}

	@Override
	public void delete(Long commentId) throws DaoException {
		Comment comment = new Comment();
		comment.setCommentId(commentId);
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();

		session.delete(comment);

		tx.commit();
		session.close();

	}

	@Override
	public List<Comment> getCommentsByNewsId(Long newsId) throws DaoException {
		throw new UnsupportedOperationException();

	}

	@Override
	public void deleteByNewsId(Long newsId) throws DaoException {
		throw new UnsupportedOperationException();

	}

}
