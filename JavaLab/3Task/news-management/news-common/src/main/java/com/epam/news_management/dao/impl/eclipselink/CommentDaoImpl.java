package com.epam.news_management.dao.impl.eclipselink;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;
import javax.sql.DataSource;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.epam.news_management.dao.ICommentDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.Comment;
import com.epam.news_management.entity.Tag;

@Repository
@Component
@Profile("eclipselink")
public class CommentDaoImpl implements ICommentDao {

	@PersistenceUnit(unitName = "entityManagerFactory")
	private EntityManagerFactory factory;

	CommentDaoImpl() {
	}

	@Override
	public Long add(Comment comment) throws DaoException {
		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		comment.setCreationDate(new Date());
		entityManager.persist(comment);
		entityManager.getEntityManagerFactory().getCache().evictAll();
		entityManager.flush();
		entityTransaction.commit();

		return comment.getCommentId();
	}

	@Override
	public Comment get(Long commentId) throws DaoException {
		Comment comment = null;
		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		comment = entityManager.find(Comment.class, commentId);

		entityTransaction.commit();
		return comment;
	}

	@Override
	public boolean edit(Comment comment) throws DaoException {
		boolean success = true;

		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		entityManager.merge(comment);
		entityManager.getEntityManagerFactory().getCache().evictAll();

		entityTransaction.commit();
		return success;

	}

	@Override
	public void delete(Long commentId) throws DaoException {
		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		Comment comment = entityManager.find(Comment.class, commentId);
		entityManager.remove(comment);
		entityManager.getEntityManagerFactory().getCache().evictAll();

		entityTransaction.commit();

	}

	@Override
	public List<Comment> getAll() throws DaoException {
		throw new UnsupportedOperationException();
	}

	@Override
	public List<Comment> getCommentsByNewsId(Long newsId) throws DaoException {
		throw new UnsupportedOperationException();

	}

	@Override
	public void deleteByNewsId(Long newsId) throws DaoException {
		throw new UnsupportedOperationException();

	}

	public EntityManagerFactory getFactory() {
		return factory;
	}

	public void setFactory(EntityManagerFactory factory) {
		this.factory = factory;
	}

}
