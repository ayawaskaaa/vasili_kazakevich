package com.epam.news_management.dao.impl.eclipselink;


import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.EntityTransaction;
import javax.persistence.PersistenceUnit;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import com.epam.news_management.dao.ITagDao;
import com.epam.news_management.dao.exception.DaoException;

import com.epam.news_management.entity.Tag;

@Repository
@Component
@Profile("eclipselink")
public class TagDaoImpl implements ITagDao {

	@PersistenceUnit(unitName = "entityManagerFactory")
	private EntityManagerFactory factory;

	TagDaoImpl() {
	}

	@Override
	public Long add(Tag tag) throws DaoException {

		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		entityManager.persist(tag);

		entityTransaction.commit();
		return tag.getTagId();

	}

	@Override
	public List<Tag> getAll() throws DaoException {

		List<Tag> tagsList = null;
		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		tagsList = (List<Tag>) entityManager.createQuery("from TAGS t",
				Tag.class).getResultList();
		entityTransaction.commit();
		return tagsList;
	}

	@Override
	public Tag get(Long tagId) throws DaoException {
		Tag tag = null;
		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		tag = entityManager.find(Tag.class, tagId);
		entityTransaction.commit();
		return tag;
	}

	@Override
	public boolean edit(Tag tag) throws DaoException {
		boolean success = true;

		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		entityManager.merge(tag);

		entityTransaction.commit();
		return success;

	}

	@Override
	public void delete(Long tagId) throws DaoException {

		EntityManager entityManager = factory.createEntityManager();
		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();

		Tag tag = entityManager.find(Tag.class, tagId);
		entityManager.remove(tag);

		entityTransaction.commit();

	}

	@Override
	public List<Tag> getTagsByNewsId(Long newsId) throws DaoException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void unlinkByNewsId(Long newsId) throws DaoException {
		throw new UnsupportedOperationException();

	}

	@Override
	public void unlinkByTagId(Long tagId) throws DaoException {
		throw new UnsupportedOperationException();

	}

	@Override
	public void link(List<Long> tags, Long newsId) throws DaoException {
		throw new UnsupportedOperationException();

	}

	public EntityManagerFactory getFactory() {
		return factory;
	}

	public void setFactory(EntityManagerFactory factory) {
		this.factory = factory;
	}
}
