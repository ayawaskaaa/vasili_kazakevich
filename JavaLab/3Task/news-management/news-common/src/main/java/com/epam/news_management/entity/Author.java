package com.epam.news_management.entity;


import java.sql.Timestamp;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;



/**
 * Author transfer object class.
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */

@Entity(name="AUTHORS")
@Table(name="AUTHORS")
@SequenceGenerator(name="AU_AUTHOR_ID_PK_SEQ", sequenceName="AU_AUTHOR_ID_PK_SEQ",allocationSize=1)
public class Author {


	private static final long serialVersionUID = -304637856394596079L;
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="AU_AUTHOR_ID_PK_SEQ")	
	@Column(name="AU_AUTHOR_ID_PK" , precision=20, nullable=false)
	private Long authorId;
	
	@NotNull	
	@Size(min=2, max=30, message="{local.error.message.addAuthor}")
	@Column(name="AU_AUTHOR_NAME", nullable=false, precision=30)
	private String authorName;
	
	@Column(name = "AU_EXPIRED", nullable = true)
	@Temporal(TemporalType.TIMESTAMP)
	private Date expired;

	
	public Long getAuthorId() {
		return authorId;
	}
	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public String getAuthorName() {
		return authorName;
	}

	public void setAuthorName(String authorName) {
		this.authorName = authorName;
	}

	public Date getExpired() {
		return expired;
	}

	public void setExpired(Date expired) {
		this.expired = expired;
	}
	

	@Override
	public String toString() {
		return "Author [authorId=" + authorId + ", authorName=" + authorName
				+ ", expired=" + expired + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result
				+ ((authorName == null) ? 0 : authorName.hashCode());
		result = prime * result + ((expired == null) ? 0 : expired.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Author other = (Author) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (authorName == null) {
			if (other.authorName != null)
				return false;
		} else if (!authorName.equals(other.authorName))
			return false;
		if (expired == null) {
			if (other.expired != null)
				return false;
		} else if (!expired.equals(other.expired))
			return false;
		return true;
	}

	


}
