package com.epam.news_management.entity;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;

import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;

import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import javax.persistence.Version;
import javax.validation.constraints.Min;
import javax.validation.constraints.Size;

import org.eclipse.persistence.annotations.PrivateOwned;

import org.hibernate.annotations.LazyCollection;
import org.hibernate.annotations.LazyCollectionOption;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * News entity object class.
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */

@Entity(name = "NEWS")
@Table(name = "NEWS")
@org.hibernate.annotations.Entity(dynamicUpdate = true)
@SequenceGenerator(name = "NW_NEWS_ID_PK_SEQ", sequenceName = "NW_NEWS_ID_PK_SEQ", allocationSize = 1)
public class News {

	private static final long serialVersionUID = -304637856394596079L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NW_NEWS_ID_PK_SEQ")
	@Column(name = "NW_NEWS_ID_PK", nullable = false, precision = 20)
	private Long newsId;

	@Version
	@Column(name = "NW_VERSION", nullable = false, precision = 20)
	private int version;

	@Size(min = 2, max = 30, message = "{local.error.message.titleSize}")
	@Column(name = "NW_TITLE", nullable = false, precision = 30)
	private String title;

	@Size(min = 2, max = 100, message = "{local.error.message.shortTextSize}")
	@Column(name = "NW_SHORT_TEXT", nullable = false, precision = 100)
	private String shortText;

	@Size(min = 2, max = 2000, message = "{local.error.message.fullTextSize}")
	@Column(name = "NW_FULL_TEXT", nullable = false, precision = 2000)
	private String fullText;

	@Column(name = "NW_CREATION_DATE", nullable = false)
	@Temporal(TemporalType.TIMESTAMP)
	private Date creationDate;

	@Column(name = "NW_MODIFICATION_DATE", nullable = false)
	private Date modificationDate;

	@LazyCollection(value = LazyCollectionOption.FALSE)
	@ManyToMany()
	@JoinTable(name = "NEWS_TAGS", joinColumns = { @JoinColumn(name = "NWTG_NEWS_ID_FK", referencedColumnName = "NW_NEWS_ID_PK") }, inverseJoinColumns = { @JoinColumn(name = "NWTG_TAG_ID_FK", referencedColumnName = "TG_TAG_ID_PK") })
	private List<Tag> tagsList;

	@LazyCollection(value = LazyCollectionOption.FALSE)
	@ManyToMany
	@JoinTable(name = "NEWS_AUTHORS", joinColumns = { @JoinColumn(name = "NWAU_NEWS_ID_FK", referencedColumnName = "NW_NEWS_ID_PK") }, inverseJoinColumns = { @JoinColumn(name = "NWAU_AUTHOR_ID_FK", referencedColumnName = "AU_AUTHOR_ID_PK") })
	private List<Author> authorsList;

	@LazyCollection(value = LazyCollectionOption.FALSE)
	@PrivateOwned
	@OneToMany(targetEntity = com.epam.news_management.entity.Comment.class, cascade = CascadeType.ALL, orphanRemoval = true)
	@JoinColumn(name = "CM_NEWS_ID_FK", referencedColumnName = "NW_NEWS_ID_PK", updatable = false)
	private List<Comment> commentList;

	@Transient
	@Min(value = 1, message = "{local.error.message.emptyAuthor}")
	private Long authorId;

	@Transient
	@NotEmpty(message = "{local.error.message.emptyTags}")
	private List<Long> tagsIdList;

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result
				+ ((authorsList == null) ? 0 : authorsList.hashCode());
		result = prime * result
				+ ((commentList == null) ? 0 : commentList.hashCode());
		result = prime * result
				+ ((creationDate == null) ? 0 : creationDate.hashCode());
		result = prime * result
				+ ((fullText == null) ? 0 : fullText.hashCode());
		result = prime
				* result
				+ ((modificationDate == null) ? 0 : modificationDate.hashCode());
		result = prime * result + ((newsId == null) ? 0 : newsId.hashCode());
		result = prime * result
				+ ((shortText == null) ? 0 : shortText.hashCode());
		result = prime * result
				+ ((tagsIdList == null) ? 0 : tagsIdList.hashCode());
		result = prime * result
				+ ((tagsList == null) ? 0 : tagsList.hashCode());
		result = prime * result + ((title == null) ? 0 : title.hashCode());
		result = prime * result + version;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		News other = (News) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (authorsList == null) {
			if (other.authorsList != null)
				return false;
		} else if (!authorsList.equals(other.authorsList))
			return false;
		if (commentList == null) {
			if (other.commentList != null)
				return false;
		} else if (!commentList.equals(other.commentList))
			return false;
		if (creationDate == null) {
			if (other.creationDate != null)
				return false;
		} else if (!creationDate.equals(other.creationDate))
			return false;
		if (fullText == null) {
			if (other.fullText != null)
				return false;
		} else if (!fullText.equals(other.fullText))
			return false;
		if (modificationDate == null) {
			if (other.modificationDate != null)
				return false;
		} else if (!modificationDate.equals(other.modificationDate))
			return false;
		if (newsId == null) {
			if (other.newsId != null)
				return false;
		} else if (!newsId.equals(other.newsId))
			return false;
		if (shortText == null) {
			if (other.shortText != null)
				return false;
		} else if (!shortText.equals(other.shortText))
			return false;
		if (tagsIdList == null) {
			if (other.tagsIdList != null)
				return false;
		} else if (!tagsIdList.equals(other.tagsIdList))
			return false;
		if (tagsList == null) {
			if (other.tagsList != null)
				return false;
		} else if (!tagsList.equals(other.tagsList))
			return false;
		if (title == null) {
			if (other.title != null)
				return false;
		} else if (!title.equals(other.title))
			return false;
		if (version != other.version)
			return false;
		return true;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public int getVersion() {
		return version;
	}

	public void setVersion(int version) {
		this.version = version;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public List<Tag> getTagsList() {
		return tagsList;
	}

	public void setTagsList(List<Tag> tagsList) {
		this.tagsList = tagsList;
	}

	public List<Author> getAuthorsList() {
		return authorsList;
	}

	public void setAuthorsList(List<Author> authorsList) {
		this.authorsList = authorsList;
	}

	public List<Comment> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<Comment> commentList) {
		this.commentList = commentList;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	public List<Long> getTagsIdList() {
		return tagsIdList;
	}

	public void setTagsIdList(List<Long> tagsIdList) {
		this.tagsIdList = tagsIdList;
	}

	@Override
	public String toString() {
		return "News [newsId=" + newsId + ", version=" + version + ", title="
				+ title + ", shortText=" + shortText + ", fullText=" + fullText
				+ ", creationDate=" + creationDate + ", modificationDate="
				+ modificationDate + ", tagsList=" + tagsList
				+ ", authorsList=" + authorsList + ", commentList="
				+ commentList + ", authorId=" + authorId + ", tagsIdList="
				+ tagsIdList + "]";
	}

}
