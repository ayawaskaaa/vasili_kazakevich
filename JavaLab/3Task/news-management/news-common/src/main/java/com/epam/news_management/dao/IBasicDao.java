package com.epam.news_management.dao;

import java.util.List;

import com.epam.news_management.dao.exception.DaoException;


/**
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */



public interface IBasicDao <T> {
	
	/**
	 * 
	 * @param object
	 * @return
	 * @throws DaoException
	 */
	public Long add(T object) throws DaoException;
	/**
	 * 
	 * @return
	 * @throws DaoException
	 */
	public List <T> getAll() throws DaoException;
	/**
	 * 
	 * @param id
	 * @return
	 * @throws DaoException
	 */
	public T get(Long id) throws DaoException;
	/**
	 * 
	 * @param object
	 * @throws DaoException
	 */
	public boolean edit(T object) throws DaoException;
	/**
	 * 
	 * @param id
	 * @throws DaoException
	 */
	public void delete (Long id) throws DaoException;
	
	

}
