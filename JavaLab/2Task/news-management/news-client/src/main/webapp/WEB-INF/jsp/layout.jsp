<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"  
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<link  rel="stylesheet" href="<c:url value="/resources/css/layout.css" />" />

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:insertAttribute name="title" ignore="true" /></title>

</head>
<body>

	<div class="mainView" align="center" >
		<div class = "header" >
			<tiles:insertAttribute name="header" />
		</div>
		<div class= "body">
			<tiles:insertAttribute name="body" />
		</div>
		<div class="footer" align="center">
			<tiles:insertAttribute name="footer" />
		</div>
	
	</div>


</body>
</html>
