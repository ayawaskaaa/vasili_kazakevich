<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page pageEncoding="UTF-8"%>
<link rel="stylesheet" 	href="<c:url value="/resources/css/home.css" />" />
<script type="text/javascript" src="<spring:url value="/resources/js/home.js"/>"></script>



	<div class="filterBlock" align="center">
		<div class="box">
			<div>
				<form:form method="get" action="filter" commandName="searchCriteria">
					<div>
						<form:select name="asd" path="authorId" >
							<form:option value="0" label="Please select the author" />
							<c:forEach var="authorList" items="${authors}">
								<c:if test="${authorList.expired==null}">
								
									<form:option value="${authorList.authorId}"
									label="${authorList.authorName}"
									selected="${sessionScope.authorId == authorList.authorId ? 'selected' : ' ' }" />
								</c:if>								
									
							</c:forEach>
						</form:select>
					</div>
					<div>
						<div class="multiselect">
							<div class="selectBox" onclick="showCheckboxes()">
								<select>
									<option>Select an option</option>
								</select>
								<div class="overSelect"></div>
							</div>
							<div id="checkboxes">
								<c:forEach var="tagsList" items="${tags}">
								
								<c:set var="contains" value="false" />
								<c:forEach var="item" items="${sessionScope.tagListId}">
												  <c:if test="${item eq tagsList.tagId}">
												    <c:set var="contains" value="true" />
												  </c:if>
								</c:forEach>								
									<label  for="${tagsList.tagId}">
										 <form:checkbox id="${tagsList.tagId}" path="tagIdList" 
											value="${tagsList.tagId}"
											checked="${contains? 'cheked' :  ''	}"	
											/>${tagsList.tagName}
									</label>
								</c:forEach>
							</div>
						</div>
					</div>

					<div>
						<input type="submit"
							value="<spring:message code="local.button.filter" />">
					</div>

				</form:form>
			</div>

			<div>
				<form action="reset" method="get">
					<input type="submit"
						value="<spring:message code="local.button.reset" />">
				</form>
			</div>
		</div>
	</div>


	<div class="newsBlock">
		<c:forEach var="newsVOList" items="${news}">
			<div class="titleBlock" align="left">
				<strong> ${newsVOList.newsItem.title}</strong> (
				<spring:message code="local.text.byAuthor" />${newsVOList.authorItem.authorName}
				)
				<div align="right">${newsVOList.newsItem.modificationDate}</div>
			</div>
			<div align="left">${newsVOList.newsItem.shortText}</div>

			<div class="box" align="right">
				<div class="tagsBlock">
					<c:forEach var="tagList" items="${newsVOList.tagItemList}">${tagList.tagName } </c:forEach>
				</div>

				<div>
					<spring:message code="local.text.comments" />
					( ${fn:length(newsVOList.comment)} )
				</div>
				<div>
					<a href="/news-client/single-news/view?&newsId=${newsVOList.newsItem.newsId}" > <spring:message code="local.button.view" /></a>
				</div>

			</div>

		</c:forEach>
	</div>

	<div class="searchResultMessage" align="center">
		<c:if test="${empty news}">
			<strong><spring:message
					code="local.text.searchResultMessage" /></strong>
		</c:if>
	</div>


	<div class="paginationBlock">
		<c:forEach var="i" begin="1" end="${pagesAmount}">
			<div>
				<form name="test" method="get" action="changePgN">

					<input type="submit" name="homePgN" value="${i}">

				</form>
			</div>
		</c:forEach>

	</div>

