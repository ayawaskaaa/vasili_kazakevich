<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<link rel="stylesheet" 	href="<c:url value="/resources/css/viewPage.css" />" />
	



	<div style=" float: left; position: absolute; left: 15px;" >
			<a href="http://localhost:8081/news-client/"><spring:message code="local.button.back" /></a>
	</div>

	<div class="viewNewsMainBlock">
	
	
		
		<div class="newsHead" style="width: 100%; clear: right; height: 50px;"  >
				<div >
				<strong> ${newsVO.newsItem.title}</strong> (
				<spring:message code="local.text.byAuthor" /> ${newsVO.authorItem.authorName}
				)
				</div>
				<div align="right" style="text-decoration:underline; float: right;" >${newsVO.newsItem.modificationDate}</div>
		</div>
		
		
		<div class="fullTextBlock" align="left" >${newsVO.newsItem.fullText}</div>
		
		<div class="commentTextBlock" align="left" >
			<c:forEach var="commentItem" items="${newsVO.comment}">
			<div>${commentItem.creationDate} </div>
			<div style="background-color: #E0E0E0; width: 30%; " ><c:out value="${commentItem.commentText}"></c:out></div>
			</c:forEach>
		</div>
		<div class="postCommentBlock" align="left"  >
			<form:form method="POST" commandName="commentTO" action="post-comment">
			<div>
				
				<form:input path="commentText" required="true" />
				<form:hidden path="newsId"  value="${newsVO.newsItem.newsId}" />
			</div>
			<div>
				
				<input type="submit"  value="<spring:message code="local.button.postComment" />" >
			</div>
			</form:form>
		</div>		
		
		
	</div>
	<c:if test="${previousNewsId!=0}">
		<div style=" float: left;position: absolute; left: 15px; " >
			<a href="view?&newsId=${previousNewsId}"><spring:message code="local.button.previous" /></a>
		</div>
	</c:if>
	<c:if test="${nextNewsId!=0}">
		<div style=" float: right; position: absolute; right: 15px;" >
			<a href="view?&newsId=${nextNewsId}"><spring:message code="local.button.next" /></a>
		</div>
	</c:if>
