package com.epam.news_client.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.news_client.controller.util.PageConfig;
import com.epam.news_management.entity.AuthorTO;
import com.epam.news_management.entity.CommentTO;
import com.epam.news_management.entity.NewsTO;
import com.epam.news_management.entity.NewsVO;
import com.epam.news_management.entity.SearchCriteria;
import com.epam.news_management.entity.TagTO;
import com.epam.news_management.service.IGeneralNewsManagementService;
import com.epam.news_management.service.exception.ServiceException;



@Controller
@RequestMapping("/")
public class ClientController {
	
	private Logger log  = Logger.getLogger(ClientController.class);
	
	public ClientController(){}
	
	@Autowired
	private IGeneralNewsManagementService service;
	
	@RequestMapping(value={ "/", "/page", "/changePgN" }, method=RequestMethod.GET)
	 public String home(Model model, HttpSession session,  @RequestParam(value="homePgN", required=false)Integer pgn) {
		SearchCriteria sessionSearchCriteria = new SearchCriteria();
		List<NewsVO> newsVO  = null;
		List<AuthorTO> authorsList=null;
		List <TagTO> tagsList = null;
		int pagesAmount = 0;
		int pageNumber = 1;		
		int itemAmount = 0;	
		sessionSearchCriteria.setAuthorId((Long) session.getAttribute("authorId"));
		sessionSearchCriteria.setTagIdList((List<Long>) session.getAttribute("tagListId"));
		
		if(pgn!=null){
			session.setAttribute("homePgN", pgn);
			}
		
		if(session.getAttribute("homePgN")!=null){
			pageNumber=Integer.parseInt(session.getAttribute("homePgN").toString());
			
		}
			
		int startIndex = PageConfig.calculateStartIndex(pageNumber);
		int endIndex = PageConfig.calculateEndIndex(pageNumber);
		
		try {								
			newsVO=service.searchByAuthorAndTag(sessionSearchCriteria, startIndex, endIndex);
			authorsList = service.getAllAuthors();
			tagsList = service.getAllTags();
			
			itemAmount=service.countNewsInSearch(sessionSearchCriteria);
			pagesAmount=PageConfig.countPageAmount(itemAmount);			
			
		} catch (Exception e) {
			log.error("Error while getting data for view page.", e);
			return "errorPage";
		}
		
		model.addAttribute("searchCriteria", new SearchCriteria());
		model.addAttribute("authorTO", new AuthorTO());
		model.addAttribute("tagIdList", new ArrayList<Long>());
		model.addAttribute("authors", authorsList);
		model.addAttribute("news", newsVO);
		model.addAttribute("tags", tagsList);
		model.addAttribute("pagesAmount", pagesAmount);
	   
	    return "home";
	 }
	
	
	@RequestMapping(value="filter", method=RequestMethod.GET)
	 public String filter(Model model, HttpSession session, SearchCriteria searchCriteria) {
		
		Long authorId= null;
		if(searchCriteria.getAuthorId()!=null){
			if(searchCriteria.getAuthorId()==0){
				authorId = null;
			}else{
				authorId=searchCriteria.getAuthorId();
			}
			session.removeAttribute("homePgN");
			session.setAttribute("authorId", authorId);
			session.setAttribute("tagListId", searchCriteria.getTagIdList());
		}
	
		return home(model, session,  null);
	 }
	
	@RequestMapping(value="reset", method=RequestMethod.GET)
	 public String reset(Model model, HttpSession session, SearchCriteria searchCriteria) {
		
		session.removeAttribute("authorId"); 
		session.removeAttribute("tagListId");
		session.removeAttribute("homePgN");
		return home(model, session,  null);
	 }
	
	


}
