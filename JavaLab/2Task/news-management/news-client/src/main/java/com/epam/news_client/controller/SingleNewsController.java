package com.epam.news_client.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.news_management.entity.CommentTO;
import com.epam.news_management.entity.NewsVO;
import com.epam.news_management.entity.SearchCriteria;
import com.epam.news_management.service.IGeneralNewsManagementService;
import com.epam.news_management.service.exception.ServiceException;

@Controller
@RequestMapping("/single-news")
public class SingleNewsController {

	private Logger log  = Logger.getLogger(SingleNewsController.class);
	
	SingleNewsController(){}
	
	@Autowired
	private IGeneralNewsManagementService service;
	
	
	@RequestMapping(value= "/post-comment", method=RequestMethod.POST)
	 public  String postComment(Model model, CommentTO comment ) {
		

		
		try {			
			service.addComment(comment);
		} catch (ServiceException e) {
			log.error("Error while getting data for view page.", e);
			return "errorPage";
		} catch (Exception e) {
			log.error("Error while getting data for view page.", e);
			return "errorPage";
		}
		

		return "redirect:/single-news/view?newsId="+comment.getNewsId();	    
	 }


	@RequestMapping(value= "/view", method=RequestMethod.GET )
	 public  String changePage( Model model,HttpSession session, @RequestParam(value="newsId", required=false)Long newsId ) {
		NewsVO newsVO = null;
		Long nextNewsId = null;
		Long previousNewsId = null;
		SearchCriteria sessionSearchCriteria = new SearchCriteria();
		sessionSearchCriteria.setAuthorId((Long) session.getAttribute("authorId"));
		sessionSearchCriteria.setTagIdList((List<Long>) session.getAttribute("tagListId"));
		
		try {
			newsVO=service.getSingleNews(newsId);
			nextNewsId= service.getNextNewsiId(sessionSearchCriteria, newsId);
			previousNewsId = service.getPreviousNewsId(sessionSearchCriteria, newsId);
			
		
			
		} catch (Exception e) {
			log.error("Error while getting data for view page.", e);
			return "errorPage";
		}
		model.addAttribute("nextNewsId",nextNewsId);
		model.addAttribute("previousNewsId", previousNewsId);
		model.addAttribute("queryString", "&newsId="+newsId);
		model.addAttribute("newsVO", newsVO);
		model.addAttribute("commentText", new CommentTO());
		model.addAttribute("commentTO", new CommentTO());
		return "viewPage";	    
	 }
	
}
