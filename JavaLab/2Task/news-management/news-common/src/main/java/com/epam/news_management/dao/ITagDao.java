package com.epam.news_management.dao;

import java.util.List;

import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.TagTO;

/**
 * TagDao interface.
 * 
 * 
 * 
 * @author Vasili_Kazakevich
 * @version 1.0
 */
public interface ITagDao extends IBasicDao<TagTO> {
	/**
	 * 
	 * @param newsId
	 * @return
	 * @throws DaoException
	 */
	public List <TagTO> getTagsByNewsId (Long newsId) throws DaoException;
	/**
	 * 
	 * @param newsId
	 * @throws DaoException
	 */
	public void unlinkByNewsId(Long newsId) throws DaoException;
	/**
	 * 
	 * @param tagId
	 * @throws DaoException
	 */
	public void unlinkByTagId(Long tagId) throws DaoException;
	/**
	 * 
	 * @param tags
	 * @param newsId
	 * @throws DaoException
	 */
	public void link(List<Long> tags, Long newsId) throws DaoException;


}
