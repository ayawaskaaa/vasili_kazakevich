package com.epam.news_management.service.impl;


import java.util.ArrayList;
import java.util.List;

import org.springframework.transaction.annotation.Transactional;

import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.AuthorTO;
import com.epam.news_management.entity.CommentTO;
import com.epam.news_management.entity.NewsTO;
import com.epam.news_management.entity.NewsVO;
import com.epam.news_management.entity.SearchCriteria;
import com.epam.news_management.entity.TagTO;
import com.epam.news_management.entity.UserTO;
import com.epam.news_management.service.IAuthorService;
import com.epam.news_management.service.ICommentService;
import com.epam.news_management.service.IGeneralNewsManagementService;
import com.epam.news_management.service.INewsService;
import com.epam.news_management.service.ITagService;
import com.epam.news_management.service.IUserService;
import com.epam.news_management.service.exception.ServiceException;


/**
 * GeneralNewsManagementService implementation.
 * 
 * Facade class that provides simplified methods required
 * by client and delegates calls to methods of existing system classes. 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
@Transactional(rollbackFor=Exception.class)

public class GeneralNewsManagementServiceImpl implements IGeneralNewsManagementService {
	
	private INewsService newsService = null;
	private ICommentService commentService = null;
	private IAuthorService authorService = null;
	private ITagService tagService = null;
	private IUserService userService=null;
	
	public GeneralNewsManagementServiceImpl(){}
	

	
	public INewsService getNewsService() {
		return newsService;
	}
	public void setNewsService(INewsService newsService) {
		this.newsService = newsService;
	}
	public ICommentService getCommentService() {
		return commentService;
	}
	public void setCommentService(ICommentService commentService) {
		this.commentService = commentService;
	}
	public IAuthorService getAuthorService() {
		return authorService;
	}
	public void setAuthorService(IAuthorService authorService) {
		this.authorService = authorService;
	}
	public ITagService getTagService() {
		return tagService;
	}
	public void settagService(ITagService tagService) {
		this.tagService = tagService;
	}
	public IUserService getUserService() {
		return userService;
	}
	public void setUserService(IUserService userService) {
		this.userService = userService;
	}
	
	
	/**
	 * Adds single news transfer object.
	 * 
	 * @param news
	 * @throws ServiceException
	 */

	
	@Override
	public void addNews(NewsTO news) throws ServiceException {
		
		newsService.addNews(news);
	}

	
	/**
	 * Adds single author transfer object.
	 * 
	 * @param author
	 * @throws ServiceException
	 */
	@Override
	public void addAuthor(AuthorTO author) throws ServiceException {
		authorService.addAuthor(author);
		
	}
	
	/**
	 * Adds single comment transfer object.
	 * 
	 * @param comment
	 * @throws ServiceException
	 */
	@Override
	public void addComment(CommentTO comment) throws ServiceException {
		commentService.addComment(comment);
		
	}


	/**
	 * Deletes tag entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void deleteTag(Long tagId) throws ServiceException {
		tagService.unlinkTagWithNewsByTagId(tagId);
		tagService.deleteTag(tagId);
		
	}
	
	/**
	 * Deletes author entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void deleteAuthor(Long authorId) throws ServiceException {
		
		authorService.unlinkAutorWhithNewsByAuthorId(authorId);
		authorService.deleteAuthor(authorId);
		
	}
	/**
	 * Deletes comment entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void deleteComment(Long commentId) throws ServiceException {
		commentService.deleteComment(commentId);
		
	}
	
	/**
	 * Adds single tag transfer object.
	 * 
	 * @param tag
	 * @throws ServiceException
	 */
	@Override
	public void addTag(TagTO tag) throws ServiceException {
		
		tagService.addTag(tag);
		
		
	}


	/**
	 * Edits news entity in data repository.
	 * 
	 * @param newsTO
	 * @throws ServiceException
	 */
	@Override
	public void editNews(NewsTO newsTO) throws ServiceException {
		newsService.editNews(newsTO);
		
	}


	/**
	 * Searches certain news according search criteria, start ans end index.
	 * 
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return list of news according search criteria, start and end index.
	 * @throws ServiceException
	 */
	@Override
	public List<NewsVO> searchByAuthorAndTag(SearchCriteria searchCriteria, int start, int end) throws ServiceException {
		List<NewsVO> newsVOList = new ArrayList<NewsVO>();
		List<NewsTO> newsList = null;
		newsList=newsService.search(searchCriteria, start, end);
		
		for(NewsTO i:newsList){
			NewsVO newsVO  = new NewsVO(); 
			newsVO.setNewsItem(i);
			newsVO.setAuthorItem(authorService.getAuthorByNewsId(i.getNewsId()));
			newsVO.setComment(commentService.getCommentsByNewsId(i.getNewsId()));
			newsVO.setTagItemList(tagService.getTagsByNewsId(i.getNewsId()));
			
			
			
			newsVOList.add(newsVO);
			
		}
		
		return newsVOList;
	}


	/**
	 * Counts items in search result.
	 * 
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return
	 * @throws ServiceException
	 */
	@Override
	public int countNewsInSearch(SearchCriteria searchCriteria) throws ServiceException {
		int countresult = newsService.countNewsInSearch(searchCriteria);
		return countresult;
	}


	/**
	 * Produces actions for adding in to data repository.
	 * 
	 * @param newsVO
	 * @throws ServiceException
	 */
	@Override
	public Long addComplexNews(NewsVO newsVO) throws ServiceException {
		
		Long newsId=null;
		NewsTO news = newsVO.getNewsItem();
		Long authorId = newsVO.getAuthorId();
		List <Long> tagsIdList = newsVO.getTagsIdList()	;
		newsId=newsService.addNews(news);
		authorService.linkAuthorWhithNews(authorId, newsId);
		tagService.linkTagsWithNews(tagsIdList, newsId);
		return newsId;
		
		
	}


	/**
	 * Deletes news entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void deleteNews(Long newsId) throws ServiceException {
		
		commentService.deleteCommentByNewsId(newsId);
		tagService.unlinkTagWithNews(newsId);
		authorService.unlinkAutorWhithNewsByNewsId(newsId);
		newsService.deleteNews(newsId);
		
		
	}


	/**
	 * Produces actions to get news value object.
	 * 
	 * @param newsId
	 * @return
	 * @throws ServiceException
	 */
	@Override
	public NewsVO getSingleNews(Long newsId) throws ServiceException {
		NewsVO newsVO = new NewsVO();
		newsVO.setNewsItem(newsService.getSingleNews(newsId));
		newsVO.setComment(commentService.getCommentsByNewsId(newsId));
		newsVO.setAuthorItem(authorService.getAuthorByNewsId(newsId));
		newsVO.setTagItemList(tagService.getTagsByNewsId(newsId));
		return newsVO;
	}

	/**
	 * Gets all authors from data repository.
	 * 
	 * @return
	 * @throws ServiceException 
	 */

	@Override
	public List<AuthorTO> getAllAuthors() throws ServiceException {
		List<AuthorTO> authorTOList = null;
		authorTOList=authorService.getAllAuthors();
	
		return authorTOList;
	}


	/**
	 * Gets all tags from data repository.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	@Override
	public List<TagTO> getAllTags() throws ServiceException {
		List<TagTO> tagsList = null;
		tagsList=tagService.getAllTags();
		return tagsList;
	}

	/**
	 * Gets the next news id in search.
	 * 
	 * @param searchCriteria
	 * @param currentNewsId
	 * @return
	 * @throws DaoException
	 */
	
	@Override
	public Long getNextNewsiId(SearchCriteria searchCriteria, Long currentNewsId)
			throws ServiceException {
		Long nextNewsId = null;
		nextNewsId=newsService.getNextNewsId(searchCriteria, currentNewsId);
		return nextNewsId;
	}


	/**
	 * Gets the previous news id in search.
	 * 
	 * @param searchCriteria
	 * @param currentNewsId
	 * @return
	 * @throws DaoException
	 */
	@Override
	public Long getPreviousNewsId(SearchCriteria searchCriteria,
			Long currentNewsId) throws ServiceException {
		Long previousNewsId=null;
		previousNewsId = newsService.getPreviousNewsId(searchCriteria, currentNewsId);
		return previousNewsId;
	}


	/**
	 * Gets user entity from data repository is it exists.
	 * 
	 * @param user
	 * @return
	 * @throws ServiceException
	 */
	@Override
	public UserTO login(UserTO user) throws ServiceException {
		UserTO userTO =null;
		userTO=userService.login(user);

		return userTO;
	}

	/**
	 * Updates news, author, tags data .
	 * 
	 * @throws ServiceException
	 */

	@Override
	public void updateComplexNews(NewsVO newsVO) throws ServiceException {
		
		
		authorService.unlinkAutorWhithNewsByNewsId(newsVO.getNewsItem().getNewsId());
		tagService.unlinkTagWithNews(newsVO.getNewsItem().getNewsId());
		
		newsService.editNews(newsVO.getNewsItem());
		authorService.linkAuthorWhithNews(newsVO.getAuthorId(), newsVO.getNewsItem().getNewsId());
		tagService.linkTagsWithNews(newsVO.getTagsIdList(), newsVO.getNewsItem().getNewsId());
		
	}

	

	/**
	 * Updates tag data.
	 * 
	 * @throws ServiceException
	 */
	@Override
	public void updateTag(TagTO tagTO) throws ServiceException {
		tagService.editTag(tagTO);
		
	}


	/**
	 * Updates author data.
	 * 
	 * @throws ServiceException
	 */
	@Override
	public void updateAuthor(AuthorTO authorTO) throws ServiceException {
		authorService.editAuthor(authorTO);
		
	}



}
