package com.epam.news_management.service.impl;

import java.util.List;

import org.apache.log4j.Logger;

import com.epam.news_management.dao.INewsDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.NewsTO;
import com.epam.news_management.entity.NewsVO;
import com.epam.news_management.entity.SearchCriteria;
import com.epam.news_management.service.INewsService;
import com.epam.news_management.service.exception.ServiceException;


/**
 * NewsService implementation.
 * 
 * Contains methods for operating news entity.
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class NewsServiceImpl implements INewsService {
	
	private INewsDao newsDao = null;
	
	private static final Logger log = Logger.getLogger(INewsDao.class);
	
	public NewsServiceImpl(){}
	
	public INewsDao getNewsDao() {
		return newsDao;
	}

	public void setNewsDao(INewsDao newsDao) {
		this.newsDao = newsDao;
	}


	/**
	 * Adds single news transfer object.
	 * 
	 * @param news
	 * @return ID of new inserted news.
	 * @throws ServiceException
	 */
	@Override
	public Long addNews(NewsTO news) throws ServiceException {
		try {
			return newsDao.add(news);
		} catch (DaoException e) {
			log.error("Exception while edding news.", e);
			throw new ServiceException("Exception while edding news.", e);
		}
	}

	/**
	 * Edits news entity in data repository.
	 * 
	 * @param news
	 * @throws ServiceException
	 */
	@Override
	public void editNews(NewsTO news) throws ServiceException {
		try {
			newsDao.edit(news);
		} catch (DaoException e) {
			log.error("Exception while news edit.", e);
			throw new ServiceException("Exception while news edit.",e);
		}
	}

	

	/**
	 * Produces actions to get news value object.
	 * 
	 * @param newsId
	 * @return news transfer object. 
	 * @throws ServiceException
	 */
	@Override
	public NewsTO getSingleNews(Long newsId) throws ServiceException {
		NewsTO news = null;
		try {
			news=newsDao.get(newsId);
		} catch (DaoException e) {
			log.error("Exception while getting news.", e);
			throw new ServiceException("Exception while getting news.", e);
		}
		return news;
	}

	/**
	 * Deletes news entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	@Override
	public void deleteNews(Long newsId) throws ServiceException {
		try {
			newsDao.delete(newsId);
		} catch (DaoException e) {
			log.error("Exception while deleting news.", e);
			throw new ServiceException("Exception while deleting news.", e);
		}

		
	}

	/**
	 * Counts items in search result.
	 * 
	 * @param searchCriteria
	 * @return count result.
	 * @throws ServiceException
	 */
	@Override
	public int countNewsInSearch(SearchCriteria searchCriteria) throws ServiceException {
		int totalNewsAmount = 0;
		try {
			totalNewsAmount=newsDao.countNewsInSearch(searchCriteria);
		} catch (DaoException e) {
			log.error("Exception while counting news.", e);
			throw new ServiceException("Exception while counting news.", e);
		}
		return totalNewsAmount;
	}

	/**
	 * Searches certain news according search criteria, start ans end index.
	 * 
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return list of news according search criteria, start and end index.
	 * @throws ServiceException
	 */
	@Override
	public List<NewsTO> search(SearchCriteria searchCriteria, int start, int end)
			throws ServiceException {
		List<NewsTO> searchResult = null;
		try {
			searchResult= newsDao.search(searchCriteria, start, end);
		} catch (DaoException e) {
			log.error("Exception while searching news.", e);
			throw new ServiceException("Exception while searching news.", e);
		}
		return searchResult;
	}
	
	/**
	 * Gets the next news id in search.
	 * 
	 * @param searchCriteria
	 * @param currentNewsId
	 * @return
	 * @throws DaoException
	 */
	@Override
	public Long getNextNewsId(SearchCriteria searchCriteria, Long currentNewsId)
			throws ServiceException {
		Long nextNewsId = null;
		
		try {
			nextNewsId = newsDao.getNextNewsId(searchCriteria, currentNewsId);
		} catch (DaoException e) {
			log.error("Exception while getting news ID.", e);
			throw new ServiceException("Exception while getting news ID.", e);
		}
		
		return nextNewsId;
	}
	
	/**
	 * Gets the previous news id in search.
	 * 
	 * @param searchCriteria
	 * @param currentNewsId
	 * @return
	 * @throws DaoException
	 */
	@Override
	public Long getPreviousNewsId(SearchCriteria searchCriteria,
			Long currentNewsId) throws ServiceException {
		Long previousNewsId=null;
		
		try {
			previousNewsId = newsDao.getPreviousNewsId(searchCriteria, currentNewsId);
		} catch (DaoException e) {
			log.error("Exception while getting news ID.", e);
			throw new ServiceException("Exception while getting news ID.", e);
		}

		return previousNewsId;
	}

	

		

}
