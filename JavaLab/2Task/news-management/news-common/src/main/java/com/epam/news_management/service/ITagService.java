package com.epam.news_management.service;

import java.util.List;

import com.epam.news_management.entity.TagTO;
import com.epam.news_management.service.exception.ServiceException;



/**
 * TagService interface.
 * 
 * Contains methods for operating tag entity.
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public interface ITagService {
	/**
	 * Adds single tag transfer object.
	 * 
	 * @param tag
	 * @throws ServiceException
	 */
	public void addTag(TagTO tag) throws ServiceException;
	
	/**
	 * Deletes tag entity from data repository.
	 * 
	 * @param tagId
	 * @throws ServiceException
	 */
	public void deleteTag(Long tagId) throws ServiceException;
	
	/**
	 * Destroys links between tags table and news table, using ID of tag.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void unlinkTagWithNews(Long newsId) throws ServiceException;
	
	/**
	 * Destroys links between tags table and news table, using ID of news.
	 * 
	 * @param tagId
	 * @throws ServiceException
	 */
	public void unlinkTagWithNewsByTagId(Long tagId) throws ServiceException;
	
	/**
	 * Gets list of certain tags entity according news id.
	 * 
	 * @param newsId
	 * @return List of tag entities linked with concrete news.
	 * @throws ServiceException
	 */
	
	public List<TagTO> getTagsByNewsId(Long newsId) throws ServiceException;
	
	/**
	 * Edits certain tag.
	 * 
	 * @param tag
	 * @throws ServiceException
	 */
	public void editTag(TagTO tag) throws ServiceException;
	
	/**
	 * Links tags with news according to their ID's.
	 * 
	 * @param tagList
	 * @param newsId
	 * @throws ServiceException
	 */
	public void linkTagsWithNews(List<Long> tagsIdList, Long newsId) throws ServiceException;
	
	/**
	 * Gets all tags from data repository.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	public List<TagTO> getAllTags() throws ServiceException;
	

}

