package com.epam.news_management.dao;

import java.util.List;

import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.CommentTO;


/**
 * 
 * @author Vasili_Kazakevich
 * @version 1.0
 */
public interface ICommentDao extends IBasicDao<CommentTO> {
	
	/**
	 * 
	 * @param newsId
	 * @return
	 * @throws DaoException
	 */
	public List <CommentTO> getCommentsByNewsId(Long newsId) throws DaoException;

	/**
	 * 
	 * @param newsId
	 * @throws DaoException
	 */
	public void deleteByNewsId(Long newsId) throws DaoException;

	

}
