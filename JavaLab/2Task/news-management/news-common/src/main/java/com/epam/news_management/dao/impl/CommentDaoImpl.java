package com.epam.news_management.dao.impl;

/**
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.news_management.dao.ICommentDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.dao.util.DAOUtil;
import com.epam.news_management.entity.CommentTO;



public class CommentDaoImpl implements ICommentDao {
	
	private static final String SQL_GET_ALL_COMMENTS_SELECT ="SELECT cm_comment_id_pk, cm_news_id_fk, cm_comment_text, cm_creation_date FROM comments ";
	
	private static final String SQL_ADD_COMMENT_INSERT = "INSERT INTO comments (cm_comment_id_pk, cm_news_id_fk, cm_comment_text,"
			+ " cm_creation_date) VALUES (CM_COMMENT_ID_PK_SEQ.nextval, ?, ?, CURRENT_TIMESTAMP) ";
	
	private static final String SQL_DELETE_COMMENT_BY_NEWS_ID_DELETE = "DELETE FROM comments WHERE cm_news_id_fk = ? ";
	
	private static final String SQL_EDIT_COMMENT_UPDATE = "UPDATE comments SET  cm_comment_text= ? ,"
			+ " cm_creation_date= CURRENT_TIMESTAMP   WHERE cm_comment_id_pk = ? ";
	
	private static final String SQL_GET_COMMENTS_BY_NEWS_ID_SELECT = "SELECT cm_comment_id_pk, cm_news_id_fk, cm_comment_text, cm_creation_date "
			+ "FROM comments WHERE cm_news_id_fk = ? ";
	
	private static final String SQL_DELETE_COMMENT_BY_COMMENT_ID_DELETE = "DELETE FROM comments WHERE cm_comment_id_pk = ? ";

	private static final String SQL_GET_COMMENTS_BY_COMMENT_ID_SELECT = "SELECT cm_comment_id_pk, cm_news_id_fk, cm_comment_text, cm_creation_date "
			+ "FROM comments WHERE cm_comment_id_pk = ? ";
	
	private DataSource dataSource = null;
	
	
	public CommentDaoImpl(){}
	
	public CommentDaoImpl(DataSource ds){
		
		this.dataSource=ds;
		
	}
	
	public DataSource getDataSource() {
		return dataSource;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}



	@Override
	public Long add(CommentTO comment) throws DaoException {
		Long commentId = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		String column[]={"cm_comment_id_pk"};
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_ADD_COMMENT_INSERT, column);
			ps.setLong(1, comment.getNewsId());
			ps.setString(2, comment.getCommentText());
			
			ps.executeUpdate();
			
			rs = ps.getGeneratedKeys();
			if(rs != null && rs.next()) {
				commentId = rs.getLong(1);
			}
			
			
		} catch (SQLException e) {
					throw new DaoException("Exception while adding new comment.", e);
				}finally{
					DAOUtil.closeConnection(con, ps, rs, dataSource);
				}
		return commentId;
	}

	@Override
	public List<CommentTO> getAll() throws DaoException {
		Connection con = null;
		Statement st = null;
		ResultSet rs = null;
		List<CommentTO> commentList = new ArrayList<CommentTO>();
		
					
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			st=con.createStatement();
			
			rs=st.executeQuery(SQL_GET_ALL_COMMENTS_SELECT);
			if(rs != null) {
				while(rs.next()){
					commentList.add(buildComment(rs));
				}
				
			}			
			
		} catch (SQLException e) {
					throw new DaoException("Exception while getting comments.", e);
				}finally{
					DAOUtil.closeConnection(con, st, rs, dataSource);
				}
		return commentList;
	}

	@Override
	public CommentTO get(Long commentId) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		CommentTO comment = null;
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_GET_COMMENTS_BY_COMMENT_ID_SELECT);
			ps.setLong(1, commentId);
			
			rs=ps.executeQuery();
			if(rs != null) {	
				while(rs.next()){
					comment=buildComment(rs);			
				}
			}
			
			
		} catch (SQLException e) {
					throw new DaoException("Exception while getting comment.", e);
				}finally{
					DAOUtil.closeConnection(con, ps, rs, dataSource);
				}
		return comment;
	}

	@Override
	public void edit(CommentTO commentTO) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		Long commentId= commentTO.getCommentId();
		Long newsId = commentTO.getNewsId();
		String text = commentTO.getCommentText();
		
		
		
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_EDIT_COMMENT_UPDATE);
		
			ps.setString(1, text);
			ps.setLong(2, commentId);
			
			ps.executeUpdate();
			
			
		} catch (SQLException e) {
					throw new DaoException("Exception while editing comment.", e);
				}finally{
					DAOUtil.closeConnection(con, ps, dataSource);
				}
		
	}

	@Override
	public void delete(Long commentId) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		
		
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_DELETE_COMMENT_BY_COMMENT_ID_DELETE);
			ps.setLong(1, commentId);
			ps.executeUpdate();
			
			
		} catch (SQLException e) {
					throw new DaoException("Exception while deleting comment.", e);
				}finally{
					DAOUtil.closeConnection(con, ps, dataSource);
				}
		
	}

	@Override
	public List<CommentTO> getCommentsByNewsId(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		List<CommentTO> commentList = new ArrayList<CommentTO>();
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_GET_COMMENTS_BY_NEWS_ID_SELECT);
			ps.setLong(1, newsId);
			
			
			rs=ps.executeQuery();
				if(rs != null) {
					while(rs.next()){
						commentList.add(buildComment(rs));
					}
					
				}
			
			
		} catch (SQLException e) {
					throw new DaoException(".", e);
				}finally{
					DAOUtil.closeConnection(con, ps, rs, dataSource);
				}
		return commentList;
	}

	@Override
	public void deleteByNewsId(Long newsId) throws DaoException {
		Connection con = null;
		PreparedStatement ps = null;
		
		
		try{
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_DELETE_COMMENT_BY_NEWS_ID_DELETE);
			ps.setLong(1, newsId);
			ps.executeUpdate();
			
			
			
		} catch (SQLException e) {
					throw new DaoException("Exception while deleting comment.", e);
				}finally{
					DAOUtil.closeConnection(con, ps, dataSource);
				}
		
	}



	private CommentTO buildComment(ResultSet rs) throws SQLException {
		CommentTO comment = new CommentTO();
		
		comment.setCommentId(rs.getLong(1));
		comment.setNewsId(rs.getLong(2));
		comment.setCommentText(rs.getString(3));
		comment.setCreationDate(rs.getTimestamp(4));
		
		return comment;
	}

	


}
