package com.epam.news_management.entity;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.AssertFalse;
import javax.validation.constraints.AssertTrue;
import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.NotEmpty;

/**
 * Author entity class.
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class NewsVO {
	
	private static final long serialVersionUID = -304637856394596079L;
	@Valid
	private NewsTO newsItem;
	
	private AuthorTO authorItem;
	
	private List<CommentTO> comment;
	
	private List<TagTO> tagItemList;
	@NotEmpty(message="{local.error.message.emptyTags}")
	private List<Long>tagsIdList ;
	
	
	@Min(value = 1, message="{local.error.message.emptyAuthor}" )
	private Long authorId;
	
	

	public NewsTO getNewsItem() {
		return newsItem;
	}

	public void setNewsItem(NewsTO newsItem) {
		this.newsItem = newsItem;
	}

	public AuthorTO getAuthorItem() {
		return authorItem;
	}

	public void setAuthorItem(AuthorTO authorItem) {
		this.authorItem = authorItem;
	}

	public List<CommentTO> getComment() {
		return comment;
	}

	public void setComment(List<CommentTO> comment) {
		this.comment = comment;
	}

	public List<TagTO> getTagItemList() {
		return tagItemList;
	}

	public void setTagItemList(List<TagTO> tagItemList) {
		this.tagItemList = tagItemList;
	}

	public List<Long> getTagsIdList() {
		return tagsIdList;
	}

	public void setTagsIdList(List<Long> tagsIdList) {
		this.tagsIdList = tagsIdList;
	}

	public Long getAuthorId() {
		return authorId;
	}

	public void setAuthorId(Long authorId) {
		this.authorId = authorId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((authorId == null) ? 0 : authorId.hashCode());
		result = prime * result
				+ ((authorItem == null) ? 0 : authorItem.hashCode());
		result = prime * result + ((comment == null) ? 0 : comment.hashCode());
		result = prime * result
				+ ((newsItem == null) ? 0 : newsItem.hashCode());
		result = prime * result
				+ ((tagItemList == null) ? 0 : tagItemList.hashCode());
		result = prime * result
				+ ((tagsIdList == null) ? 0 : tagsIdList.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		NewsVO other = (NewsVO) obj;
		if (authorId == null) {
			if (other.authorId != null)
				return false;
		} else if (!authorId.equals(other.authorId))
			return false;
		if (authorItem == null) {
			if (other.authorItem != null)
				return false;
		} else if (!authorItem.equals(other.authorItem))
			return false;
		if (comment == null) {
			if (other.comment != null)
				return false;
		} else if (!comment.equals(other.comment))
			return false;
		if (newsItem == null) {
			if (other.newsItem != null)
				return false;
		} else if (!newsItem.equals(other.newsItem))
			return false;
		if (tagItemList == null) {
			if (other.tagItemList != null)
				return false;
		} else if (!tagItemList.equals(other.tagItemList))
			return false;
		if (tagsIdList == null) {
			if (other.tagsIdList != null)
				return false;
		} else if (!tagsIdList.equals(other.tagsIdList))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "NewsVO [newsItem=" + newsItem + ", authorItem=" + authorItem
				+ ", comment=" + comment + ", tagItemList=" + tagItemList
				+ ", tagsIdList=" + tagsIdList + ", AuthorId=" + authorId + "]";
	}

	
	

}
