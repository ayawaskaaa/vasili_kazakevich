package com.epam.news_management.service;

import java.util.List;

import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.AuthorTO;
import com.epam.news_management.entity.CommentTO;
import com.epam.news_management.entity.NewsTO;
import com.epam.news_management.entity.NewsVO;
import com.epam.news_management.entity.SearchCriteria;
import com.epam.news_management.entity.TagTO;
import com.epam.news_management.entity.UserTO;
import com.epam.news_management.service.exception.ServiceException;

;

public interface IGeneralNewsManagementService {
	
	/**
	 * Adds single news transfer object.
	 * 
	 * @param news
	 * @throws ServiceException
	 */
	public void addNews(NewsTO news) throws ServiceException;
	
	/**
	 * Produces actions for adding in to data repository.
	 * 
	 * @param newsVO
	 * @throws ServiceException
	 */
	public Long addComplexNews(NewsVO newsVO) throws ServiceException;
	
	/**
	 * Adds single author transfer object.
	 * 
	 * @param author
	 * @throws ServiceException
	 */
	public void addAuthor(AuthorTO author) throws ServiceException;
	
	/**
	 * Adds single comment transfer object.
	 * 
	 * @param comment
	 * @throws ServiceException
	 */
	public void addComment(CommentTO comment) throws ServiceException;
	
	/**
	 * Adds single tag transfer object.
	 * 
	 * @param tag
	 * @throws ServiceException
	 */
	public void addTag(TagTO tag) throws ServiceException;
	
	/**
	 * Deletes news entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteNews(Long newsId) throws ServiceException;
	
	/**
	 * Deletes tag entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteTag(Long newsId) throws ServiceException;
	
	/**
	 * Deletes author entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteAuthor(Long authorId) throws ServiceException;
	
	/**
	 * Deletes comment entity from data repository.
	 * 
	 * @param newsId
	 * @throws ServiceException
	 */
	public void deleteComment(Long commentId) throws ServiceException;
	
	
	/**
	 * Edits news entity in data repository.
	 * 
	 * @param newsTO
	 * @throws ServiceException
	 */
	public void editNews(NewsTO newsTO) throws ServiceException;
	
	/**
	 * Produces actions to get news value object.
	 * 
	 * @param newsId
	 * @return
	 * @throws ServiceException
	 */
	public NewsVO getSingleNews(Long newsId) throws ServiceException;
	
	/**
	 * Searches certain news according search criteria, start ans end index.
	 * 
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return list of news according search criteria, start and end index.
	 * @throws ServiceException
	 */
	public List<NewsVO> searchByAuthorAndTag(SearchCriteria searchCriteria, int start, int end ) throws ServiceException;
	
	/**
	 * Counts items in search result.
	 * 
	 * @param searchCriteria
	 * @param start
	 * @param end
	 * @return
	 * @throws ServiceException
	 */
	public int countNewsInSearch(SearchCriteria searchCriteria) throws ServiceException;
	
	/**
	 * Gets all authors from data repository.
	 * 
	 * @return
	 * @throws ServiceException 
	 */
	public List<AuthorTO> getAllAuthors() throws ServiceException;
	
	/**
	 * Gets all tags from data repository.
	 * 
	 * @return
	 * @throws ServiceException
	 */
	public List<TagTO> getAllTags() throws ServiceException;
	
	/**
	 * Gets the next news id in search.
	 * 
	 * @param searchCriteria
	 * @param currentNewsId
	 * @return
	 * @throws DaoException
	 */
	public Long getNextNewsiId(SearchCriteria searchCriteria, Long currentNewsId) throws ServiceException;
	
	/**
	 * Gets the previous news id in search.
	 * 
	 * @param searchCriteria
	 * @param currentNewsId
	 * @return
	 * @throws DaoException
	 */
	public Long getPreviousNewsId (SearchCriteria searchCriteria, Long currentNewsId) throws ServiceException;
	
	/**
	 * Gets user entity from data repository is it exists.
	 * 
	 * @param user
	 * @return
	 * @throws ServiceException
	 */
	public UserTO login (UserTO user) throws ServiceException;
	/**
	 * Updates news, author, tags data .
	 * 
	 * @throws ServiceException
	 */
	public void updateComplexNews(NewsVO newsVO) throws ServiceException;

	/**
	 * Updates tag data.
	 * 
	 * @throws ServiceException
	 */
	public void updateTag(TagTO tagTO) throws ServiceException;
	/**
	 * Updates author data.
	 * 
	 * @throws ServiceException
	 */
	public void updateAuthor(AuthorTO authorTO) throws ServiceException;

}

