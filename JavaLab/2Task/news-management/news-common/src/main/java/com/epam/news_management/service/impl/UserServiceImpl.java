package com.epam.news_management.service.impl;

import org.apache.log4j.Logger;

import com.epam.news_management.dao.IUserDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.UserTO;
import com.epam.news_management.service.IUserService;
import com.epam.news_management.service.exception.ServiceException;

/**
 * User interface implementation.
 * 
 * Contains methods for operating user entity.
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class UserServiceImpl implements IUserService {
	
	private static final Logger log = Logger.getLogger(UserServiceImpl.class);
	private IUserDao userDao=null;
	


	public void setUserDao(IUserDao userDao) {
		this.userDao = userDao;
	}


	UserServiceImpl(){}
	

	/**
	 * Gets user entity from data repository is it exists.
	 * 
	 * @param user
	 * @return
	 * @throws ServiceException
	 */
	@Override
	public UserTO login(UserTO user) throws ServiceException {

		UserTO userTO=null;
		
		try {
			userTO=userDao.login(user);
		} catch (DaoException e) {
			log.error("Exception while logining.", e);
			throw new ServiceException("Exception while logining.", e);
		} 
		return userTO;
	}

}
