package com.epam.news_management.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.news_management.dao.IUserDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.dao.util.DAOUtil;
import com.epam.news_management.entity.UserTO;

public class UserDaoImpl implements IUserDao {
	
	private static final String SQL_SELECT_USER="SELECT  u.us_user_id_pk, u.us_user_name, u.us_login, r.RL_ROLE_NAME "
			+ "FROM users u JOIN ROLES r ON us_user_id_pk=rl_user_id_fk "
			+ "WHERE us_login = ?  and us_password = ?  ";
	
	private DataSource dataSource = null;
	
	UserDaoImpl(){}
	UserDaoImpl(DataSource ds){
		this.dataSource=ds;
	}

	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
	}
	
	@Override
	public UserTO login(UserTO user) throws DaoException{
		
		String password = user.getPassword();
		String login = user.getUserLogin();
		UserTO authorizedUser = null;
		Connection con = null;
		PreparedStatement ps = null;
		ResultSet rs = null;
		try {
			con = DataSourceUtils.doGetConnection(dataSource);
			ps = con.prepareStatement(SQL_SELECT_USER);			
			ps.setString(1,login);
			ps.setString(2,password);
			
			rs=ps.executeQuery();
			
			if(rs != null) {
				
				while(rs.next()){
				
					authorizedUser=buildUser(rs);
				}
				
				
			}
			
		} catch (SQLException e) {
			throw new DaoException("Exception while getting user.", e);
		}finally{
			DAOUtil.closeConnection(con, ps, rs, dataSource);
		}



		
		return authorizedUser;
	}
	private UserTO buildUser(ResultSet rs) throws SQLException {
		UserTO userTO = new UserTO();
		userTO.setUserId(rs.getLong(1));
		userTO.setUserName(rs.getString(2));
		userTO.setUserLogin(rs.getString(3));
		userTO.setRole(rs.getString(4));
		return userTO;
	}

}
