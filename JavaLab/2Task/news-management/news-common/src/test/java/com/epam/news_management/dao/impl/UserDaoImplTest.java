package com.epam.news_management.dao.impl;

import static org.junit.Assert.*;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;

import com.epam.news_management.dao.IUserDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.UserTO;
@DataSet(value="NewsManagementDataSet.xml",loadStrategy = CleanInsertLoadStrategy.class)
public class UserDaoImplTest extends UnitilsJUnit4 {
	
	private IUserDao userDao;
	
	@TestDataSource
	private DataSource dataSource;

	@Before
	public void setUp() throws Exception {
		assertNotNull(dataSource);
		userDao = new UserDaoImpl(dataSource);
		assertNotNull(userDao);
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void loginTest() throws DaoException {
		UserTO user=new UserTO();
		UserTO expectedUserTO=null;
		user.setUserLogin("testLogin");
		user.setPassword("testPassword");
		expectedUserTO=userDao.login(user);
		System.out.println(expectedUserTO.toString());
		assertNotNull(expectedUserTO);
	}

}
