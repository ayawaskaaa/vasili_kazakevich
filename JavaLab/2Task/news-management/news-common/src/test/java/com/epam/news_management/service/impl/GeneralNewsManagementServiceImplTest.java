package com.epam.news_management.service.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;
















import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyListOf;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.news_management.entity.AuthorTO;
import com.epam.news_management.entity.CommentTO;
import com.epam.news_management.entity.NewsTO;
import com.epam.news_management.entity.NewsVO;
import com.epam.news_management.entity.SearchCriteria;
import com.epam.news_management.entity.TagTO;
import com.epam.news_management.service.IAuthorService;
import com.epam.news_management.service.ICommentService;
import com.epam.news_management.service.INewsService;
import com.epam.news_management.service.ITagService;
import com.epam.news_management.service.exception.ServiceException;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:NewsManagementSpringContexTest.xml"})
public class GeneralNewsManagementServiceImplTest {
	
	
	@Mock
	private INewsService newsServiceMock;
	
	@Mock
	private IAuthorService authorServiceMock;
	
	@Mock
	private ITagService tagServiceMock;
	
	@Mock
	private ICommentService commentServiceMock;
	
	@Mock
	private NewsVO newsVOMock;
	
	
	@InjectMocks
	@Autowired
	private GeneralNewsManagementServiceImpl generalService;
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		 assertNotNull(newsServiceMock);
		 assertNotNull(authorServiceMock);
		 assertNotNull(tagServiceMock);
		 assertNotNull(commentServiceMock);
		 
		
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void addNewsTest() throws ServiceException {
		
		when(newsServiceMock.addNews(new NewsTO())).thenReturn(anyLong());
		generalService.addNews(new NewsTO());
		verify(newsServiceMock, times(1)).addNews(new NewsTO());
		
	}
	
	@Test
	public void addAuthorTest() throws ServiceException {
		
		when(authorServiceMock.addAuthor(new AuthorTO())).thenReturn(anyLong());
		generalService.addAuthor(new AuthorTO());
		verify(authorServiceMock, times(1)).addAuthor(new AuthorTO());
		
	}
	
	@Test
	public void addCommentTest() throws ServiceException {
		when(commentServiceMock.addComment(new CommentTO())).thenReturn(anyLong());
		generalService.addComment(new CommentTO());
		verify(commentServiceMock, times(1)).addComment(new CommentTO());
		
	}
	
	@Test
	public void deleteTagTest() throws ServiceException {
		generalService.deleteTag(anyLong());
		verify(tagServiceMock, times(1)).deleteTag(anyLong());
		
	}
	
	@Test
	public void deleteAuthorTest() throws ServiceException {
		generalService.deleteAuthor(anyLong());
		verify(authorServiceMock, times(1)).deleteAuthor(anyLong());
		
	}
	
	@Test
	public void deleteCommentTest() throws ServiceException {
		generalService.deleteComment(anyLong());
		verify(commentServiceMock, times(1)).deleteComment(anyLong());
		
	}
	
	@Test
	public void addTagTest() throws ServiceException {
		
		generalService.addTag(new TagTO());
		verify(tagServiceMock, times(1)).addTag(new TagTO());
		
	}
	
	@Test
	public void editNewsTest() throws ServiceException {
		generalService.editNews(new NewsTO());
		verify(newsServiceMock, times(1)).editNews(new NewsTO());
		
	}
	
	@Test
	public void searchByAuthorAndTagTest() throws ServiceException {
		
		List<NewsVO> resultNewsList = null;
		when(newsServiceMock.search(any(SearchCriteria.class), anyInt(), anyInt())).thenReturn(Arrays.asList(new NewsTO(),new NewsTO(),new NewsTO()));
		resultNewsList=generalService.searchByAuthorAndTag(any(SearchCriteria.class), anyInt(), anyInt());
		verify(newsServiceMock, times(1)).search(any(SearchCriteria.class), anyInt(), anyInt());
		assertNotNull(resultNewsList);
		
	}
	
	@Test
	public void countNewsInSearchTest() throws ServiceException {
		int expectedCount = 1;
		int resultCount = 0;
		when(newsServiceMock.countNewsInSearch(any(SearchCriteria.class))).thenReturn(Integer.valueOf(1));
		resultCount=newsServiceMock.countNewsInSearch(any(SearchCriteria.class));
		verify(newsServiceMock, times(1)).countNewsInSearch(any(SearchCriteria.class));
		assertEquals(expectedCount, resultCount);
	}
	
	@Test
	public void addComplexNewsTest() throws ServiceException {
		
		NewsVO newsVO = new NewsVO();
		TagTO tagTO = new TagTO();
		tagTO.setTagId(1L);
		newsVO.setNewsItem(new NewsTO());
		newsVO.setAuthorItem(new AuthorTO());
		newsVO.setTagItemList(Arrays.asList(tagTO,tagTO,tagTO));
		
		when(newsVOMock.getNewsItem()).thenReturn(new NewsTO());
		when(newsVOMock.getAuthorItem()).thenReturn(new AuthorTO());
		when(newsVOMock.getTagItemList()).thenReturn(Arrays.asList(new TagTO(),new TagTO(),new TagTO()));
		
		when(newsServiceMock.addNews(new NewsTO())).thenReturn(anyLong());
	
		
		
		generalService.addComplexNews(newsVO);
		verify(newsServiceMock, times(1)).addNews(any(NewsTO.class));
		verify(authorServiceMock, times(1)).linkAuthorWhithNews( anyLong(), anyLong());
		verify(tagServiceMock, times(1)).linkTagsWithNews(anyListOf(Long.class),anyLong());
	}
	
	@Test
	public void deleteNewsTest() throws ServiceException {
		generalService.deleteNews(anyLong());
		verify(commentServiceMock, times(1)).deleteCommentByNewsId(anyLong());
		verify(authorServiceMock, times(1)).unlinkAutorWhithNewsByNewsId(anyLong());
		verify(tagServiceMock, times(1)).unlinkTagWithNews(anyLong());		
		verify(newsServiceMock, times(1)).deleteNews(anyLong());
	}
	
	@Test
	public void getSingleNewsTest() throws ServiceException {
		
		NewsVO resultNewsVO =null;
		when(newsServiceMock.getSingleNews(anyLong())).thenReturn(new NewsTO());
		when(authorServiceMock.getAuthorByNewsId(anyLong())).thenReturn(new AuthorTO());
		when(commentServiceMock.getCommentsByNewsId(anyLong())).thenReturn(Arrays.asList(new CommentTO(),new CommentTO(),new CommentTO()));
		when(tagServiceMock.getTagsByNewsId(anyLong())).thenReturn(Arrays.asList(new TagTO(),new TagTO(),new TagTO()));
		resultNewsVO= generalService.getSingleNews(anyLong());
		verify(commentServiceMock, times(1)).getCommentsByNewsId(anyLong());
		verify(authorServiceMock, times(1)).getAuthorByNewsId(anyLong());			
		verify(newsServiceMock, times(1)).getSingleNews(anyLong());
		verify(tagServiceMock, times(1)).getTagsByNewsId(anyLong());
		assertNotNull(resultNewsVO);
	}
	
	
	
	
	
	
}
