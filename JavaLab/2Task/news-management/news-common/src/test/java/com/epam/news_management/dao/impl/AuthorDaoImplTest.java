package com.epam.news_management.dao.impl;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;






import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.annotation.ExpectedDataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;

import com.epam.news_management.dao.IAuthorDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.AuthorTO;

@DataSet(value="NewsManagementDataSet.xml",loadStrategy = CleanInsertLoadStrategy.class)
public class AuthorDaoImplTest extends UnitilsJUnit4 {
	
	private IAuthorDao authorDao;
	
	@TestDataSource
	private DataSource dataSource;
	


	@Before
	public void setUp() throws Exception {
		assertNotNull(dataSource);
		authorDao = new AuthorDaoImpl(dataSource);
		assertNotNull(authorDao);
	}
	

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void addTest() {
		
	}
	@Test
	public void getAllTest() throws DaoException {
		List <AuthorTO> resultAuthorsList = new  ArrayList<AuthorTO>();
		resultAuthorsList=authorDao.getAll();
		assertNotNull(resultAuthorsList);
		
	}
	@Test
	public void getTest() throws DaoException {
		AuthorTO expectedAuthor = new AuthorTO();
		AuthorTO resultAuthor = null;
		expectedAuthor.setAuthorId(1L);
		expectedAuthor.setAuthorName("testName1");
		expectedAuthor.setExpired(null);
		resultAuthor=authorDao.get(1L);
		assertEquals(expectedAuthor, resultAuthor);
		
	}
	@Test
	public void editTest() throws DaoException {
		AuthorTO expectedAuthor = new AuthorTO();
		AuthorTO resultAuthor = null;
		expectedAuthor.setAuthorId(1L);
		expectedAuthor.setAuthorName("edit");
		expectedAuthor.setExpired(null);
		authorDao.edit(expectedAuthor);
		resultAuthor=authorDao.get(1L);
		assertEquals(expectedAuthor, resultAuthor);
		
	}
	@Test
	public void deleteTest() throws DaoException {
		AuthorTO resultAuthor = null;
		authorDao.delete(1L);
		resultAuthor = authorDao.get(1L);
		assertNotNull(resultAuthor.getExpired());
		
	}
	@Test
	@ExpectedDataSet
	public void linkTest() throws DaoException {
				
		authorDao.link(3L, 3L);
		
		
		
	}

	@Test
	@ExpectedDataSet
	public void unlinkByNewsIdTest() throws DaoException {
		authorDao.unlinkByNewsId(1L);
	}

	@Test
	@ExpectedDataSet
	public void unlinkByAuthorIdTest() throws DaoException {
		
		authorDao.unlinkByAuthorId(1L);
		
	}

	@Test
	public void getAuthorByNewsIdTest() throws DaoException {
		AuthorTO expectedAuthor = new AuthorTO();
		AuthorTO resultAuthor = null;
		expectedAuthor.setAuthorId(1L);
		expectedAuthor.setAuthorName("testName1");
		expectedAuthor.setExpired(null);
		resultAuthor=authorDao.getAuthorByNewsId(1L);
		assertEquals(expectedAuthor, resultAuthor);
	}


}
