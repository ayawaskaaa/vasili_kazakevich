package com.epam.news_management.service.impl;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.news_management.dao.IAuthorDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.AuthorTO;
import com.epam.news_management.service.exception.ServiceException;


@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:NewsManagementSpringContexTest.xml"})
public class AuthorServiceImplTest {
	
	@Mock
	private IAuthorDao authorDaoMock;
	

	
	@InjectMocks
	@Autowired
	private AuthorServiceImpl authorAction;
	

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Before
	public void setUp() throws Exception {
		MockitoAnnotations.initMocks(this);
		assertNotNull(authorAction);
		assertNotNull(authorDaoMock);
		

	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void addAuthorTest() throws DaoException, ServiceException {
		Long expectedId = 1L;
		Long resultId = null;
		when(authorDaoMock.add(new AuthorTO())).thenReturn(Long.valueOf(1L));
		resultId=authorAction.addAuthor(new AuthorTO());
		verify(authorDaoMock, times(1)).add(new AuthorTO());
		assertEquals(expectedId, resultId);
		
	}
	@Test
	public void unlinkAutorWhithNewsTest() throws DaoException, ServiceException {
		
		authorAction.unlinkAutorWhithNewsByNewsId(anyLong());
		verify(authorDaoMock, times(1)).unlinkByNewsId(anyLong());
		
	}
	@Test
	public void unlinkAutorWhithNewsByAuthorIdTest() throws DaoException, ServiceException {
		
		authorAction.unlinkAutorWhithNewsByAuthorId(anyLong());
		verify(authorDaoMock, times(1)).unlinkByAuthorId(anyLong());
		
	}
	@Test
	public void deleteAuthorTest() throws ServiceException, DaoException {
		
		authorAction.deleteAuthor(anyLong());
		verify(authorDaoMock, times(1)).delete(anyLong());
		
	}
	@Test
	public void getSingleAuthorTest() throws DaoException, ServiceException {
		
		AuthorTO expectedAuthor = new AuthorTO();
		AuthorTO resultAuthor = null;
		when(authorDaoMock.getAuthorByNewsId(anyLong())).thenReturn(new AuthorTO());
		resultAuthor = authorAction.getAuthorByNewsId(anyLong());
		verify(authorDaoMock, times(1)).getAuthorByNewsId(anyLong());
		assertEquals(expectedAuthor, resultAuthor);
		
		
	}
	@Test
	public void editAuthorTest() throws ServiceException, DaoException {
		
		authorAction.editAuthor(new AuthorTO());
		verify(authorDaoMock, times(1)).edit(new AuthorTO());
		
	}
	
	@Test
	public void linkAuthorWhithNews() throws ServiceException, DaoException {
		authorAction.linkAuthorWhithNews(anyLong(), anyLong());
		verify(authorDaoMock, times(1)).link(anyLong(), anyLong());
	}


}
