package com.epam.news_management.service.impl;

import java.util.List;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.news_management.dao.INewsDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.NewsTO;
import com.epam.news_management.entity.SearchCriteria;
import com.epam.news_management.service.exception.ServiceException;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:NewsManagementSpringContexTest.xml"})
public class NewsServiceImplTest {
	
	@Mock
	private INewsDao newsDaoMock;
	
	
	@InjectMocks
	@Autowired
	 private NewsServiceImpl newsService;

	@BeforeClass
	   public static void setUpClass() {
	   }
	@AfterClass
	   public static void tearDownClass() {
	   }
	
	@Before
    public void setUp() {
		 MockitoAnnotations.initMocks(this);
		 assertNotNull(newsService);
		 assertNotNull(newsDaoMock);

	}
	@After
    public void tearDown() {
    }
	
	@Test
	public void addNewsTest() throws DaoException, ServiceException{
		Long expectedId = 1L;
		Long result = null;
		when(newsDaoMock.add(any(NewsTO.class))).thenReturn(Long.valueOf(1L));
		result=newsService.addNews(new NewsTO());
		verify(newsDaoMock, times(1)).add(any(NewsTO.class));
		assertEquals(expectedId, result);		
		
	}
	
	@Test
	public void editNewsTest() throws DaoException, ServiceException{
		newsService.editNews(new NewsTO());
		verify(newsDaoMock, times(1)).edit(any(NewsTO.class));
	}
	
	
	
	@Test
	public void getSingleNewsTest() throws DaoException, ServiceException{
		NewsTO expectedNewsTO = new NewsTO();
		NewsTO resultNewsTO = null;
		when(newsDaoMock.get(anyLong())).thenReturn(new NewsTO());
		resultNewsTO=newsService.getSingleNews(anyLong());
		verify(newsDaoMock, times(1)).get(anyLong());
		assertEquals(expectedNewsTO, resultNewsTO);
		
	}
	
	@Test
	public void deleteNewsTest() throws ServiceException, DaoException{
		newsService.deleteNews(anyLong());
		verify(newsDaoMock, times(1)).delete(anyLong());
	}
	
	@Test
	public void countNewsInSearchTest() throws DaoException, ServiceException{
		int expectedCount = 1;
		int resultCount = 0;
		when(newsDaoMock.countNewsInSearch(any(SearchCriteria.class))).thenReturn(Integer.valueOf(1));
		resultCount=newsService.countNewsInSearch(any(SearchCriteria.class));
		verify(newsDaoMock, times(1)).countNewsInSearch(any(SearchCriteria.class));
		assertEquals(expectedCount, resultCount);
		
	}
	
	@Test
	public void searchTest() throws DaoException, ServiceException{
		List<NewsTO> expectedList=Arrays.asList(new NewsTO(),new NewsTO(),new NewsTO());
		List<NewsTO> resultList=null;
		when(newsDaoMock.search(any(SearchCriteria.class), anyInt(), anyInt())).thenReturn(Arrays.asList(new NewsTO(),new NewsTO(),new NewsTO()));
		resultList=newsService.search(any(SearchCriteria.class), anyInt(), anyInt());
		verify(newsDaoMock, times(1)).search(any(SearchCriteria.class), anyInt(), anyInt());
		assertEquals(expectedList,resultList);
		
		
	}
	

	
	
	
}
