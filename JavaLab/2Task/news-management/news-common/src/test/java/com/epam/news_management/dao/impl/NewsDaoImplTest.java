package com.epam.news_management.dao.impl;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;







import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.support.DirtiesContextTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.unitils.UnitilsJUnit4;
import org.unitils.database.DatabaseUnitils;
import org.unitils.database.annotations.TestDataSource;
import org.unitils.database.annotations.Transactional;
import org.unitils.dbunit.annotation.DataSet;
import org.unitils.dbunit.datasetloadstrategy.impl.CleanInsertLoadStrategy;

import com.epam.news_management.dao.INewsDao;
import com.epam.news_management.dao.exception.DaoException;
import com.epam.news_management.entity.NewsTO;
import com.epam.news_management.entity.SearchCriteria;

@DataSet(value="NewsManagementDataSet.xml",loadStrategy = CleanInsertLoadStrategy.class)
public class NewsDaoImplTest extends UnitilsJUnit4 {
	

	private INewsDao newsDao;

	@TestDataSource
	private DataSource dataSource;
	

	@Before
	public void setUp() {
		
		assertNotNull(dataSource);
		newsDao = new NewsDaoImpl(dataSource);
		assertNotNull(newsDao);
	}

	@After
	public void tearDown() throws SQLException {
	
		
		
	}

	

	@Test
	public void addTest() throws DaoException  {
		NewsTO newsTO = new NewsTO();
		NewsTO resultNewsTO=null;
		Long generetedId = null;
		String expectedTitle = "testTitle1";
		String expectedShortText="testShortText1";
		String expectedFullText = "testFullText1";
		String resultTitle = null;
		String resultShortText=null;
		String resultFullText = null;
		newsTO.setTitle("testTitle1");
		newsTO.setShortText("testShortText1");
		newsTO.setFullText("testFullText1");		
		
		generetedId = newsDao.add(newsTO);
		
		newsTO.setNewsId(generetedId);		
		
		resultNewsTO=newsDao.get(generetedId);	
		
		resultTitle=resultNewsTO.getTitle();
		resultShortText=resultNewsTO.getShortText();
		resultFullText=resultNewsTO.getFullText();
		
		assertEquals(expectedTitle, resultTitle);
		assertEquals(expectedShortText, resultShortText);
		assertEquals(expectedFullText, resultFullText);

	}
	
	@Test
	public void getAllTest() throws DaoException{
		List<NewsTO> resultNewsList = new ArrayList<NewsTO>();
		resultNewsList=newsDao.getAll();
		assertNotNull(resultNewsList);
		
	}

	@Test
	
	public void getTest() throws DaoException {
		
		NewsTO resultNewsTO = null;
		String expectedTitle = "testTitle1";
		String expectedShortText="testShortText1";
		String expectedFullText = "testFullText1";
		String resultTitle = null;
		String resultShortText=null;
		String resultFullText = null;
		resultNewsTO=newsDao.get(1L);

		resultTitle=resultNewsTO.getTitle();
		resultShortText=resultNewsTO.getShortText();
		resultFullText=resultNewsTO.getFullText();
		assertEquals(expectedTitle, resultTitle);
		assertEquals(expectedShortText, resultShortText);
		assertEquals(expectedFullText, resultFullText);
		

	}
	
	
	@Test
	public void editTest() throws DaoException{
		NewsTO newsTO = new NewsTO();
		NewsTO resultNewsTO=null;
		
		String expectedTitle = "edit1";
		String expectedShortText="edit1";
		String expectedFullText = "edit1";
		String resultTitle = null;
		String resultShortText=null;
		String resultFullText = null;
		
		newsTO.setTitle("edit1");
		newsTO.setShortText("edit1");
		newsTO.setFullText("edit1");
		newsTO.setNewsId(1L);
	
		newsDao.edit(newsTO);
		
		resultNewsTO=newsDao.get(1L);
		
		resultTitle=resultNewsTO.getTitle();
		resultShortText=resultNewsTO.getShortText();
		resultFullText=resultNewsTO.getFullText();
		
		assertEquals(expectedTitle, resultTitle);
		assertEquals(expectedShortText, resultShortText);
		assertEquals(expectedFullText, resultFullText);

		
			
	}
	@Test
	public void deleteTest() throws DaoException{
		Long testId = 1L;
		NewsTO newsTO = null;
		TagDaoImpl tagDao= new TagDaoImpl(dataSource);
		AuthorDaoImpl authorDao = new AuthorDaoImpl(dataSource);
		CommentDaoImpl commentDao = new CommentDaoImpl(dataSource);
		tagDao.unlinkByNewsId(testId);
		authorDao.unlinkByNewsId(testId);
		commentDao.deleteByNewsId(testId);
		newsDao.delete(testId);
		newsTO=newsDao.get(testId);
		assertNull(newsTO);
		
		
	}
	@Test
	public void countNewsInSearchTest() throws DaoException{
		SearchCriteria searchCriteria = new SearchCriteria();
		Long auIdLong = 1L;
		
		List<Long> tagList = new ArrayList<Long>();
		tagList.add(1L);
	//	searchCriteria.setAuthorId(auIdLong);
		searchCriteria.setTagIdList(tagList);

		int result = 0;
		result=newsDao.countNewsInSearch(searchCriteria);
		assertNotNull(Integer.valueOf(result));
		
		
	}
	
	@Test
	public void searchTest() throws DaoException{
		SearchCriteria searchCriteria = new SearchCriteria();		
		List<NewsTO> result = null;
		result=newsDao.search(searchCriteria, 1, 3);
		assertNotNull(result);
		
	}

	
	

}
