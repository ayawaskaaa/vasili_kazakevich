
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link rel="stylesheet" 	href="<c:url value="/resources/css/loginPageHeader.css" />" />

	<div id="mainBlock">
		<div id="headerName" align="left">

			<h1>
				<spring:message code="local.text.header" />
			</h1>

		</div>
		
	<div id="rightHeaderDiv"> 		
				
		<div id="localeBlock" align="right" >
			<a href="?language=en <c:if test="${queryString!=null}">${queryString}</c:if>"><spring:message code="local.button.en" /></a>
			<a href="?language=ru <c:if test="${queryString!=null}">${queryString}</c:if>"><spring:message code="local.button.ru" /></a>
		</div>
		
		</div>
	</div>
