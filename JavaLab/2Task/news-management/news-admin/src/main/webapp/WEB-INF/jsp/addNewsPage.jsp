<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ page pageEncoding="UTF-8"%>


<link rel="stylesheet"
	href="<c:url value="/resources/css/addNewsPage.css" />" />
<script type="text/javascript"
	src="<spring:url value="/resources/js/tagSelectBar.js"/>"></script>
<fmt:setBundle basename="locale" var="bundle" />
<fmt:message bundle="${bundle}" key="local.date.format" var="pattern" />
<fmt:formatDate value="${date}" pattern="${pattern}" var="formattedDate" />



<div id="addNewsMainBlock">
	<form:form action="add" commandName="newsVO">
		<div id="titleBlock" class="block">
			<div class="textClass">
				<spring:message code="local.text.title" />
			</div>
			<div>
				<form:input path="newsItem.title" type="text" maxlength="30"
					size="50" />
			</div>
			<div class="errorMessage">
				<form:errors path="newsItem.title" cssclass="error"></form:errors>
			</div>
		</div>

		<div id="dateBlock" class="block" align="left">
			<div class="textClass">
				<spring:message code="local.text.date" />
			</div>
			<div class="textField">
				<input type="text" name="creationDate" value="${formattedDate} " />
			</div>
			<div class="errorMessage">
				<c:if test="${dateError}">
					<spring:message code="local.error.message.invalidDate" />
				</c:if>
			</div>
		</div>

		<div id="biefBlock" class="block" align="left">
			<div class="textClass">
				<spring:message code="local.text.brief" />
			</div>
			<div>
				<form:textarea path="newsItem.shortText" rows="3" cols="50"
					maxlength="100" />

			</div>
			<div class="errorMessage">
				<form:errors path="newsItem.shortText" cssclass="error"></form:errors>
			</div>
		</div>

		<div id="contentBlock" class="block" align="left">
			<div class="textClass">
				<spring:message code="local.text.content" />
			</div>
			<div>
				<form:textarea path="newsItem.fullText" rows="7" cols="50"
					maxlength="2000" />

			</div>
			<div class="errorMessage">
				<form:errors path="newsItem.fullText" cssclass="error"></form:errors>
			</div>
		</div>

		<div id="tagAndAuthorBlock">

			<div id="authorBlock">
				<form:select path="authorId">
					<form:option value="0" label="Please select the author" />
					<c:forEach var="authorList" items="${authorsList}">
						<c:if test="${authorList.expired==null}">

							<form:option value="${authorList.authorId}"
								label="${authorList.authorName}"
								selected="${sessionScope.authorId == authorList.authorId ? 'selected' : ' ' }" />
						</c:if>
					</c:forEach>
				</form:select>
			</div>
			<div id="tagBlock">
				<div class="multiselect">
					<div class="selectBox" onclick="showCheckboxes()">
						<select>
							<option>Select an option</option>
						</select>
						<div class="overSelect"></div>
					</div>
					<div id="checkboxes">
						<c:forEach var="tagsList" items="${tagList}">

							<c:set var="contains" value="false" />
							<c:forEach var="item" items="${sessionScope.tagListId}">
								<c:if test="${item eq tagsList.tagId}">
									<c:set var="contains" value="true" />
								</c:if>
							</c:forEach>
							<label for="${tagsList.tagId}"> <form:checkbox
									id="${tagsList.tagId}" path="tagsIdList"
									value="${tagsList.tagId}"
									checked="${contains? 'cheked' :  ''	}" />${tagsList.tagName}
							</label>
						</c:forEach>
					</div>
				</div>
			</div>
			<div class="errorMessage"
				style="margin-top: 20px; padding-top: 20px;">
				<form:errors path="tagsIdList" cssclass="error"></form:errors>
			</div>
			<div class="errorMessage">
				<form:errors path="authorId" cssclass="error"></form:errors>
			</div>
		</div>

		<div id="saveBlock">
			<input type="submit"
				value="<spring:message code="local.button.save" />">

		</div>
	</form:form>
</div>
