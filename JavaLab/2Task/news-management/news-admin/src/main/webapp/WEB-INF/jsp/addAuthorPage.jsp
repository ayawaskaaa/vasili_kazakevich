<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page pageEncoding="UTF-8"%>

<script type="text/javascript" src="<c:url value="/resources/js/jquery-1.11.3.min.js" />"></script>
<script type="text/javascript" src="<spring:url value="/resources/js/openEditBar.js"/>"></script>
<link rel="stylesheet" type="text/css"	href="<c:url value="/resources/css/addAuthorPage.css" />">





	<div id="addAuthorMainBlock">
		<c:forEach var="authors" items="${authorList}">
			<script type="text/javascript">
				$(function() {
					$('#baseUrl${authors.authorId}').click(	function() {
					window.location = $(this).attr('href')
					+ $('#appendUrl${authors.authorId}').val();
					return false;
					});
				});
			</script>

			<div id="editAuthorRow">
				<div class="authorText">
					<spring:message code="local.text.author" />
				</div>
				<div class="authorName">
					<input id="appendUrl${authors.authorId}" type="text"
						value="${authors.authorName}" size="50" maxlength="30"
						path="authorName">

				</div>
				<div id="${authors.authorId}">
					<a
						onclick="showText('${authors.authorName}','${authors.authorId}')"
						href="javascript:void(0);"> <spring:message
							code="local.ref.edit" />
					</a>
				</div>
				<div class="editPanel" id="${authors.authorName}">
					<a href="/news-admin/author/update?&authorId=${authors.authorId}&authorName="
						id="baseUrl${authors.authorId}">
							<spring:message	code="local.ref.update" />
						</a> 
						<a	href="/news-admin/author/make-expired?&authorId=${authors.authorId}">
							<spring:message	code="local.ref.expire" />
						</a> 
						<a	onclick="showText('${authors.authorId}','${authors.authorName}')"
						href="javascript:void(0);">
						 <spring:message	code="local.ref.cancel" />
					</a>
					
				</div>
				<div id="expired">
					<c:if test="${authors.expired!=null}"><font color="grey"><spring:message code="local.text.expired" /></font></c:if>	
				</div>
					
					
				
	
			</div>
			
		</c:forEach>
		<div id="newAuthorBlock">

			<form:form action="add" commandName="authorTO"	id="newAuthorForm" method="post">
				<div class="authorText">
					<spring:message code="local.text.addAuthor" />
				</div>
				<div class="authorName" id="newAuthor">
					<form:input path="authorName" required="true" maxlength="30"
						size="50" />
				</div>
				<div id="savePanel">
					<a href="javascript:;"
						onclick="document.getElementById('newAuthorForm').submit();">
						<spring:message code="local.ref.save" />
					</a>

				</div>
				<div>
					<form:errors path="authorName" cssclass="error"></form:errors>
				</div>
			</form:form>

		</div>
	</div>

