<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"  
"http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title><tiles:insertAttribute name="title" ignore="true" /></title>

<link rel="stylesheet" type="text/css"	href="<c:url value="/resources/css/mainLayout.css" />">



</head>
<body>

	<div id="mainView" >
	
		<div id="header">
			<tiles:insertAttribute name="header" />
		</div>

		<div id="centralBlock">
			<div id="leftMenu">
				<div id="menuBar">
					<tiles:insertAttribute name="menu" />
				</div>
			</div>

			<div id="mainBodyDiv">
				<div id="body">
				 <tiles:insertAttribute name="body" />
				</div>
			</div>
		</div>


		<div id="footer" align="center">
			<tiles:insertAttribute name="footer" />
		</div>

	</div>




</body>
</html>
