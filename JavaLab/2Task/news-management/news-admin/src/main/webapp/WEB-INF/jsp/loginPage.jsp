<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ page pageEncoding="UTF-8"%>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<link rel="stylesheet" type="text/css"
	href="<c:url value="/resources/css/loginPage.css" />">



	<div id="loginPageMainBlock">
			<div id="errorMessage">
		<c:if test="${param.login_error}">
			<spring:message code="local.error.message.authorization" />
		</c:if>
	</div>
		<form action="/news-admin/login" method="post">
			<input type="hidden" name="${_csrf.parameterName}"
				value="${_csrf.token}" />

			<div id="loginBlock">
				<div class="textBlock">
					<strong><spring:message code="local.text.login" /></strong>
				</div>
				<div class="textFeild">
					<input type="text" name="username" size="20">
				</div>
			</div>
			<div id="passwordBlock">
				<div class="textBlock">
					<strong><spring:message code="local.text.password" /></strong>
				</div>
				<div class="textFeild">
					<input type="password" name="password" size="20">
				</div>
			</div>
			<div id="loginButtonBlock">
				<input type="submit" id="loginButton"
					value="<spring:message code="local.button.login" />">
			</div>


		</form>

	</div>
