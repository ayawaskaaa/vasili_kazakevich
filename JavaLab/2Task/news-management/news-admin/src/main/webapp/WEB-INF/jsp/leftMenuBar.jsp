<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<link rel="stylesheet" type="text/css"	href="<c:url value="/resources/css/leftMenuBar.css" />" >



	<div id ="menuBarBlock">
		<div id="options">
			<div >
				<li><a href="/news-admin/page">
				<c:choose>
				<c:when test="${newsListFlag }"><strong><spring:message code="local.menu.newsPage" /></strong></c:when>
				<c:otherwise><spring:message code="local.menu.newsPage" /></c:otherwise>
				
				</c:choose>
								
				</a></li>
			</div>
			<div>
				<li>
					<a  href="/news-admin/news/add-news-page">
						<c:choose >
							<c:when test="${addNewsPageFlag }"><strong><spring:message code="local.menu.addNewsPage" /></strong></c:when>
							<c:otherwise><spring:message code="local.menu.addNewsPage" /></c:otherwise>
							
						</c:choose>
							
					</a>
				</li>
			</div>
			<div>
				<li>
					<a href="/news-admin/author/add-author-page">
						<c:choose >
							<c:when test="${addAuthorPageFlag }"><strong><spring:message code="local.menu.addAuthorPage" /></strong></c:when>
							<c:otherwise><spring:message code="local.menu.addAuthorPage" /></c:otherwise>
						</c:choose>
						
					</a>
				</li>
			</div>
			<div>
				<li>
					<a href="/news-admin/tag/add-tag-page">
						<c:choose  >
							<c:when test="${addTagPageFlag }"><strong><spring:message code="local.menu.addTagPage" /></strong></c:when>
							<c:otherwise><spring:message code="local.menu.addTagPage" /></c:otherwise>
						</c:choose>
						
					</a>
				</li>
			</div>
		
		</div>
	</div>
