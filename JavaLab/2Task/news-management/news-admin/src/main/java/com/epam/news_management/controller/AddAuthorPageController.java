package com.epam.news_management.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.news_management.entity.AuthorTO;
import com.epam.news_management.entity.TagTO;
import com.epam.news_management.service.IGeneralNewsManagementService;
import com.epam.news_management.service.exception.ServiceException;

@Controller
@RequestMapping("/author")
public class AddAuthorPageController {

	private Logger log = Logger.getLogger(AddAuthorPageController.class);

	@Autowired
	private IGeneralNewsManagementService service;

	@RequestMapping( value="/add-author-page " , method=RequestMethod.GET )
	public String addAuthorPage(Model model) {

		List<AuthorTO> authorList = null;

		try {

			authorList = service.getAllAuthors();

		} catch (ServiceException e) {
			log.error("Error while getting data for autor  page.", e);
			return "errorPage";
		} catch (Exception e) {
			log.error("Error while getting data for autor  page.", e);
			return "errorPage";
		}
		model.addAttribute("authorTO", new AuthorTO());
		model.addAttribute("authorList", authorList);
		model.addAttribute("addAuthorPageFlag", true);

		return "addAuthorPage";
	}

	@RequestMapping(value="/add", method=RequestMethod.POST)
	public String addNewsAuthor(@Valid AuthorTO author, BindingResult result,
			Model model) {

		if (result.hasErrors()) {
			List<AuthorTO> authorList = null;
			try {

				authorList = service.getAllAuthors();

			} catch (ServiceException e) {
				log.error("Error while getting data for autor  page.", e);
				return "errorPage";
			} catch (Exception e) {
				log.error("Error while getting data for autor  page.", e);
				return "errorPage";
			}

			model.addAttribute("authorList", authorList);

			return "addAuthorPage";
		}

		try {
			if (!author.getAuthorName().isEmpty()) {
				service.addAuthor(author);
			}

		} catch (Exception e) {
			log.error("Error while saving author.", e);
			return "errorPage";
		}

		return "redirect:/author/add-author-page";
	}

	@RequestMapping(value="/update", method=RequestMethod.GET)
	public String udateAuthor(
			Model model,
			@RequestParam(value = "authorId", required = false) Long authorId,
			@RequestParam(value = "authorName", required = false) String authorName

	) {
		AuthorTO authorTO = new AuthorTO();
		authorTO.setAuthorId(authorId);
		authorTO.setAuthorName(authorName);

		try {
			if (!authorName.isEmpty()) {
				service.updateAuthor(authorTO);
			}
		} catch (Exception e) {
			log.error("Error while updating author.", e);
			return "errorPage";
		}

		return "redirect:/author/add-author-page";
	}

	@RequestMapping(value="/make-expired" , method=RequestMethod.GET)
	public String makeAuthorExpired(Model model,
			@RequestParam(value = "authorId", required = false) Long authorId) {
		AuthorTO authorTO = new AuthorTO();
		authorTO.setAuthorId(authorId);

		try {
			service.deleteAuthor(authorId);
		} catch (Exception e) {
			log.error("Error while updating author.", e);
			return "errorPage";
		}

		return "redirect:/author/add-author-page";
	}

}
