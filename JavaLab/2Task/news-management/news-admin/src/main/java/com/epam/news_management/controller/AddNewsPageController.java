package com.epam.news_management.controller;


import java.text.ParseException;
import java.util.List;

import javax.servlet.ServletRequest;
import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.news_management.controller.util.DateHelper;
import com.epam.news_management.entity.AuthorTO;
import com.epam.news_management.entity.NewsVO;
import com.epam.news_management.entity.TagTO;
import com.epam.news_management.service.IGeneralNewsManagementService;
import com.epam.news_management.service.exception.ServiceException;

@Controller
@RequestMapping("/news")
public class AddNewsPageController {

	private Logger log = Logger.getLogger(AddNewsPageController.class);

	@Autowired
	private IGeneralNewsManagementService service;

	@RequestMapping(value = "/add-news-page", method = RequestMethod.GET)
	public String addNewsPage(Model model, ServletRequest request) {
		DateHelper dateHelper = new DateHelper();
		
		List<AuthorTO> authorsList = null;
		List<TagTO> tagList = null;



		try {

			authorsList = service.getAllAuthors();
			tagList = service.getAllTags();

		} catch (Exception e) {
			log.error("Error while getting data for add news page.", e);
			return "errorPage";
		}
		model.addAttribute("authorTO", new AuthorTO());
		model.addAttribute("tagTO", new TagTO());
		model.addAttribute("newsVO", new NewsVO());		
		model.addAttribute("date", dateHelper.getCurrentDate());
		model.addAttribute("tagList", tagList);
		model.addAttribute("authorsList", authorsList);
		model.addAttribute("addNewsPageFlag", true);

		return "addNewsPage";
	}

	@RequestMapping({ "/add" })
	public String addNews(@Valid NewsVO newsVO, BindingResult result,
			Model model, @RequestParam(value="creationDate", required=false)String creationDate) {
		Long newNewsIdLong = null;		
		DateHelper dateHelper = new DateHelper();
		if(!dateHelper.isThisDateValid(creationDate)){
			System.out.println("NOT VALID");
			List<AuthorTO> authorsList = null;
			List<TagTO> tagList = null;


			try {
				
				authorsList = service.getAllAuthors();
				tagList = service.getAllTags();

			} catch (Exception e) {
				log.error("Error while getting data for edit page.", e);
				return "errorPage";
			}
			model.addAttribute("tagList", tagList);
			model.addAttribute("authorsList", authorsList);
			model.addAttribute("dateError", true);
			model.addAttribute("date", dateHelper.getCurrentDate());

			return "addNewsPage";
		
			
		}
		

		
		if (result.hasErrors()) {
			List<AuthorTO> authorsList = null;
			List<TagTO> tagList = null;


			try {
				
				authorsList = service.getAllAuthors();
				tagList = service.getAllTags();

			} catch (Exception e) {
				log.error("Error while getting data for edit page.", e);
				return "errorPage";
			}
			model.addAttribute("tagList", tagList);
			model.addAttribute("authorsList", authorsList);

			model.addAttribute("date", dateHelper.getCurrentDate());

			return "addNewsPage";
		}

		try {
			newsVO.getNewsItem().setCreationDate(dateHelper.getNewTimestamp(creationDate));  
			newNewsIdLong=service.addComplexNews(newsVO);
		} catch (Exception e) {
			log.error("Error while adding coplex news.", e);
			return "errorPage";
		}

		return "redirect:/edit-news/edit-page?&newsId="
				+ newNewsIdLong;

	}

}
