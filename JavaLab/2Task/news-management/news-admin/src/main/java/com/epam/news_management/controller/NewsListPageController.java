package com.epam.news_management.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.news_management.controller.util.PageConfig;
import com.epam.news_management.entity.AuthorTO;
import com.epam.news_management.entity.CommentTO;
import com.epam.news_management.entity.NewsTO;
import com.epam.news_management.entity.NewsVO;
import com.epam.news_management.entity.SearchCriteria;
import com.epam.news_management.entity.TagTO;
import com.epam.news_management.entity.UserTO;
import com.epam.news_management.service.IGeneralNewsManagementService;
import com.epam.news_management.service.exception.ServiceException;

@Controller
@RequestMapping("/")
public class NewsListPageController {

	private Logger log = Logger.getLogger(NewsListPageController.class);

	@Autowired
	private IGeneralNewsManagementService service;

	@RequestMapping(value={ "/", "/page", "/changePgN" }, method=RequestMethod.GET)
	public String newsListPage(Model model, HttpSession session,
			@RequestParam(value = "homePgN", required = false) Integer pgn) {
		
		SearchCriteria sessionSearchCriteria = new SearchCriteria();
		List<NewsVO> newsVO = null;
		List<AuthorTO> authorsList = null;
		List<TagTO> tagsList = null;
		int pagesAmount = 0;	
		int pageNumber = 1;

		sessionSearchCriteria.setAuthorId((Long) session
				.getAttribute("authorId"));
		sessionSearchCriteria.setTagIdList((List<Long>) session
				.getAttribute("tagListId"));

		if (pgn != null) {
			session.setAttribute("homePgN", pgn);
		}

		if (session.getAttribute("homePgN") != null) {
			pageNumber = Integer.parseInt(session.getAttribute("homePgN")
					.toString());

		}
		int itemAmount = 0;
		
		int startIndex = PageConfig.calculateStartIndex(pageNumber);
		int endIndex = PageConfig.calculateEndIndex(pageNumber);

		try {
			
			newsVO = service.searchByAuthorAndTag(sessionSearchCriteria,
					startIndex, endIndex);
			authorsList = service.getAllAuthors();
			tagsList = service.getAllTags();

			itemAmount = service.countNewsInSearch(sessionSearchCriteria);
			
			pagesAmount= PageConfig.countPageAmount(itemAmount);

		} catch (ServiceException e) {
			log.error("Error while using home page", e);
			return "errorPage";
		} catch (Exception e) {
			log.error("Error while getting data for view page.", e);
			return "errorPage";
		}
		model.addAttribute("searchCriteria", new SearchCriteria());
		model.addAttribute("authors", authorsList);
		model.addAttribute("news", newsVO);
		model.addAttribute("tags", tagsList);
		model.addAttribute("pagesAmount", pagesAmount);
		model.addAttribute("newsListFlag", true);

		return "newsListPage";
	}

	@RequestMapping(value="filter", method=RequestMethod.GET)
	public String filter(Model model, HttpSession session,
			SearchCriteria searchCriteria) {

		Long authorId = null;
		if (searchCriteria.getAuthorId() != null) {
			if (searchCriteria.getAuthorId() == 0) {
				authorId = null;
			} else {
				authorId = searchCriteria.getAuthorId();
			}
			session.removeAttribute("homePgN");
			session.setAttribute("authorId", authorId);
			session.setAttribute("tagListId", searchCriteria.getTagIdList());
		}

		return newsListPage(model, session, null);
	}

	@RequestMapping(value="reset", method=RequestMethod.GET)
	public String reset(Model model, HttpSession session,
			SearchCriteria searchCriteria) {
		
		session.removeAttribute("authorId"); 
		session.removeAttribute("tagListId");
		session.removeAttribute("homePgN");

		return newsListPage(model, session, null);
	}

	@RequestMapping(value="delete", method=RequestMethod.POST)
	public String deleteNews(
			Model model,
			HttpSession session,
			@RequestParam(value = "newsIdToDelete", required = false) List<Long> newsIdList) {
		try {
			if(newsIdList!=null){
				for (Long i : newsIdList) {

					service.deleteNews(i);

				}
			}
			
		} catch (Exception e) {
			log.error("Error while deleting news.", e);
			return "errorPage";
		}

		return "redirect:/page";
	}
	


}
