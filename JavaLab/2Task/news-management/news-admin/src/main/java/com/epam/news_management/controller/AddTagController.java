package com.epam.news_management.controller;

import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.news_management.entity.TagTO;
import com.epam.news_management.service.IGeneralNewsManagementService;
import com.epam.news_management.service.exception.ServiceException;

@Controller
@RequestMapping( "/tag" )
public class AddTagController {

	private Logger log = Logger.getLogger(AddTagController.class);

	@Autowired
	private IGeneralNewsManagementService service;

	@RequestMapping(value= "/add-tag-page" , method=RequestMethod.GET)
	public String addTagPage(Model model) {

		List<TagTO> tagList = null;

		try {

			tagList = service.getAllTags();

		} catch (ServiceException e) {
			log.error("Error while getting data for tag page.", e);
			return "errorPage";
		} catch (Exception e) {
			log.error("Error while getting data for tag page.", e);
			return "errorPage";
		}
		model.addAttribute("tagTO", new TagTO());
		model.addAttribute("tagList", tagList);
		model.addAttribute("addTagPageFlag", true);

		return "addTagPage";
	}

	@RequestMapping(value= "/add", method=RequestMethod.POST )
	public String addNewsAuthor(@Valid TagTO tag, BindingResult result,
			Model model) {

		if (result.hasErrors()) {
			List<TagTO> tagList = null;

			try {

				tagList = service.getAllTags();

			} catch (ServiceException e) {
				log.error("Error while getting data for tag page.", e);
				return "errorPage";
			} catch (Exception e) {
				log.error("Error while getting data for tag page.", e);
				return "errorPage";
			}

			model.addAttribute("tagList", tagList);

			return "addTagPage";
		}

		try {
			if (!tag.getTagName().isEmpty()) {
				service.addTag(tag);
			}

		} catch (Exception e) {
			log.error("Error while saving tag.", e);
			return "errorPage";
		}

		return "redirect:/tag/add-tag-page";
	}

	@RequestMapping(value= "/update", method=RequestMethod.GET)
	public String udateTag(Model model,
			@RequestParam(value = "tagId", required = false) Long tagId,
			@RequestParam(value = "tagName", required = false) String tagName

	) {
		TagTO tagTO = new TagTO();
		tagTO.setTagId(tagId);
		tagTO.setTagName(tagName);
		try {
			if (!tagName.isEmpty()) {
				service.updateTag(tagTO);
			}
		} catch (Exception e) {
			log.error("Error while updating tag.", e);
			return "errorPage";
		}

		return "redirect:/tag/add-tag-page";
	}

	@RequestMapping(value="/delete", method=RequestMethod.GET)
	public String deleteTag(Model model, 
			@RequestParam(value = "tagId", required = false) Long tagId) {

		try {

			service.deleteTag(tagId);
		} catch (Exception e) {
			log.error("Error while getting data for tag page.", e);
			return "errorPage";
		}

		return "redirect:/tag/add-tag-page";
	}

}
