package com.epam.news_management.controller.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateHelper {

	private static final String EN_FORMATE = "MM/dd/yyyy";
	private static final String RU_FORMATE = "dd.MM.yyyy";

	public boolean isThisDateValid(String dateToValidate) {

		if (dateToValidate == null) {
			return false;
		}

		try {
			SimpleDateFormat sdf = new SimpleDateFormat(EN_FORMATE);
			sdf.setLenient(false);

			Date date = sdf.parse(dateToValidate);

		} catch (ParseException e) {
			try {
				SimpleDateFormat sdf = new SimpleDateFormat(RU_FORMATE);
				sdf.setLenient(false);
				Date date = sdf.parse(dateToValidate);

			} catch (ParseException e1) {
				return false;
			}
		}

		return true;
	}

	public Timestamp getNewTimestamp(String creationDate) throws ParseException {

		Timestamp timestamp = null;
		try {
			DateFormat formatter;
			formatter = new SimpleDateFormat(EN_FORMATE);

			Date date = formatter.parse(creationDate);
			timestamp = new Timestamp(date.getTime());
		} catch (ParseException e) {

			DateFormat formatter;
			formatter = new SimpleDateFormat(RU_FORMATE);

			Date date = formatter.parse(creationDate);
			timestamp = new Timestamp(date.getTime());

		}

		return timestamp;

	}

	public Date getCurrentDate() {

		return new Date();

	}

}
