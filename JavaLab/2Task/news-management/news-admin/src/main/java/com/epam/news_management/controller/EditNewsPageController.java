package com.epam.news_management.controller;


import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.news_management.controller.util.DateHelper;
import com.epam.news_management.entity.AuthorTO;
import com.epam.news_management.entity.NewsVO;
import com.epam.news_management.entity.TagTO;
import com.epam.news_management.service.IGeneralNewsManagementService;
import com.epam.news_management.service.exception.ServiceException;

@Controller
@RequestMapping("/edit-news" )
public class EditNewsPageController {

	private Logger log = Logger.getLogger(EditNewsPageController.class);

	@Autowired
	private IGeneralNewsManagementService service;

	@RequestMapping(value= "/edit-page" , method=RequestMethod.GET)
	public String changePage(Model model, @RequestParam(value = "newsId", required = false) Long newsId) {
		DateHelper dateHelper = new DateHelper();
		NewsVO newsVO = null;
		List<AuthorTO> authorsList = null;
		List<TagTO> tagList = null;
		

		try {
			newsVO = service.getSingleNews(newsId);
			authorsList = service.getAllAuthors();
			tagList = service.getAllTags();

		} catch (ServiceException e) {
			log.error("Error while getting data for edit page.", e);
			return "errorPage";
		} catch (Exception e) {
			log.error("Error while getting data for edit page.", e);
			return "errorPage";
		}
		model.addAttribute("authorTO", new AuthorTO());
		model.addAttribute("tagTO", new TagTO());
		model.addAttribute("newsVO", new NewsVO());
		model.addAttribute("queryString", "&newsId=" + newsId);
		model.addAttribute("tagList", tagList);
		model.addAttribute("authorsList", authorsList);
		model.addAttribute("newsVO", newsVO);
		model.addAttribute("date", dateHelper.getCurrentDate());

		return "editNewsPage";
	}

	@RequestMapping(value="/edit" , method=RequestMethod.POST)
	public String editNews(@Valid NewsVO newsVO, BindingResult result,
			Model model, @RequestParam(value="creationDate", required=false)String creationDate) {
		DateHelper dateHelper = new DateHelper();
		
		if(!dateHelper.isThisDateValid(creationDate)){
			List<AuthorTO> authorsList = null;
			List<TagTO> tagList = null;
			
			try {

				authorsList = service.getAllAuthors();
				tagList = service.getAllTags();

			} catch (ServiceException e) {
				log.error("Error while getting data for edit page.", e);
				return "errorPage";
			} catch (Exception e) {
				log.error("Error while getting data for edit page.", e);
				return "errorPage";
			}
			
			model.addAttribute("dateError", true);
			model.addAttribute("tagList", tagList);
			model.addAttribute("authorsList", authorsList);
			

			return "editNewsPage";
		}
		if (result.hasErrors()) {
			List<AuthorTO> authorsList = null;
			List<TagTO> tagList = null;
			
			try {

				authorsList = service.getAllAuthors();
				tagList = service.getAllTags();

			} catch (ServiceException e) {
				log.error("Error while getting data for edit page.", e);
				return "errorPage";
			} catch (Exception e) {
				log.error("Error while getting data for edit page.", e);
				return "errorPage";
			}

			model.addAttribute("tagList", tagList);
			model.addAttribute("authorsList", authorsList);
			

			return "editNewsPage";
		}

		try {
			newsVO.getNewsItem().setCreationDate(dateHelper.getNewTimestamp(creationDate));  
			System.out.println(newsVO.toString());
			service.updateComplexNews(newsVO);
		} catch (Exception e) {
			log.error("Error while updating news.", e);
			return "errorPage";
		}

		return "redirect:/edit-news/edit-page?&newsId="
				+ newsVO.getNewsItem().getNewsId();
	}

}
