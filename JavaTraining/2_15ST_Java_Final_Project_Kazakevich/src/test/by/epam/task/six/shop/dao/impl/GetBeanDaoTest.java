package test.by.epam.task.six.shop.dao.impl;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import by.epam.task.six.shop.dao.GetBeanAction;
import by.epam.task.six.shop.dao.connection_pool.ConnectionPoolException;
import by.epam.task.six.shop.dao.connection_pool.ShopConnectionPool;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.dao.impl.GetBeanDao;
import by.epam.task.six.shop.entity.BlackListBean;
import by.epam.task.six.shop.entity.ShopItem;
import by.epam.task.six.shop.entity.UserOrder;

public class GetBeanDaoTest {

	private static final String SQL_COUNT_USERS_IN_BL = "SELECT COUNT(id_user) FROM blackList ;";
	private static final String SQL_COUNT_ORDERS = "SELECT COUNT(id) FROM orders ;";
	private static final String SQL_COUNT_USER_ORDERS = "SELECT COUNT(id) FROM orders "
			+ " WHERE id_user = 1 ;";

	private static ShopConnectionPool shopConnectionPool = null;

	@BeforeClass
	public static void prepeareConnection() throws ConnectionPoolException {

		shopConnectionPool = ShopConnectionPool.getInstance();
		shopConnectionPool.initPoolData();

	}

	@AfterClass
	public static void disposeConnectionPool() throws ConnectionPoolException {

		shopConnectionPool.dispose();
	}

	@Test
	public void getBlackListTest() throws ActionDaoException {

		GetBeanAction getBeanDao = GetBeanDao.getInstance();

		List<BlackListBean> blackList = new ArrayList<BlackListBean>();

		blackList = getBeanDao.getBlackList(1);

		assertNotNull("GetBeanDao returned null", blackList);

	}

	@Test
	public void getOrdersTest() throws ActionDaoException {

		GetBeanAction getBeanDao = GetBeanDao.getInstance();
		List<UserOrder> order = new ArrayList<UserOrder>();
		order = getBeanDao.getOrders(1);
		assertNotNull("GetBeanDao returned null", order);

	}

	@Test
	public void getItemsForMainPageTest() throws ActionDaoException {
		GetBeanAction getBeanDao = GetBeanDao.getInstance();
		List<ShopItem> item = new ArrayList<ShopItem>();
		item = getBeanDao.getItemsForMainPage(1);
		assertNotNull("GetBeanDao returned null", item);

	}

	@Test
	public void getUserOrdersTest() throws ActionDaoException {
		GetBeanAction getBeanDao = GetBeanDao.getInstance();
		List<UserOrder> userOrder = new ArrayList<UserOrder>();
		userOrder = getBeanDao.getUserOrders(1, 1);
		assertNotNull("GetBeanDao returned null", userOrder);
	}

	@Test
	public void countShopItems() throws ActionDaoException,
			ConnectionPoolException, SQLException {
		Connection con = null;
		ResultSet rs = null;
		GetBeanAction getBeanDao = GetBeanDao.getInstance();
		con = shopConnectionPool.takeConnection();
		Statement countStatement;
		int totalUnits = 0;
		countStatement = con.createStatement();
		rs = countStatement.executeQuery(SQL_COUNT_USERS_IN_BL);

		if (rs.next()) {
			totalUnits = rs.getInt(1);
		}

		shopConnectionPool.closeConnection(con, countStatement, rs);

		assertEquals("Counting doesn't match", totalUnits,
				getBeanDao.countUsersInBlackList());
	}

	@Test
	public void countUserOrdersTest() throws ActionDaoException, ConnectionPoolException, SQLException {
		Connection con = null;
		ResultSet rs = null;
		GetBeanAction getBeanDao = GetBeanDao.getInstance();
		Statement countStatement = null;
		int totalUnits = 0;

		try {
			con = shopConnectionPool.takeConnection();
			totalUnits = 0;

			countStatement = con.createStatement();
			rs = countStatement.executeQuery(SQL_COUNT_USER_ORDERS);

			if (rs.next()) {
				totalUnits = rs.getInt(1);
			}

		} catch (ConnectionPoolException e) {
			throw new ConnectionPoolException(
					"ConnectionPoolException in countUserOrdersTest "
							+ "whule taking connection. ", e);
		} catch (SQLException e) {
			throw new SQLException("SQLException in countUserOrdersTest. ", e);
		} finally {
		
				shopConnectionPool.closeConnection(con, countStatement, rs);

		}

		assertEquals("Counting doesn't match", totalUnits,
				getBeanDao.countUserOrders(1));
	}

	@Test
	public void getShopListTest() throws ActionDaoException, ConnectionPoolException {
		GetBeanAction getBeanDao = GetBeanDao.getInstance();
		ShopItem item = new ShopItem();
		item = getBeanDao.getShopList(1);
		assertNotNull("GetBeanDao returned null", item);
	}

	@Test
	public void countOrdersTest() throws ActionDaoException, ConnectionPoolException, SQLException {
		Connection con = null;
		ResultSet rs = null;
		GetBeanAction getBeanDao = GetBeanDao.getInstance();
		Statement countStatement = null;
		int totalUnits = 0;
		try {
			con = shopConnectionPool.takeConnection();

			countStatement = con.createStatement();
			rs = countStatement.executeQuery(SQL_COUNT_ORDERS);

			if (rs.next()) {
				totalUnits = rs.getInt(1);
			}
		} catch (ConnectionPoolException e) {
			throw new ConnectionPoolException(
					"ConnectionPoolException in countUserOrdersTest "
							+ "whule taking connection. ", e);
		} catch (SQLException e) {
			throw new SQLException("SQLException in countOrdersTest. ", e);
		} finally {
			
				shopConnectionPool.closeConnection(con, countStatement, rs);
			
		}

		assertEquals("Counting doesn't match", totalUnits,
				getBeanDao.countOrders());
	}

}
