package test.by.epam.task.six.shop.dao.impl;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import by.epam.task.six.shop.dao.InsertDataAction;
import by.epam.task.six.shop.dao.connection_pool.ConnectionPoolException;
import by.epam.task.six.shop.dao.connection_pool.ShopConnectionPool;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.dao.impl.InsertDataDao;
import by.epam.task.six.shop.entity.ShopItem;

public class InsertDataDaoTest {
	
	private static ShopConnectionPool shopConnectionPool = null;
	
	private final int USER_ID=4;
	private final String TEST = "test";
	private final String SQL_COUNT_BLOCKS_IN_BL="SELECT COUNT(id_block) FROM blacklist;";
	private final String SQL_COUNT_ITEMS="SELECT COUNT(id) FROM item;";
	private final String SQL_DELETE_USER_FROM_BL ="DELETE FROM blacklist WHERE id_user = 4 ;";
	private final String SQL_DELETE_ITEM ="DELETE FROM item WHERE name = 'test' ;";
	

	@BeforeClass
	public static void prepeareConnection() throws ConnectionPoolException {

		shopConnectionPool = ShopConnectionPool.getInstance();
		shopConnectionPool.initPoolData();

	}

	@AfterClass
	public static void disposeConnectionPool() throws ConnectionPoolException {

		shopConnectionPool.dispose();
	}

	
	@Test
	public void addNewItemTest() throws ActionDaoException, ConnectionPoolException, SQLException{
		InsertDataAction addNewItem = InsertDataDao.getInstance();
		Connection con = null;
		ResultSet rs = null;
		Statement countStatement = null;
		Statement deleteStatement = null;
		int afterDelete = 0;
		int beforeAdd = 0;
		int afterAdd = 0;
		ShopItem shopItem = new ShopItem();
		shopItem.setAmount(0);
		shopItem.setDescription(TEST);
		shopItem.setImage(TEST);
		shopItem.setMaterial(TEST);
		shopItem.setName(TEST);
		shopItem.setPrice(0);
		shopItem.setSize(0);
		
		
		try {
			con=shopConnectionPool.takeConnection();
			countStatement=con.createStatement();
			deleteStatement = con.createStatement();
			rs= countStatement.executeQuery(SQL_COUNT_ITEMS);
			while(rs.next()){
				beforeAdd = rs.getInt(1);
			}
			addNewItem.addNewItem(shopItem);
			
			rs= countStatement.executeQuery(SQL_COUNT_ITEMS);
			while(rs.next()){
				afterAdd = rs.getInt(1);
			}
			
			deleteStatement.executeUpdate(SQL_DELETE_ITEM);
			
			rs= countStatement.executeQuery(SQL_COUNT_ITEMS);
			
			while(rs.next()){
				afterDelete = rs.getInt(1);
			}
			assertEquals("User was not added.",beforeAdd+1, afterAdd);
			assertEquals("User was not deleted.",beforeAdd, afterDelete);
			
		} catch (ConnectionPoolException e) {
			throw new ConnectionPoolException ("Exception while taking pool connection.",e);
		} catch (SQLException e) {
			throw new SQLException("SQLException in addNewItemTest.",e);
		} finally {
			shopConnectionPool.closeConnection(deleteStatement);
			shopConnectionPool.closeConnection(con, countStatement, rs);
		}
		
		
	}
	
	@Test
	public void setUserToBlackList() throws ActionDaoException,  SQLException, ConnectionPoolException {
		InsertDataAction insertUserToBL = InsertDataDao.getInstance();
		Connection con = null;
		ResultSet rs = null;
		Statement countStatement = null;
		Statement deleteStatement = null;
		int afterDelete = 0;
		int beforeAdd = 0;
		int afterAdd = 0;
		
		try {
			con=shopConnectionPool.takeConnection();
			countStatement=con.createStatement();
			deleteStatement = con.createStatement();
			rs= countStatement.executeQuery(SQL_COUNT_BLOCKS_IN_BL);
			
			while(rs.next()){
				beforeAdd = rs.getInt(1);
			}
			insertUserToBL.setUserToBlackList(USER_ID);
			rs= countStatement.executeQuery(SQL_COUNT_BLOCKS_IN_BL);
			
			while(rs.next()){
				afterAdd = rs.getInt(1);
			}
			deleteStatement.executeUpdate(SQL_DELETE_USER_FROM_BL);
			rs= countStatement.executeQuery(SQL_COUNT_BLOCKS_IN_BL);
			
			while(rs.next()){
				afterDelete = rs.getInt(1);
			}
			
			assertEquals("User was not added.",beforeAdd+1, afterAdd);
			assertEquals("User was not deleted.",beforeAdd, afterDelete);
			
			
		} catch (SQLException e) {
			throw new SQLException("Error while counting units." , e);
		} catch (ConnectionPoolException e) {
			throw new ConnectionPoolException ("Exception while taking pool connection.",e);
		}finally {		
				shopConnectionPool.closeConnection(deleteStatement);
				shopConnectionPool.closeConnection(con, countStatement, rs);
			
		}
		
		
		
	}


}
