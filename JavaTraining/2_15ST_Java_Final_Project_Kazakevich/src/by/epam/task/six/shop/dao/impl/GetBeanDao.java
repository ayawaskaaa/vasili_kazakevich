package by.epam.task.six.shop.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import by.epam.task.six.shop.dao.GetBeanAction;
import by.epam.task.six.shop.dao.connection_pool.ConnectionPoolException;
import by.epam.task.six.shop.dao.connection_pool.ShopConnectionPool;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.entity.BlackListBean;
import by.epam.task.six.shop.entity.ShopItem;
import by.epam.task.six.shop.entity.UserOrder;
/**
 * Collects lists of application beans.
 * 
 * Collects data from data repository and returns application beans.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class GetBeanDao implements GetBeanAction {

	private static final GetBeanDao instance = new GetBeanDao();

	private static final String SQL_SELECT_USER_ORDER = "SELECT  O.id, O.state, It.name, It.image, U.amount, U.price_for_item "
			+ " FROM orders O INNER JOIN item  It  ON O.id_item=It.id "
			+ " inner join item_in_order U ON O.id = U.id_order WHERE O.id_user = ? LIMIT 5 OFFSET ? ;";
	private static final String SQL_SELECT_COUNT_SHOP_OREDERS = "SELECT COUNT(id) FROM orders "
			+ " WHERE id_user = ? ;";
	private static final String SQL_SELECT_SHOP_ITEMS = "SELECT id, name, image, material, size, price, amount, description FROM item"
			+ " LIMIT 5 OFFSET ?;";
	private static final String SQL_SELECT_COUNT_SHOP_ITEMS = "SELECT COUNT(id) FROM item ;";
	private static final String SQL_SELECT_SHOP_LIST_ITEMS = "SELECT id, name, image, material, size, price, amount, description FROM item"
			+ " WHERE id= ? ;";
	private static final String SQL_SELECT_ORDERS = "SELECT  O.id, O.id_user, O.state, It.name, It.image, U.amount, U.price_for_item "
			+ " FROM orders O INNER JOIN item  It  ON O.id_item=It.id "
			+ " inner join item_in_order U ON O.id = U.id_order LIMIT 5 OFFSET ? ;";
	private static final String SQL_SELECT_COUNT_ALL_SHOP_OREDERS = "SELECT COUNT(id) FROM orders ;";

	private static final String SQL_SELECT_BLACK_LIST = "SELECT B.id_block, B.id_user, U.login FROM blacklist B"
			+ " INNER JOIN user U ON B.id_user=U.id LIMIT 5 OFFSET ? ;";
	private static final String SQL_SELECT_COUNT_USERS_IN_BLACK_LIST = "SELECT COUNT(id_user) FROM blackList ;";
	

	@Override
	public List<BlackListBean> getBlackList(int pageNumber)	throws ActionDaoException {
		Connection con = null;
		PreparedStatement getBLStatement = null;
		ResultSet rs = null;
		List<BlackListBean> blackList = new ArrayList<BlackListBean>();
		ShopConnectionPool shopConnectionPool = ShopConnectionPool.getInstance();

		try {
			con = shopConnectionPool.takeConnection();

			getBLStatement = con.prepareStatement(SQL_SELECT_BLACK_LIST);
			getBLStatement.setInt(1, pageNumber);
			rs = getBLStatement.executeQuery();

			while (rs.next()) {
				blackList.add(fillBlackList(rs));

			}

		} catch (ConnectionPoolException e) {

			throw new ActionDaoException("Error while getting connection pool.", e);

		} catch (SQLException e) {
			System.out.println(e);
			throw new ActionDaoException("SQLException was caused while geting orders.", e);

		} finally {

				shopConnectionPool.closeConnection(con, getBLStatement, rs);

		}

		return blackList;
	}

	@Override
	public List<UserOrder> getOrders(int pageNumber) throws ActionDaoException {
		Connection con = null;
		PreparedStatement getOrdersStatement = null;
		ResultSet rs = null;
		List<UserOrder> orderList = new ArrayList<UserOrder>();
		ShopConnectionPool shopConnectionPool = ShopConnectionPool.getInstance();

		try {
			con = shopConnectionPool.takeConnection();

			getOrdersStatement = con.prepareStatement(SQL_SELECT_ORDERS);
			getOrdersStatement.setInt(1, pageNumber);
			rs = getOrdersStatement.executeQuery();

			while (rs.next()) {
				orderList.add(fillAdminOrderList(rs));

			}
		} catch (ConnectionPoolException e) {

			throw new ActionDaoException("Error while getting connection pool.", e);

		} catch (SQLException e) {			
			throw new ActionDaoException("SQLException was caused while geting orders.", e);
		}catch (NumberFormatException e) {			
			throw new ActionDaoException("SQLException was caused while geting orders.", e);
		} finally {
				shopConnectionPool.closeConnection(con, getOrdersStatement, rs);
		}

		return orderList;
	}

	@Override
	public List<ShopItem> getItemsForMainPage(int pageNumber)
			throws ActionDaoException {
		Connection con = null;
		PreparedStatement selectItemStatement = null;
		ResultSet rs = null;
		ShopConnectionPool shopConnectionPool = ShopConnectionPool.getInstance();

		List<ShopItem> shopItemList = new ArrayList<ShopItem>();

		try {

			con = shopConnectionPool.takeConnection();
			selectItemStatement = con.prepareStatement(SQL_SELECT_SHOP_ITEMS);
			selectItemStatement.setInt(1, pageNumber);
			rs = selectItemStatement.executeQuery();

			while (rs.next()) {

				shopItemList.add(fillShopItem(rs));
			}

		} catch (ConnectionPoolException e) {
			throw new ActionDaoException("Exception in GetBeanDao while getting connection.", e);
		} catch (SQLException e) {
			throw new ActionDaoException("Exception in GetBeanDao while executing request from DB.", e);
		}catch (NumberFormatException e) {			
			throw new ActionDaoException("SQLException was caused while executing request from DB.", e);
		} finally {

				shopConnectionPool.closeConnection(con, selectItemStatement, rs);

		}
		return shopItemList;

	}

	@Override
	public List<UserOrder> getUserOrders(int userId, int pageNumber) throws ActionDaoException {
		ShopConnectionPool shopConnectionPool = ShopConnectionPool.getInstance();
		Connection con = null;
		PreparedStatement getOrdersStatement = null;
		ResultSet rs = null;

		List<UserOrder> orderList = new ArrayList<UserOrder>();

		try {
			con = shopConnectionPool.takeConnection();

			getOrdersStatement = con.prepareStatement(SQL_SELECT_USER_ORDER);

			getOrdersStatement.setInt(1, userId);
			getOrdersStatement.setInt(2, pageNumber);

			rs = getOrdersStatement.executeQuery();

			while (rs.next()) {
				orderList.add(fillUserOrderList(rs));
			}
		} catch (ConnectionPoolException e) {
			throw new ActionDaoException("Exception in while getting connection.", e);
		} catch (NumberFormatException e) {
			throw new ActionDaoException("Exception while using ResultSet.", e);
		} catch (SQLException e) {
			throw new ActionDaoException("Error while getting info from DB.", e);
		} finally {
				shopConnectionPool.closeConnection(con, getOrdersStatement, rs);

		}
		return orderList;
	}

	@Override
	public ShopItem getShopList(int itemId) throws ActionDaoException {
		final ShopConnectionPool shopConnectionPool = ShopConnectionPool.getInstance();
		Connection con = null;
		ResultSet rs = null;
		PreparedStatement getShopListStatement = null;

		ShopItem shopItem = new ShopItem();

		try {
			con = shopConnectionPool.takeConnection();

			getShopListStatement = con.prepareStatement(SQL_SELECT_SHOP_LIST_ITEMS);

			getShopListStatement.setInt(1, itemId);
			

			rs = getShopListStatement.executeQuery();

			if (rs.next()) {
				shopItem=fillShopItem(rs);
			}
			
		} catch (ConnectionPoolException e) {
			throw new ActionDaoException(
					"Error while getting pool connection.", e);
		} catch (NumberFormatException e) {
			throw new ActionDaoException("Exception while using ResultSet.", e);
		} catch (SQLException e) {
			throw new ActionDaoException("Error while getting info from DB.", e);
		} finally {
				
				shopConnectionPool.closeConnection(con, getShopListStatement,rs);

		}

		return shopItem;
	}

	@Override
	public int countShopItems() throws ActionDaoException {
		int totalUnits = 0;
		Connection con = null;
		Statement countStatement = null;
		ResultSet rs = null;
		ShopConnectionPool shopConnectionPool = ShopConnectionPool.getInstance();
		try {

			con = shopConnectionPool.takeConnection();
			countStatement = con.createStatement();

			rs = countStatement.executeQuery(SQL_SELECT_COUNT_SHOP_ITEMS);

			while (rs.next()) {
				totalUnits = rs.getInt(1);

			}

		} catch (SQLException e) {
			throw new ActionDaoException("Error while geting page info.", e);
		} catch (ConnectionPoolException e) {
			throw new ActionDaoException("Exception in while getting connection.", e);
		} finally {
				shopConnectionPool.closeConnection(con, countStatement, rs);

		}

		return totalUnits;
	}

	@Override
	public int countUserOrders(int userId) throws ActionDaoException {
		
		int totalUnits = 0;
		Connection con = null;
		PreparedStatement countStatement = null;
		ResultSet rs = null;
		ShopConnectionPool shopConnectionPool = ShopConnectionPool.getInstance();
		try {
			con = shopConnectionPool.takeConnection();

			countStatement = con
					.prepareStatement(SQL_SELECT_COUNT_SHOP_OREDERS);
			countStatement.setInt(1, userId);
			rs = countStatement.executeQuery();

			while (rs.next()) {
				totalUnits = rs.getInt(1);

			}
		} catch (ConnectionPoolException e) {
			throw new ActionDaoException("Exception in while getting connection.", e);
		} catch (SQLException e) {
			throw new ActionDaoException("Error while geting page info.", e);
		} finally {
				shopConnectionPool.closeConnection(con, countStatement, rs);

		}

		return totalUnits;
	}

	@Override
	public int countOrders() throws ActionDaoException {
		Connection con = null;
		Statement counter = null;
		ResultSet rs = null;
		int totalUnits = 0;
		ShopConnectionPool shopConnectionPool = ShopConnectionPool.getInstance();
		try {

			con = shopConnectionPool.takeConnection();
			counter = con.createStatement();

			rs = counter.executeQuery(SQL_SELECT_COUNT_ALL_SHOP_OREDERS);

			while (rs.next()) {
				totalUnits = rs.getInt(1);

			}

		} catch (SQLException e) {

			throw new ActionDaoException("Error in counting orders.", e);

		} catch (ConnectionPoolException e) {

			throw new ActionDaoException("Error while getting connection pool.", e);

		} finally {
				shopConnectionPool.closeConnection(con, counter, rs);
		}

		return totalUnits;
	}

	public static GetBeanAction getInstance() throws ActionDaoException {
		return instance;
	}

	@Override
	public int countUsersInBlackList() throws ActionDaoException {
		Connection con = null;
		Statement counter = null;
		ResultSet rs = null;
		int totalUnits = 0;
		ShopConnectionPool shopConnectionPool = ShopConnectionPool.getInstance();
		try {

			con = shopConnectionPool.takeConnection();
			counter = con.createStatement();

			rs = counter.executeQuery(SQL_SELECT_COUNT_USERS_IN_BLACK_LIST);

			while (rs.next()) {
				totalUnits = rs.getInt(1);

			}

		} catch (SQLException e) {

			throw new ActionDaoException("Error in counting orders.", e);

		} catch (ConnectionPoolException e) {

			throw new ActionDaoException("Error while getting connection pool.", e);

		} finally {
				shopConnectionPool.closeConnection(con, counter, rs);

		}

		return totalUnits;
	}

	private ShopItem fillShopItem(ResultSet rs) throws NumberFormatException,
			SQLException {

		ShopItem shopItem = new ShopItem();

		shopItem.setId(Integer.parseInt(rs.getString(1)));

		shopItem.setName(rs.getString(2));

		shopItem.setImage(rs.getString(3));

		shopItem.setMaterial(rs.getString(4));

		shopItem.setSize(Integer.parseInt(rs.getString(5)));

		shopItem.setPrice(Integer.parseInt(rs.getString(6)));

		shopItem.setAmount(Integer.parseInt(rs.getString(7)));

		shopItem.setDescription(rs.getString(8));

		return shopItem;
	}

	private UserOrder fillUserOrderList(ResultSet rs) throws NumberFormatException,	SQLException {

		UserOrder userOrder = new UserOrder();

		userOrder.setOrderId(Integer.parseInt(rs.getString(1)));

		userOrder.setState(rs.getString(2));

		userOrder.setItemName(rs.getString(3));

		userOrder.setImage(rs.getString(4));

		userOrder.setAmount(Integer.parseInt(rs.getString(5)));

		userOrder.setPrice(Integer.parseInt(rs.getString(6)));

		return userOrder;
	}

	private BlackListBean fillBlackList(ResultSet rs) throws SQLException {

		BlackListBean blackList = new BlackListBean();

		blackList.setBlockId(rs.getInt(1));

		blackList.setUserId(rs.getInt(2));

		blackList.setLogin(rs.getString(3));

		return blackList;
	}
	
	private UserOrder fillAdminOrderList(ResultSet rs) throws NumberFormatException, SQLException{
		
		UserOrder userOrder = new UserOrder();

		userOrder.setOrderId(Integer.parseInt(rs.getString(1)));
		
		userOrder.setUserId(Integer.parseInt(rs.getString(2)));

		userOrder.setState(rs.getString(3));

		userOrder.setItemName(rs.getString(4));

		userOrder.setImage(rs.getString(5));

		userOrder.setAmount(Integer.parseInt(rs.getString(6)));

		userOrder.setPrice(Integer.parseInt(rs.getString(7)));

		

		return userOrder;
		}

}
