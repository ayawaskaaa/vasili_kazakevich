package by.epam.task.six.shop.service.util;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.controller.JspPageName;
import by.epam.task.six.shop.dao.GetBeanAction;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.dao.impl.GetBeanDao;
import by.epam.task.six.shop.entity.ShopItem;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
/**
 * Gets ShopItem objects from data repository.
 * 
 * Helps service layer classes to get array of ShopItem type objects.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class UserPageProvider  {
	
	private static UserPageProvider instance = new UserPageProvider();
	
	public static UserPageProvider getInstance(){
		return instance;
	};


	public List<ShopItem> getUserItemList(HttpServletRequest request,	HttpServletResponse response) throws CommandException {
		int pageNumber;
		String page = JspPageName.USER_PAGE;
		GetBeanAction pageInfoDao = null;
		int allShopItems = 0;
		List<ShopItem> shopItemList;

		try {
			pageInfoDao = GetBeanDao.getInstance();

			allShopItems = pageInfoDao.countShopItems();

			openPageFromBegining(request);

			setToSessionTotalNumberOfUnits(allShopItems, request);
			

			if (request.getSession().getAttribute(ServiceRequestParameterName.PAGE_NUMBER) == null) {
				pageNumber = 0;
				shopItemList = pageInfoDao.getItemsForMainPage(pageNumber);

				return shopItemList;
			

			} else {
				pageNumber = countPageNumbers(Integer.parseInt(request.getSession()
						.getAttribute(ServiceRequestParameterName.PAGE_NUMBER).toString()));
				shopItemList = pageInfoDao.getItemsForMainPage(pageNumber);				
				
				return shopItemList;
			}
		} catch (ActionDaoException e) {
			throw new CommandException("Exception while getting DAO", e);
		}


	}

	private void setToSessionTotalNumberOfUnits(Object totalItemCount, HttpServletRequest request) {
		request.getSession().setAttribute(ServiceRequestParameterName.TOTAL_UNITS, totalItemCount);

	}

	private int countPageNumbers(int number) {
		if (number == 1) {
			return 0;
		} else {
			int pageNumber = (number - 1) * 5;
			return pageNumber;
		}
	}

	private void openPageFromBegining(HttpServletRequest request) {
		if (request.getSession().getAttribute(ServiceRequestParameterName.PAGE_FLAG) != null) {

			if (!request.getSession().getAttribute(ServiceRequestParameterName.PAGE_FLAG).equals(JspPageName.USER_PAGE)) {
				request.getSession().setAttribute(ServiceRequestParameterName.PAGE_NUMBER,
								request.getParameter(ServiceRequestParameterName.FROM_FIRST_PAGE));
				
				request.getSession().setAttribute(ServiceRequestParameterName.PAGE_FLAG,
						JspPageName.USER_PAGE);

			}

		} else {
			request.getSession().setAttribute(ServiceRequestParameterName.PAGE_NUMBER,
							request.getParameter(ServiceRequestParameterName.FROM_FIRST_PAGE));
			
			request.getSession().setAttribute(ServiceRequestParameterName.PAGE_FLAG,
					JspPageName.USER_PAGE);

		}
	}





}
