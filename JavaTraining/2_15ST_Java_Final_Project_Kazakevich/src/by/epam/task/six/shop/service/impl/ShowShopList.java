package by.epam.task.six.shop.service.impl;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.controller.JspPageName;
import by.epam.task.six.shop.controller.ControllerParameterName;
import by.epam.task.six.shop.dao.GetBeanAction;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.dao.impl.GetBeanDao;
import by.epam.task.six.shop.entity.ShopItem;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;

/**
 * Prepares all information for building shop list page.
 *  
 * Contains method for getting array of ShopItem objects according to item id in users
 * shop list and making them available for further usage.  
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class ShowShopList implements ICommand {

	@Override
	public String execute(HttpServletRequest request,	HttpServletResponse response) throws CommandException {

		int pageNumber;
		GetBeanAction shopListDao = null;
		List itemIdList;
		List<ShopItem> shopList;
		int allShopItems ;

		try {

			if(request.getParameter(ServiceRequestParameterName.TEMP_MESSAGE)!=null){
				request.setAttribute(ServiceRequestParameterName.TEMP_MESSAGE,
						request.getParameter(ServiceRequestParameterName.TEMP_MESSAGE));
			}
			openPageFromBegining(request);
			itemIdList = (List) request.getSession().getAttribute(ServiceRequestParameterName.SHOP_LIST);			

			shopListDao = GetBeanDao.getInstance();
			
			

			allShopItems  = itemIdList.size();

			setToSessionTotalNumberOfUnits(allShopItems , request);

			if (request.getSession().getAttribute(ServiceRequestParameterName.PAGE_NUMBER) == null) {
				pageNumber = 0;
				shopList = getShopList(request, shopListDao, itemIdList, pageNumber);
				
				request.setAttribute(ControllerParameterName.SIMPLE_INFO, shopList);
				request.setAttribute(ServiceRequestParameterName.USER_MESSAGE,
						request.getParameter(ServiceRequestParameterName.USER_MESSAGE));

			} else {

				pageNumber = countPageNumbers(Integer.parseInt(request.getSession()
						.getAttribute(ServiceRequestParameterName.PAGE_NUMBER).toString()));

				shopList = getShopList(request, shopListDao, itemIdList, pageNumber);
				request.setAttribute(ControllerParameterName.SIMPLE_INFO, shopList);

				request.setAttribute(ServiceRequestParameterName.USER_MESSAGE,
						request.getParameter(ServiceRequestParameterName.USER_MESSAGE));
			}


		}  catch (ActionDaoException e) {
			throw new CommandException("Exception while getting info.",e);
		}

		return JspPageName.SHOP_LIST_PAGE;
	}

	private List<ShopItem> getShopList(HttpServletRequest request,
			GetBeanAction shopList, List itemIdList, int pageNumber)
			throws ActionDaoException {
		List<ShopItem> formedList = new ArrayList<ShopItem>();
		itemIdList = (List) request.getSession().getAttribute(ServiceRequestParameterName.SHOP_LIST);
		if (itemIdList.size() > pageNumber + 5) {
			for (int i = pageNumber; i < itemIdList.size()- (itemIdList.size() - (pageNumber + 5)); i++) {

				formedList.add(shopList.getShopList(Integer.parseInt(itemIdList.get(i).toString())));
				
			}
		} else {
			for (int i = pageNumber; i < itemIdList.size(); i++) {

				formedList.add(shopList.getShopList(Integer.parseInt(itemIdList	.get(i).toString())));
			}
		}
		return formedList;
	}

	private void setToSessionTotalNumberOfUnits(int totalItemCount, HttpServletRequest request) {
		request.getSession().setAttribute(ServiceRequestParameterName.TOTAL_UNITS, totalItemCount);

	}

	private int countPageNumbers(int number) {
		if (number == 1) {
			return 0;
		} else {
			int pageNumber = (number - 1) * 5;
			return pageNumber;
		}
	}

	private void openPageFromBegining(HttpServletRequest request) {
		if (request.getSession().getAttribute(ServiceRequestParameterName.PAGE_FLAG) != null) {

			if (!request.getSession().getAttribute(ServiceRequestParameterName.PAGE_FLAG).equals(JspPageName.SHOP_LIST_PAGE)) {
				request.getSession().setAttribute(ServiceRequestParameterName.PAGE_NUMBER,
								request.getParameter(ServiceRequestParameterName.FROM_FIRST_PAGE));
				
				request.getSession().setAttribute(ServiceRequestParameterName.PAGE_FLAG,
						JspPageName.SHOP_LIST_PAGE);

			}

		} else {
			request.getSession().setAttribute(ServiceRequestParameterName.PAGE_NUMBER,
							request.getParameter(ServiceRequestParameterName.FROM_FIRST_PAGE));
			
			request.getSession().setAttribute(ServiceRequestParameterName.PAGE_FLAG,
					JspPageName.SHOP_LIST_PAGE);

		}
	}

}
