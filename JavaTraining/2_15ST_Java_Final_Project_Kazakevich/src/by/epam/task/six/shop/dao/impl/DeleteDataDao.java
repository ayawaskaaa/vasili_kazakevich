package by.epam.task.six.shop.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import by.epam.task.six.shop.dao.DeleteDataAction;
import by.epam.task.six.shop.dao.connection_pool.ConnectionPoolException;
import by.epam.task.six.shop.dao.connection_pool.ShopConnectionPool;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
/**
 * Interface contains tools for deleting data.
 * 
 * Takes parameters for navigation and deletes data from data repository.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class DeleteDataDao implements DeleteDataAction {

	private static final String SQL_UPDATE_DELETE_USER_FROM_BLACK_LIST="DELETE FROM blacklist WHERE id_block = ?";
	private static final String SQL_UPDATE_DELETE_ITEM="DELETE FROM item WHERE id = ?";
	
	private static final DeleteDataDao instance = new DeleteDataDao();
	
	@Override
	public void deleteFromBlackList(int blockId) throws ActionDaoException {
		Connection con = null;
		PreparedStatement deleteStatement = null;
		final ShopConnectionPool shopConnectionPool = ShopConnectionPool.getInstance();
		try {
			con = shopConnectionPool.takeConnection();
			deleteStatement=con.prepareStatement(SQL_UPDATE_DELETE_USER_FROM_BLACK_LIST);
			deleteStatement.setInt(1, blockId);
			
			deleteStatement.executeUpdate();
			
			
			
		} catch (ConnectionPoolException e) {
			throw new ActionDaoException("Exception while getting pool connection in DeleteFromBlackListDao", e);
		} catch (SQLException e) {
			throw new ActionDaoException("Exception while deleting in DeleteFromBlackListDao.", e);
		}finally{
				shopConnectionPool.closeConnection(con,deleteStatement);

			}
		

	}

	@Override
	public void deleteItem(int itemId) throws ActionDaoException {
		Connection con = null;
		PreparedStatement deleteStatement = null;
		final ShopConnectionPool shopConnectionPool = ShopConnectionPool.getInstance();
		try {
			con = shopConnectionPool.takeConnection();
			deleteStatement=con.prepareStatement(SQL_UPDATE_DELETE_ITEM);
			deleteStatement.setInt(1, itemId);
			
			deleteStatement.executeUpdate();
			
			
			
		} catch (ConnectionPoolException e) {
			throw new ActionDaoException("Exception while getting pool connection in AddItemDao", e);
		} catch (SQLException e) {
			throw new ActionDaoException("Exception while statement creation in AddItemDao.", e);
		}finally{
				shopConnectionPool.closeConnection(con,deleteStatement);

			}
		

	}
	
	public static DeleteDataAction getInstance() throws ActionDaoException {
		
		return instance;	
	}

}
