package by.epam.task.six.shop.dao;

/**
 * Definition of parameters used in DAO.
 * 
 * All parameters which used in DAO are contained here.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class DaoParemeterName {
	
	private DaoParemeterName(){}
	
	public static final String LOGIN = "login";
	public static final String PASSWORD = "password";
	public static final String CONFIRM_PASSWORD = "confirm_password";
	public static final String MAIL = "mail";



}
