package by.epam.task.six.shop.dao;

import by.epam.task.six.shop.dao.exception.ActionDaoException;

/**
 * Interface contains tools for deleting data.
 * 
 * Takes parameters for navigation and deletes data from data repository.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public interface DeleteDataAction {
	
	public void deleteFromBlackList(int blockId) throws ActionDaoException;
	
	public void deleteItem(int itemId) throws ActionDaoException;

}
