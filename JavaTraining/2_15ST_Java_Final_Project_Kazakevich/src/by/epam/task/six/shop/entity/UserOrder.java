package by.epam.task.six.shop.entity;
/**
 * Project JavaBean component.
 * 
 * Contains components of shop order.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class UserOrder {
	
	private int userId;	
	private int orderId;
	private String itemName;
	private String image;
	private int amount;
	private int price;
	private String state;
	
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public int getOrderId() {
		return orderId;
	}
	public void setOrderId(int orderId) {
		this.orderId = orderId;
	}
	public String getItemName() {
		return itemName;
	}
	public void setItemName(String itemName) {
		this.itemName = itemName;
	}
	public String getImage() {
		return image;
	}
	public void setImage(String image) {
		this.image = image;
	}
	public int getAmount() {
		return amount;
	}
	public void setAmount(int amount) {
		this.amount = amount;
	}
	public int getPrice() {
		return price;
	}
	public void setPrice(int price) {
		this.price = price;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}

}
