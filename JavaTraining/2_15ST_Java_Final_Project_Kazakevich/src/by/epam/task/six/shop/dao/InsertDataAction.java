package by.epam.task.six.shop.dao;

import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.entity.ShopItem;
/**
 * Inserts data to data repository.
 * 
 * Takes enter parameters and writes them in to data repository.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public interface InsertDataAction {
	
	public void makeAnOrder(int userId, int itemId, int amount, int priceForItem) throws ActionDaoException ;
	
	public void setUserToBlackList(int idUser) throws ActionDaoException;
	
	public void addNewItem(ShopItem item) throws ActionDaoException ;
	
	
}
