package by.epam.task.six.shop.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.controller.ControllerParameterName;
import by.epam.task.six.shop.controller.JspPageName;
import by.epam.task.six.shop.entity.ShopItem;
import by.epam.task.six.shop.entity.ShopUser;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
import by.epam.task.six.shop.service.util.AdminPageProvider;
/**
 * Prepares all information for building admin page.
 *  
 * Contains method for getting array of ShopItem objects and making them available for further usage.  
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class BuildAdminPage implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		
		AdminPageProvider adminPageProvider = null;
		List<ShopItem> shopItemList;
		ShopUser user;
		String userLogin;	
		
		user = (ShopUser) request.getSession().getAttribute(ServiceRequestParameterName.USER_INFO);
		userLogin = user.getLogin();
		adminPageProvider=AdminPageProvider.getInstance();
		shopItemList= adminPageProvider.getAdminItemList(request, response);
		
		if(request.getParameter(ServiceRequestParameterName.TEMP_MESSAGE)!=null){
			request.setAttribute(ServiceRequestParameterName.TEMP_MESSAGE,
					request.getParameter(ServiceRequestParameterName.TEMP_MESSAGE));
		}
		request.setAttribute(ControllerParameterName.SIMPLE_INFO, shopItemList);
		request.setAttribute(ServiceRequestParameterName.USER_MESSAGE, userLogin);
		return JspPageName.ADMIN_PAGE;
		
	}

}
