package by.epam.task.six.shop.controller.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import by.epam.task.six.shop.controller.ControllerParameterName;

/**
 * Checks users rights for some actions.
 * 
 * Class checks all requests. If user is unsigned or has no rights for some action
 * he will be redirected to start page.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class ShopUserPageSecurityFilter implements Filter {

	private int idRole;
	private static final String REDIRECTION_TO_VISITOR_PAGE = "http://localhost:8080/ColanderShop/controller?command=build_visitor_page&userMessage=illegal_enter";

	@Override
	public void destroy() {
		

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {

		HttpServletRequest request = (HttpServletRequest) req;
		HttpServletResponse response = (HttpServletResponse) resp;


		if (request.getParameter(ControllerParameterName.COMMAND_NAME).equals(
				ControllerParameterName.SHOW_SHOP_LIST)
				|| request.getParameter(ControllerParameterName.COMMAND_NAME)
						.equals(ControllerParameterName.BUILD_ORDER_LIST)
				|| request.getParameter(ControllerParameterName.COMMAND_NAME)
						.equals(ControllerParameterName.PAY_FOR_ORDER)
				|| request.getParameter(ControllerParameterName.COMMAND_NAME)
						.equals(ControllerParameterName.DELETE_FROM_SHOP_LIST)
				|| request.getParameter(ControllerParameterName.COMMAND_NAME)
						.equals(ControllerParameterName.MAKE_AN_ORDER)) {

			if (request.getSession().getAttribute(request.getSession().getId()) == null) {

				request.setAttribute("userMessage", "illegal_enter");

				response.sendRedirect(REDIRECTION_TO_VISITOR_PAGE);
				return;

			}
		}

		if (request.getParameter(ControllerParameterName.COMMAND_NAME).equals(
				ControllerParameterName.DELETE_ITEM)
				|

				request.getParameter(ControllerParameterName.COMMAND_NAME)
						.equals(ControllerParameterName.UPDATE_ITEM)
				| request.getParameter(ControllerParameterName.COMMAND_NAME)
						.equals(ControllerParameterName.BUILD_BLACK_LIST_PAGE)
				| request.getParameter(ControllerParameterName.COMMAND_NAME)
						.equals(ControllerParameterName.BUILD_ADMIN_ORDER_LIST)
				| request.getParameter(ControllerParameterName.COMMAND_NAME)
						.equals(ControllerParameterName.SET_TO_BLACK_LIST)
				| request.getParameter(ControllerParameterName.COMMAND_NAME)
						.equals(ControllerParameterName.DELETE_FROM_BLACK_LIST)
				| request.getParameter(ControllerParameterName.COMMAND_NAME)
						.equals(ControllerParameterName.CHANGE_ORDER_STATE)
				| request.getParameter(ControllerParameterName.COMMAND_NAME)
						.equals(ControllerParameterName.BUILD_ADMIN_PAGE)) {


			if (request.getSession().getAttribute(request.getSession().getId()) == null
					|| Integer.parseInt(request.getSession()
							.getAttribute(request.getSession().getId())
							.toString()) != 2) {

				request.setAttribute("userMessage", "illegal_enter");

				response.sendRedirect(REDIRECTION_TO_VISITOR_PAGE);
				return;

			}
		}

		chain.doFilter(req, resp);

	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {
		

	}

}
