package by.epam.task.six.shop.dao;

import java.util.List;

import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.entity.BlackListBean;
import by.epam.task.six.shop.entity.ShopItem;
import by.epam.task.six.shop.entity.UserOrder;
/**
 * Collects lists of application beans.
 * 
 * Collects data from data repository and returns application beans.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public interface GetBeanAction {
	
	public List<BlackListBean> getBlackList(int pageNumber)	throws ActionDaoException;
	public List<UserOrder> getOrders(int pageNumber) throws ActionDaoException ;
	public List<ShopItem> getItemsForMainPage(int pageNumber) throws ActionDaoException ;
	public List<UserOrder> getUserOrders(int userId, int pageNumber) throws ActionDaoException;
	public ShopItem getShopList(int itemId) throws ActionDaoException;
	
	public int  countShopItems() throws ActionDaoException;
	public int  countUserOrders(int userId) throws ActionDaoException;
	public int countOrders() throws ActionDaoException;
	public int countUsersInBlackList() throws ActionDaoException;

}
