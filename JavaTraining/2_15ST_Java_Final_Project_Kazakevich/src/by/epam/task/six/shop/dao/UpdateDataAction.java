package by.epam.task.six.shop.dao;

import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.entity.ShopItem;
/**
 * Interface for updating data in data repository.
 * 
 * Takes parameters and replaces old data by new.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public interface UpdateDataAction {
	
	public void changeState(String state, int idOrder) throws ActionDaoException;
	public void updateItem(ShopItem item) throws ActionDaoException;
	public void payForOrder(int idUser, int idOrder) throws ActionDaoException;

	

}
