package by.epam.task.six.shop.dao.exception;

import by.epam.task.six.shop.controller.exception.ControllerException;
/**
 * DAO layer exception.
 *  
 * Signals that an error occurred in connection pool.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class ConnectionDaoException extends ControllerException{
	
	private static final long serialVersionUID=1L;
	
	public ConnectionDaoException(String msg) {
		super(msg);
	}

	public ConnectionDaoException(String msg, Exception e) {
		super(msg, e);
	}

	
	
	

}