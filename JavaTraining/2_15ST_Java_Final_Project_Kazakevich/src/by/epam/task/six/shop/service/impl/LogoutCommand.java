package by.epam.task.six.shop.service.impl;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.exception.CommandException;
/**
 * Performs logout action. 
 * 
 * User session should be invalidated. User is directed to start page.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class LogoutCommand implements ICommand {
	
	private static final String BUILD_VISITOR_PAGE_REQUEST="?command=build_visitor_page";
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		
	
			request.getSession().removeAttribute(request.getSession().getId());
			request.getSession().invalidate();
			return BUILD_VISITOR_PAGE_REQUEST;
	}

}
