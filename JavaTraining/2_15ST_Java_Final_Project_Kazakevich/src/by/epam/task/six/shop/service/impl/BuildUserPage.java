package by.epam.task.six.shop.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.controller.ControllerParameterName;
import by.epam.task.six.shop.controller.JspPageName;
import by.epam.task.six.shop.entity.ShopItem;
import by.epam.task.six.shop.entity.ShopUser;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
import by.epam.task.six.shop.service.util.UserPageProvider;
/**
 * Prepares all information for building user page.
 *  
 * Contains method for getting array of ShopItams objects and making them available for further usage.  
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class BuildUserPage implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		
		UserPageProvider userPageProvider = null;
		List<ShopItem> shopItemList;
		ShopUser user;
		String userLogin;
		
		if(request.getParameter(ServiceRequestParameterName.TEMP_MESSAGE)!=null){
			request.setAttribute(ServiceRequestParameterName.TEMP_MESSAGE,
					request.getParameter(ServiceRequestParameterName.TEMP_MESSAGE));
		}
		
		user = (ShopUser) request.getSession().getAttribute(ServiceRequestParameterName.USER_INFO);
		userLogin = user.getLogin();
		userPageProvider=UserPageProvider.getInstance();
		shopItemList= userPageProvider.getUserItemList(request, response);
		request.setAttribute(ControllerParameterName.SIMPLE_INFO, shopItemList);
		request.setAttribute(ServiceRequestParameterName.USER_MESSAGE, userLogin);
		return JspPageName.USER_PAGE;
		
	}

}
