package by.epam.task.six.shop.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import by.epam.task.six.shop.dao.InsertDataAction;
import by.epam.task.six.shop.dao.connection_pool.ConnectionPoolException;
import by.epam.task.six.shop.dao.connection_pool.ShopConnectionPool;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.entity.ShopItem;
/**
 * Inserts data to data repository.
 * 
 * Takes enter parameters and writes them in to data repository.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class InsertDataDao implements InsertDataAction {

	private static final String SQL_INSERT_USER_IN_TO_BL = " INSERT IGNORE INTO blacklist (id_user) values( ? ) ;";
	private static final String SQL_INSERT_NEW_ORDER = "INSERT INTO orders(id, id_user,id_item, state) values"
			+ "(?, ?, ?, '�� �������') ;";

	private static final String SQL_UPDATE_ITEM_AMOUNT="UPDATE item SET amount = amount - ? WHERE id = ? ;";
	private static final String SQL_INSERT_ITEM_IN_ORDER = "INSERT INTO item_in_order (id_order, id_item, amount, price_for_item) values"
			+ "(?, ?, ?, ?) ;";
	private static final String SQL_SELECT_COUNT_SHOP_OREDERS = "SELECT COUNT(id) FROM orders ;";
	private static final String SQL_UPDATE_ADD_NEW_ITEM = "INSERT INTO item (name, image, material, size, amount,"
			+ " description, price) values (?, ?, ?, ?, ?, ?, ?) ;";

	private static final InsertDataDao instance = new InsertDataDao();

	@Override
	public void makeAnOrder(int userId, int itemId, int amount, int priceForItem) throws ActionDaoException {

		final ShopConnectionPool shopConnectionPool = ShopConnectionPool.getInstance();

		Connection con = null;
		ResultSet rs = null;
		PreparedStatement insertOrder = null;
		Statement counter = null;
		PreparedStatement insertItemInOrder = null;
		PreparedStatement updateItemAmount = null;
		int orderId = 0;

		try {

			con = shopConnectionPool.takeConnection();

			counter = con.prepareStatement(SQL_SELECT_COUNT_SHOP_OREDERS);

			rs = counter.executeQuery(SQL_SELECT_COUNT_SHOP_OREDERS);

			if (rs.next()) {

				orderId = rs.getInt(1);

			}

			insertOrder = con.prepareStatement(SQL_INSERT_NEW_ORDER);
			insertOrder.setInt(1, orderId);
			insertOrder.setInt(2, userId);
			insertOrder.setInt(3, itemId);
			insertOrder.executeUpdate();

			insertItemInOrder = con.prepareStatement(SQL_INSERT_ITEM_IN_ORDER);
			insertItemInOrder.setInt(1, orderId);
			insertItemInOrder.setInt(2, itemId);
			insertItemInOrder.setInt(3, amount);
			insertItemInOrder.setInt(4, priceForItem);

			insertItemInOrder.executeUpdate();
			
			updateItemAmount=con.prepareStatement(SQL_UPDATE_ITEM_AMOUNT);
			updateItemAmount.setInt(1, amount);
			updateItemAmount.setInt(2, itemId);
			updateItemAmount.executeUpdate();

		} catch (SQLException e) {

			throw new ActionDaoException("SQLException in InsertDataDao class.", e);

		} catch (ConnectionPoolException e) {
			throw new ActionDaoException("Exception while getting pool connection", e);
		} finally {
				
				shopConnectionPool.closeConnection(insertItemInOrder);
				shopConnectionPool.closeConnection(updateItemAmount);
				
				shopConnectionPool.closeConnection(counter);
				shopConnectionPool.closeConnection(con, insertOrder);
		}
	}

	@Override
	public void setUserToBlackList(int idUser) throws ActionDaoException {
		Connection con = null;
		PreparedStatement setToBLStatement = null;
		final ShopConnectionPool shopConnectionPool = ShopConnectionPool.getInstance();

		try {
			con = shopConnectionPool.takeConnection();

			setToBLStatement = con.prepareStatement(SQL_INSERT_USER_IN_TO_BL);
			setToBLStatement.setInt(1, idUser);
			setToBLStatement.executeUpdate();

		} catch (ConnectionPoolException e) {
			throw new ActionDaoException("Exception while getting pool connection", e);
		} catch (SQLException e) {

			throw new ActionDaoException("SQLException while insert user in black list.", e);
		} finally {
				shopConnectionPool.closeConnection(con, setToBLStatement);
		}

	}

	@Override
	public void addNewItem(ShopItem item) throws ActionDaoException {
		Connection con = null;
		PreparedStatement addStatement = null;
		final ShopConnectionPool shopConnectionPool = ShopConnectionPool
				.getInstance();
		try {
			con = shopConnectionPool.takeConnection();
			addStatement = con.prepareStatement(SQL_UPDATE_ADD_NEW_ITEM);
			addStatement.setString(1, item.getName());
			addStatement.setString(2, item.getImage());
			addStatement.setString(3, item.getMaterial());
			addStatement.setInt(4, item.getSize());
			addStatement.setInt(5, item.getAmount());
			addStatement.setString(6, item.getDescription());
			addStatement.setInt(7, item.getPrice());

			addStatement.executeUpdate();

		} catch (SQLException e) {

			throw new ActionDaoException("Adding failed in InsertDataDao.", e);

		} catch (ConnectionPoolException e) {

			throw new ActionDaoException("Exception while getting pool connection in AddItemDao", e);

		} finally {

				shopConnectionPool.closeConnection(con, addStatement);

		}

	}

	public static InsertDataAction getInstance() throws ActionDaoException {

		return instance;
	}

}
