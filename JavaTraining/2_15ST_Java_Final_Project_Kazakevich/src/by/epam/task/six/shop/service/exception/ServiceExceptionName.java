package by.epam.task.six.shop.service.exception;

/**
 * Definition of errors names in service layer.
 * 
 * All errors names are contained here.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class ServiceExceptionName {
	
	private ServiceExceptionName(){}
	
	public static final String UNIQUE_LOGIN = "unique_loginexception";
	public static final String AUTHORIZATION = "authorizationexception";
	public static final String LOGIN = "loginexception";
	public static final String PASSWORD = "passwordexception";
	public static final String CONFIRM = "confirmexception";
	public static final String MAIL = "mailexception";
	

}
