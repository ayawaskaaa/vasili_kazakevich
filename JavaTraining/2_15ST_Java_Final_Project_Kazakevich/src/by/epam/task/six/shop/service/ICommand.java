package by.epam.task.six.shop.service;
/**
 * 
 * Interface is designed to execute user commands.
 * 
 * Realization of this interface depends on user command.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.service.exception.CommandException;

public interface ICommand {
public String execute (HttpServletRequest request, HttpServletResponse response) throws CommandException; 
}