package by.epam.task.six.shop.controller.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
/**
 * 
 * Changes character encoding to UTF-8.
 * 
 * This filter takes characters from request and changes its encoding to UTF-8.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class CharsetShopFilter implements Filter {

	private String encoding;

	private final String CHAR_ENCODING = "characterEncoding";

	@Override
	public void destroy() {

	}

	@Override
	public void doFilter(ServletRequest req, ServletResponse resp,
			FilterChain chain) throws IOException, ServletException {

		req.setCharacterEncoding(encoding);
		resp.setCharacterEncoding(encoding);

		chain.doFilter(req, resp);
	}

	@Override
	public void init(FilterConfig fConfig) throws ServletException {

		encoding = fConfig.getInitParameter(CHAR_ENCODING);

	}

}
