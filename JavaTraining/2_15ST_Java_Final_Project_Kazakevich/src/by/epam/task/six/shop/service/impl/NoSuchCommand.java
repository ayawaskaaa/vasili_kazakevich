package by.epam.task.six.shop.service.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.controller.JspPageName;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.exception.CommandException;

/**
 * Reacts on situation, when command does't exist in commands list of {@link CommandHelper.class}.
 * 
 * If command is not found, user will be directed to error page.
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */

public class NoSuchCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		
		return JspPageName.ERROR_PAGE;
	}

}
