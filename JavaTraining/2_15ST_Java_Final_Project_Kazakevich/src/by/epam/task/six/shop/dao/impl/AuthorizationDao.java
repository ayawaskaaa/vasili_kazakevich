package by.epam.task.six.shop.dao.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import by.epam.task.six.shop.dao.AuthorizationAction;
import by.epam.task.six.shop.dao.connection_pool.ConnectionPoolException;
import by.epam.task.six.shop.dao.connection_pool.ShopConnectionPool;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.entity.ShopUser;
/**
 * Interface contains tools related to authorization.
 * 
 * Takes parameters for login or register actions, collect data for response and
 * returns object of ShopUser type.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class AuthorizationDao implements AuthorizationAction {

	private static final String SQL_SELECT_ID_ROLE_BY_LOGIN_AND_PASSWORD = "SELECT id, login, mail,id_role  FROM user WHERE password=? AND login=?;";
	private static final String SQL_INSERT_NEW_USER = "INSERT INTO user(login,password,mail,id_role) values"
			+ "(?,?,?, 1) ;";
	private static final String SQL_SELECT_NEW_USER_ID = "SELECT last_insert_id() ;";
	private static final AuthorizationDao instance = new AuthorizationDao();

	@Override
	public ShopUser registerNewUser(String name, String password, String mail) throws ActionDaoException {

		Connection con = null;
		ShopUser user = new ShopUser();
		ResultSet rs = null ;

		PreparedStatement registerStatement = null;
		Statement getIdStatement = null;
		final ShopConnectionPool shopConnectionPool = ShopConnectionPool.getInstance();

		try {
			con = shopConnectionPool.takeConnection();

			registerStatement = con.prepareStatement(SQL_INSERT_NEW_USER);
			registerStatement.setString(1, name);
			registerStatement.setString(2, password);
			registerStatement.setString(3, mail);

			registerStatement.executeUpdate();
			
			getIdStatement = con.createStatement();			
			rs = getIdStatement.executeQuery(SQL_SELECT_NEW_USER_ID);
			if (rs.next()) {
				user = fillUser(rs, name, mail, 1);
			}

		} catch (ConnectionPoolException e) {
			throw new ActionDaoException("Exception while getting pool connection in AuthorizationDao",	e);
		} catch (SQLException e) {
			throw new ActionDaoException("Registration failed.", e);
		} finally {
			
				if(registerStatement!=null){
				shopConnectionPool.closeConnection(registerStatement);
				}
				if(rs!=null|getIdStatement!=null){
				
				shopConnectionPool.closeConnection(con, getIdStatement, rs);
				}			

		}

		return user;

	}

	@Override
	public ShopUser login(String login, String password) throws ActionDaoException {

		Connection con = null;
		ShopUser user = new ShopUser();
		ResultSet rs = null;

		PreparedStatement loginStatement = null;
		final ShopConnectionPool shopConnectionPool = ShopConnectionPool.getInstance();

		try {
			con = shopConnectionPool.takeConnection();

			loginStatement = con.prepareStatement(SQL_SELECT_ID_ROLE_BY_LOGIN_AND_PASSWORD);

			loginStatement.setString(2, login);

			loginStatement.setString(1, password);

			rs = loginStatement.executeQuery();

			if (rs.next()) {
				user = fillUser(rs);
			} else {
				throw new ActionDaoException("User entered wrong enter parameters.");
			}
		} catch (ConnectionPoolException e) {
			throw new ActionDaoException("Exception while getting pool connection in AuthorizationDao",
					e);
		} catch (SQLException e) {
			throw new ActionDaoException("Login failed.", e);
		} finally {
				shopConnectionPool.closeConnection(con, loginStatement, rs);			

		}
		return user;
	}

	private ShopUser fillUser(ResultSet rs) throws NumberFormatException,
			SQLException {
		ShopUser user = new ShopUser();

		user.setId(Integer.parseInt(rs.getString(1)));

		user.setLogin((rs.getString(2)));

		user.setMail(rs.getString(3));

		user.setIdRole(Integer.parseInt(rs.getString(4)));

		return user;
	}

	public static AuthorizationAction getInstance() throws ActionDaoException	 {

		return instance;
	}

	private ShopUser fillUser(ResultSet rs, String userName, String userMail,
			int role) throws NumberFormatException, SQLException {

		ShopUser shopUser = new ShopUser();

		shopUser.setId(Integer.parseInt(rs.getString(1)));

		shopUser.setLogin(userName);

		shopUser.setMail(userMail);

		shopUser.setIdRole(role);

		return shopUser;
	}

}
