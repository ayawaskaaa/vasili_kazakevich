package by.epam.task.six.shop.controller;
/**
 * Definition of pages of application.
 * 
 * All jsp pages names are contained here.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public final class JspPageName {
	
	private JspPageName(){}
	
	public static final String USER_PAGE = "/WEB-INF/jsp/userPage.jsp";
	public static final String VISITOR_PAGE = "/WEB-INF/jsp/visitorPage.jsp";
	public static final String ERROR_PAGE = "error.jsp";
	public static final String ADMIN_PAGE = "/WEB-INF/jsp/adminPage.jsp";
	public static final String LOGIN_PAGE = "/WEB-INF/jsp/loginPage.jsp";
	public static final String REGISTER_PAGE = "/WEB-INF/jsp/registerPage.jsp";
	public static final String INDEX_PAGE = "/WEB-INF/jsp/index.jsp";
	public static final String SHOP_LIST_PAGE = "/WEB-INF/jsp/shopListPage.jsp";
	public static final String ORDER_PAGE = "/WEB-INF/jsp/orderPage.jsp";
	public static final String ADMIN_ORDER_PAGE = "/WEB-INF/jsp/adminOrdersPage.jsp";
	public static final String BLACK_LIST_PAGE = "/WEB-INF/jsp/blackListPage.jsp";
	
	
	

}
