package by.epam.task.six.shop.dao.connection_pool;

/**
 * Definition of database parameters.
 * 
 * All database parameters are contained here.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class DBParameter {
	
	private DBParameter(){}
	
	public static final String DB_DRIVER="db.driver";
	public static final String DB_URL="db.url";
	public static final String DB_USER="db.user";
	public static final String DB_PASSWORD="db.password";
	public static final String DB_POOL_SIZE="db.poolsize";
	

}
