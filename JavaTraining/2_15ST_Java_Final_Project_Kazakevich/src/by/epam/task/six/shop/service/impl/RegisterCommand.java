package by.epam.task.six.shop.service.impl;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.controller.JspPageName;
import by.epam.task.six.shop.dao.AuthorizationAction;
import by.epam.task.six.shop.dao.DaoParemeterName;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.dao.impl.AuthorizationDao;
import by.epam.task.six.shop.dao.shop_md5.ShopMD5;
import by.epam.task.six.shop.entity.ShopUser;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
import by.epam.task.six.shop.service.exception.ServiceExceptionName;
import by.epam.task.six.shop.service.exception.WrongConfirmException;
import by.epam.task.six.shop.service.exception.WrongLoginException;
import by.epam.task.six.shop.service.exception.WrongMailException;
import by.epam.task.six.shop.service.exception.WrongPasswordException;
/**
 * Performs register action.
 * 
 * If user entered valid parameters, they will be singed in data repository. After successful sign gets ShopUser object
 * and makes it available for further usage. If enter parameters don't pass validation or user with such login already exist,
 * exception should be thrown. User is redirected to register page.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class RegisterCommand implements ICommand {

	private final Pattern passwordPattern = Pattern
			.compile("(?=^.{8,}$)((?=.*\\d)|(?=.*\\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$");
	private final Pattern mailPattern = Pattern
			.compile("^[_A-Za-z0-9-]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9-]+)*(\\.[A-Za-z]{2,})$");
	private final Pattern loginPattern = Pattern
			.compile("^[a-zA-Z][a-zA-Z0-9-_\\.]{1,20}$");
	private static final String URL_TO_USER_PAGE = "?command=build_user_page";
	private static final String URL_TO_REGISTER_PAGE_LOGIN = "?command=to_register_page"
			+ "&errorMessage=loginexception";
	private static final String URL_TO_REGISTER_PAGE_PASSWORD = "?command=to_register_page"
			+ "&errorMessage=passwordexception";
	private static final String URL_TO_REGISTER_PAGE_CONFIRM = "?command=to_register_page"
			+ "&errorMessage=confirmexception";
	private static final String URL_TO_REGISTER_PAGE_MAIL = "?command=to_register_page"
			+ "&errorMessage=mailexception";
	private static final String URL_TO_REGISTER_PAGE_UNIQUE = "?command=to_register_page"
			+ "&errorMessage=unique_loginexception";


	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) throws CommandException {

		String page = null;
		AuthorizationAction registerDao = null;
		ShopUser user;
		StringBuffer newRequest;

		try {
			if (registerValidation(request)) {

				String name = request.getParameter(DaoParemeterName.LOGIN);
				
				String mail = request.getParameter(DaoParemeterName.MAIL);

				String password = ShopMD5.getHash(request.getParameter(DaoParemeterName.PASSWORD));

				registerDao = AuthorizationDao.getInstance();

				user = (ShopUser) registerDao.registerNewUser(name, password, mail);

				
				
				request.getSession(true).setAttribute(request.getSession().getId(), 1);
				request.getSession(true).setAttribute(ServiceRequestParameterName.USER_INFO, user);
				return page=request.getRequestURL().toString()+URL_TO_USER_PAGE;

			}
		} catch (WrongLoginException e) {
			page = request.getRequestURL().toString()+URL_TO_REGISTER_PAGE_LOGIN;
			

		} catch (WrongPasswordException e) {

			
			 page = request.getRequestURL().toString()+URL_TO_REGISTER_PAGE_PASSWORD;

		} catch (WrongConfirmException e) {
			
			page = request.getRequestURL().toString()+URL_TO_REGISTER_PAGE_CONFIRM;


		} catch (WrongMailException e) {
			
			page = request.getRequestURL().toString()+URL_TO_REGISTER_PAGE_MAIL;

		} catch (ActionDaoException e1) {

			page = request.getRequestURL().toString()+URL_TO_REGISTER_PAGE_UNIQUE;
			

		} 

		return page;
	}

	private boolean registerValidation(HttpServletRequest request)
			throws WrongLoginException, WrongPasswordException,
			WrongConfirmException, WrongMailException {

		String name = request.getParameter(DaoParemeterName.LOGIN);
		String password = request.getParameter(DaoParemeterName.PASSWORD);
		String confirmPassword = request.getParameter(DaoParemeterName.CONFIRM_PASSWORD);
		String mail = request.getParameter(DaoParemeterName.MAIL);
		Matcher passwordMatcher;
		Matcher mailMatcher;
		Matcher loginMatcher;

		if (name == null) {
			throw new WrongLoginException("Login is null");
		}
		if (password == null) {
			throw new WrongPasswordException("Password is null");
		}
		if (confirmPassword == null) {
			throw new WrongConfirmException("Confirm is null");
		}
		if (mail == null) {
			throw new WrongMailException("Mail is null");
		}

		passwordMatcher = passwordPattern.matcher(password);
		mailMatcher = mailPattern.matcher(mail);
		loginMatcher = loginPattern.matcher(name);

		if (!loginMatcher.matches()) {

			throw new WrongLoginException("User entered invalid login");
		} else if (!passwordMatcher.matches()) {

			throw new WrongPasswordException("Password is invalid");
		} else if (!mailMatcher.matches()) {
			throw new WrongMailException("Mail is invalid");
		} else if (password.equals(confirmPassword)) {
			return true;
		} else {
			throw new WrongConfirmException("Confirm password does not match");
		}

	}

}
