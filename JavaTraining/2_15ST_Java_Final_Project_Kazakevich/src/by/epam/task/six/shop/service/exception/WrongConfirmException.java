package by.epam.task.six.shop.service.exception;

import by.epam.task.six.shop.controller.exception.ControllerException;
/**
 * Service layer exception.
 *  
 * If confirm password doesn't matches primary password,
 * than exception is thrown. 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class WrongConfirmException extends ControllerException {

private static final long serialVersionUID=1L;
	
	public WrongConfirmException(String msg) {
		super(msg);
	}

	public WrongConfirmException(String msg, Exception e) {
		super(msg, e);
	}

	
}
