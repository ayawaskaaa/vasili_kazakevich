package by.epam.task.six.shop.service.impl;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.controller.ControllerParameterName;
import by.epam.task.six.shop.controller.JspPageName;
import by.epam.task.six.shop.entity.UserOrder;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
import by.epam.task.six.shop.service.util.AdminOrderListProvider;
/**
 * Prepares all information for building admin order page.
 *  
 * Contains method for getting array of ShopOrder objects and making them available for further usage.  
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class BuildAdminOrderPage implements ICommand {

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) throws CommandException {
		
		List<UserOrder> orderList=null;
		AdminOrderListProvider orderProvider = AdminOrderListProvider.getInstance();
		
		
		if(request.getParameter(ServiceRequestParameterName.TEMP_MESSAGE)!=null){
			request.setAttribute(ServiceRequestParameterName.TEMP_MESSAGE,
					request.getParameter(ServiceRequestParameterName.TEMP_MESSAGE));
		}
		
		orderList=orderProvider.getUserOrders(request, response);
		request.setAttribute(ControllerParameterName.SIMPLE_INFO, orderList);
		

		return JspPageName.ADMIN_ORDER_PAGE;
	}

}
