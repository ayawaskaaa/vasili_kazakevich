package by.epam.task.six.shop.shop_taglib;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import by.epam.task.six.shop.entity.BlackListBean;
/**
 * Custom tag for black list page.
 * 
 * Corresponds for right layout of elements on black list page.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class BlackListTag extends TagSupport {
	
	private ArrayList <BlackListBean> blackList;
	private String deleteButton;
	private int unitNumber;
	private int pages;
	private String contextPath;

	private final static Logger log = Logger.getLogger(BlackListTag.class);
	
	public String getContextPath() {
		return contextPath;
	}
	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}
	public int getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(int unitNumber) {
		this.unitNumber = unitNumber;
	}
	

	public ArrayList <BlackListBean> getblackList() {
		return blackList;
	}
	public void setblackList(ArrayList<BlackListBean> blackList) {
		this.blackList = blackList;
		
	}
	public String getDeleteButton() {
		return deleteButton;
	}
	public void setDeleteButton(String deleteButton) {
		this.deleteButton = deleteButton;
	}



	
	
	public int doStartTag (){
		try {
			pageContext.getOut().write("<tr>");
			
		} catch (IOException e) {
			log.error("Error in writing from BlackListBeanTag.", e);
		};
		
		return EVAL_BODY_INCLUDE;}
	
	
	public int doAfterBody(){
		try {
			
		for(int i=0; i<blackList.size();i++){
			
			
			pageContext.getOut().write("<form   action=\""+contextPath+"/controller\" method=\"post\">");
			pageContext.getOut().write("<input  type=\"hidden\" name=\"command\" value=\"delete_from_black_list\"/>");
			pageContext.getOut().write("<input  type=\"hidden\" name=\"block_id\" value=\""+blackList.get(i).getBlockId()+"\"/>");
			pageContext.getOut().write("<td>"+blackList.get(i).getBlockId()+"</td>");
			pageContext.getOut().write("<td>"+blackList.get(i).getUserId()+"</td>");
			pageContext.getOut().write("<td>"+blackList.get(i).getLogin()+"</td>");
			pageContext.getOut().write("<td><input  type=\"submit\"  value=\""+deleteButton+"\"/>");
			pageContext.getOut().write("</form>");
			

			
			pageContext.getOut().write("<tr>");
			
			
			
			
			

		}
			

		} catch (IOException e) {
			log.error("Error in writing from BlackListTag.", e);
		};

		
		return SKIP_BODY;}
	
	

	
	public int doEndTag(){
		try {
			
			if(unitNumber%5==0){
				pages=unitNumber/5;
			}else{
				pages=(unitNumber/5)+1;
			}
			
			pageContext.getOut().write("<tr>");
			pageContext.getOut().write("</table>");
			pageContext.getOut().write("</center>");
		

			pageContext.getOut().write("<table border=0 align=\"right\" >");

			for(int i = 1; i<pages+1;i++){
				
				pageContext.getOut().write("<td><form   action=\""+contextPath+"/controller\" method=\"post\">");
				pageContext.getOut().write("<input  type=\"hidden\" name=\"command\" value=\"set_current_page\"/>");
				pageContext.getOut().write("<input  type=\"hidden\" name=\"pageNumber\" value=\""+i+"\"/>");
				pageContext.getOut().write("<input  type=\"submit\"  value=\""+i+"\"/>");
				pageContext.getOut().write("</form></td>");
				

			}
			pageContext.getOut().write("</table>");
			
		} catch (IOException e) {
			log.error("Error in writing from BlackListBeanTag.", e);
		};
		return SKIP_BODY;}

}



