package by.epam.task.six.shop.service.impl;


import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
/**
 * Sets item to shop list.
 * 
 * Saves item id in users session.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class SetItemToShopList implements ICommand {
	
	private static final String URL_TO_USER_PAGE_ADD = "?command=build_user_page&temp_message=success_add";

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

		List shopList = new ArrayList();



			if (request.getSession(true).getAttribute(ServiceRequestParameterName.SHOP_LIST) == null) {
				
				shopList.add(request.getParameter(ServiceRequestParameterName.ITEM_ID));
				
				request.getSession().setAttribute(ServiceRequestParameterName.SHOP_LIST, shopList);
				
				return request.getRequestURL()+ URL_TO_USER_PAGE_ADD;

			} else {

				shopList = (List) request.getSession().getAttribute(ServiceRequestParameterName.SHOP_LIST);
				if (!shopList.contains(request.getParameter(ServiceRequestParameterName.ITEM_ID))) {
					
					shopList.add(request.getParameter(ServiceRequestParameterName.ITEM_ID));
				}
				request.getSession().setAttribute(ServiceRequestParameterName.SHOP_LIST, shopList);
				return request.getRequestURL()+ URL_TO_USER_PAGE_ADD;
			}
	

	}

}
