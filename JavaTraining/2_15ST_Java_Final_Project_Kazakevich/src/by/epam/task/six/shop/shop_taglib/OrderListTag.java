package by.epam.task.six.shop.shop_taglib;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import by.epam.task.six.shop.entity.UserOrder;
/**
 * Custom tag for user order page.
 * 
 * Corresponds for right layout of elements on user order page.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class OrderListTag extends TagSupport {
	
	private ArrayList <UserOrder> orderList;
	private String contextPath;
	private String payButton;

	private int unitNumber;
	private int pages;
	private static Logger log = Logger.getLogger(OrderListTag.class);
	
	
	public int getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(int unitNumber) {
		this.unitNumber = unitNumber;
	}
	
	public String getContextPath() {
		return contextPath;
	}
	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}
	public String getpayButton() {
		return payButton;
	}
	public void setpayButton(String payButton) {
		this.payButton = payButton;
	}
	public ArrayList <UserOrder> getorderList() {
		return orderList;
	}
	public void setorderList(ArrayList<UserOrder> orderList) {
		this.orderList = orderList;
		
	}
	
	
	public int doStartTag (){
		try {
			pageContext.getOut().write("<tr>");
			
		} catch (IOException e) {
			log.error("Error in writing from UserOrderTag.", e);
		};
		
		return EVAL_BODY_INCLUDE;}
	
	
	public int doAfterBody(){
		try {
			
		for(int i=0; i<orderList.size();i++){
			

			
			
			pageContext.getOut().write("<td>"+orderList.get(i).getOrderId()+"</td>");
			pageContext.getOut().write("<td>"+orderList.get(i).getItemName()+"</td>");
			pageContext.getOut().write("<td><img alt=\"image\" src=\""+contextPath+orderList.get(i).getImage()+"\"></td>");
			pageContext.getOut().write("<td>"+orderList.get(i).getAmount()+"</td>");
			pageContext.getOut().write("<td>"+orderList.get(i).getPrice()+"</td>");
			pageContext.getOut().write("<td>"+orderList.get(i).getState()+"</td>");
			pageContext.getOut().write("<td><form   action=\""+contextPath+"/controller\" method=\"post\">");
			pageContext.getOut().write("<input  type=\"hidden\" name=\"command\" value=\"pay_for_order\"/>");
			pageContext.getOut().write("<input  type=\"hidden\" name=\"order_id\" value=\""+orderList.get(i).getOrderId()+"\"/>");
			pageContext.getOut().write("<input  type=\"submit\"  value=\""+payButton+"\"/>");
			pageContext.getOut().write("</form></td>");
			pageContext.getOut().write("<td>"+orderList.get(i).getAmount()*orderList.get(i).getPrice()+"</td>");

			pageContext.getOut().write("<tr>");
			
			
			

		}
			

		} catch (IOException e) {
			log.error("Error in writing from UserOrderTag.", e);
		};

		
		return SKIP_BODY;}
	
	

	
	public int doEndTag(){
		try {
			
			if(unitNumber%5==0){
				pages=unitNumber/5;
			}else{
				pages=(unitNumber/5)+1;
			}
			
			pageContext.getOut().write("<tr>");
			pageContext.getOut().write("</table>");
			pageContext.getOut().write("</center>");
		

			pageContext.getOut().write("<table border=0 align=\"right\" >");

			for(int i = 1; i<pages+1;i++){
				
				pageContext.getOut().write("<td><form   action=\""+contextPath+"/controller\" method=\"post\">");
				pageContext.getOut().write("<input  type=\"hidden\" name=\"command\" value=\"set_current_page\"/>");
				pageContext.getOut().write("<input  type=\"hidden\" name=\"pageNumber\" value=\""+i+"\"/>");
				pageContext.getOut().write("<input  type=\"submit\"  value=\""+i+"\"/>");
				pageContext.getOut().write("</form></td>");
				

			}
			pageContext.getOut().write("</table>");

			
		} catch (IOException e) {
			log.error("Error in writing from OrderListTag.", e);
		};
		return SKIP_BODY;}

}

