package by.epam.task.six.shop.service.util;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.controller.JspPageName;
import by.epam.task.six.shop.dao.GetBeanAction;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.dao.impl.GetBeanDao;
import by.epam.task.six.shop.entity.ShopItem;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
/**
 * Gets ShopItem objects from data repository.
 * 
 * Helps service layer classes to get array of ShopItem type objects.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class AdminPageProvider  {
	
	private static AdminPageProvider instance = new AdminPageProvider();
	
	public static AdminPageProvider getInstance(){
		return instance;
	};


	public List<ShopItem> getAdminItemList(HttpServletRequest request,	HttpServletResponse response) throws CommandException {

		int pageNumber;
	
		GetBeanAction itemListDao;
		int allShopItems = 0;
		List<ShopItem> shopItemList;

		try {

			itemListDao = GetBeanDao.getInstance();				
			
			openPageFromBegining(request);			

			allShopItems = itemListDao.countShopItems();	

			setPageNumberToSession(request);			

			setToSessionTotalNumberOfUnits(allShopItems, request);		
			
			if(request.getParameter(ServiceRequestParameterName.ERROR_MESSAGE)!=null){
				request.setAttribute(ServiceRequestParameterName.ERROR_MESSAGE, 
						request.getParameter(ServiceRequestParameterName.ERROR_MESSAGE));
			}
		
			
			if (request.getSession().getAttribute(ServiceRequestParameterName.PAGE_NUMBER) == null) {
				pageNumber = 0;
				shopItemList = itemListDao.getItemsForMainPage(pageNumber);
				return shopItemList;

			} else {
				pageNumber = countPageNumbers(Integer.parseInt(request.getSession()
						.getAttribute(ServiceRequestParameterName.PAGE_NUMBER).toString()));
				shopItemList = itemListDao.getItemsForMainPage(pageNumber);
				return shopItemList;
			}

		} catch (ActionDaoException e) {

			throw new CommandException("Exception in building admin page.", e);
		}

	
	}

	private void setToSessionTotalNumberOfUnits(int totalItemCount,
			HttpServletRequest request) {
		request.getSession().setAttribute(ServiceRequestParameterName.TOTAL_UNITS, totalItemCount);

	}

	private int countPageNumbers(int number) {
		if (number == 1) {
			return 0;
		} else {
			int pageNumber = (number - 1) * 5;
			return pageNumber;
		}
	}

	private void setPageNumberToSession(HttpServletRequest request) {
		if (request.getAttribute(ServiceRequestParameterName.PAGE_NUMBER) != null) {
			request.getSession().setAttribute(ServiceRequestParameterName.PAGE_NUMBER,	
					request.getAttribute(ServiceRequestParameterName.PAGE_NUMBER));
		}
	}

	private void openPageFromBegining(HttpServletRequest request) {
		if (request.getSession().getAttribute(ServiceRequestParameterName.PAGE_FLAG) != null) {

			if (!request.getSession().getAttribute(ServiceRequestParameterName.PAGE_FLAG).equals(JspPageName.ADMIN_PAGE)) {
				request.getSession().setAttribute(ServiceRequestParameterName.PAGE_NUMBER,
								request.getParameter(ServiceRequestParameterName.FROM_FIRST_PAGE));
				
				request.getSession().setAttribute(ServiceRequestParameterName.PAGE_FLAG,
						JspPageName.ADMIN_PAGE);

			}

		} else {
			request.getSession().setAttribute(ServiceRequestParameterName.PAGE_NUMBER,
							request.getParameter(ServiceRequestParameterName.FROM_FIRST_PAGE));
			
			request.getSession().setAttribute(ServiceRequestParameterName.PAGE_FLAG,
					JspPageName.ADMIN_PAGE);

		}
	}

}
