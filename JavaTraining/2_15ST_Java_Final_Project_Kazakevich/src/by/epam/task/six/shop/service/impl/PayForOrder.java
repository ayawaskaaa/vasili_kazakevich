package by.epam.task.six.shop.service.impl;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.dao.UpdateDataAction;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.dao.impl.UpdateDataDao;
import by.epam.task.six.shop.entity.ShopUser;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;


/**
 * Changes user order state.
 *  
 * Changes state of order on:"оплачено".
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */

public class PayForOrder implements ICommand {
	
	private static final String URL_TO_ORDER_LIST_PAY = "?command=build_order_list"
			+ "&temp_message=success_pay";

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) throws CommandException {

		int idUser;
		int idOrder;
		UpdateDataAction payDao = null;
		ShopUser user;
		
		try {
			user=(ShopUser) request.getSession().getAttribute(ServiceRequestParameterName.USER_INFO);
			idUser = user.getId();
			idOrder=Integer.parseInt(request.getParameter(ServiceRequestParameterName.ORDER_ID));
			payDao = UpdateDataDao.getInstance();
			payDao.payForOrder(idUser, idOrder);
			
			
			return request.getRequestURL().toString()+URL_TO_ORDER_LIST_PAY;	
		} catch (ActionDaoException e) {
			throw new CommandException ("Payment faild because of error in UpdateDataDao class.", e);
		}
		
		
	}
	
	

}
