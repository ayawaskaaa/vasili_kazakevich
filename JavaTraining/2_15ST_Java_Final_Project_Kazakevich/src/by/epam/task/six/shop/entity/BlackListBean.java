package by.epam.task.six.shop.entity;
/**
 * Project JavaBean component.
 * 
 * Contains components of shop black list.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class BlackListBean {

	private int blockId;
	private int userId;
	public int getBlockId() {
		return blockId;
	}
	public void setBlockId(int blockId) {
		this.blockId = blockId;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getLogin() {
		return login;
	}
	public void setLogin(String login) {
		this.login = login;
	}
	private String login;
	
}
