package by.epam.task.six.shop.service.exception;

import by.epam.task.six.shop.controller.exception.ControllerException;
/**
 * Service layer exception.
 *  
 * If enter parameters are null, than WrongEnterParametersException will be thrown.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class WrongEnterParametersException extends ControllerException {
	
	private static final long serialVersionUID = 1L;
	
	public WrongEnterParametersException(String msg){
		super(msg);
	}

	public WrongEnterParametersException(String msg, Exception e ){
		super(msg, e);
	}
}
