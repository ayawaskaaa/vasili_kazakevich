package by.epam.task.six.shop.service.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.controller.JspPageName;
import by.epam.task.six.shop.dao.UpdateDataAction;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.dao.impl.UpdateDataDao;
import by.epam.task.six.shop.entity.ShopItem;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
import by.epam.task.six.shop.service.exception.WrongEnterParametersException;
/**
 * Updates item information in data repository.
 * 
 * After enter parameter check old data is replaced by new. In data repository.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class UpdateItem implements ICommand {
	
	private static final String URL_TO_ADMIN_PAGE_CHANGE = "?command=build_admin_page&temp_message=success_change";
	private static final String URL_TO_ADMIN_PAGE_WRONG_ENTER_PARAMETER = "?command=build_admin_page"
			+ "&errorMessage=wrong_enter_parameters_exception";

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {
		String url=null;
		ShopItem item;
		UpdateDataAction updateItemDao = null;
		try {
			if (checkParameters(request)) {

				item = fillItem(request);

				updateItemDao = UpdateDataDao.getInstance();

				updateItemDao.updateItem(item);

				url= request.getRequestURL().toString()+URL_TO_ADMIN_PAGE_CHANGE;
			}

		} catch (ActionDaoException e) {

			throw new CommandException(
					"Updating failed because of error in UpdateDataDao class. ",
					e);

		} catch (WrongEnterParametersException e) {			
				
			url= request.getRequestURL().toString()+URL_TO_ADMIN_PAGE_WRONG_ENTER_PARAMETER;

			
		}
		return url;
	}

	private boolean checkParameters(HttpServletRequest request)
			throws WrongEnterParametersException {

		String name = request.getParameter(ServiceRequestParameterName.ITEM_NAME);
		String image = request.getParameter(ServiceRequestParameterName.ITEM_IMAGE);
		String material = request.getParameter(ServiceRequestParameterName.ITEM_MATERIAL);
		String description = request.getParameter(ServiceRequestParameterName.ITEM_DESCRIPTION);

		try {
			
			int size = Integer.parseInt(request.getParameter(ServiceRequestParameterName.ITEM_SIZE));
			int amount = Integer.parseInt(request.getParameter(ServiceRequestParameterName.ITEM_AMOUNT));
			int price = Integer.parseInt(request.getParameter(ServiceRequestParameterName.ITEM_PRICE));
			
		} catch (NumberFormatException e) {
			throw new WrongEnterParametersException(
					"size, amount or price is null");
		}

		if (name == null) {
			throw new WrongEnterParametersException("Name is null");
		}
		if (image == null) {
			throw new WrongEnterParametersException("image is null");
		}
		if (material == null) {
			throw new WrongEnterParametersException("material is null");
		}
		if (description == null) {
			throw new WrongEnterParametersException("description is null");
		}

		return true;

	}

	private ShopItem fillItem(HttpServletRequest request) {
		ShopItem shopItem = new ShopItem();

		shopItem.setId(Integer.parseInt(request.getParameter(ServiceRequestParameterName.ITEM_ID)));
		shopItem.setName(request.getParameter(ServiceRequestParameterName.ITEM_NAME));
		shopItem.setImage(request.getParameter(ServiceRequestParameterName.ITEM_IMAGE));
		shopItem.setMaterial(request.getParameter(ServiceRequestParameterName.ITEM_MATERIAL));
		shopItem.setSize(Integer.parseInt(request.getParameter(ServiceRequestParameterName.ITEM_SIZE)));
		shopItem.setPrice(Integer.parseInt(request.getParameter(ServiceRequestParameterName.ITEM_PRICE)));
		shopItem.setAmount(Integer.parseInt(request.getParameter(ServiceRequestParameterName.ITEM_AMOUNT)));
		shopItem.setDescription(request.getParameter(ServiceRequestParameterName.ITEM_DESCRIPTION));

		return shopItem;

	}

}
