package by.epam.task.six.shop.controller.exception;

/**
 * Controller layer exception.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class ControllerException extends Exception {
	
	private static final long serialVersionIUD = 1L;
	private Exception hiddenException;
	
	public ControllerException(String msg){
		super(msg);
	}
	
	public ControllerException(String msg , Exception e){
		
		super(msg,e);

	}
	


}
