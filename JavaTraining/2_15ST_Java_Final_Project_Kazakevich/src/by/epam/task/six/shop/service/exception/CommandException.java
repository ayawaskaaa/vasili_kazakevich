package by.epam.task.six.shop.service.exception;

import by.epam.task.six.shop.controller.exception.ControllerException;

/**
 * Service layer exception.
 *  
 *All exceptions in service layer and lower layers should be wrapped in CommandException. 
 *Service layer must throw only CommandException type exceptions.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */

public class CommandException extends ControllerException {
	
	private static final long serialVersionUID = 1L;
	
	public CommandException(String msg){
		super(msg);
	}

	public CommandException(String msg, Exception e ){
		super(msg, e);
	}
}