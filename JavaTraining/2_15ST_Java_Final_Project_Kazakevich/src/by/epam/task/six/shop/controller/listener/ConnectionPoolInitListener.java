package by.epam.task.six.shop.controller.listener;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import org.apache.log4j.Logger;
import by.epam.task.six.shop.dao.connection_pool.ConnectionPoolException;
import by.epam.task.six.shop.dao.connection_pool.ShopConnectionPool;
/**
 * Connection poll initializer.
 * 
 * Initializes connection pool when servlet context is initialized and
 * destroys connection pool after servlet context is destroyed.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class ConnectionPoolInitListener implements  ServletContextListener {

	private static final Logger log = Logger.getLogger(ShopRequestListener.class);
	
	@Override
	public void contextDestroyed(ServletContextEvent arg0) {

	try {
		final  ShopConnectionPool shopConnectionPool=ShopConnectionPool.getInstance();
		shopConnectionPool.dispose();
	} catch (ConnectionPoolException e) {
		log.error("Error in connection pool disposing.", e);
	}
	}
	
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		
	
	 try {
		 final  ShopConnectionPool shopConnectionPool=ShopConnectionPool.getInstance();
		shopConnectionPool.initPoolData();
	} catch (ConnectionPoolException e) {
		log.error("Error in connection pool initializing.", e);
	}
	}
}
