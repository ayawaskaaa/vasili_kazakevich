package by.epam.task.six.shop.service.impl;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import by.epam.task.six.shop.dao.UpdateDataAction;
import by.epam.task.six.shop.dao.exception.ActionDaoException;
import by.epam.task.six.shop.dao.impl.UpdateDataDao;
import by.epam.task.six.shop.service.ICommand;
import by.epam.task.six.shop.service.ServiceRequestParameterName;
import by.epam.task.six.shop.service.exception.CommandException;
/**
 * Changes current state of order.
 *  
 * Takes new order state parameter and changes order state with the help of UpdateDataDao instance.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class ChangeOrderState implements ICommand {
	
	private static final String URL_TO_ADMIN_ORDER_LIST_CHANGE = "?command=build_admin_order_list"
			+ "&temp_message=success_change";

	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws CommandException {

		int idOrder = 0;
		String state = null;
		UpdateDataAction changeStateDao = null;

		try {
			
			if(validateParameters(request)){
				
			idOrder = Integer.parseInt(request.getParameter(ServiceRequestParameterName.ORDER_ID));

			state = request.getParameter(ServiceRequestParameterName.ORDER_STATE_BUTTON);

			changeStateDao = UpdateDataDao.getInstance();
			changeStateDao.changeState(state, idOrder);
			}
			return request.getRequestURL()+ URL_TO_ADMIN_ORDER_LIST_CHANGE;
			

		} catch (ActionDaoException e) {
			throw new CommandException(	"Enable to change state because of error in UpdateDataDao class.",	e);
		} catch (NumberFormatException e){
			throw new CommandException("Error while getting parameters.",e);
		}
	}
	
	private boolean validateParameters(HttpServletRequest request){
		if(request.getParameter(ServiceRequestParameterName.ORDER_STATE_BUTTON)==null
				||request.getParameter(ServiceRequestParameterName.ORDER_ID)==null){
			return false;
		}
		
		return true;
		
	}

}
