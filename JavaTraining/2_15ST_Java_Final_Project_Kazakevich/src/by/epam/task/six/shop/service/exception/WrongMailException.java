package by.epam.task.six.shop.service.exception;

import by.epam.task.six.shop.controller.exception.ControllerException;
/**
 * Service layer exception.
 *  
 * If user enters mail that doesn't matches determined pattern,
 * than exception is thrown. 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class WrongMailException extends ControllerException {

private static final long serialVersionUID=1L;
	
	public WrongMailException(String msg) {
		super(msg);
	}

	public WrongMailException(String msg, Exception e) {
		super(msg, e);
	}

	
}
