package by.epam.task.six.shop.dao.connection_pool;

import java.util.ResourceBundle;
/**
 * Provides an access to database properties file.
 * 
 * Makes values available by a key name.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class DBResourseManager {
	
	private final static DBResourseManager instance = new DBResourseManager();
	
	private ResourceBundle bundle = ResourceBundle.getBundle("db");
	
	
	public static DBResourseManager getInstance(){
		return instance;
	}
	
	public String getValue(String key){
		return bundle.getString(key);
	}
	
}
