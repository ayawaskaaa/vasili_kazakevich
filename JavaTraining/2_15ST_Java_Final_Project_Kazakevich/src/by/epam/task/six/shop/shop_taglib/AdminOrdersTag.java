package by.epam.task.six.shop.shop_taglib;

import java.io.IOException;
import java.util.ArrayList;
import javax.servlet.jsp.tagext.TagSupport;

import org.apache.log4j.Logger;

import by.epam.task.six.shop.entity.UserOrder;
/**
 * Custom tag for admin order page.
 * 
 * Corresponds for right layout of elements on admin order page.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class AdminOrdersTag extends TagSupport {
	
	private ArrayList <UserOrder> adminOrderList;
	private String contextPath;
	private String changeButton;
	private String blockState;
	private String executedState;
	private String inProcessState;
	private String setButton;
	
	private int unitNumber;
	private int pages;
	private static final Logger log = Logger.getLogger(AdminOrdersTag.class);
	

	public String getChangeButton() {
		return changeButton;
	}
	public void setChangeButton(String changeButton) {
		this.changeButton = changeButton;
	}
	public String getBlockState() {
		return blockState;
	}
	public void setBlockState(String blockState) {
		this.blockState = blockState;
	}
	public String getExecutedState() {
		return executedState;
	}
	public void setExecutedState(String executedState) {
		this.executedState = executedState;
	}
	public String getInProcessState() {
		return inProcessState;
	}
	public void setInProcessState(String inProcessState) {
		this.inProcessState = inProcessState;
	}
	public String getSetButton() {
		return setButton;
	}
	public void setSetButton(String setButton) {
		this.setButton = setButton;
	}




	public String getsetButton() {
		return setButton;
	}
	public void setsetButton(String setButton) {
		this.setButton = setButton;
	}
	public int getUnitNumber() {
		return unitNumber;
	}
	public void setUnitNumber(int unitNumber) {
		this.unitNumber = unitNumber;
	}
	
	public String getContextPath() {
		return contextPath;
	}
	public void setContextPath(String contextPath) {
		this.contextPath = contextPath;
	}

	public ArrayList <UserOrder> getadminOrderList() {
		return adminOrderList;
	}
	public void setadminOrderList(ArrayList<UserOrder> adminOrderList) {
		this.adminOrderList = adminOrderList;
		
	}
	
	
	public int doStartTag (){
		try {
			pageContext.getOut().write("<tr>");
			
		} catch (IOException e) {
			log.error("Error in writing from UserOrderTag.", e);
		};
		
		return EVAL_BODY_INCLUDE;}
	
	
	public int doAfterBody(){
		try {
			
		for(int i=0; i<adminOrderList.size();i++){
			

			
			pageContext.getOut().write("<td>"+adminOrderList.get(i).getUserId()+"</td>");
			pageContext.getOut().write("<td>"+adminOrderList.get(i).getOrderId()+"</td>");
			pageContext.getOut().write("<td>"+adminOrderList.get(i).getItemName()+"</td>");
			pageContext.getOut().write("<td><img alt=\"image\" src=\""+contextPath+adminOrderList.get(i).getImage()+"\"></td>");
			pageContext.getOut().write("<td>"+adminOrderList.get(i).getAmount()+"</td>");
			pageContext.getOut().write("<td>"+adminOrderList.get(i).getPrice()+"</td>");
			pageContext.getOut().write("<td>"+adminOrderList.get(i).getState()+"</td>");
			pageContext.getOut().write("<td>"+adminOrderList.get(i).getAmount()*adminOrderList.get(i).getPrice()+"</td>");
			pageContext.getOut().write("<td align=\"left\" ><form   action=\""+contextPath+"/controller\" method=\"post\">");
			pageContext.getOut().write("<input  type=\"hidden\" name=\"command\" value=\"change_order_state\"/>");
			pageContext.getOut().write("<input  type=\"hidden\" name=\"order_id\" value=\""+adminOrderList.get(i).getOrderId()+"\"/>");
			pageContext.getOut().write("<input type=\"radio\" name=\"order_state_button\" value=\"������������\">"+blockState+"<Br>");
			pageContext.getOut().write("<input type=\"radio\" name=\"order_state_button\" value=\"��������\">"+executedState+"<Br>");
			pageContext.getOut().write("<input type=\"radio\" name=\"order_state_button\" value=\"� ��������\">"+inProcessState+"<Br>");
			pageContext.getOut().write("<input  type=\"submit\"  value=\""+changeButton+"\"/>");
			pageContext.getOut().write("</form></td>");
			pageContext.getOut().write("<td><form   action=\""+contextPath+"/controller\" method=\"post\">");
			pageContext.getOut().write("<input  type=\"hidden\" name=\"command\" value=\"set_to_black_list\"/>");
			pageContext.getOut().write("<input  type=\"hidden\" name=\"user_id\" value=\""+adminOrderList.get(i).getUserId()+"\"/>");
			pageContext.getOut().write("<input  type=\"submit\"  value=\""+setButton+"\"/>");
			pageContext.getOut().write("</form></td>");
			

			pageContext.getOut().write("<tr>");
			
			
			

		}
			

		} catch (IOException e) {
			log.error("Error in writing from UserOrderTag.", e);
		};

		
		return SKIP_BODY;}
	
	

	
	public int doEndTag(){
		try {
			
			if(unitNumber%5==0){
				pages=unitNumber/5;
			}else{
				pages=(unitNumber/5)+1;
			}
			
			pageContext.getOut().write("<tr>");
			pageContext.getOut().write("</table>");
			pageContext.getOut().write("</center>");
		

			pageContext.getOut().write("<table border=0 align=\"right\" >");

			for(int i = 1; i<pages+1;i++){
				
				pageContext.getOut().write("<td><form   action=\""+contextPath+"/controller\" method=\"post\">");
				pageContext.getOut().write("<input  type=\"hidden\" name=\"command\" value=\"set_current_page\"/>");
				pageContext.getOut().write("<input  type=\"hidden\" name=\"pageNumber\" value=\""+i+"\"/>");
				pageContext.getOut().write("<input  type=\"submit\"  value=\""+i+"\"/>");
				pageContext.getOut().write("</form></td>");
				

			}
			pageContext.getOut().write("</table>");

			
		} catch (IOException e) {
			log.error("Error in writing from AdminOrdersTag.", e);
		};
		return SKIP_BODY;}

}
