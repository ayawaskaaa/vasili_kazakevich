package by.epam.task.first.aircompany.manager;

/**
 * WrongClaimExeption made by author.
 * 
 * Exception throws when client enters wrong claim
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class WrongClaimException extends Exception {
	
	/**
	 * Class constructor without parameters. 
	 */
	public WrongClaimException() {
	}
	
	/**
	 * Class constructor. 
	 * @param message - a message, which accompanies the exception
	 * @param exception - type of throwable exception
	 */
	public WrongClaimException(String message, Throwable exception) {
	super(message, exception);
	}
	
	/**
	 * Class constructor. 
	 * @param message - a message, which accompanies the exception
	 */
	public WrongClaimException(String message) {
	super(message);
	}
	
	/**
	 * Class constructor. 

	 * @param exception - type of throwable exception
	 */
	public WrongClaimException (Throwable exception) {
	super(exception);
	}

}
