package by.epam.task.first.aircompany.logic;

/**
 * WrongParameterException made by author.
 * 
 * Exception throws when client enters wrong parameters
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class WrongParameterException extends Exception  {
	
	/**
	 * Class constructor without parameters. 
	 */
	public WrongParameterException() {
	}
	/**
	 * Class constructor. 
	 * @param message - a message, which accompanies the exception
	 * @param exception - type of throwable exception
	 */
	public WrongParameterException(String message, Throwable exception) {
	super(message, exception);
	}
	/**
	 * Class constructor. 
	 * @param message - a message, which accompanies the exception
	 */
	public WrongParameterException(String message) {
	super(message);
	}
	/**
	 * Class constructor. 
	 * @param exception - type of throwable exception
	 */
	public WrongParameterException (Throwable exception) {
	super(exception);
	}

}
