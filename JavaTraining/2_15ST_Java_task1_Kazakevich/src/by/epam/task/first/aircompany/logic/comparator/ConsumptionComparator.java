package by.epam.task.first.aircompany.logic.comparator;

import java.util.Comparator;
import by.epam.task.first.aircompany.entity.AbstractAirplane;
/**
 * Class contains comparator for list sorting.
 * 
 * This class realizes comparator to sort a list of airplanes by range;
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class ConsumptionComparator implements Comparator<AbstractAirplane>  {

	/**
	 * This is the overriding of method of comparator interface.
	 * @param o1 first parameter to compare
	 * @param o2 second parameter to compare
	 * @return the result of comparison 
	 */
	@Override
	public  int compare(AbstractAirplane o1, AbstractAirplane o2) {
		
		return o1.getRange() - o2.getRange();
	}

	
	
}
