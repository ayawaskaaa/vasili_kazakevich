package by.epam.task.first.aircompany.logic;

import java.util.ArrayList;
import by.epam.task.first.aircompany.entity.AbstractAirplane;

/**
 * Class contains counting logic.
 * 
 * This class can count total capacity and total load capacity from list of
 * plains.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */

public final class AirplaneParametersCounter {
	
	/**
	 * private constructor
	 */
	private AirplaneParametersCounter(){}

	/**
	 * Final static method which counts total capacity of all airplanes in the list.
	 * 
	 * @param list is a list of planes 
	 * @return the value of total capacity of all airplanes in the list.
	 */
	public final static int getTotalCapacity(ArrayList<AbstractAirplane> list) {
		int totalCapacity = 0;
		for (AbstractAirplane i : list) {
			totalCapacity = totalCapacity + i.getCapacity();
		}

		return totalCapacity;
	}

	/**
	 * Static method which counts total load capacity of all airplanes in the
	 * list.
	 * 
	 * @param list is a list of planes 
	 * @return the value of total load capacity of all airplanes in the list.
	 */
	public final static int getTotalLoadCapacity(ArrayList<AbstractAirplane> list) {
		int totalLoadCapacity = 0;
		for (AbstractAirplane i : list) {
			totalLoadCapacity = totalLoadCapacity + i.getLoadCapacity();
		}

		return totalLoadCapacity;
	}

}

