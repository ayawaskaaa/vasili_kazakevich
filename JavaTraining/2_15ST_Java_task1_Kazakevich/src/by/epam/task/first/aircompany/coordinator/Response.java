package by.epam.task.first.aircompany.coordinator;


import java.util.HashMap;
import java.util.Map;

/**
 * Response class.
 * 
 * Stores responses using hashmap.
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */

public class Response {
	
private Map<String, Object> request = new HashMap <String, Object>();
	
/**
 * Puts requests in to list.
 * @param key serves a key for search in list
 * @param value will be stored in list
 */
	public void setParam(String key, Object value){
		request.put(key, value);
	}
	/**
	 * 
	 * @param key serves a key for search in list
	 * @return value that matches key
	 */
	public Object getParam(String key){
		Object concreteValue=request.get(key);
		
		return concreteValue;}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((request == null) ? 0 : request.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Response other = (Response) obj;
		if (request == null) {
			if (other.request != null)
				return false;
		} else if (!request.equals(other.request))
			return false;
		return true;
	}
	@Override
	public String toString() {
		return getClass().getName()+"[request=" + request + "]";
	}
	

}
