package by.epam.task.first.aircompany.entity;

/**
 * A class of passenger type airplane.
 * 
 * This class is s a subclass of AbstractAirplane class
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class PassPlane extends AbstractAirplane {
	
	/**
	 * Creation of passenger plane parameters 
	 */
	private String comfort;
	
	/**
	 * Class constructor.
	 * initializes:
	 * @param capacity - superclass variable 
	 * @param loadCapacity - superclass variable
	 * @param range - superclass variable 
	 * @param consumpsion - superclass variable  
	 * @param comfort  level of plane comfort
	 */
	
	public PassPlane(int capacity, int loadCapacity, int range,
			int consumpsion, String comfort) {
		super(capacity, loadCapacity, range, consumpsion);
		this.comfort = comfort;
	}

	public String getComfort() {
		return comfort;
	}

	public void setComfort(String comfort) {
		this.comfort = comfort;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((comfort == null) ? 0 : comfort.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (!super.equals(obj)){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		PassPlane other = (PassPlane) obj;
		if (comfort == null) {
			if (other.comfort != null)
				return false;
		} else if (!comfort.equals(other.comfort)){
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return getClass().getName()+"[comfort=" + comfort + ", capacity=" 
				+ getCapacity() + ", loadCapacity=" + getLoadCapacity() 
				+ ", range=" + getRange() + ", consumpsion=" + getConsumpsion() +  "]";
	}
	
}
