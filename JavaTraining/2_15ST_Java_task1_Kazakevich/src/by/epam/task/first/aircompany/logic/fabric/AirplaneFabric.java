package by.epam.task.first.aircompany.logic.fabric;


import by.epam.task.first.aircompany.coordinator.Request;
import by.epam.task.first.aircompany.entity.FreightPlane;
import by.epam.task.first.aircompany.entity.Hangar;
import by.epam.task.first.aircompany.entity.PassPlane;
import by.epam.task.first.aircompany.entity.PlaneParameter;
import by.epam.task.first.aircompany.entity.PrivPlane;
import by.epam.task.first.aircompany.logic.WrongParameterException;
/**
 * Airplane fabric.
 * 
 * This is the airplane fabric. It takes type of the plane type and its parameters
 *  and creates a copy of concrete airplane. 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public final class AirplaneFabric {

	
	private AirplaneFabric(){}
/**
 * Final static method which creates a copy of concrete airplane. Type of airplane
 * depends on incoming type of airplane.
 * @param parameter is a type of plane that must be created
 * @param req is a storage of plane parameters
 * @throws WrongParameterException if client makes a request with wrong parameters
 */
	public final static void getPlane(String parameter, Request req) throws WrongParameterException {
		
		if((String)req.getParameter(PlaneParameter.TYPE)==null){
			throw new WrongParameterException("Check your parametrs! Plane type is epsent.");
			}else{
			switch (parameter){
			case "pass": if((int)req.getParameter(PlaneParameter.CAPACITY)<0||(int)req.getParameter(PlaneParameter.LOADCAPACITY)<0||
					(int)req.getParameter(PlaneParameter.RANGE)<0||(int)req.getParameter(PlaneParameter.CONSUMPTION)<0||
					(String)req.getParameter(PlaneParameter.COMFORT)==null){
				throw new WrongParameterException("Check your parametrs!");
			}else{				
					Hangar.putToHangar(new PassPlane((int)req.getParameter(PlaneParameter.CAPACITY), (int)req.getParameter(PlaneParameter.LOADCAPACITY),
					(int)req.getParameter(PlaneParameter.RANGE), (int)req.getParameter(PlaneParameter.CONSUMPTION),
					(String)req.getParameter(PlaneParameter.COMFORT))
					);
			
			break;
			}
			case "freight":if((int)req.getParameter(PlaneParameter.CAPACITY)<0||(int)req.getParameter(PlaneParameter.LOADCAPACITY)<0||
					(int)req.getParameter(PlaneParameter.RANGE)<0||(int)req.getParameter(PlaneParameter.CONSUMPTION)<0||
					(int)req.getParameter(PlaneParameter.VOLUME)<0){
				throw new WrongParameterException("Check your parametrs!");
			}else{ Hangar.putToHangar( new FreightPlane((int)req.getParameter(PlaneParameter.CAPACITY), (int)req.getParameter(PlaneParameter.LOADCAPACITY),
					(int)req.getParameter(PlaneParameter.RANGE), (int)req.getParameter(PlaneParameter.CONSUMPTION),
					(int)req.getParameter(PlaneParameter.VOLUME))
			);
			break;
			}
			case "private": if((int)req.getParameter(PlaneParameter.CAPACITY)<0||(int)req.getParameter(PlaneParameter.LOADCAPACITY)<0||
					(int)req.getParameter(PlaneParameter.RANGE)<0||(int)req.getParameter(PlaneParameter.CONSUMPTION)<0||
					(String)req.getParameter(PlaneParameter.OWNER)==null){
				throw new WrongParameterException("Check your parametrs!");
			}else{ Hangar.putToHangar( new PrivPlane((int)req.getParameter(PlaneParameter.CAPACITY), (int)req.getParameter(PlaneParameter.LOADCAPACITY),
					(int)req.getParameter(PlaneParameter.RANGE), (int)req.getParameter(PlaneParameter.CONSUMPTION),
					(String)req.getParameter(PlaneParameter.OWNER))
			);
			break;
			}
			default: System.out.println("error");
				throw new WrongParameterException("Check your plane type. Must be: freight, pass or private.");
			
}
		}
		
	}
}
