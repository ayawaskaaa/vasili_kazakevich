package by.epam.task.first.aircompany.logic;

import java.util.ArrayList;
import by.epam.task.first.aircompany.entity.AbstractAirplane;
/**
 * Class contains searching logic.
 * 
 * This class can find airplanes which correspond to a given diapason of airplane consumption.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public final class AirplanesParametersSearcher {
	/**
	 * private constructor
	 */
	private AirplanesParametersSearcher(){}
	
	/**
	 * Final static method which searches an airplane which correspond to a given diapason of
	 * airplane consumption.
	 * @param list is a list of airplanes
	 * @param lowerEdge  Indicates the lower edge of diapason.
	 * @param upperEdge Indicates the upper edge of diapason.
	 * @return list of airplanes which correspond to a given diapason of airplane consumption.
	 * @throws WrongParameterException if client enters wrong parameters
	 */
	public final static ArrayList<AbstractAirplane> searchByConsumption(ArrayList<AbstractAirplane> list, int lowerEdge, int upperEdge) throws WrongParameterException{
		
		ArrayList <AbstractAirplane> chosenPlane = new <AbstractAirplane> ArrayList();
		for(AbstractAirplane i:list){
			
			
			if(i.getConsumpsion()>=lowerEdge&&i.getConsumpsion()<=upperEdge){
				chosenPlane.add(i);
			}
			
		}
		if (chosenPlane.isEmpty()){
			throw new WrongParameterException("There are no plains by your request");
		}
		return chosenPlane;
	}

	
	
}