package by.epam.task.first.aircompany.main;

import by.epam.task.first.aircompany.coordinator.Request;
import by.epam.task.first.aircompany.coordinator.Response;
import by.epam.task.first.aircompany.entity.PlaneParameter;
import by.epam.task.first.aircompany.logic.WrongParameterException;
import by.epam.task.first.aircompany.manager.AircompanyManager;
import by.epam.task.first.aircompany.manager.WrongClaimException;

/**
 * Client class.
 * 
 * This class makes requests to Manager class and gets responses
 * with help of Request and Response classes.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */

public class Client {
/**
 * Method main contains requests to Manager class. In this method where realized all 
 * responses to manager class, to demonstrate his potential.
 * You can ask Manager class to create and fill your airport by airplanes of different
 * types. You can ask Manager class to do some manipulations with your airport.
 * @param args of main
 */
	public static void main(String[] args) {
		
		
		AircompanyManager manager = new AircompanyManager();
		Request passPlane = new Request();
		passPlane.setParameter(PlaneParameter.CAPACITY, 10000);
		passPlane.setParameter(PlaneParameter.CONSUMPTION, 300);
		passPlane.setParameter(PlaneParameter.LOADCAPACITY, 30000);
		passPlane.setParameter(PlaneParameter.RANGE, 1000);
		passPlane.setParameter(PlaneParameter.COMFORT,"econom");
		passPlane.setParameter(PlaneParameter.TYPE, "pass");
		
		Request freightPlane = new Request();
		freightPlane.setParameter(PlaneParameter.CAPACITY, 20000);
		freightPlane.setParameter(PlaneParameter.CONSUMPTION, 600);
		freightPlane.setParameter(PlaneParameter.LOADCAPACITY, 60000);
		freightPlane.setParameter(PlaneParameter.RANGE, 800);
		freightPlane.setParameter(PlaneParameter.VOLUME,63000);
		freightPlane.setParameter(PlaneParameter.TYPE, "freight");
		
		Request privatePlane = new Request();
		privatePlane.setParameter(PlaneParameter.CAPACITY, 20000);
		privatePlane.setParameter(PlaneParameter.CONSUMPTION, 800);
		privatePlane.setParameter(PlaneParameter.LOADCAPACITY, 60000);
		privatePlane.setParameter(PlaneParameter.RANGE, 2000);
		privatePlane.setParameter(PlaneParameter.OWNER,"Abramovich");
		privatePlane.setParameter(PlaneParameter.TYPE, "private");
		
		try {
			Response firstPassAirplane = manager.execute("add", passPlane);
			Response secondPassAirplane = manager.execute("add", passPlane);
			Response thirdPassAirplane = manager.execute("add", passPlane);
			Response newFreightAirplane = manager.execute("add", freightPlane);
			Response newPrivateAirplane = manager.execute("add", privatePlane);
		} catch (WrongParameterException | WrongClaimException e) {
		
			e.printStackTrace();
		} catch(Exception e){
			e.printStackTrace();
		}
		
		
		Response showMyAirport;
		try {
			showMyAirport = manager.execute("show");
			System.out.println("Here is your airport: "+showMyAirport.getParam("aircompany"));
		} catch (WrongClaimException e1) {
			e1.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		Response countCapacity;
		try {
			countCapacity = manager.execute("countCapacity");
			System.out.println("Total  capacity: "+countCapacity.getParam("countCapacity"));
		} catch (WrongClaimException e1) {
			e1.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
	
		
		Response countLoadCapacity;
		try {
			countLoadCapacity = manager.execute("countLoadCapacity");
			System.out.println("Total load capacity: "+countLoadCapacity.getParam("countLoadCapacity"));
		} catch (WrongClaimException e1) {
			e1.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		Response sortByRange;
		try {
			sortByRange = manager.execute("sortByRange");
			System.out.println("Sort result: "+sortByRange.getParam("sortByRange"));
		} catch (WrongClaimException e1) {
			e1.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		Request searchParameter = new Request();
		searchParameter.setParameter(PlaneParameter.LOWER_EDGE, 200);
		searchParameter.setParameter(PlaneParameter.UPPER_EDGE, 500);
		Response searchByConsumpsion;
		
		try {
			searchByConsumpsion = manager.execute("searchByConsumpsion", searchParameter);
			System.out.println("Search result: "+searchByConsumpsion.getParam("searchByConsumpsion"));
		} catch (WrongParameterException | WrongClaimException e) {
			e.printStackTrace();
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

}
