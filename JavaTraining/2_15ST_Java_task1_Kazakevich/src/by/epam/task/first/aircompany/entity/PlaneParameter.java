package by.epam.task.first.aircompany.entity;

/**
 * Enum of plane parameters.
 * 
 * Enumeration of possible plane parameters
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public enum PlaneParameter {

	CAPACITY, LOADCAPACITY, 
	RANGE, CONSUMPTION,
	VOLUME, COMFORT, OWNER, TYPE,LOWER_EDGE,UPPER_EDGE;

}