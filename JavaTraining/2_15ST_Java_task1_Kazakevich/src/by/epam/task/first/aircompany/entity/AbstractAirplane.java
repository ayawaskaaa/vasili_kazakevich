package by.epam.task.first.aircompany.entity;

/**
 * Abstract class for entity creation.
 * 
 * This class serves the basic class for creation of planes of different types.
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public abstract class AbstractAirplane {

	/**
	 * Creation of basic parameters of plane
	 */
	private int capacity;
	private int loadCapacity;
	private int range;
	private int consumpsion;
	
	
	
	/**
	 * Class constructor.
	 * initializes:
	 * @param capacity capacity of plane
	 * @param loadCapacity loadCapacityof plane
	 * @param range range of plane
	 * @param consumpsion  consumption of plane
	 */
	
	public AbstractAirplane(int capacity, int loadCapacity, int range,
			int consumpsion) {
		
		this.capacity = capacity;
		this.loadCapacity = loadCapacity;
		this.range = range;
		this.consumpsion = consumpsion;
	}
	
	public int getCapacity() {
		return capacity;
	}
	public void setCapacity(int capacity) {
		this.capacity = capacity;
	}
	public int getLoadCapacity() {
		return loadCapacity;
	}
	public void setLoadCapacity(int loadCapacity) {
		this.loadCapacity = loadCapacity;
	}
	public int getRange() {
		return range;
	}
	public void setRange(int range) {
		this.range = range;
	}
	public int getConsumpsion() {
		return consumpsion;
	}
	public void setConsumpsion(int consumpsion) {
		this.consumpsion = consumpsion;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (obj == null){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		AbstractAirplane other = (AbstractAirplane) obj;
		if (capacity != other.capacity){
			return false;
		}
		if (consumpsion != other.consumpsion){
			return false;
		}
		if (loadCapacity != other.loadCapacity){
			return false;
		}
		if (range != other.range){
			return false;
		}
		return true;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + capacity;
		result = prime * result + consumpsion;
		result = prime * result + loadCapacity;
		result = prime * result + range;
		return result;
	}
	
	@Override
	public String toString() {
		return getClass().getName()+" [capacity=" + capacity + ", loadCapacity="
				+ loadCapacity + ", range=" + range + ", consumpsion="
				+ consumpsion + "]";
	}
}

