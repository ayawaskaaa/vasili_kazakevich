package by.epam.task.first.aircompany.manager;

import by.epam.task.first.aircompany.coordinator.Request;
import by.epam.task.first.aircompany.coordinator.Response;
import by.epam.task.first.aircompany.entity.Hangar;
import by.epam.task.first.aircompany.entity.PlaneParameter;
import by.epam.task.first.aircompany.logic.AircompanySorter;
import by.epam.task.first.aircompany.logic.AirplaneParametersCounter;
import by.epam.task.first.aircompany.logic.AirplanesParametersSearcher;
import by.epam.task.first.aircompany.logic.WrongParameterException;
import by.epam.task.first.aircompany.logic.fabric.AirplaneFabric;

/**
 * Manager class coordinates all requests coming from client.
 * 
 * Class links client requests with logic blocks and outputs result. Checks the
 * incoming parameters before giving any tasks to other classes.
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class AircompanyManager {

	/**
	 * Parameterized method that chooses the action due to type of clients response.
	 * 
	 * @param key is a type of clients response
	 * @param req parameters given with response, which should be sent to concrete method 
	 * @return an answer to client.
	 * @throws WrongParameterException if client makes a request with wrong parameters
	 * @throws WrongClaimException if client makes a request with wrong claim
	 */
	public Response execute (String key, Request req) throws WrongParameterException, WrongClaimException{
	
	switch(key){
	case "add" : AirplaneFabric.getPlane((String)req.getParameter(PlaneParameter.TYPE), req ) ;
			
		Response resp = new Response();
		resp.setParam("airplane", req);
		return resp;
	
	case "searchByConsumpsion" : 
		
		if ((int)req.getParameter(PlaneParameter.LOWER_EDGE) < 0 || (int)req.getParameter(PlaneParameter.UPPER_EDGE) < (int)req.getParameter(PlaneParameter.LOWER_EDGE)) {

			throw new WrongParameterException("You entered wrong parameters.");
		}
		if (Hangar.getHangar().isEmpty()) {

			throw new WrongClaimException("The hangar is empty.");

		} else {
	Response searchByConsumpsion = new Response();
	searchByConsumpsion.setParam("searchByConsumpsion", AirplanesParametersSearcher.searchByConsumption(Hangar.getHangar(),
			(int)req.getParameter(PlaneParameter.LOWER_EDGE), (int)req.getParameter(PlaneParameter.UPPER_EDGE)));
	return searchByConsumpsion;
		}
	
		
	}
	return null;
	}

	/**
	 * 
	 * @param string a key 
	 * @return a response 
	 * @throws WrongClaimException if client makes wrong claim
	 */
	public Response execute(String string) throws WrongClaimException {
		switch(string){
		case "show":if (Hangar.getHangar().isEmpty()) {

			throw new WrongClaimException("The hangar is empty.");

		} else { Response show = new Response();
		show.setParam("aircompany", Hangar.getHangar());
		return show;
		}
		case "countCapacity": if (Hangar.getHangar().isEmpty()) {

			throw new WrongClaimException("The hangar is empty.");

		} else { Response countCapacity = new Response();
		countCapacity.setParam("countCapacity", AirplaneParametersCounter.getTotalCapacity(Hangar.getHangar()));
		return countCapacity;
		}
		case "countLoadCapacity": if (Hangar.getHangar().isEmpty()) {

			throw new WrongClaimException("The hangar is empty.");

		} else { Response countLoadCapacity = new Response();
		countLoadCapacity.setParam("countLoadCapacity", AirplaneParametersCounter.getTotalLoadCapacity(Hangar.getHangar()));
		return countLoadCapacity;
		}
		case "sortByRange": if (Hangar.getHangar().isEmpty()) {

			throw new WrongClaimException("The hangar is empty.");

		} else { Response sortByRange = new Response();
		
		sortByRange.setParam("sortByRange", AircompanySorter.sort(Hangar.getHangar()));
		return sortByRange;
		}
		default: throw new WrongClaimException("You have entered the wrong claim!");
	}
		
	}
	


	
}
	

