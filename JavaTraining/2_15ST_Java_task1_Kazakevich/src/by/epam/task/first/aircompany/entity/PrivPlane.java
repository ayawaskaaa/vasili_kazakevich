package by.epam.task.first.aircompany.entity;

/**
 * A class of private type airplane.
 * 
 * This class is s a subclass of AbstractAirplane.  
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class PrivPlane extends AbstractAirplane{

	/**
	 * Creation of private plane parameters 
	 */
	private String owner;

	/**
	 * Class constructor.
	 * initializes:
	 * @param capacity - superclass variable 
	 * @param loadCapacity - superclass variable
	 * @param range - superclass variable 
	 * @param consumpsion - superclass variable 
	 * @param owner the owner of the plane  
	 */
	public PrivPlane(int capacity, int loadCapacity, int range,
			int consumpsion, String owner) {
		super(capacity, loadCapacity, range, consumpsion);
		this.owner = owner;
	}

	public String getOwner() {
		return owner;
	}

	public void setOwner(String owner) {
		this.owner = owner;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((owner == null) ? 0 : owner.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (!super.equals(obj)){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		PrivPlane other = (PrivPlane) obj;
		if (owner == null) {
			if (other.owner != null)
				return false;
		} else if (!owner.equals(other.owner)){
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return getClass().getName()+"[owner=" + owner + ", capacity=" 
				+ getCapacity() + ", loadCapacity=" + getLoadCapacity() 
				+ ", range=" + getRange() + ", consumpsion=" + getConsumpsion() +  "]";
	}
	
	
}