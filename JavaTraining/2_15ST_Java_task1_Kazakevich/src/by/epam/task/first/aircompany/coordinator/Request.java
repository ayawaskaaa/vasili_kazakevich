package by.epam.task.first.aircompany.coordinator;

import java.util.HashMap;
import java.util.Map;

import by.epam.task.first.aircompany.entity.PlaneParameter;
/**
 * Requests class.
 * 
 * Stores all request parameters using hashmap.
 * 
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class Request {

private Map<PlaneParameter, Object> params = new HashMap <PlaneParameter, Object>();
	
/**
 * Puts parameters in to list.
 * @param key serves a key for search in list
 * @param value will be stored in list
 */
	public void setParameter(PlaneParameter key, Object value){
		params.put(key, value);
	}
	
	/**
	 * Gets parameters from list
	 * @param key serves a key for search in list
	 * @return value that matches key
	 */
	public Object getParameter(PlaneParameter key){
		Object concreteValue=params.get(key);
		
		return concreteValue;}



@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((params == null) ? 0 : params.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Request other = (Request) obj;
		if (params == null) {
			if (other.params != null)
				return false;
		} else if (!params.equals(other.params))
			return false;
		return true;
	}


	@Override
	public String toString() {
		return getClass().getName()+"[params=" + params + "]";
	}
	
}
