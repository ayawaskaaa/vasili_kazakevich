package by.epam.task.first.aircompany.entity;

/**
 * A class of freight type airplane.
 * 
 * This class is s a subclass of AbstractAirplane class.  
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class FreightPlane extends AbstractAirplane {
	
	/**
	 * Creation of freight plane parameters 
	 */
	private int volume;
	
	/**
	 * Class constructor.
	 * initializes:
	 * @param capacity - superclass variable 
	 * @param loadCapacity - superclass variable 
	 * @param range - superclass variable 
	 * @param consumpsion - superclass variable  
	 * @param volume volume of the plane 
	 */
	

	public FreightPlane(int capacity, int loadCapacity, int range,
			int consumpsion, int volume) {
		super(capacity, loadCapacity, range, consumpsion);
		this.volume = volume;
	}
	
	public int getVolume() {
		return volume;
	}


	public void setVolume(int volume) {
		this.volume = volume;
	}
	
	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + volume;
		return result;
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj){
			return true;
		}
		if (!super.equals(obj)){
			return false;
		}
		if (getClass() != obj.getClass()){
			return false;
		}
		FreightPlane other = (FreightPlane) obj;
		if (volume != other.volume){
			return false;
		}
		return true;
	}
	 
	@Override
	public String toString() {
		return getClass().getName()+" [ volume="+ getVolume() + ", capacity=" 
				+ getCapacity() + ", loadCapacity=" + getLoadCapacity() 
				+ ", range=" + getRange() + ", consumpsion=" + getConsumpsion() +  "]";
	}
}
