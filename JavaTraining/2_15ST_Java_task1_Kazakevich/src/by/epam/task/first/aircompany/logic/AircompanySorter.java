package by.epam.task.first.aircompany.logic;

import java.util.ArrayList;
import java.util.Collections;
import by.epam.task.first.aircompany.entity.AbstractAirplane;
import by.epam.task.first.aircompany.entity.Hangar;
import by.epam.task.first.aircompany.logic.comparator.ConsumptionComparator;


/**
 * Class contains logic for list sorting.
 * 
 * This class can sort a list of airplanes by range;
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public final class AircompanySorter {
	
	/**
	 * private constructor
	 */
	private AircompanySorter(){}

	/**
	 * This method sorts list of airplanes by consumption of each plane using 
	 * ConsumptionComparator
	 * @param arrayList list to sort
	 * @return sorted list of AbstractAirplane type
	 */
public final static  ArrayList<AbstractAirplane> sort (ArrayList<AbstractAirplane> arrayList){
		
	ConsumptionComparator comp = new ConsumptionComparator();
	Collections.sort(Hangar.getHangar(), comp);
	return arrayList;
	}
}
