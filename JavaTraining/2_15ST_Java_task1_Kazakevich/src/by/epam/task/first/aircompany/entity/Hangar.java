package by.epam.task.first.aircompany.entity;

import java.util.ArrayList;
/**
 * A Hangar for plains.
 * 
 * A storage for created planes.
 * 
 * @author Kazakevich_Vasili
 * @version 1.0
 */
public class Hangar {
	
	private static ArrayList <AbstractAirplane> hangar = new ArrayList <AbstractAirplane>();

	
	/**
	 * Method returns a list of planes
	 * @return a list of planes
	 */
	public static ArrayList<AbstractAirplane> getHangar() {
		return hangar;
	}
/**
 * Puts planes of AbstractAirplane type in list
 * @param plane is an object which is placed in to hangar
 */
	
	public static void putToHangar(AbstractAirplane plane){
		hangar.add(plane);
	}

	
	
	
}
