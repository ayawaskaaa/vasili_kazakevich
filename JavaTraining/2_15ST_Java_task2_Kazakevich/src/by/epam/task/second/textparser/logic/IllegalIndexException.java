package by.epam.task.second.textparser.logic;

public class IllegalIndexException extends Exception {
	
	public IllegalIndexException() {
	}

	public IllegalIndexException(String message, Throwable exception) {
	super(message, exception);
	}

	public IllegalIndexException(String message) {
	super(message);
	}
	
	public IllegalIndexException (Throwable exception) {
	super(exception);
	}

}
