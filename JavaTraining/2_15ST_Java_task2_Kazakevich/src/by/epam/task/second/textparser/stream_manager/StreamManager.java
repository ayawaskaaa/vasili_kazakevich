package by.epam.task.second.textparser.stream_manager;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;


public class StreamManager {
	
	private FileReader fr;
	private String path;

	public StreamManager (String path){
		
		this.path=path;
		
	}
	
	public StringBuilder read() throws IOException, FileNotFoundException{
		
		StringBuilder str = new StringBuilder();
		BufferedReader br = new BufferedReader(new FileReader(path));
		
		String file=null;
		while ((file = br.readLine()) != null) {
			str.append(file);
		}
	
		return str;
		}
	
		
	
}
