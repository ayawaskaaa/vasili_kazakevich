package by.epam.task.second.textparser.logic.parser;

public class TextContentException extends Exception {
	public TextContentException() {
	}

	public TextContentException(String message, Throwable exception) {
	super(message, exception);
	}

	public TextContentException(String message) {
	super(message);
	}
	
	public TextContentException (Throwable exception) {
	super(exception);
	}
}
