package by.epam.task.second.textparser.logic;

import org.apache.log4j.Logger;


public final class WordRemove {
private static Logger log = Logger.getLogger(WordRemove.class);
	private WordRemove(){}
	
public static final String TEXT_PARSE= "\\b[bcdfghgklmnpqrstvwxzBCDFGHJKLMNPQRSTVWXZ]\\w{2,2}\\b";

	public  static final String parsToText( String text, int symbols) throws IllegalIndexException{
		if (symbols<1||symbols>10){
			log.error("Client made a wrong claim.");
			throw new IllegalIndexException("Search index must be beetwing 1 and 10. Please chech it out!");
		
		}
		String cleanText=null;
		symbols=symbols-1;
		cleanText=(java.util.Arrays.toString(text.split("\\b[bcdfghgklmnpqrstvwxzBCDFGHJKLMNPQRSTVWXZ]\\w{"+symbols+","+symbols+"}\\b")));
		return cleanText;
		
	    }
}
