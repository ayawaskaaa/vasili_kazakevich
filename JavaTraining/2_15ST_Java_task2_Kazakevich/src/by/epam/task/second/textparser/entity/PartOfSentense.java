package by.epam.task.second.textparser.entity;

import java.util.ArrayList;


public class PartOfSentense implements TextComponent {


	private ArrayList<TextComponent> parts = new ArrayList<TextComponent>();
	private String partOfText;

	

	@Override
	public void addComponent(TextComponent component) {
		parts.add(component);
		
	}

	@Override
	public TextComponent getComponent(int index) {
		
		return parts.get(index);
	}


	@Override
	public ArrayList<TextComponent> getList() {
		
		return parts;
	}

	@Override
	public String getStringValue() {

		return partOfText+" ";
		
	}

	public void setPartOfString(String str) {
		partOfText=str;
		
	}

}
