package by.epam.task.second.textparser.entity;

import java.util.ArrayList;


public interface TextComponent {

	 void addComponent(TextComponent component);
	 
	 TextComponent getComponent(int index);
	 
	 ArrayList<TextComponent> getList();
	 
	 String getStringValue() ;
}
