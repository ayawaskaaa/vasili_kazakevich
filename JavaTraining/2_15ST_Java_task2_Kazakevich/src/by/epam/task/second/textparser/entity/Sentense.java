package by.epam.task.second.textparser.entity;

import java.util.ArrayList;


public class Sentense implements TextComponent {
	
	
	private ArrayList<TextComponent> partOfSentense = new ArrayList<TextComponent>();


	@Override
	public void addComponent(TextComponent component) {
		partOfSentense.add(component);
		
	}

	@Override
	public TextComponent getComponent(int index) {
		
		return partOfSentense.get(index);
	}

	@Override
	public ArrayList<TextComponent> getList() {
		
		return partOfSentense;
	}


	@Override
	public String getStringValue() {

		StringBuilder line = new StringBuilder();
		for(TextComponent i : partOfSentense){
		line.append(i.getStringValue());
		}
		return line.toString();
	}

}
