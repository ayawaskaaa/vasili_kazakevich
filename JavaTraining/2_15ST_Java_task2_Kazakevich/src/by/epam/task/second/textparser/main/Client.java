package by.epam.task.second.textparser.main;


import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.log4j.Logger;
import by.epam.task.second.textparser.entity.TextComponent;
import by.epam.task.second.textparser.logic.IllegalIndexException;
import by.epam.task.second.textparser.logic.WordMatchSearcher;
import by.epam.task.second.textparser.logic.WordRemove;
import by.epam.task.second.textparser.logic.parser.TextComponentParser;
import by.epam.task.second.textparser.logic.parser.TextContentException;
import by.epam.task.second.textparser.stream_manager.StreamManager;



public class Client {

	public static void main(String[] args) {
		
	Logger log = Logger.getLogger(Client.class);
	final String path = "C:\\Users\\�������������\\Desktop\\������� ����� java\\code\\���\\task.txt";
	String text=null;
	
	    StreamManager streamManager= new StreamManager(path);	
	    
	    try {
			text=(streamManager.read()).toString();
			System.out.println("Text before parsing:"+text);
		} catch (FileNotFoundException e) {
			log.error(e);
			e.printStackTrace();
		} catch (IOException e) {
			log.error(e);
			e.printStackTrace();
		}catch (Exception e) {
			log.error(e);
			e.printStackTrace();
		}


	    
	    TextComponent parsedText;
		try {
			parsedText = TextComponentParser.parsToText(text);
		
	    
	System.out.println("Text after parsing: "+parsedText.getStringValue());
	
	

		System.out.println("The exclusive word of first sentense is: "+WordMatchSearcher.findMatch(parsedText));
	
	
	try {
		System.out.println("Text whithout some words: "+WordRemove.parsToText(parsedText.getStringValue(), 3));
	} catch (IllegalIndexException e) {
		log.error("Client made a wrong claim.");
		e.printStackTrace();
	}catch (Exception e) {
		log.error(e);
		e.printStackTrace();
	}
	
		} catch (TextContentException e1) {
			log.error(e1);
			e1.printStackTrace();
		}catch (Exception e1) {
			log.error(e1);
			e1.printStackTrace();
		}

	}
	   
	    
	
}
