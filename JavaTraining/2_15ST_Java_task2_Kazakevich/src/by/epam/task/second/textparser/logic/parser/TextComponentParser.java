package by.epam.task.second.textparser.logic.parser;

import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.apache.log4j.Logger;
import by.epam.task.second.textparser.entity.PartOfSentense;
import by.epam.task.second.textparser.entity.Sentense;
import by.epam.task.second.textparser.entity.Text;
import by.epam.task.second.textparser.entity.TextComponent;



public class TextComponentParser {
	
	private static final Logger log= Logger.getLogger(TextComponentParser.class);
	
	public static final String TEXT_PARSE= "(?s)(class|void).{0,20}[\\{].{60,420}[\\}]( \n)?";

	public static final String SENTENSE_PARSE= "(?s)(([A-Z1-9]|1\\.|1\\.1\\.|1\\.2\\.)" +
			"[^\\.]{5,}((Statement \\n)+|(s \\n)+|[^1]\\.\\n?))";
	
	public static final String PART_OF_SENTENSE_PARSE= "[A-Za-z]+|([0-9-\\.:\\(\\),\"'%=>;\n])";
	
	private TextComponentParser(){
		
	}    
	
	public  static final TextComponent parsToText( String text) throws TextContentException{
		Text head;
		String cleanText=null;
		cleanText=(java.util.Arrays.toString(text.split(TEXT_PARSE)));
		head= new Text();
		if(log.isDebugEnabled()){
		log.debug("Creation of Text object. All Sentense objects will be held here.");
		}
		parsToSentense(head, cleanText);
		if(head.getList().isEmpty()){
			log.error("Text has no body or its format is undefined.");
			throw new TextContentException("Text has no body or its format is undefined.");
			
		}
		return head;
	    }

	
	public static void parsToSentense(TextComponent head, String cleantext)  {
		Sentense sentenseItem;
		Pattern sentensePattern = Pattern.compile(SENTENSE_PARSE);
		Matcher m = sentensePattern.matcher(cleantext);
				
		while(m.find()){			
			head.addComponent(sentenseItem = new Sentense());
			if(log.isDebugEnabled()){
			log.debug("Creation of Sentense object. All PartOfSentense objects will be held here.");
			}
			parsToPartOfSentense(sentenseItem, m.group());
			
		}

		
	}


	public final static void parsToPartOfSentense(TextComponent sentenseItem, String text) {
		PartOfSentense partOfSentenseItem;
		Pattern partOfSentensePattern = Pattern.compile(PART_OF_SENTENSE_PARSE);
		Matcher m = partOfSentensePattern.matcher(text);
				
		while(m.find()){
			
			sentenseItem.addComponent(partOfSentenseItem = new PartOfSentense());
			if(log.isDebugEnabled()){
			log.debug("Creation of PartOfSentense object. All string parts will be held here.");
			}
			
			partOfSentenseItem.setPartOfString(m.group());	
			
		}

		}
		
}
