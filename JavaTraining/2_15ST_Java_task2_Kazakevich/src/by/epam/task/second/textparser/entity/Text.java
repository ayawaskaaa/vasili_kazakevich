package by.epam.task.second.textparser.entity;

import java.util.ArrayList;


public class Text implements TextComponent {
		
	private ArrayList<TextComponent> sentense =  new ArrayList<TextComponent>();

	@Override
	public ArrayList<TextComponent> getList(){
		return sentense;
		
	}
	@Override
	public void addComponent(TextComponent component) {
		sentense.add(component);
		
	}

	@Override
	public TextComponent getComponent(int index) {
		
		return sentense.get(index);
	}
	

	
	@Override
	public String getStringValue() {

		StringBuilder line = new StringBuilder();
		for(TextComponent i : sentense){
		line.append(i.getStringValue());
		}
		return line.toString();
	}



}
