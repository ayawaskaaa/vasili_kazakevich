package by.epam.task.third.callcenter.center;

public class Operator  {

	private int id;
	
	public Operator(int id){
		this.id=id;
	}
	
	public void talk() throws InterruptedException{		
			Thread.sleep(1000);		
	}

	public int getId() {
		
		return id;
	}
		
	
}
