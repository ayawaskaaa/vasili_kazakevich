package by.epam.task.third.callcenter.client;

import java.util.Random;
import org.apache.log4j.Logger;
import by.epam.task.third.callcenter.center.Operator;
import by.epam.task.third.callcenter.center.Queue;

public class Client implements Runnable {

	private final Logger log = Logger.getLogger(Client.class);
	private Queue queue;
	private volatile boolean stopThread = false;
	private int id;
	
	@Override
	public void run() {

		while (!stopThread) {

			try {
				call();
				thinking();

			} catch (InterruptedException e) {
				log.error(e);
				e.printStackTrace();
			} catch (Exception e2) {
				log.error(e2);
				e2.printStackTrace();
			}
		}

	}

	public Client(Queue qeue, int id) {
		this.queue = qeue;
		this.id = id;
	}

	public void stopThread() {
		stopThread = true;
	}

	/*
	 * Method makes a call to callcenter. Before taking operator from queue
	 * Client checks: if queue has an available operator. If he is available,
	 * client talks with him and returns him back to operator queue after ending
	 * of conversation. If not, with the help of random number he chooses: he
	 * will wait, recall or end his call.
	 */
	public void call() throws InterruptedException {

		if(log.isDebugEnabled()){
			log.debug("Client-" + id + " calls to:"	+ queue.getClass().getName());
		}
		Random rand = new Random();
		int number = rand.nextInt(3);

		if (queue.checkForAvailable()) {
			Operator op = queue.pollFromQueue();

			if(log.isDebugEnabled()){log.debug("Client-" + id + " talks to operator:"
					+ op.getId());
			}
			op.talk();
			if(log.isDebugEnabled()){log.debug("Client-" + id
					+ " finished conversation whith operator:" + op.getId());
			}
			queue.setToQueue(op);

		} else {
			if (number == 0) {
				waitForOperator();
			}
			if (number == 1) {
				if(log.isDebugEnabled()){log.debug("Client-" + id + " recalls");
				retryCall();
				}
			} else {
				if(log.isDebugEnabled()){log.debug("Client-" + id + " ends call");
				endCall();
				}
			}

		}

	}

	// Imitation of non-active client
	public void thinking() throws InterruptedException {
		Thread.sleep(500);
	}

	/*
	 * Client tries to take an unavailable operator from queue, but thread-safe
	 * arrayblockingqueue blocks client until operator gets available.
	 */

	public void waitForOperator() throws InterruptedException {

		if(log.isDebugEnabled()){
			log.debug("Client-" + id + " is waiting for free operator.");
		}
		
		Operator op = queue.pollFromQueue();

		if(log.isDebugEnabled()){
			log.debug("Client-" + id + " talks to operator:" + op.getId());
		}
		
		op.talk();
		if(log.isDebugEnabled()){
			log.debug("Client-" + id+ " finished conversation whith operator:" + op.getId());
		}
		queue.setToQueue(op);

	}

	// imitation of end of the call
	public void endCall() throws InterruptedException {
		thinking();
	}

	// Client recalls to callcenter
	public void retryCall() throws InterruptedException {
		call();
	}

	/*
	 * Overriding implemented run method. This thread has two conditions: "call"
	 * and "thinking" This condition cycles until it being stopped from main
	 * thread with "stopThread method".
	 */



}
