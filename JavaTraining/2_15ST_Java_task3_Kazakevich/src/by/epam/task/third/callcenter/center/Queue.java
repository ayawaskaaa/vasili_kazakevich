package by.epam.task.third.callcenter.center;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;



public class Queue {

	 /*All operators are held in this queue. This queue is thread-safe, so if there is no
	  * operators in the queue, client will wait for available. In case: operator is available,
	  * client takes it from queue and removes him from it. After client finishes call- returns 
	  * him back to queue.
	  *  */
	private BlockingQueue <Operator> operatorQueue = new ArrayBlockingQueue<Operator>(3);

	
	/*Method checks queue for available operators and returns boolean: empty or not*/ 
	
	public boolean checkForAvailable(){
		if(operatorQueue.isEmpty()){
			return false;
		}else{
		return true;
		}
	}
	
	// After client finishes call he returns  operator back to queue.
	public void  setToQueue(Operator operator) throws InterruptedException{
		operatorQueue.put(operator);
		
	}
	
	
	// Client takes operator from queue to talk to him and removes this operator from queue
	public Operator pollFromQueue() throws InterruptedException{
		
		return operatorQueue.take();
		
	}
		
	
}
