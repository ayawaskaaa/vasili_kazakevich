package by.epam.task.third.callcenter.main;


import org.apache.log4j.Logger;
import by.epam.task.third.callcenter.center.Operator;
import by.epam.task.third.callcenter.center.Queue;
import by.epam.task.third.callcenter.client.Client;

public class Main {

	public static void main(String[] args) {
	final Logger log = Logger.getLogger(Main.class) ;
		
		 /*Creating a queue. This queue is a coordinator
		  * between client and operator
		  * */
	
		Queue mainQueue = new Queue();
		
		// Creation of operators
		Operator o1 = new Operator(1);
		Operator o2 = new Operator(2);
		Operator o3= new Operator(3);
		
		//Adding operators to queue
		try {
			mainQueue.setToQueue(o1);
			mainQueue.setToQueue(o2);
			mainQueue.setToQueue(o3);
		} catch (InterruptedException e) {
			log.error(e);
			
		}catch (Exception e1) {
			log.error(e1);
			
		}
		
		// client creation
		
		Client c1 = new Client(mainQueue,1);
		Client c2 = new Client(mainQueue,2);
		Client c3 = new Client(mainQueue,3);
		Client c4 = new Client(mainQueue,4);
		Client c5 = new Client(mainQueue,5);
		Client c6 = new Client(mainQueue,6);
		Client c7 = new Client(mainQueue,7);
		Client c8 = new Client(mainQueue,8);
		Client c9 = new Client(mainQueue,9);
		Client c10 = new Client(mainQueue,10);
		Client c11 = new Client(mainQueue,11);
		Client c12= new Client(mainQueue,12);
		
		// Clients start to call to callcenter, in this case - it's mainQueue object
		new Thread(c1).start();
		new Thread(c2).start();
		new Thread(c3).start();
		new Thread(c4).start();
		new Thread(c5).start();
		new Thread(c6).start();
		new Thread(c7).start();
		new Thread(c8).start();
		new Thread(c9).start();
		new Thread(c10).start();
		new Thread(c11).start();
		new Thread(c12).start();
		try {
			Thread.sleep(10000);
		} catch (InterruptedException e) {
			log.error(e);
		}catch (Exception e) {
			log.error(e);
		}
		
		
		// Clients call for 10 seconds, then stop.
		
		c1.stopThread();
		c2.stopThread();
		c3.stopThread();
		c4.stopThread();
		c5.stopThread();
		c6.stopThread();
		c7.stopThread();
		c8.stopThread();
		c9.stopThread();
		c10.stopThread();
		c11.stopThread();
		c12.stopThread();
		

		
		
	}

}
