package by.epam.task.five.chemistry.dao.impl;


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;

import by.epam.task.five.chemistry.dao.XMLDao;
import by.epam.task.five.chemistry.dao.XMLDaoException;
import by.epam.task.five.chemistry.entity.Medicine;


public class StaxXmlDao implements XMLDao {

		
		private static final StaxXmlDao instance = new StaxXmlDao();
		
		
		public static XMLDao getInstance (){
			return instance;
		}

		@Override
		public List<Medicine> parse(String resourseName) throws XMLDaoException   {
			 List<Medicine> medicineList;
			 XMLInputFactory inputfactory;
			 FileInputStream inputSream=null;
				
				URL url;
				try {
					url = new URL(resourseName);

				
				medicineList=new ArrayList<>();

				XMLStreamReader reader = null;
				
				inputfactory= XMLInputFactory.newInstance();
			
					
						inputSream=new FileInputStream(new File(url.toURI()));
					
					reader = inputfactory.createXMLStreamReader(inputSream);
					String name;
					while(reader.hasNext()){
						int type = reader.next();
						if (type==XMLStreamConstants.START_ELEMENT){
							name=reader.getLocalName();

							
							if("medicament".equals(name)){

								Medicine medicineInstance = buildMedicine(reader);
								medicineList.add(medicineInstance);

							}
						}
					}
				} catch (FileNotFoundException e) {
					throw new XMLDaoException("FileNotFoundException in StaxXmlDao.",e);
					
				} catch (XMLStreamException e) {
					throw new XMLDaoException("XMLStreamException in StaxXmlDao.",e);
				} catch (URISyntaxException e) {
					throw new XMLDaoException("URISyntaxException in StaxXmlDao.",e);
				} catch (MalformedURLException e) {
					throw new XMLDaoException("MalformedURLException in StaxXmlDao.",e);
				}finally{
					if(inputSream!=null){
						try {
							inputSream.close();
						} catch (IOException e) {
							throw new XMLDaoException("IOException in StaxXmlDao.",e);
						}
					}
				}
					return medicineList;
				
				
			}

			private Medicine buildMedicine(XMLStreamReader reader) throws XMLDaoException {
				
				Medicine medicineInstance = new Medicine();
				
				medicineInstance.setId(reader.getAttributeValue(null, "id"));
				
				if(reader.getAttributeValue(null, "group")!=null){
					medicineInstance.setGroup(reader.getAttributeValue(null, "group"));
				}
				try {
				String name;
				while(reader.hasNext()){
					int type= reader.next();
					switch (type){
					case XMLStreamConstants.START_ELEMENT:
						name=reader.getLocalName();
						switch(name){
						case "name":
						
								medicineInstance.setName(getXMLText(reader));
							
							break;
						case "analog":
							medicineInstance.setAnalog(getXMLText(reader));
							break;
						case "number":
							medicineInstance.setNumber(getXMLText(reader));
							break;
						case "expiration-date": 
							medicineInstance.setExpirationDate(getXMLText(reader));
							break;
						case "organisation":
							medicineInstance.setOrganisation(getXMLText(reader));
							break;
						case "package-type":
							medicineInstance.setPackType(getXMLText(reader));
							break;
						case "amount":
							medicineInstance.setAmount(getXMLText(reader));
							break;
						case "price":
							medicineInstance.setPrice(getXMLText(reader));
							break;
						case "dose":
							medicineInstance.setDose(getXMLText(reader));
							break;	
						case "reception":
							medicineInstance.setReception(getXMLText(reader));
							break;
						case "enum":
							medicineInstance.setEnumForm(getXMLText(reader));
							break;
						case "pharm":
							medicineInstance.setPharm(getXMLText(reader));
							break;	
						}
						break;
						
					case XMLStreamConstants.END_ELEMENT:
						name=reader.getLocalName();
						if("medicament".equals(name)){

							return medicineInstance;
						}
						break;
					}
				}
				} catch (XMLDaoException e) {
					throw new XMLDaoException("XMLDaoException in StaxXmlDao.",e);
				} catch (XMLStreamException e) {
					throw new XMLDaoException("XMLStreamException in StaxXmlDao.",e);
				}
				
				
				return null;
			}
			
			
			private String getXMLText(XMLStreamReader reader) throws XMLDaoException  {
				String text = null;
				try {
					if(reader.hasNext()){
						reader.next();
						text=reader.getText();
					}
				} catch (XMLStreamException e) {
					throw new XMLDaoException("XMLStreamException in StaxXmlDao.",e);
				}
				return text;
				
			}
		
		}

		

	


