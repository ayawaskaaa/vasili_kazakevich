package by.epam.task.five.chemistry.controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import by.epam.task.five.chemistry.logic.CommandException;
import by.epam.task.five.chemistry.logic.CommandHelper;
import by.epam.task.five.chemistry.logic.ICommand;




public class ParserAppController extends HttpServlet {
	private static final long SerialVersionUID =1L;
	static Logger log = Logger.getLogger(ParserAppController.class);
	
	public ParserAppController(){
		super();
	} 
	
	protected void doGet  (HttpServletRequest request, HttpServletResponse response) {}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)    {
		String commandName= request.getParameter(RequestParameterName.COMMAND_NAME);
		String page = null;
		try{
		ICommand command;
		
		command = CommandHelper.getInstance().getCommand(commandName);
			

		
		
			page=command.execute(request);
		}catch(CommandException e){
			log.error(e);
			page=JspPageName.ERROR_PAGE;
		}catch(Exception e){
			log.error(e);
			page=JspPageName.ERROR_PAGE;
		}
		
		RequestDispatcher dispatcher = request.getRequestDispatcher(page);
		
		if(dispatcher!=null){
			try {
				dispatcher.forward(request, response);
			} catch (ServletException e) {
				log.error("ServletException was caused, while using dispatcher.",e);
				e.printStackTrace();
			} catch (IOException e) {
				log.error("IOException was caused, while using dispatcher.",e);
				e.printStackTrace();
			}
		}else{
			errorMessageDireclyFromresponse(response);
		}
			
			
			
			
			
	}
	
	private void errorMessageDireclyFromresponse(HttpServletResponse response) {
		response.setContentType("text/html");
		try {
			response.getWriter().println("E R R O R");
		} catch (IOException e) {
			log.error("Error in servlet writing. "+e);
			e.printStackTrace();
		}
		
	}
}