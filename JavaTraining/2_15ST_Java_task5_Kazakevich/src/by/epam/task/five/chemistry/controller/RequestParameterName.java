package by.epam.task.five.chemistry.controller;

public final class RequestParameterName {

	private RequestParameterName(){}
		public static final String COMMAND_NAME= "command";
		public static final String SIMPLE_INFO = "simpleinfo";
		public static final String FILE_NAME = "filename";
	
}
