package by.epam.task.five.chemistry.logic;

public enum CommandName {
	
	NO_SUCH_COMMAND,SAX_PARSER_COMMAND,
	STAX_PARSER_COMMAND,DOM_PARSER_COMMAND

}
