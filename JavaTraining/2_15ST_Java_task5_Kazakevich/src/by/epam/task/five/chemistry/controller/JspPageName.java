package by.epam.task.five.chemistry.controller;

public class JspPageName {
	
	private JspPageName(){}
	
	public static final String USER_PAGE = "jsp/userPage.jsp";
	public static final String ERROR_PAGE = "error.jsp";

}
