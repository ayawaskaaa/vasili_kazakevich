package by.epam.task.five.chemistry.dao.impl;


import java.io.IOException;
import java.util.List;

import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;
import org.xml.sax.helpers.XMLReaderFactory;

import by.epam.task.five.chemistry.dao.XMLDao;
import by.epam.task.five.chemistry.dao.XMLDaoException;
import by.epam.task.five.chemistry.entity.Medicine;





public final class SaxXmlDao implements XMLDao {
	
	private static final SaxXmlDao instance = new SaxXmlDao();
	
	
	public static XMLDao getInstance (){
		return instance;
	}

	@Override
	public List<Medicine> parse(String resourseName) throws XMLDaoException  {
		SaxHandlerXmlDao handler; 
		
		XMLReader reader;
		try {
			reader = XMLReaderFactory.createXMLReader();
		
		handler = new SaxHandlerXmlDao();
		reader.setContentHandler(handler);
		reader.parse(resourseName);
		} catch (SAXException e) {
			throw new XMLDaoException("SAXException in SaxXmlDao.",e);
		} catch (IOException e) {
			throw new XMLDaoException("IOException in SaxXmlDao.",e);
		}

			return handler.getMedicineList();}
		
	}

	


