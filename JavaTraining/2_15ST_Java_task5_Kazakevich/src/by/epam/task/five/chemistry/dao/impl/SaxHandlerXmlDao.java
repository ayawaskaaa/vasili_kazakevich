package by.epam.task.five.chemistry.dao.impl;

import java.util.ArrayList;
import java.util.List;
import org.xml.sax.Attributes;
import org.xml.sax.helpers.DefaultHandler;
import by.epam.task.five.chemistry.entity.Medicine;

public class SaxHandlerXmlDao extends DefaultHandler {
	
	private List <Medicine> medicineList = null;
	private Medicine medicineInstance= null;
	private String value= null;
	
	public SaxHandlerXmlDao (){
		medicineList = new ArrayList <Medicine>();
	};
	
	public List<Medicine> getMedicineList() {
		return medicineList;
	}

	public void setMedicineList(List<Medicine> medicineList) {
		this.medicineList = medicineList;
	}
	/*@Override
	public void startDocument(){
		//System.out.println("This text was parsed by SAX parser:  ");
		
	}*/
	
	@Override
	public void startElement(String uri, String localName, String qName, Attributes attrs){
	
		if("medicament".equals(localName)){
			medicineInstance=new Medicine();
			medicineInstance.setId(attrs.getValue(1));
			if(attrs.getLength()==2){
				medicineInstance.setGroup(attrs.getValue(0));
			}
		}else{
			value=localName;			
		}
		
	}
	
	@Override
	public void characters(char[] ch, int start, int length){
		
		String s=new String(ch, start, length).trim();
	
		if(value!=null){
			switch(value){
			case "name":
				medicineInstance.setName(s);
				break;
			case "analog":
				medicineInstance.setAnalog(s);
				break;
			case "number":
				medicineInstance.setNumber(s);
				break;
			case "expiration-date": 
				medicineInstance.setExpirationDate(s);
				break;
			case "organisation":
				medicineInstance.setOrganisation(s);
				break;
			case "package-type":
				medicineInstance.setPackType(s);
				break;
			case "amount":
				medicineInstance.setAmount(s);
				break;
			case "price":
				medicineInstance.setPrice(s);
				break;
			case "dose":
				medicineInstance.setDose(s);
				break;	
			case "reception":
				medicineInstance.setReception(s);
				break;
			case "enum":
				medicineInstance.setEnumForm(s);
				break;
			case "pharm":
				medicineInstance.setPharm(s);
				break;	
			}
		}
		value=null;

	}
	


	@Override
	public void endElement(String uri, String localName, String qName){
		
		if("medicament".equals(localName)){
			medicineList.add(medicineInstance);
			medicineInstance=null;			
		}
	}
	

	/*@Override
	public void endDocument(){
		//System.out.println("\nParsing ended  ");
	}
	*/

}
