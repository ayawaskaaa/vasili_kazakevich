package by.epam.task.five.chemistry.dao;

import java.util.List;

import by.epam.task.five.chemistry.entity.Medicine;

public interface XMLDao {
	
	List<Medicine> parse(String resourseName) throws XMLDaoException ;

}
