package by.epam.task.five.chemistry.logic.impl;



import java.util.List;

import javax.servlet.http.HttpServletRequest;


import by.epam.task.five.chemistry.controller.JspPageName;
import by.epam.task.five.chemistry.controller.RequestParameterName;
import by.epam.task.five.chemistry.dao.XMLDAOFactory;
import by.epam.task.five.chemistry.dao.XMLDao;
import by.epam.task.five.chemistry.dao.XMLDaoException;
import by.epam.task.five.chemistry.entity.Medicine;
import by.epam.task.five.chemistry.logic.CommandException;
import by.epam.task.five.chemistry.logic.ICommand;

public class DomParserCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws CommandException  {
		String page = null;
		XMLDao dao = null;

		try{
			
			dao=XMLDAOFactory.getInstance().getDAO(XMLDAOFactory.DAOType.DOM);

			List<Medicine> info = dao.parse(this.getClass().getClassLoader().getResource( request.getParameter(RequestParameterName.FILE_NAME)).toString());
			
			request.setAttribute(RequestParameterName.SIMPLE_INFO, info);
			
			page = JspPageName.USER_PAGE;
		}catch(XMLDaoException e){
			throw new CommandException("can not get DAO", e);
		}
		
		return page;
	}

}
