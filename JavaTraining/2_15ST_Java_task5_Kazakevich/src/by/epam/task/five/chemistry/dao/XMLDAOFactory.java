package by.epam.task.five.chemistry.dao;


import by.epam.task.five.chemistry.dao.impl.DomXmlDao;
import by.epam.task.five.chemistry.dao.impl.SaxXmlDao;
import by.epam.task.five.chemistry.dao.impl.StaxXmlDao;

public class XMLDAOFactory {
	
	private static final XMLDAOFactory instance = new XMLDAOFactory();
	
	public static XMLDAOFactory getInstance() throws XMLDaoException {
		return instance; 
	}

	public XMLDao getDAO(DAOType type) throws XMLDaoException {
		XMLDao dao;
		switch(type){
		case SAX:
			return SaxXmlDao.getInstance();
		case STAX:
			return StaxXmlDao.getInstance();
		case DOM:
			return DomXmlDao.getInstance();
		
		default: 
			throw new XMLDaoException("No such DAO");
		}
		
		
		
	}
	public enum DAOType {
		SAX, STAX, DOM;
	}
	
}
