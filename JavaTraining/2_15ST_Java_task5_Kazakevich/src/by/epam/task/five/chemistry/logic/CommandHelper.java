package by.epam.task.five.chemistry.logic;




import java.util.HashMap;
import java.util.Map;

import by.epam.task.five.chemistry.logic.impl.DomParserCommand;
import by.epam.task.five.chemistry.logic.impl.NoSuchCommand;
import by.epam.task.five.chemistry.logic.impl.SaxParserCommand;
import by.epam.task.five.chemistry.logic.impl.StaxParserCommand;



public final class CommandHelper {
	
	private static final  CommandHelper instance = new CommandHelper();
	
	private Map<CommandName, ICommand> commands = new HashMap<>();
	
	public CommandHelper() {
		
		commands.put(CommandName.NO_SUCH_COMMAND, new NoSuchCommand());
		commands.put(CommandName.SAX_PARSER_COMMAND, new SaxParserCommand());
		commands.put(CommandName.STAX_PARSER_COMMAND, new StaxParserCommand());
		commands.put(CommandName.DOM_PARSER_COMMAND, new DomParserCommand());
	}
	
	public static CommandHelper getInstance () throws CommandException  {
		
		return instance;
	}
	
	
	
	public ICommand getCommand(String commandName){
		CommandName name = CommandName.valueOf(commandName.toUpperCase());
		ICommand command;
		if(null!=name){command= commands.get(name);
		}else{
			command = commands.get(CommandName.NO_SUCH_COMMAND);
		}
		return command;
		
	}
	

}
