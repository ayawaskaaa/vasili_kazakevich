package by.epam.task.five.chemistry.logic.impl;

import javax.servlet.http.HttpServletRequest;

import by.epam.task.five.chemistry.controller.JspPageName;
import by.epam.task.five.chemistry.logic.CommandException;
import by.epam.task.five.chemistry.logic.ICommand;

public class NoSuchCommand implements ICommand {

	@Override
	public String execute(HttpServletRequest request) throws CommandException {
		
		return JspPageName.ERROR_PAGE;
	}

}
