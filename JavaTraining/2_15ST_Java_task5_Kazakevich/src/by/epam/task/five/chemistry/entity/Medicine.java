package by.epam.task.five.chemistry.entity;

public class Medicine {
	
	private String medicine;
	private String medicament;
	private String id;
	private String group;
	private String name;
	private String analogs;
	private String analog="";
	private String versions="";
	private String certificate="";
	private String number;
	private String expirationDate;
	private String organisation ;
	private String pack="";
	private String packType;
	private String amount;
	private String price;
	private String dosage="";
	private String dose;
	private String reception;
	
	private String medicineTag;
	private String medicamentTag;
	private String idTag= "Medicament ID";
	private String groupTag = "Group";
	private String nameTag = "Medicament name";
	private String analogsTag="Analogs";
	private String analogTag="Medicament analogs:";
	private String versionsTag= "Versions of medicament";
	private String certificateTag= "Certificate";
	private String numberTag = "Number:";
	private String expirationDateTag ="Expiration date:";
	private String organisationTag = "organisation";
	private String packTag="Package";
	private String packTypeTag="Package type:";
	private String amountTag="Amount:";
	private String priceTag="Price";
	private String dosageTag="Dosage";
	private String doseTag="Dose:";
	private String receptionTag= "Reception";
	
	
	public String getMedicineTag() {
		return medicineTag;
	}
	public void setMedicineTag(String medicineTag) {
		this.medicineTag = medicineTag;
	}
	public String getMedicamentTag() {
		return medicamentTag;
	}
	public void setMedicamentTag(String medicamentTag) {
		this.medicamentTag = medicamentTag;
	}
	public String getIdTag() {
		return idTag;
	}
	public void setIdTag(String idTag) {
		this.idTag = idTag;
	}
	public String getGroupTag() {
		return groupTag;
	}
	public void setGroupTag(String groupTag) {
		this.groupTag = groupTag;
	}
	public String getNameTag() {
		return nameTag;
	}
	public void setNameTag(String nameTag) {
		this.nameTag = nameTag;
	}
	public String getAnalogsTag() {
		return analogsTag;
	}
	public void setAnalogsTag(String analogsTag) {
		this.analogsTag = analogsTag;
	}
	public String getAnalogTag() {
		return analogTag;
	}
	
	public void setAnalogTag(String analogTag) {
		this.analogTag = analogTag;
	}
	public String getVersionsTag() {
		return versionsTag;
	}
	public void setVersionsTag(String versionsTag) {
		this.versionsTag = versionsTag;
	}
	public String getCertificateTag() {
		return certificateTag;
	}
	public void setCertificateTag(String certificateTag) {
		this.certificateTag = certificateTag;
	}
	public String getNumberTag() {
		return numberTag;
	}
	public void setNumberTag(String numberTag) {
		this.numberTag = numberTag;
	}
	public String getExpirationDateTag() {
		return expirationDateTag;
	}
	public void setExpirationDateTag(String expirationDateTag) {
		this.expirationDateTag = expirationDateTag;
	}
	public String getOrganisationTag() {
		return organisationTag;
	}
	public void setOrganisationTag(String organisationTag) {
		this.organisationTag = organisationTag;
	}
	public String getPackTag() {
		return packTag;
	}
	public void setPackTag(String packTag) {
		this.packTag = packTag;
	}
	public String getPackTypeTag() {
		return packTypeTag;
	}
	public void setPackTypeTag(String packTypeTag) {
		this.packTypeTag = packTypeTag;
	}
	public String getAmountTag() {
		return amountTag;
	}
	public void setAmountTag(String amountTag) {
		this.amountTag = amountTag;
	}
	public String getPriceTag() {
		return priceTag;
	}
	public void setPriceTag(String priceTag) {
		this.priceTag = priceTag;
	}
	public String getDosageTag() {
		return dosageTag;
	}
	public void setDosageTag(String dosageTag) {
		this.dosageTag = dosageTag;
	}
	public String getDoseTag() {
		return doseTag;
	}
	public void setDoseTag(String doseTag) {
		this.doseTag = doseTag;
	}
	public String getReceptionTag() {
		return receptionTag;
	}
	public void setReceptionTag(String receptionTag) {
		this.receptionTag = receptionTag;
	}
	
/////////////////////////////////////	
	public String getMedicine() {
		return medicine;
	}
	public void setMedicine(String medicine) {
		this.medicine = medicine;
	}
	public String getMedicament() {
		return medicament;
	}
	public void setMedicament(String medicament) {
		this.medicament = medicament;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getAnalogs() {
		return analogs;
	}
	public void setAnalogs(String analogs) {
		this.analogs=analogs;
	}
	public String getAnalog() {
		return analog;
	}
	public void setAnalog(String analogg) {

		this.analog=analog.concat(analogg+"<br/>");
	}
	public String getVersions() {
		return versions;
	}
	public void setVersions(String versions) {
		this.versions = versions;
	}
	public String getCertificate() {
		return certificate;
	}
	public void setCertificate(String certificate) {
		this.certificate = certificate;
	}
	public String getNumber() {
		return number;
	}
	public void setNumber(String number) {
		this.number = number;
	}
	public String getExpirationDate() {
		return expirationDate;
	}
	public void setExpirationDate(String expirationDate) {
		this.expirationDate = expirationDate;
	}
	public String getOrganisation() {
		return organisation;
	}
	public void setOrganisation(String organisation) {
		this.organisation = organisation;
	}
	public String getPack() {
		return pack;
	}
	public void setPack(String pack) {
		this.pack = pack;
	}
	public String getPackType() {
		return packType;
	}
	public void setPackType(String packType) {
		this.packType = packType;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getDosage() {
		return dosage;
	}
	public void setDosage(String dosage) {
		this.dosage = dosage;
	}
	public String getDose() {
		return dose;
	}
	public void setDose(String dose) {
		this.dose = dose;
	}
	public String getReception() {
		return reception;
	}
	public void setReception(String reception) {
		this.reception = reception;
	}
	public String getEnumForm() {
		return enumForm;
	}
	public void setEnumForm(String enumForm) {
		this.enumForm = enumForm;
	}
	public String getPharm() {
		return pharm;
	}
	public void setPharm(String pharm) {
		this.pharm = pharm;
	}
	private String enumForm;
	private String pharm;
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getGroup() {
		return group;
	}
	public void setGroup(String group) {
		this.group = group;
	}



}
