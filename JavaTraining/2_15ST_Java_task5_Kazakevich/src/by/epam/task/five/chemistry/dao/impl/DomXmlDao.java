package by.epam.task.five.chemistry.dao.impl;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import by.epam.task.five.chemistry.dao.XMLDao;
import by.epam.task.five.chemistry.dao.XMLDaoException;
import by.epam.task.five.chemistry.entity.Medicine;

public class DomXmlDao implements XMLDao {
	private static final DomXmlDao instance = new DomXmlDao();


	
	public static XMLDao getInstance (){
		return instance;
	}

	@Override
	public List<Medicine> parse(String resourseName) throws XMLDaoException   {
		 DocumentBuilder docBuilder;
		 List<Medicine> medicineList = null;
		try {
		 medicineList=new ArrayList<>();
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
			docBuilder=factory.newDocumentBuilder();
		
		Document doc=null;
		
		doc= docBuilder.parse(resourseName);
		Element root = doc.getDocumentElement();
		
		NodeList medicineNodeList = root.getElementsByTagName("medicament");
		
			for(int i=0; i<medicineNodeList.getLength(); i++){
				Element medicineElement = (Element) medicineNodeList.item(i);
				Medicine medicineInstance = buildMedicine(medicineElement);
				medicineList.add(medicineInstance);
			}
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SAXException e) {
			throw new XMLDaoException("SAXException in DomXmlDao.",e);
		} catch (IOException e) {
			throw new XMLDaoException("URISyntaxException in DomXmlDao.",e);
		}
			return medicineList;
	

}
		
	



		private Medicine buildMedicine(Element medicineElement) {
			Medicine medicineInstance = new Medicine();
			
			if(medicineElement.getAttribute("group")!=null){
				medicineInstance.setGroup(medicineElement.getAttribute("group"));
				}
			medicineInstance.setId(medicineElement.getAttribute("id"));
			
			medicineInstance.setName(getElementTextContent(medicineElement, "name"));

			medicineInstance.setAnalog(getElementTextContent(medicineElement,"analog").trim());

			medicineInstance.setNumber(getElementTextContent(medicineElement,"number"));

			medicineInstance.setExpirationDate(getElementTextContent(medicineElement,"expiration-date"));

			medicineInstance.setOrganisation(getElementTextContent(medicineElement,"organisation"));

			medicineInstance.setPackType(getElementTextContent(medicineElement,"package-type"));

			medicineInstance.setAmount(getElementTextContent(medicineElement,"amount"));

			medicineInstance.setPrice(getElementTextContent(medicineElement,"price"));

			medicineInstance.setDose(getElementTextContent(medicineElement,"dose"));

			medicineInstance.setReception(getElementTextContent(medicineElement,"reception"));

			medicineInstance.setEnumForm(getElementTextContent(medicineElement,"enum"));

			medicineInstance.setPharm(getElementTextContent(medicineElement,"pharm"));

			
			return medicineInstance;
		}
		
		private static String getElementTextContent(Element element, String elementName){
			
			NodeList nList = element.getElementsByTagName(elementName);
			Node node = nList.item(0);
			String text=node.getTextContent();
			
			return text;
			}
			

		
	

}
